TkOffice
========

A "Poor Man's" approach to an Office package, which follows
these principles:

Make the most use out of existing Tcl/Tk facilities
---------------------------------------------------

Tcl/Tk already provides objects for
[rich text](https://tcl.tk/man/tcl8.5/TkCmd/text.htm), 
[tables](https://tcl.tk/man/tcl8.5/TkCmd/grid.htm),
[vector graphics](https://tcl.tk/man/tcl8.5/TkCmd/canvas.htm), and
[pixel graphics](https://tcl.tk/man/tcl8.5/TkCmd/photo.htm).
The memory storage structure, the graphic rendering, and the basic 
operations for modifying these items are already there! We only need to 
implement keyboard/mouse editing interaction and disk storage formats in 
order to make true Office components out of these widgets.

For the latter task, we take the classical approach of using Tcl scripts 
(executed by a safe interpreter) as a data format.

Worse is better
---------------

See the
[essay by Richard P. Gabriel](https://en.wikipedia.org/wiki/Worse_is_better).

Similar to the "Poor Man" saying, we state that *worse is better than 
nothing*! This package is meant as a "first stage" (see the workings of 
parachutes and early space rockets) documentation system that allows the 
creators to better organize thoughts which in turn could enable the 
creation of a more sophisticated system, possibly in C++, maybe for 
Haiku/BeOS.

Why not use an existing office system? Each one has its shortcomings - 
it lacks some features that the authors of this package deem crucial for 
thoughts organization. Those features are at the core of this 
development.

Hyperlinks everywhere
---------------------

Hyperlinks are most commonly implemented in text documents, although no 
word processor has the page-replacement and tabbing comfort known from 
modern-day web browsers. But text is not enough: We want hyper-linkable 
graphics! These have been pioneered as "hypergraphics" by 
[Tgif](https://en.wikipedia.org/wiki/Tgif_(program)) where they are 
linked to an "attribute" feature (poor man's schematic editor for 
electrical engineering), but their creation is sort of awkward. However, 
Tgif has a nice web-browser-like navigation mode, which we want to 
emulate in this package.

Hyper-linkable graphics open vast possibilities in two-dimensional 
organization, such as mind maps and spatial file management (a feature 
of file managers that is today only present on the Macintosh, while 
historically it was available on Windows and some Linux file managers as 
well).

File management, Wiki style, included
-------------------------------------

It should not be required to leave the office package in order to open 
another document. We want to implement concepts where a contemporary 
file manager would not be enough, such as poly-hierarchical ordering of 
documents (similar to MediaWiki categories) with a reverse-lookup (which 
document belongs to which categories?) and 1D (list-style) and/or 2D 
(mindmap-style) spatial ordering of entries, together with optional 
attributes such as color groups.

Unlimited nesting
-----------------

Text should be able to contain graphics, which contain text, which 
contains tables of texts and graphics, etc. !

Screen orientation
------------------

Documents should adapt to the width of their window. Fixed page sizes 
are not taken into consideration. There is currently no demand for 
printing. This could later be handled by export filters, where the 
"heavy-lifting" may then be performed by well-established systems such 
as LaTeX.

Quick start-up on old computers
-------------------------------

The word processor must start up approximately as fast as the simplest 
text editor of the system (such as Mousepad, Windows Notepad, 
Notepad++), otherwise it has no use since the author would then prefer 
to create a plain text document whenever he can.

The other components should have similar start-up times, although there 
is less competition (e.g. from Inkscape).
