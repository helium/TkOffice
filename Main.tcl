#!/usr/bin/tclsh8.5
# Copyright 2021, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

## Make tclIndex accessible
set dir [file dirname [info script]]
source [file join $dir tclIndex]

## Set up the GUI translations
package require msgcat
namespace import ::msgcat::mc
foreach namespace {common menus drawing richtext welcomescreen} {
	msgcat::mcload [file join $dir $namespace msgs]
}

## GUI setup
package require Tk

if {![package vsatisfies $tk_version 8.6]} {
	# Tk before version 8.6 does not have built-in PNG image format support
	catch {package require tkpng}
}

wm state . withdrawn
wm geometry . 640x400
ttk_useTheme clam
tk_setup_virtual_events

ttk_menu .menubar
.menubar configure -relief raised

pack [welcomescreen::create_gui .welcome] -fill both -expand true
welcomescreen::set_window_title .

update idletasks
wm state . normal
