# Copyright 2020 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc make_utility_window {window parent} {
	switch [tk windowingsystem] {
		x11 {
			wm group $window $parent
			wm attributes $window -type utility
		}
		win32 {  # TODO: untested!
			wm group $window $parent
			wm attributes $window -toolwindow yes
		}
		aqua {  # TODO: untested!
			wm group $window $parent
		}
	}
}
