# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Fix window paths from naive concatenation, which gives wrong results
# when dealing with the root window:
#
# $window | $window.sub.path | Comment
# --------+------------------+--------------
# .mywin  | .mywin.sub.path  | OK
# .       | ..sub.path       | Must be fixed

proc fix_toplevel_child {path} {
	if {[string match ..* $path]} {
		return [string range $path 1 end]
	} else {
		return $path
	}
}
