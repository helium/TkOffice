# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

## Workaround for Tk's limitations on double-click recognition
#
# 1. Tk (before 8.6.10, see TIP 532) has an "event ring" buffer
#    with 30 events, which are searched for matching a double click.
#    If the first click triggers enough rearrangements of widgets
#    (such as in the tag list treeview, which modifies the tag
#    property panels depending on the list entry), the resulting
#    Configure, Map, Unmap, etc. events move the first click out
#    of the event ring, such that the second click is treated as
#    a single click.
#
# 2. For triple and quadruple clicks, all clicks must occur in a
#    500-ms time window, which is very hard to achieve for quadruple
#    clicks.
#
# Usage: Add "DoubleClick" as the first bindtag of the widget:
#          bindtags $widget [concat DoubleClick [bindtags $widget]]
#        You can then bind the widget to <ButtonPress-1>, and only
#        single clicks will be processed by that binding, as the
#        multi-click case of the DoubleClick binding returns a
#        "break" code.

namespace eval doubleclick_workaround {

bind DoubleClick <ButtonPress-1> {doubleclick_workaround::click %W %x %y {}}
bind DoubleClick <Shift-ButtonPress-1> {doubleclick_workaround::click %W %x %y Shift}
bind DoubleClick <Control-ButtonPress-1> {doubleclick_workaround::click %W %x %y Control}
bind DoubleClick <Shift-Control-ButtonPress-1> {doubleclick_workaround::click %W %x %y {Shift Control}}
bind DoubleClick <KeyPress> {doubleclick_workaround::keypress %W %K}

proc add_to_widget {w} {
	set tags [bindtags $w]
	if {[lindex $tags 0] eq {DoubleClick}} return
	if {{DoubleClick} in $tags} {
		error "DoubleClick should be the first bind tag, have \"$tags\"
	}
	bindtags $w [concat DoubleClick [bindtags $w]]
}

proc click {w x y modifier_keys} {
	variable last_x
	variable last_y
	variable last_milliseconds
	variable click_count

	set now [clock milliseconds]
	incr click_count($w)

	if {[info exists last_x($w)] && [info exists last_y($w)]
	&& [info exists last_milliseconds($w)]
	&& abs($x - $last_x($w)) <= 5 && abs($y - $last_y($w)) <= 5
	&& $now - $last_milliseconds($w) <= 500} {
		if {$modifier_keys eq {}} {
			switch $click_count($w) 2 {
				event generate $w <<DoubleClick>> -x $x -y $y
			} 3 {
				event generate $w <<TripleClick>> -x $x -y $y
			} default {
				event generate $w <<QuadrupleClick>> -x $x -y $y
			}
			set code break
		} else {
			set code ok
		}
	} else {
		set click_count($w) 1
		set code ok
	}

	set last_x($w) $x
	set last_y($w) $y
	set last_milliseconds($w) $now

	return -code $code
}

proc keypress {w keysym} {
	variable last_milliseconds
	variable click_count

	# The original <Double-ButtonPress> still fires on a click-keypress-click
	# sequence when the keypress is a modifier key (exception: AltGr = ISO_Level3_Shift).
	# Emulate that here:
	# (TODO: Keys on non-PC keyboards, such as Meta or Command (Mac).)
	if {$keysym in {Shift_L Shift_R Control_L Control_R Alt_L Alt_R Super_L Super_R}} return

	set last_milliseconds($w) 0
	set click_count($w) 0
}

}
