# Copyright 2012, 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

## Utility procedures related to the bindtags mechanism

# Add additional bindtags (in $args) to those already defined for $window.
# The tags are appended just before ".", or at the end of the tag list.
proc bindtag_add {window args} {
	set tags [bindtags $window]
	set dotpos [lsearch -exact $tags .]
	if {$dotpos==-1} {
		lappend tags {*}$args
	} else {
		set tags [linsert $tags $dotpos {*}$args]
	}
	bindtags $window $tags
}

proc bindtag_remove {window tag_to_remove} {
	set tags [bindtags $window]
	lremoveitem tags $tag_to_remove
	bindtags $window $tags
}

proc bindtag_replace {window old_tag new_tag} {
	set tags [bindtags $window]
	set pos [lsearch -exact $tags $old_tag]
	if {$pos != -1} {
		bindtags $window [lreplace $tags $pos $pos $new_tag]
		return true
	} else {
		return false
	}
}
