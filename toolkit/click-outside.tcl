# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# This "Click outside" mechanism notifies registered widgets
# when the user has clicked outside them. In contrast to the
# "grab" mechanism, clicks are not "consumed" by the registered
# widgets, i.e. they still interact with the widgets under the
# mouse pointer.
# After delivering a <<ClickOutside>> event, the widget is
# automatically deregistered. It may reregister itself inside
# the event handler.

proc click_outside_register {widget} {
	variable click_outside_registered

	lappend click_outside_registered $widget
}

bind all <ButtonPress> +[list click_outside_handler %W]

proc click_outside_handler {{widget_clicked {}}} {
	variable click_outside_registered

	if {![info exists click_outside_registered]} return
	# Speedup for common case
	if {$click_outside_registered eq {}} return

	set new_registered [list]
	set pending [list]
	foreach widget $click_outside_registered {
		if {$widget ne $widget_clicked} {
			if {[winfo exists $widget]} {
				lappend pending $widget
			}
		} else {
			lappend new_registered $widget
		}
	}
	# This variable is written before generating any events, such that
	# event handlers may modify it through [click_outside_register].
	set click_outside_registered $new_registered

	foreach widget $pending {
		event generate $widget <<ClickOutside>>
	}
}
