# Copyright 2020-2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval ttk_fixes {}

set_ifnotexists ::ttk_fixes::menus [list]

proc ttk_menu {windowpath args} {
	lappend ::ttk_fixes::menus $windowpath
	menu $windowpath -tearoff no {*}$args
	bindtag_add $windowpath ttk_menu
	# call theme-specific menu configurator
	::ttk_fixes::menu.[ttk_currentTheme] $windowpath
	return $windowpath
}
# Need lremoveitem_nocomplain, since transient menubar menus also have
# the bindtag ttk_menu but ttk_menu has not been called for them.
bind ttk_menu <Destroy> {lremoveitem_nocomplain ::ttk_fixes::menus %W}

## Theme-specific menu configurators
# Clam
proc ::ttk_fixes::menu.clam {menu} {
	$menu configure -background #dcdad5
	$menu configure -activebackground #4a6984 -activeforeground #ffffff
	$menu configure -borderwidth 1 -relief solid  ;# black 1px border around menu
	$menu configure -activeborderwidth 1
}
# Classic
proc ::ttk_fixes::menu.classic {menu} {
	$menu configure -background #d9d9d9
	$menu configure -activebackground #ececec -activeforeground #000000
	$menu configure -borderwidth 2 -relief raised
	$menu configure -activeborderwidth 2
	# "-selectcolor" not changed, as the Motif-style radio-/checkbuttons are unavailable in Tk 8.5
}
# Alt (near-Windows 95)
proc ::ttk_fixes::menu.alt {menu} {
	$menu configure -background #d9d9d9
	$menu configure -activebackground #4a6984 -activeforeground #ffffff
	$menu configure -borderwidth 2 -relief raised
	$menu configure -activeborderwidth 1
}
# Default
proc ::ttk_fixes::menu.default {menu} {
	$menu configure -background #d9d9d9
	$menu configure -activebackground #ececec -activeforeground #000000
	$menu configure -borderwidth 1 -relief raised
	$menu configure -activeborderwidth 1
}
