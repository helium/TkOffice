# Copyright 2020-2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval ttk_fixes {}

set_ifnotexists ::ttk_fixes::treeview_scrollbars [list]


proc ttk_treeview_place_scrollbar {scrollbar} {
	set treeview [winfo parent $scrollbar]
	# Padding documentation: see Ttk_GetPaddingFromObj
	set padding [$treeview cget -padding]
	lappend padding default default default default
	lassign $padding pad_left pad_top pad_right pad_bottom
	if {$pad_left eq "default"} {set pad_left 0}
	if {$pad_top eq "default"} {set pad_top $pad_left}
	if {$pad_right eq "default"} {set pad_right $pad_left}
	if {$pad_bottom eq "default"} {set pad_bottom $pad_top}

	switch [ttk_currentTheme] {
		clam - classic {incr pad_right 2; incr pad_bottom 2}
		alt - default {incr pad_right 1; incr pad_bottom 1}
	}


	if {{headings} in [$treeview cget -show]} {
		set heading_height [expr {$pad_top + 4}]
		set x [expr {$pad_left + 10}]
		while {[$treeview identify region $x $heading_height] eq "heading"} {
			incr heading_height
		}
		switch [ttk_currentTheme] {
			clam {incr heading_height 1}
			classic - alt - default {}
		}
	} else {
		set heading_height $pad_top
		switch [ttk_currentTheme] {
			clam - classic - alt {incr heading_height 2}
			default {incr heading_height 1}
		}
	}

	place $scrollbar -anchor se \
		-relx 1 -x [expr {-$pad_right}] -rely 1 -y [expr {-$pad_bottom}] \
		-relheight 1 -height [expr {-$pad_bottom - $heading_height}]

	if {$scrollbar ni $::ttk_fixes::treeview_scrollbars} {
		lappend ::ttk_fixes::treeview_scrollbars $scrollbar
	}

	$treeview configure -yscrollcommand [list $scrollbar set]
	$scrollbar configure -command [list $treeview yview]
}
