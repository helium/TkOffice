# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval ttk_fixes {}

# Work around treeview misbehaviour:
# [$treeview selection] changes much earlier than when the <<TreeviewSelect>>
# event is issued, such that procedures bound to <FocusOut> of another widget
# already get the new selection!
# This behavior looks out of place but is not easy to change without introducing
# "after idle" events:
# 1. The user clicks on a treeview entry. The event binding (usually
#    ttk::treeview::Press) sets the focus on the treeview, which adds a
#    <FocusOut> event on the previously focused widget to the event queue,
#    then (in the same procedure) the treeview's selection is changed
#    and a <<TreeviewSelect>> virtual event is added to the queue.
# 2. The <FocusOut> event on the previously focused widget is processed,
#    its event handler (binding) already sees the new selection.
# 3. The <<TreeviewSelect>> virtual event is processed on the treeview.
# Using this script, you can use $::ttk_fixes::selection_of_treeview($widget)
# in your <FocusOut> bindings to use the old value.

variable ::ttk_fixes::selection_of_treeview

proc ttk_treeview {windowpath args} {
	ttk::treeview $windowpath {*}$args
	bindtag_add $windowpath TreeviewSelectionFix
	set ::ttk_fixes::selection_of_treeview($windowpath) [list]
	return $windowpath
}

bind TreeviewSelectionFix <<TreeviewSelect>> {
	set ::ttk_fixes::selection_of_treeview(%W) [%W selection]
}

bind TreeviewSelectionFix <Destroy> {
	unset -nocomplain ::ttk_fixes::selection_of_treeview(%W)
}
