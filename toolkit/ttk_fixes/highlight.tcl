# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Sets the highlight properties of non-Ttk widgets such as text and canvas
# designed as main editor areas.

proc highlight_setup {w} {
	switch [ttk_currentTheme] {
		clam {
			# ttk::entry has the same colors in the "clam" theme
			$w configure -highlightcolor #6f9dc6 -highlightbackground #dcdad5
			$w configure -border 0 -highlightthickness 1
		}
		classic {
			$w configure -highlightcolor black -highlightbackground #d9d9d9
			$w configure -border 2 -relief sunken -highlightthickness 1
		}
		alt {
			$w configure -highlightcolor #414141 -highlightbackground #d9d9d9
			$w configure -border 1 -relief sunken -highlightthickness 1
		}
		default {
			$w configure -highlightcolor #4a6984 -highlightbackground #d9d9d9
			$w configure -border 0 -highlightthickness 1
		}
	}
}
