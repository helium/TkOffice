# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# The "clam" TTk style has slightly rounded corners on
# buttons, frames and similar widgets. However, the 1x1 px areas
# outside these corners take over the background color from inside
# the widget, which may look awful.
# This module provides the theme-aware "ttk_fixes::clam_corners"
# procedure which draws tiny 1x1 px frames in the correct color
# to fix this issue.

namespace eval ttk_fixes {

variable clam_corner_dict [dict create]

proc add_clam_corners {widget background} {
	foreach dir {nw ne sw se} relx {0 1 0 1} rely {0 0 1 1} {
		place [frame $widget.corner_$dir -bg $background] \
			-relx $relx -rely $rely -anchor $dir
	}
}

proc remove_clam_corners {widget} {
	destroy $widget.corner_nw $widget.corner_ne \
		$widget.corner_sw $widget.corner_se
}

proc clam_corners {widget {background #dcdad5}} {
	variable clam_corner_dict

	if {![winfo exists $widget]} {
		error "widget \"$widget\" does not exist"
	}
	dict set clam_corner_dict $widget $background
	if {[ttk_currentTheme] eq {clam}} {
		add_clam_corners $widget $background
	}
	bind $widget <Destroy> {ttk_fixes::clam_corners_destroy %W}
}

proc clam_corners_destroy {widget} {
	variable clam_corner_dict
	dict unset clam_corner_dict $widget
}

proc clam_corners_change_theme {} {
	variable clam_corner_dict
	variable clam_corner_prev_theme

	if {![info exists clam_corner_prev_theme]} {
		set clam_corner_prev_theme default
	}
	set cur_theme [ttk_currentTheme]

	if {$cur_theme eq {clam} && $clam_corner_prev_theme ne {clam}} {
		dict for {widget background} $clam_corner_dict {
			add_clam_corners $widget $background
		}
	} elseif {$cur_theme ne {clam} && $clam_corner_prev_theme eq {clam}} {
		dict for {widget background} $clam_corner_dict {
			remove_clam_corners $widget
		}
	}

	set clam_corner_prev_theme $cur_theme
}

}
