# Copyright 2012, 2020-2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval ::ttk_fixes {}

set ::ttk_fixes::currentTheme default

proc ttk_currentTheme {} {
	return $::ttk_fixes::currentTheme
}

set_ifnotexists ::ttk_fixes::menus [list]
set_ifnotexists ::ttk_fixes::texts [list]
set_ifnotexists ::ttk_fixes::toplevels [list .]
set_ifnotexists ::ttk_fixes::treeview_scrollbars [list]

## "Wrapper" for 'ttk::style theme use' that triggers rearrangements in
## various parts of ttk_fixes on theme change, e.g. changing the padding
## widths of text widgets inside of ttk::frames.
proc ttk_useTheme {theme} {
	ttk::style theme use $theme
	set ::ttk_fixes::currentTheme $theme

	ttk_configure_styles

	# Tk library procedure redefinition:
	# Improve focus being kept on the main editor object (e.g. text or canvas).
	proc ::ttk::takefocus {w} {
		expr {[$w cget -style] ni {Toolbutton ThreestateButton.TCheckbutton} &&
			  [$w instate !disabled] && [winfo viewable $w]}
	}

	foreach menu $::ttk_fixes::menus {
		::ttk_fixes::menu.$theme $menu
	}
	foreach text $::ttk_fixes::texts {
		::ttk_fixes::text.$theme $text
	}
	foreach toplevel $::ttk_fixes::toplevels {
		::ttk_fixes::toplevel.$theme $toplevel
	}
	ttk_fixes::clam_corners_change_theme
	update idletasks
	foreach scrollbar $::ttk_fixes::treeview_scrollbars {
		ttk_treeview_place_scrollbar $scrollbar
	}
}
