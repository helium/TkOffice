# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# ttk::entry widgets can only be set to a certain text by means of their textvariable,
# therefore we are auto-creating these variables here:
proc make_entry {w args} {
	set ::entry_var($w) {}
	ttk::entry $w {*}$args -textvariable entry_var($w)
	foreach event {<Return> <KP_Enter> <FocusOut>} {
		bind $w $event {event generate %W <<Entered>>}
	}
	return $w
}

proc entry_set {w string} {
	set ::entry_var($w) $string
}
