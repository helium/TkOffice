# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc treeview_enable_multi_selection {treeview} {
	bindtag_replace $treeview Treeview TreeviewMultiSelection
}

namespace eval toolkit::treeview_multi_selection {

set namespace [namespace current]
bind TreeviewMultiSelection <Destroy> [list ${namespace}::forget_anchors %W]
bind TreeviewMultiSelection <ButtonPress-1> [list ${namespace}::button_press %W %x %y]
bind TreeviewMultiSelection <Control-ButtonPress-1> [list ${namespace}::ctrl_button_press %W %x %y]
bind TreeviewMultiSelection <Shift-ButtonPress-1> [list ${namespace}::shift_button_press %W %x %y]
bind TreeviewMultiSelection <B1-Motion> [list ${namespace}::register_drag %W]
bind TreeviewMultiSelection <ButtonRelease-1> [list ${namespace}::button_release %W %x %y]
bind TreeviewMultiSelection <Key-Up> [list ${namespace}::key_up_down %W prev]
bind TreeviewMultiSelection <Key-Down> [list ${namespace}::key_up_down %W next]
bind TreeviewMultiSelection <Shift-Key-Up> [list ${namespace}::shift_key_up_down %W prev]
bind TreeviewMultiSelection <Shift-Key-Down> [list ${namespace}::shift_key_up_down %W next]
bind TreeviewMultiSelection <Key-Home> [list ${namespace}::key_home_end %W 0]
bind TreeviewMultiSelection <Key-End> [list ${namespace}::key_home_end %W end]
bind TreeviewMultiSelection <Key-Prior> {%W yview scroll -1 pages}
bind TreeviewMultiSelection <Key-Next> {%W yview scroll 1 pages}
unset namespace
ttk::copyBindings TtkScrollable TreeviewMultiSelection

proc selection_set {w new_selection} {
	$w selection set $new_selection
	forget_anchors $w
}

proc forget_anchors {w} {
	variable anchor_item
	variable prev_selection_extend_item

	unset -nocomplain anchor_item($w)
	unset -nocomplain prev_selection_extend_item($w)
}

proc button_press {w x y} {
	variable anchor_item
	variable prev_selection_extend_item
	variable tap_without_modifier

	set tap_without_modifier yes
	focus $w
	set item [$w identify item $x $y]
	if {$item eq {}} return
	if {$item in [$w selection]} {
		set anchor_item($w) $item
		unset -nocomplain prev_selection_extend_item($w)
		return
	}
	select_single_item $w $item
}

set tap_without_modifier no

proc register_drag {w} {
	variable tap_without_modifier

	set tap_without_modifier no
}

proc button_release {w x y} {
	variable tap_without_modifier

	if {$tap_without_modifier} {
		set item [$w identify item $x $y]
		if {$item ne {}} {
			select_single_item $w $item
		}
	}
	set tap_without_modifier no
}

proc ctrl_button_press {w x y} {
	variable anchor_item
	variable prev_selection_extend_item

	focus $w
	set item [$w identify item $x $y]
	if {$item eq {}} return
	$w focus $item
	$w selection toggle $item
	set anchor_item($w) $item
	unset -nocomplain prev_selection_extend_item($w)
}

proc shift_button_press {w x y} {
	extend_selection $w [$w identify item $x $y]
}

proc extend_selection {w item} {
	variable anchor_item
	variable prev_selection_extend_item

	if {[info exists anchor_item($w)]} {
		set anchor $anchor_item($w)
	} else {
		set selection [$w selection]
		if {[llength $selection] == 1} {
			set anchor $selection
			set anchor_item($w) $anchor
		} else {
			return
		}
	}
	focus $w
	if {$item eq {}} return
	$w focus $item

	if {$anchor in [$w selection]} {
		if {[info exists prev_selection_extend_item($w)]} {
			$w selection remove [item_range $w $anchor $prev_selection_extend_item($w)]
		}
		$w selection add [item_range $w $anchor $item]
	} else {
		$w selection remove [item_range $w $anchor $item]
	}
	set prev_selection_extend_item($w) $item
}

proc key_up_down {w dir} {
	if {$dir ni {prev next}} {error "bad direction \"$dir\": must be prev or next"}
	set focus [$w focus]
	set new [$w $dir $focus]
	if {$new eq {}} {set new $focus}

	select_single_item $w $new
}

proc shift_key_up_down {w dir} {
	if {$dir ni {prev next}} {error "bad direction \"$dir\": must be prev or next"}
	set focus [$w focus]
	set new [$w $dir $focus]

	extend_selection $w $new
}

proc key_home_end {w index} {
	select_single_item $w [lindex [$w children {}] $index]
}

proc select_single_item {w item} {
	variable anchor_item
	variable prev_selection_extend_item

	$w focus $item
	$w selection set $item
	$w see $item
	set anchor_item($w) $item
	unset -nocomplain prev_selection_extend_item($w)
}

proc item_range {treeview start_item end_item} {
	set items [list]
	if {[$treeview index $start_item] > [$treeview index $end_item]} {
		lassign [list $start_item $end_item] end_item start_item
	}
	set cur_item $start_item
	lappend items $cur_item
	while {$cur_item ne $end_item} {
		set cur_item [$treeview next $cur_item]
		lappend items $cur_item
	}
	return $items
}

proc is_contiguous {treeview} {
	foreach item [$treeview selection] {
		if {[info exists base_item]} {
			if {[$treeview next $base_item] ne $item} {
				return no
			}
		}
		set base_item $item
	}
	return yes
}

}
