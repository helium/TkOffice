# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc treeview_make_reorderable {treeview} {
	bindtag_add $treeview TreeviewReorder
}

namespace eval toolkit::treeview_reorder {

set namespace [namespace current]
bind TreeviewReorder <ButtonPress-1> [list ${namespace}::button_press %W %x %y]
bind TreeviewReorder <Control-ButtonPress-1> [list ${namespace}::button_press_disabled %W]
bind TreeviewReorder <Shift-ButtonPress-1> [list ${namespace}::button_press_disabled %W]
bind TreeviewReorder <B1-Motion> [list ${namespace}::motion %W %x %y]
bind TreeviewReorder <ButtonRelease-1> [list ${namespace}::motion_finish %W %x %y]
bind TreeviewReorder <B1-Leave> [list ${namespace}::auto_scan_start %W %y]
bind TreeviewReorder <B1-Enter> [list ${namespace}::auto_scan_cancel]
unset namespace

proc make_listpos_indicator {treeview x y width} {
	# Slight imperfection: If the treeview height is such that the last visible item
	# is partially visible (with more than half of its height), the supplied indicator
	# position $y lies outside the treeview's visible area when the mouse cursor
	# is moved over the last visible item's lower half. The correct behavior would
	# involve starting the auto-scroll operation already here. This implementation
	# does not start scrolling but makes sure that the indicator line is visible
	# if and only if a mouse button release would result in a list reordering.
	# Advantage: Simpler code, auto-scroll operation decoupled from the rest.
	# Disadvantage: The indicator line is drawn at a "slightly wrong" location in order
	# to be visible.
	set y [expr {min($y, [winfo height $treeview]-2)}]

	if {! [winfo exists $treeview.indicator]} {
		frame $treeview.indicator -bg #6f9dc6
	}
	place $treeview.indicator -x $x -y $y -width $width -height 2

	# Treeview may have scrollbar as child widget
	foreach child [winfo children $treeview] {
		if {[winfo class $child] eq {TScrollbar}} {
			lower $treeview.indicator $child
			break  ;# lowest child already handled
		}
	}
}

proc button_press_disabled {treeview} {
	variable motion_disable

	set motion_disable 1
}

proc button_press {treeview x y} {
	variable motion_disable

	set hover_item [$treeview identify item $x $y]
	set motion_disable [expr {$hover_item eq {}}]
	if {[$treeview tag has reorder::always_first $hover_item]} {
		set motion_disable 1
	}
	if {[$treeview tag has reorder::always_first [lindex [$treeview selection] 0]]} {
		set motion_disable 1
	}
}

proc motion {treeview x y} {
	variable last_y
	variable last_item
	variable motion_disable

	if {$motion_disable} return
	$treeview configure -cursor fleur

	set last_y $y

	if {$y >= [winfo height $treeview]} {
		destroy $treeview.indicator
		return
	}

	set hover_item [$treeview identify item $x $y]
	set bbox [$treeview bbox $hover_item]
	if {$bbox eq {}} {
		if {![info exists last_item]} {
			set last_item [lindex [$treeview children {}] end]
		}
		set hover_item $last_item
		set bbox [$treeview bbox $last_item]
		lassign $bbox bbox_x bbox_y width height
		if {$bbox eq {} || $y < $bbox_y + $height || $y >= [winfo height $treeview]} {
			destroy $treeview.indicator
			return
		}
	}
	set hover_index [$treeview index $hover_item]
	set selection [$treeview selection]
	set first_selected_index [$treeview index [lindex $selection 0]]
	set last_selected_index [$treeview index [lindex $selection end]]
	lassign $bbox bbox_x bbox_y width height
	if {$y >= $bbox_y + $height / 2} {
		if {$hover_index >= $first_selected_index && $hover_index < $last_selected_index} {
			destroy $treeview.indicator
			return
		}
		# Show the indicator line just below the currently hovered item
		make_listpos_indicator $treeview $bbox_x [expr {$bbox_y + $height - 1}] $width
	} elseif {![$treeview tag has reorder::always_first $hover_item]} {
		if {$hover_index > $first_selected_index && $hover_index <= $last_selected_index} {
			destroy $treeview.indicator
			return
		}
		# Show the indicator line just above the currently hovered item
		make_listpos_indicator $treeview $bbox_x [expr {$bbox_y - 1}] $width
	} else {
		destroy $treeview.indicator
	}
}

proc motion_finish {treeview x y} {
	variable last_item
	variable motion_disable

	auto_scan_cancel
	$treeview configure -cursor {}
	if {$motion_disable} return
	if {![winfo exists $treeview.indicator]} return  ;# Prevent reordering on simple clicks without dragging
	destroy $treeview.indicator

	if {$y >= [winfo height $treeview]} {
		unset -nocomplain last_item
		return
	}

	set hover_item [$treeview identify item $x $y]
	set bbox [$treeview bbox $hover_item]
	if {$bbox eq {}} {
		if {![info exists last_item]} {
			set last_item [lindex [$treeview children {}] end]
		}
		set hover_item $last_item
		set bbox [$treeview bbox $last_item]
		lassign $bbox bbox_x bbox_y width height
		if {$bbox eq {} || $y < $bbox_y + $height || $y >= [winfo height $treeview]} {
			unset last_item
			return
		}
	}
	unset -nocomplain last_item
	set hover_index [$treeview index $hover_item]
	set selection [$treeview selection]
	if {$selection eq {}} return
	set first_selected_index [$treeview index [lindex $selection 0]]
	set last_selected_index [$treeview index [lindex $selection end]]
	lassign $bbox bbox_x bbox_y width height
	set new_index_offset 0
	if {$y < $bbox_y + $height / 2} {
		# Mouse cursor above vertical center of line - different target position
		incr new_index_offset -1
		# Items moved inside selection range - abort move
		if {$hover_index > $first_selected_index && $hover_index <= $last_selected_index} return
	} else {
		# Items moved inside selection range - abort move
		if {$hover_index >= $first_selected_index && $hover_index < $last_selected_index} return
	}
	if {$hover_index < $first_selected_index || $hover_index == $first_selected_index && $new_index_offset == -1} {
		# Moving items up requires index adjustment
		incr new_index_offset
	}

	set new_index [expr {$hover_index + $new_index_offset}]
	switch $new_index_offset {
		 0 {set relative_item $hover_item}
		 1 {set relative_item [$treeview next $hover_item]}
		-1 {set relative_item [$treeview prev $hover_item]}
	}

	if {[llength $selection] == 1} {
		if {$new_index > $first_selected_index} {
			event generate $treeview <<MoveDown>> -data [list $selection $relative_item]
		} elseif {$new_index < $first_selected_index} {
			if {[$treeview tag has reorder::always_first $relative_item]} {
				return
			}
			event generate $treeview <<MoveUp>> -data [list $selection $relative_item]
		} else {
			return  ;# item moved onto itself
		}
		$treeview move $selection {} $new_index
	} else {
		if {$new_index >= $last_selected_index} {
			event generate $treeview <<MoveDownMultiple>> -data [list $selection $relative_item]
			set move_up no
		} elseif {$new_index <= $first_selected_index} {
			if {[$treeview tag has reorder::always_first $relative_item]} {
				return
			}
			event generate $treeview <<MoveUpMultiple>> -data [list $selection $relative_item]
			set move_up yes
		} else {
			return  ;# item moved onto itself
		}
		foreach item $selection {
			$treeview move $item {} $new_index
			if {$move_up} {
				incr new_index
			}
		}
	}
}

set auto_scan_afterid {}

proc auto_scan_start {treeview y} {
	variable motion_disable
	variable last_y

	if {![info exists motion_disable] || $motion_disable} return
	set last_y $y
	auto_scan $treeview
}

proc auto_scan {treeview} {
	variable auto_scan_afterid
	variable last_y

	set height [winfo height $treeview]
	if {$last_y > $height-2} {
		$treeview yview scroll +1 units
		set distance [expr {$last_y - ($height-2)}]
	} elseif {$last_y < 2} {
		$treeview yview scroll -1 units
		set distance [expr {2 - $last_y}]
	} else {
		return
	}
	set auto_scan_afterid [after [expr {200 / ($distance / 10 + 1)}] [namespace current]::auto_scan $treeview]
}

proc auto_scan_cancel {} {
	variable auto_scan_afterid
	after cancel $auto_scan_afterid
	set auto_scan_afterid {}
}

proc move_selection_up_by_one {treeview} {
	set selection [$treeview selection]
	if {$selection eq {}} return
	set prev_item {}
	if {[llength $selection] == 1 || [toolkit::treeview_multi_selection::is_contiguous $treeview]} {
		set prev_item [$treeview prev [lindex $selection 0]]
		if {$prev_item eq {} || [$treeview tag has reorder::always_first $prev_item]} return
	}
	# If a non-contiguous multi-selection is present, just move all selected items next to each other
	# starting at the first one (which stays). In this case, $prev_item is deliberately kept empty.

	set remaining_items [lassign $selection selection_first_item]
	set selection_first_pos [$treeview index $selection_first_item]
	if {$prev_item ne {}} {
		$treeview move $selection_first_item {} [expr {$selection_first_pos - 1}]
	} else {
		incr selection_first_pos
	}

	foreach item $remaining_items {
		$treeview move $item {} $selection_first_pos
		incr selection_first_pos
	}

	if {[llength $selection] == 1} {
		event generate $treeview <<MoveUp>> -data [list $selection $prev_item]
	} else {
		event generate $treeview <<MoveUpMultiple>> -data [list $selection $prev_item]
	}
}

proc move_selection_down_by_one {treeview} {
	set selection [$treeview selection]
	if {$selection eq {}} return
	set next_item {}
	if {[llength $selection] == 1 || [toolkit::treeview_multi_selection::is_contiguous $treeview]} {
		set next_item [$treeview next [lindex $selection end]]
		if {$next_item eq {} || [$treeview tag has entire_text [lindex $selection 0]]} return
	}
	# If a non-contiguous multi-selection is present, just move all selected items next to each other
	# such that the last selected item stays. In this case, $next_item is deliberately not set.

	set remaining_items [lrange $selection 0 end-1]
	set selection_last_item [lindex $selection end]
	set selection_last_pos [$treeview index $selection_last_item]
	if {$next_item ne {}} {
		incr selection_last_pos
		$treeview move $selection_last_item {} $selection_last_pos
	} else {
		set next_item $selection_last_item  ;# for "event generate"
	}

	foreach item [lreverse $remaining_items] {
		incr selection_last_pos -1
		$treeview move $item {} $selection_last_pos
	}

	if {[llength $selection] == 1} {
		event generate $treeview <<MoveDown>> -data [list $selection $next_item]
	} else {
		event generate $treeview <<MoveDownMultiple>> -data [list $selection $next_item]
	}
}

}
