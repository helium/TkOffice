# Copyright 2021-2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc ttk_configure_styles {} {
	ttk::style configure red.TEntry -fieldbackground #fcc
	ttk::style configure Panel.TLabelframe.Label -font TkHeadingFont
	ttk::style configure msgline.TButton -width 0 -padding {2 1}

	if {[ttk_currentTheme] ne {classic}} {
		ttk::style layout LeftAlignedImage.TButton {
			Button.border -sticky nswe -border 1 -children {
				Button.focus -sticky nswe -children {
					Button.padding -sticky nswe -children {
						Button.image -side left -sticky w
						Button.text -side left -sticky {} -expand true
						Button.padding -side left -sticky we
					}
				}
			}
		}
	} else {
		ttk::style layout LeftAlignedImage.TButton {
			Button.highlight -sticky nswe -children {
				Button.border -sticky nswe -border 1 -children {
					Button.padding -sticky nswe -children {
						Button.image -side left -sticky w
						Button.text -side left -sticky {} -expand true
						Button.padding -side left -sticky we
					}
				}
			}
		}
	}
	switch [ttk_currentTheme] {
		clam {set padding {5 5 0 5}}
		alt {set padding {1 1 0 1}}
		default {set padding {3 3 0 3}}
		classic {set padding {3m 1m 0 1m}}
	}
	ttk::style configure LeftAlignedImage.TButton -padding $padding

	ttk::style configure Toolbutton -padding {2 0}
	ttk::style layout ThreestateButton.TCheckbutton [ttk::style layout Toolbutton]
	ttk::style configure ThreestateButton.TCheckbutton {*}[ttk::style configure Toolbutton]
	ttk::style map ThreestateButton.TCheckbutton {*}[ttk::style map Toolbutton]
	switch [ttk_currentTheme] {
		clam {
			ttk::style map ThreestateButton.TCheckbutton \
				-relief {disabled flat selected sunken pressed sunken active raised alternate solid} \
				-background {disabled #dcdad5 pressed #bab5ab selected #bab5ab active #eeebe7} \
				-lightcolor {pressed #bab5ab selected #bab5ab} \
				-darkcolor {pressed #bab5ab selected #bab5ab}
			ttk::style map TFrame \
				-bordercolor {focus #4a6984}
		}
		classic {
			ttk::style map ThreestateButton.TCheckbutton \
				-relief {disabled flat selected sunken pressed sunken active raised alternate solid}
			ttk::style map TFrame {}
			ttk::style configure welcomescreen.TButton -highlightcolor #95b77d
		}
		alt {
			ttk::style map ThreestateButton.TCheckbutton \
				-relief {disabled flat selected sunken pressed sunken active raised alternate solid} \
				-bordercolor [list alternate $::ttk::theme::alt::colors(-border)]
			ttk::style map TFrame {}
		}
		default {
			ttk::style map ThreestateButton.TCheckbutton \
				-relief {disabled flat selected sunken pressed sunken active raised alternate solid}
			ttk::style map TFrame {}
		}
	}

	switch [ttk_currentTheme] {
		clam {
			ttk::style configure Invalid.TEntry -fieldbackground #faa
			ttk::style configure Invalid.TEntry -darkcolor #f55 -lightcolor #f55 -bordercolor #f00
			ttk::style map Invalid.TEntry -darkcolor {} -lightcolor {} -bordercolor {}
			ttk::style configure Invalid.TSpinbox -fieldbackground #faa
			ttk::style configure Invalid.Modern.TSpinbox -fieldbackground #faa
		}
		classic - alt - default {
			ttk::style configure Invalid.TEntry -fieldbackground #faa
			ttk::style configure Invalid.TSpinbox -fieldbackground #faa
			ttk::style configure Invalid.Modern.TSpinbox -fieldbackground #faa
		}
	}

	switch [ttk_currentTheme] {
		clam {
			ttk::style configure Modern.TSpinbox -padding {4 1} -arrowsize 14
			ttk::style layout Modern.TSpinbox {
				Spinbox.field -side top -sticky we -children {
					Spinbox.uparrow -side right -sticky nse
					Spinbox.downarrow -side right -sticky nse
					Spinbox.padding -sticky nswe -children {
						Spinbox.textarea -sticky nswe
					}
				}
			}
			# Cannot create a blue border on focus, as that would also
			# cause blue borders on the arrow buttons. Try this:
			# ttk::style map Modern.TSpinbox -lightcolor {focus #6f9dc6} \
			# 	-darkcolor {focus #6f9dc6} -bordercolor {focus #4a6984}
		}
		classic {
			ttk::style configure Modern.TSpinbox -padding {4 1} -arrowsize 14
			ttk::style layout Modern.TSpinbox {
				Spinbox.highlight -sticky nswe -children {
					Spinbox.field -sticky nswe -children {
						Spinbox.uparrow -side right -sticky e
						Spinbox.downarrow -side right -sticky e
						Spinbox.padding -sticky nswe -children {
							Spinbox.textarea -sticky nswe
						}
					}
				}
			}
			ttk::style layout TCombobox {
				Combobox.highlight -sticky nswe -children {
					Combobox.field -sticky nswe -children {
						Combobox.downarrow -side right -sticky ns
						Combobox.padding -expand 1 -sticky nswe -children {
							Combobox.textarea -sticky nswe
						}
					}
				}
			}
		}
		alt {
			# The arrows of arrow buttons are not vertically centered in the "alt" and
			# "default" themes.
			# One could compensate this by making the arrows larger, which does not
			# harmonize with combobox arrows and does not work well at larger TkTextFont
			# sizes.
			# Another option is the replacement of Spinbox.uparrow by Spinbox.rightarrow
			# and Spinbox.downarrow by Spinbox.leftarrow.
			ttk::style configure Modern.TSpinbox -padding {4 1} -arrowsize 14
			ttk::style layout Modern.TSpinbox {
				Spinbox.field -side top -sticky we -children {
					Spinbox.uparrow -side right -sticky nse
					Spinbox.downarrow -side right -sticky nse
					Spinbox.padding -sticky nswe -children {
						Spinbox.textarea -sticky nswe
					}
				}
			}
		}
		default {
			ttk::style configure Modern.TSpinbox -padding {4 1} -arrowsize 12
			ttk::style layout Modern.TSpinbox {
				Spinbox.field -side top -sticky we -children {
					Spinbox.uparrow -side right -sticky nse
					Spinbox.downarrow -side right -sticky nse
					Spinbox.padding -sticky nswe -children {
						Spinbox.textarea -sticky nswe
					}
				}
			}
		}
	}
}
