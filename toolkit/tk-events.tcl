# Copyright 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc tk_setup_virtual_events {} {
	# Remove virtual event definitions that interfere with other commands
	# in TkOffice.
	# See proc ::tk::EventMotifBindings in ${your_Tk_installation}/tk.tcl
	# (e.g. ${your_Tk_installation} = /usr/share/tcltk/tk8.5/)
	event delete <<Cut>> <Control-Key-w> <Control-Lock-Key-W>
	event delete <<Paste>> <Control-Key-y> <Control-Lock-Key-Y>

	event add <<Redo>> <Control-Key-y> <Control-Lock-Key-Y>
	# TODO: Do more with virtual events. These will help when TkOffice will be
	# adapted to Mac OS, where <Control-...> generally becomes <Command-...>.
}
