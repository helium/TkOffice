# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

proc xy_outside_window {window x y} {
	expr {$x < 0 || $y < 0 || $x >= [winfo width $window] || $y >= [winfo height $window]}
}

# Modelled after ttk::bindMouseWheel
proc bindControlMouseWheel {bindtag callback} {
	switch -- [tk windowingsystem] {
		x11 {
			bind $bindtag <Control-ButtonPress-4> "$callback -1"
			bind $bindtag <Control-ButtonPress-5> "$callback +1"
		}
		win32 {
			bind $bindtag <Control-MouseWheel> [append callback { [expr {-(%D/120)}]}]
		}
		aqua {
			bind $bindtag <Control-MouseWheel> [append callback { [expr {-(%D)}]} ]
		}
	}
}
