# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc bind_combobox {combobox script} {
	foreach event {<Return> <KP_Enter> <FocusOut> <<ComboboxSelected>>} {
		bind $combobox $event $script
	}
}
