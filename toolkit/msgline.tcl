# Copyright 2011, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc msgline {parent title message buttons {color #ccbbff}} {
# buttons is an array of n*4 entries (text underlinePos icon command)

	frame $parent.msgline -cursor left_ptr -background $color -border 1 -relief solid
	place $parent.msgline -anchor sw -relwidth 1 -rely 1

	ttk::label $parent.msgline.title -text $title -font TkCaptionFont -background $color
	grid $parent.msgline.title -row 0 -column 0

	ttk::label $parent.msgline.msg -text $message -justify left -background $color
	bind $parent.msgline.msg <Configure> {%W configure -wraplength [expr max(75,%w-4)]}
	grid $parent.msgline.msg -row 0 -column [set col 1] -sticky nesw
	grid columnconfigure $parent.msgline 1 -weight 1

	set buttonid 0
	foreach {text underlinePos icon command} $buttons {
		set button $parent.msgline.button[incr buttonid]
		ttk::button $button \
			-text $text -underline $underlinePos \
			-command [concat $command \; msgline_disappear $parent] \
		    -style msgline.TButton -image $icon -compound left
		grid $button -row 0 -column [incr col] -padx 2 -pady 2 -sticky nesw
		if {$buttonid == 0} {
			grid configure $button -padx {0 2}
		}
		set key [string tolower [string index $text $underlinePos]]
		bind $button <Alt-$key> {%W invoke}
		bind $parent.msgline <Alt-$key> [list $button invoke]
	}

	bind $button <Tab> [list tk::TabToWindow $parent.msgline.button1]
	bind $button <Tab> +break
	bind $parent.msgline.button1 <<PrevWindow>> [list tk::TabToWindow $button]
	bind $parent.msgline.button1 <<PrevWindow>> +break
	bind $parent.msgline <<PrevWindow>> [list tk::TabToWindow $button]
	bind $parent.msgline <<PrevWindow>> +break
}

proc msgline_disappear {parent} {
	if [winfo exists $parent.msgline] {destroy $parent.msgline}
}
proc msgline_default {parent buttonText} {
	set buttonid 0
	while {[winfo exists [set button $parent.msgline.button[incr buttonid]]]} {
		if {$buttonText==[$button cget -text]} {
			$button configure -default active
			foreach window {"" .title .msg} {
				bind $parent.msgline$window <ButtonPress> [list $button invoke]
			}
			return
		}
	}
	error "button $buttonText not found"
}
proc msgline_grab {parent} {
	focus $parent.msgline
	grab $parent.msgline
}
