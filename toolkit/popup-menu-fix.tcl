# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

proc fix_simple_popup_menu {menu} {
	if {[tk windowingsystem] eq "x11"} {
		bindtag_replace $menu Menu SimplePopupMenu
	}
}

bind SimplePopupMenu <Escape> {simple_popup_menu::escape %W}
bind SimplePopupMenu <Motion> {simple_popup_menu::motion %W %x %y}
bind SimplePopupMenu <ButtonRelease> {simple_popup_menu::button_release %W %x %y}
bind SimplePopupMenu <ButtonPress> {simple_popup_menu::button_press %W %x %y}

namespace eval simple_popup_menu {

proc escape {menu} {
	unpost $menu
}

proc motion {menu x y} {
	variable pointer_has_been_inside

	if {[$menu index @$x,$y] eq {none}} {
		if {![info exists pointer_has_been_inside($menu)]} {
			# Keep a programmatically activated entry active until the
			# mouse cursor enters the menu
			return
		}
	} else {
		set pointer_has_been_inside($menu) yes
	}
	$menu activate @$x,$y
	tk::GenerateMenuSelect $menu  ;# <<MenuSelect>> event on actual selection changes
}

proc button_release {menu x y} {
	variable button_pressed
	variable pointer_has_been_inside

	if {[$menu index @$x,$y] eq {none}} {
		if {[info exists button_pressed($menu)]
		|| [info exists pointer_has_been_inside($menu)]} {
			unpost $menu
		}
	} else {
		unpost $menu
		uplevel #0 [list $menu invoke @$x,$y]
	}
}

proc button_press {menu x y} {
	variable button_pressed

	if {[$menu index @$x,$y] eq {none}} {
		unpost $menu
	} else {
		tk::MenuButtonDown $menu
	}
}

proc unpost {menu} {
	variable button_pressed
	variable pointer_has_been_inside

	unset -nocomplain button_pressed($menu)
	unset -nocomplain pointer_has_been_inside($menu)
	tk::MenuUnpost $menu
}

}
