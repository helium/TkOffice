# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc bind_spinbox {spinbox script} {
	$spinbox configure -command [string map [list %W $spinbox] $script]
	foreach event {<Return> <KP_Enter> <FocusOut>} {
		bind $spinbox $event {invoke_spinbox %W}
	}
}

proc invoke_spinbox {spinbox} {
	uplevel #0 [$spinbox cget -command]
}

# Patch MouseWheel handling of spinboxes such that a multi-click
# event (on Windows/Mac, with e. g. dir=-4) performs not just one
# increment/decrement
proc ttk::spinbox::MouseWheel {w dir} {
	# Directly call Spin, as <<Increment>> / <<Decrement>> do not
	# support a "count" argument.
	# Minus sign, as the number is incremented on the gesture that
	# normally scrolls up.
	ttk::spinbox::Spin $w [expr {-$dir}]
}
