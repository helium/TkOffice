# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Implementation of canvas-less vertically scrolling frame

proc make_scrolling_frame {container content} {
	global scrolling_frame_offset
	global scrolling_frame_last_width
	global scrolling_frame_width_mode

	ttk::frame $container
	ttk::scrollbar $container.scroll -orient vertical -command \
		[list scroll_scrolling_frame $container $content]
	set scrolling_frame_offset($container) 0
	set scrolling_frame_last_width($container) 0
	set scrolling_frame_width_mode($container) normal
	place $content -in $container -x 0 -y 0 -relwidth 1 -width 0
	bind $container <Configure> +[list configure_scrolling_frame $container $content outer]
	bind $content <Configure> +[list configure_scrolling_frame $container $content]

	raise $content $container
	return $container
}

proc scrolling_frame_width_mode {container new_width_mode} {
	upvar #0 scrolling_frame_width_mode($container) width_mode
	set width_mode $new_width_mode
}

proc configure_scrolling_frame {container content {cause inner}} {
	upvar #0 scrolling_frame_offset($container) offset
	upvar #0 scrolling_frame_last_width($container) last_width
	upvar #0 scrolling_frame_width_mode($container) width_mode

	set content_reqwidth [winfo reqwidth $content]
	if {$width_mode eq "increase-only"} {
		if {$content_reqwidth < $last_width} {
			set content_reqwidth $last_width
		}
	}
	set last_width $content_reqwidth

	set scrollbar_visible [expr {"$container.scroll" in [place slaves $container]}]

	set content_reqheight [winfo reqheight $content]
	set container_height [winfo height $container]

	if {$container_height >= $content_reqheight
	&& ($width_mode ne "increase-only" || $cause eq "outer" || !$scrollbar_visible)} {
		place configure $content -relwidth 1 -width 0
		place configure $content -y 0
		$container.scroll set 0 1
		place forget $container.scroll
		$container configure -width $content_reqwidth
		set offset 0
	} else {
		set offset [expr {max(0, min($offset, $content_reqheight - $container_height))}]
		set scrollbar_width [winfo reqwidth $container.scroll]
		place configure $content -relwidth 1 -width [expr {-$scrollbar_width}]
		place configure $content -y [expr {-$offset}]
		$container.scroll set [expr {$offset / double($content_reqheight)}] \
			[expr {($offset + $container_height) / double($content_reqheight)}]
		place $container.scroll -relx 1 -y 0 -relheight 1 -anchor ne
		$container configure -width [expr {$content_reqwidth + $scrollbar_width}]
	}
	grid configure $container
}

proc scroll_scrolling_frame {container content op amount args} {
	upvar #0 scrolling_frame_offset($container) offset

	set content_reqheight [winfo reqheight $content]
	set container_height [winfo height $container]

	if {$op eq {moveto}} {
		set offset [expr {int($amount * $content_reqheight)}]
	} elseif {$op eq {scroll}} {
		if {$args eq {units}} {
			;# Scroll increment (20) should be an option for make_scrolling_frame
			set unit 20
		} elseif {$args eq {pages}} {
			set unit $container_height
		} elseif {[llength $args] != 1} {
			error {wrong # args: should be "... scroll number units|pages"}
		} else {
			error "bad argument \"$args\": must be units or pages"
		}
		set offset [expr {$offset + int($amount * $unit)}]
	} else {
		error "unknown option \"$op\": must be moveto or scroll"
	}

	configure_scrolling_frame $container $content
}
