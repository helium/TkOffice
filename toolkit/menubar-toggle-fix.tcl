# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Work around Tk bugs with handling the menubar of toplevels:
# - Stuck menubar when removing it with [$toplevel configure -menu {}]
# - Missing menubar when adding it with [$toplevel configure -menu $menu]
# These bugs only affect maximized and full-screen windows.
# If we apply the [wm geometry] hack to non-maximized non-fullscreen windows,
# the windows move to the left upper screen corner (geometry +0+0),
# if they have been withdrawn at the last [wm geometry] invocation.

proc menubar_toggle_fix {toplevel} {
	# TODO: Untested for non-X11 platforms
	switch [tk windowingsystem] {
		x11 {set zoomed [wm attributes $toplevel -zoomed]}
		default {set zoomed [expr {[wm state $toplevel] eq {zoomed}}]}
	}

	if {$zoomed || [wm attributes $toplevel -fullscreen]} {
		wm geometry $toplevel [wm geometry $toplevel]
	}
}
