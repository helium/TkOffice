#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam
. configure -bg #dcdad5
pack [ttk::treeview .tv -show {} -columns tagName -selectmode browse] \
	-side left -padx {6 0} -pady 6 -fill both -expand true
.tv column tagName -width 120
pack [ttk::scrollbar .scroll -orient vertical] \
	-side right -padx {2 6} -pady 6 -fill y
.tv configure -yscrollcommand {.scroll set}
.scroll configure -command {.tv yview}

foreach text {
	One Two Three Four Five Six Seven Eight Nine Ten
	Eleven Twelve Thirteen Fourteen Fifteen Sixteen Seventeen Eighteen Nineteen
} {
	.tv insert {} end -values [list "[incr i]. $text"]
}
foreach tens {Twenty Thirty Forty Fifty Sixty} {
	foreach ones {{} -one -two -three -four -five -six -seven -eight -nine} {
		.tv insert {} end -values [list "[incr i]. $tens$ones"]
	}
}


proc make_listpos_indicator {treeview x y width} {
	# Slight imperfection: If the treeview height is such that the last visible item
	# is partially visible (with more than half of its height), the supplied indicator
	# position $y lies outside the treeview's visible area when the mouse cursor
	# is moved over the last visible item's lower half. The correct behavior would
	# involve starting the auto-scroll operation already here. This implementation
	# does not start scrolling but makes sure that the indicator line is visible
	# if and only if a mouse button release would result in a list reordering.
	# Advantage: Simpler code, auto-scroll operation decoupled from the rest.
	# Disadvantage: The indicator line is drawn at a "slightly wrong" location in order
	# to be visible.
	set y [expr {min($y, [winfo height $treeview]-2)}]

	if {! [winfo exists $treeview.indicator]} {
		frame $treeview.indicator -bg #6f9dc6
	}
	place $treeview.indicator -x $x -y $y -width $width -height 2
}

bind .tv <ButtonPress-1> {listpos_button_press %W %x %y}
bind .tv <B1-Motion> {listpos_motion %W %x %y}
bind .tv <ButtonRelease-1> {listpos_auto_scan_cancel; listpos_motion_finish %W %x %y}
bind .tv <B1-Leave> {listpos_auto_scan_start %W %y}
bind .tv <B1-Enter> {listpos_auto_scan_cancel}

proc listpos_button_press {treeview x y} {
	variable listpos_motion_disable

	set hover_item [$treeview identify item $x $y]
	set listpos_motion_disable [expr {$hover_item eq {}}]
}

proc listpos_motion {treeview x y} {
	variable last_y
	variable last_item
	variable listpos_motion_disable

	if {$listpos_motion_disable} return
	set last_y $y

	if {$y >= [winfo height $treeview]} {
		destroy $treeview.indicator
		return
	}

	set hover_item [$treeview identify item $x $y]
	set bbox [$treeview bbox $hover_item]
	if {$bbox eq {}} {
		if {![info exists last_item]} {
			set last_item [lindex [$treeview children {}] end]
		}
		set bbox [$treeview bbox $last_item]
		lassign $bbox bbox_x bbox_y width height
		if {$bbox eq {} || $y < $bbox_y + $height || $y >= [winfo height $treeview]} {
			destroy $treeview.indicator
			return
		}
	}
	lassign $bbox bbox_x bbox_y width height
	if {$y > $bbox_y + $height / 2} {
		# Show the indicator line just below the currently hovered item
		make_listpos_indicator $treeview $bbox_x [expr {$bbox_y + $height - 1}] $width
	} else {
		# Show the indicator line just above the currently hovered item
		make_listpos_indicator $treeview $bbox_x [expr {$bbox_y - 1}] $width
	}
	$treeview configure -cursor fleur
}

proc listpos_motion_finish {treeview x y} {
	variable last_item
	variable listpos_motion_disable

	if {$listpos_motion_disable} return

	destroy $treeview.indicator
	$treeview configure -cursor {}

	if {$y >= [winfo height $treeview]} {
		unset -nocomplain last_item
		return
	}

	set hover_item [$treeview identify item $x $y]
	set bbox [$treeview bbox $hover_item]
	if {$bbox eq {}} {
		if {![info exists last_item]} {
			set last_item [lindex [$treeview children {}] end]
		}
		set bbox [$treeview bbox $last_item]
		set hover_item $last_item
		lassign $bbox bbox_x bbox_y width height
		if {$bbox eq {} || $y < $bbox_y + $height} {
			unset last_item
			return
		}
	}
	unset -nocomplain last_item
	set hover_index [$treeview index $hover_item]
	set moved_item [$treeview selection]
	set moved_index [$treeview index $moved_item]

	if {$hover_index == $moved_index} return  ;# item moved onto itself

	lassign $bbox bbox_x bbox_y width height
	set new_index_offset 0
	if {$y < $bbox_y + $height / 2} {
		incr new_index_offset -1
	}
	if {$hover_index < $moved_index} {
		incr new_index_offset
	}

	set new_index [expr {$hover_index + $new_index_offset}]
	switch $new_index_offset {
		 0 {set relative_item $hover_item}
		 1 {set relative_item [$treeview next $hover_item]}
		-1 {set relative_item [$treeview prev $hover_item]}
	}

	set moved_content [lindex [$treeview item $moved_item -values] 0]
	set relative_content [lindex [$treeview item $relative_item -values] 0]
	if {$new_index > $moved_index} {
		puts "raise {$moved_content} above {$relative_content}"
	} elseif {$new_index < $moved_index} {
		puts "lower {$moved_content} below {$relative_content}"
	} else {
		return  ;# item moved onto itself
	}
	$treeview move $moved_item {} $new_index
}

set auto_scan_afterid {}

proc listpos_auto_scan_start {treeview y} {
	variable listpos_motion_disable
	variable last_y

	if {![info exists listpos_motion_disable] || $listpos_motion_disable} return
	set last_y $y
	listpos_auto_scan $treeview
}

proc listpos_auto_scan {treeview} {
	variable auto_scan_afterid
	variable last_y

	set height [winfo height $treeview]
	if {$last_y > $height-2} {
		$treeview yview scroll +1 units
		set distance [expr {$last_y - ($height-2)}]
	} elseif {$last_y < 2} {
		$treeview yview scroll -1 units
		set distance [expr {2 - $last_y}]
	} else {
		return
	}
	set auto_scan_afterid [after [expr {200 / ($distance / 10 + 1)}] listpos_auto_scan $treeview]
}

proc listpos_auto_scan_cancel {} {
	variable auto_scan_afterid
	after cancel $auto_scan_afterid
	set auto_scan_afterid {}
}
