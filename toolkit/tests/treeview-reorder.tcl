#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam
. configure -bg #dcdad5
pack [ttk::treeview .tv -show {} -columns tagName -selectmode browse] -padx 6 -pady 6
.tv column tagName -width 120

foreach text {One Two Three Four Five Six Seven Eight} {
	.tv insert {} end -values [list $text]
}

proc make_listpos_indicator {treeview x y width} {
	if {! [winfo exists $treeview.indicator]} {
		frame $treeview.indicator -bg #6f9dc6
	}
	place $treeview.indicator -x $x -y $y -width $width -height 2
}

bind .tv <ButtonPress-1> {listpos_button_press %W %x %y}
bind .tv <B1-Motion> {listpos_motion %W %x %y}
bind .tv <ButtonRelease-1> {listpos_motion_finish %W %x %y}

proc listpos_button_press {treeview x y} {
	variable button_press_on_empty_area

	set hover_item [$treeview identify item $x $y]
	set button_press_on_empty_area [expr {$hover_item eq {}}]
}

proc listpos_motion {treeview x y} {
	variable last_item
	variable button_press_on_empty_area

	if {$button_press_on_empty_area} return

	set hover_item [$treeview identify item $x $y]
	set bbox [$treeview bbox $hover_item]
	if {$bbox eq {}} {
		if {![info exists last_item]} {
			set last_item [lindex [$treeview children {}] end]
		}
		set bbox [$treeview bbox $last_item]
		lassign $bbox bbox_x bbox_y width height
		if {$y < $bbox_y + $height || $y >= [winfo height $treeview]} {
			destroy $treeview.indicator
			return
		}
	}
	lassign $bbox bbox_x bbox_y width height
	if {$y > $bbox_y + $height / 2} {
		# Show the indicator line just below the currently hovered item
		make_listpos_indicator $treeview $bbox_x [expr {$bbox_y + $height - 1}] $width
	} else {
		# Show the indicator line just above the currently hovered item
		make_listpos_indicator $treeview $bbox_x [expr {$bbox_y - 1}] $width
	}
	$treeview configure -cursor fleur
}

proc listpos_motion_finish {treeview x y} {
	variable last_item
	variable button_press_on_empty_area

	if {$button_press_on_empty_area} return

	destroy $treeview.indicator
	$treeview configure -cursor {}

	set hover_item [$treeview identify item $x $y]
	set bbox [$treeview bbox $hover_item]
	if {$bbox eq {}} {
		if {![info exists last_item]} {
			set last_item [lindex [$treeview children {}] end]
		}
		set bbox [$treeview bbox $last_item]
		set hover_item $last_item
		lassign $bbox bbox_x bbox_y width height
		if {$y < $bbox_y + $height || $y >= [winfo height $treeview]} {
			unset last_item
			return
		}
	}
	unset -nocomplain last_item
	set hover_index [$treeview index $hover_item]
	set moved_item [$treeview selection]
	set moved_index [$treeview index $moved_item]

	if {$hover_index == $moved_index} return  ;# item moved onto itself

	lassign $bbox bbox_x bbox_y width height
	set new_index_offset 0
	if {$y < $bbox_y + $height / 2} {
		incr new_index_offset -1
	}
	if {$hover_index < $moved_index} {
		incr new_index_offset
	}

	set new_index [expr {$hover_index + $new_index_offset}]
	switch $new_index_offset {
		 0 {set relative_item $hover_item}
		 1 {set relative_item [$treeview next $hover_item]}
		-1 {set relative_item [$treeview prev $hover_item]}
	}

	set moved_content [lindex [$treeview item $moved_item -values] 0]
	set relative_content [lindex [$treeview item $relative_item -values] 0]
	if {$new_index > $moved_index} {
		puts "raise {$moved_content} above {$relative_content}"
	} elseif {$new_index < $moved_index} {
		puts "lower {$moved_content} below {$relative_content}"
	} else {
		return  ;# item moved onto itself
	}
	$treeview move $moved_item {} $new_index
}
