#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Warp-based alternative to the tk::TextAutoScan mechanism when
# the mouse pointer leaves the text widget during selection

ttk::style theme use clam

pack [ttk::frame .radios] -side top -fill x -pady 3
ttk::label .radios.mode -text "Selection Mode:"
ttk::radiobutton .radios.autoscan -text "Tk AutoScan" -variable mode -value autoscan -command set_mode
ttk::radiobutton .radios.captive -text "Captive Pointer" -variable mode -value captive -command set_mode
pack {*}[winfo children .radios] -side left -padx 3
set mode autoscan

pack [text .text -wrap word -width 70] -fill both -expand true -side left
pack [ttk::scrollbar .scroll -orient vertical] -fill y -side right
.scroll configure -command {.text yview}
.text configure -yscrollcommand {.scroll set}

### Generate random text

proc randint {min max} {expr {int(rand()*($max-$min+1)) + $min}}

set sentences [list {The quick brown fox jumps over the rusty rail.} {Jackdaws love my big sphinx of quartz.}]

set num_paragraphs 100
for {set paragraph 1} {$paragraph <= $num_paragraphs} {incr paragraph} {
	set num_sentences [randint 1 6]
	for {set sentence 1} {$sentence <= $num_sentences} {incr sentence} {
		.text insert end [lindex $sentences [randint 0 1]]
		if {$sentence < $num_sentences} {
			.text insert end { }
		}
	}
	if {$paragraph < $num_paragraphs} {
		.text insert end \n\n
	}
}

### Text selection behavior

proc set_mode {} {
	global mode
	switch $mode autoscan {
		bindtags .text {.text Text . all}
	} captive {
		bindtags .text {.text CaptiveText . all}
	}
}

bind CaptiveText <ButtonPress-1> {text_button_press %W %x %y}
bind CaptiveText <B1-Motion> {text_drag %W %x %y}
foreach event {<MouseWheel> <Button-4> <Button-5>} {
	bind CaptiveText $event [bind Text $event]
}

proc text_button_press {text x y} {
	$text mark set insert [tk::TextClosestGap $text $x $y]
	$text mark set sel_anchor insert
	$text mark set old_cur insert
	$text tag remove sel 1.0 end
	focus $text
}

proc text_drag {text x y} {
	set height [winfo height $text]
	set speedup 3
	if {$y >= $height} {
		$text yview scroll [expr {$speedup * ($y-$height+1)}] pixels
		event generate $text <B1-Motion> -x $x -y [expr {$height-1}] -warp true -when tail
	} elseif {$y < 0} {
		$text yview scroll [expr {$speedup * $y}] pixels
		event generate $text <B1-Motion> -x $x -y 0 -warp true -when tail
	}

	set cur [tk::TextClosestGap $text $x $y]
	if {[$text compare $cur == old_cur]} return
	$text mark set old_cur $cur

	if {[$text compare $cur < sel_anchor]} {
		set first $cur
		set last sel_anchor
	} else {
		set first sel_anchor
		set last $cur
	}
	$text tag remove sel 1.0 end
	$text mark set insert $cur
	$text tag add sel $first $last
	$text tag remove sel $last end
}
