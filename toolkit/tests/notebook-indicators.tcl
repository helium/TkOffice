#!/usr/bin/wish8.5
# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam
. configure -bg #dcdad5

pack [ttk::notebook .nb] -fill both -expand true
.nb configure -padding {0 0 -2 0}
.nb add [text .nb.t1] -text {Text 1}
.nb add [text .nb.t2] -text {Text 2}
.nb add [text .nb.t3] -text {Text 3}
.nb.t1 insert end "Text 1 here."
.nb.t2 insert end "\nText 2 here."
.nb.t3 insert end "\n\nText 3 here."

pack [ttk::frame .styles] -fill x -expand true
pack [ttk::label .styles.lbl -text Style:] -side left
foreach style [ttk::style theme names] {
	pack [ttk::radiobutton .styles.$style -variable ttk_style -value $style -text $style -command set_style] -side left
}
proc set_style {} {
	global ttk_style
	ttk::style theme use $ttk_style
}
set ttk_style clam

proc measure_tab_end_x {notebook} {
	set x 2
	set tab_index 0
	set end_x [list]
	while {$tab_index ne {}} {
		set new_index [$notebook identify tab [incr x] 10]
		if {$new_index ne $tab_index} {
			lappend end_x [expr {$x - 1}]
			set tab_index $new_index
		}
	}
	return $end_x
}

proc measure_tab_height {notebook} {
	set y 2
	while {[$notebook identify tab 2 [incr y]] ne {}} {}
	return $y
}

proc make_tabpos_indicator {notebook index} {
	if {! [winfo exists $notebook.indicator]} {
		frame $notebook.indicator -bg #6f9dc6
	}
	if {$index < 0} {
		place forget $notebook.indicator
		return
	}
	set ends [concat 0 [measure_tab_end_x $notebook]]
	set x [lindex $ends [expr {min($index, [llength $ends]-1)}]]
	set height [measure_tab_height $notebook]
	place $notebook.indicator -x $x -y 2 -width 3 -height [expr {$height-2}]
}

proc tick {} {
	global current_tab
	if {![info exists current_tab]} {set current_tab -1}
	make_tabpos_indicator .nb $current_tab
	incr current_tab
	if {$current_tab > [.nb index end]} {
		set current_tab -1
	}
	after 1000 tick
}
tick
