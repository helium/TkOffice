#!/usr/bin/wish8.5
# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam
. configure -bg #dcdad5

pack [ttk::notebook .nb] -fill both -expand true
.nb configure -padding {0 0 -2 0}
.nb add [text .nb.t1] -text {Text 1}
.nb add [text .nb.t2] -text {Text 2}
.nb add [text .nb.t3] -text {Text 3}
.nb add [text .nb.t4] -text {Text 4}
.nb.t1 insert end "Text 1 here."
.nb.t2 insert end "\nText 2 here."
.nb.t3 insert end "\n\nText 3 here."
.nb.t4 insert end "\n\n\nText 4 here."

pack [ttk::frame .styles] -fill x -expand true
pack [ttk::label .styles.lbl -text Style:] -side left
foreach style [ttk::style theme names] {
	pack [ttk::radiobutton .styles.$style -variable ttk_style -value $style -text $style -command set_style] -side left
}
proc set_style {} {
	global ttk_style
	ttk::style theme use $ttk_style
}
set ttk_style clam

proc measure_tab_boundaries {notebook} {
	set x 2
	set tab_index 0
	set boundaries [list 0]
	while {$tab_index ne {}} {
		set new_index [$notebook identify tab [incr x] 10]
		if {$new_index ne $tab_index} {
			lappend boundaries [expr {$x - 1}]
			set tab_index $new_index
		}
	}
	return $boundaries
}

proc measure_tab_height {notebook} {
	set y 2
	while {[$notebook identify tab 2 [incr y]] ne {}} {}
	return $y
}

proc make_tabpos_indicator {notebook x} {
	if {! [winfo exists $notebook.indicator]} {
		frame $notebook.indicator -bg #6f9dc6
	}
	set height [measure_tab_height $notebook]
	place $notebook.indicator -x $x -y 2 -width 3 -height [expr {$height-2}]
}

proc remove_tabpos_indicator {notebook} {
	destroy $notebook.indicator
}

proc notebook_pointer_index {notebook x} {
	global tab_boundaries
	if {![info exists tab_boundaries]} {
		set tab_boundaries [measure_tab_boundaries $notebook]
	}
	# Example: tab_boundaries {0 20 60 80} -> midpoints {10 40 70}
	set len [llength $tab_boundaries]
	for {set i 1} {$i < $len} {incr i} {
		lassign [lrange $tab_boundaries $i-1 $i] left right
		lappend midpoints [expr {($left+$right)>>1}]  ;# >>1 is /2
	}
	# Return the index of the first midpoint that is larger than the x argument
	set idx 0
	foreach midpoint $midpoints {
		if {$midpoint > $x} break
		incr idx
	}
	return $idx
}

proc notebook_move_active_tab {notebook new_idx} {
	# Notebook tabs can be moved (relocated) with [$notebook insert $window $idx],
	# but this works only well when $idx is adjacent to the old position of $window.
	# Otherwise, a garbled order results (e.g. 1 2 3 4 -> 3 1 2 4 when moving 1 to
	# the 3rd position).
	# This procedure takes care of moving the tab in multiple steps by 1 position
	# each.
	set tab [$notebook select]
	set old_idx [$notebook index current]
	if {$new_idx == $old_idx} return
	set dir [expr {$new_idx > $old_idx ? 1 : -1}]
	for {set idx [expr {$old_idx+$dir}]} {$idx != $new_idx+$dir} {incr idx $dir} {
		$notebook insert $tab $idx
	}
}

proc notebook_button_motion {notebook x y} {
	global tab_boundaries
	set idx [notebook_pointer_index $notebook $x]
	make_tabpos_indicator $notebook [lindex $tab_boundaries $idx]
}

proc notebook_button_release {notebook x y} {
	global tab_boundaries
	set idx [notebook_pointer_index $notebook $x]
	if {$idx > [$notebook index current]} {
		incr idx -1
	}
	notebook_move_active_tab $notebook $idx
	
	remove_tabpos_indicator $notebook
	unset tab_boundaries
}

bind TNotebook <Button1-Motion> {notebook_button_motion %W %x %y}
bind TNotebook <ButtonRelease-1> {notebook_button_release %W %x %y}
