#!/usr/bin/wish8.5
# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc make_scrolling_frame {container content} {
	global scrolling_frame_offset

	ttk::frame $container
	ttk::scrollbar $container.scroll -orient vertical -command \
		[list scroll_scrolling_frame $container $content]
	set scrolling_frame_offset($container) 0
	place $content -in $container -x 0 -y 0 -relwidth 1 -width 0
	bind $container <Configure> +[list configure_scrolling_frame $container $content]
	bind $content <Configure> +[list configure_scrolling_frame $container $content]

	raise $content $container
	return $container
}

proc configure_scrolling_frame {container content} {
	upvar #0 scrolling_frame_offset($container) offset

	set content_reqheight [winfo reqheight $content]
	set container_height [winfo height $container]

	if {$container_height >= $content_reqheight} {
		place configure $content -relwidth 1 -width 0
		place configure $content -y 0
		$container.scroll set 0 1
		place forget $container.scroll
		$container configure -width [winfo reqwidth $content]
		set offset 0
	} else {
		set offset [expr {max(0, min($offset, $content_reqheight - $container_height))}]
		set scrollbar_width [winfo reqwidth $container.scroll]
		place configure $content -relwidth 1 -width [expr {-$scrollbar_width}]
		place configure $content -y [expr {-$offset}]
		$container.scroll set [expr {$offset / double($content_reqheight)}] \
			[expr {($offset + $container_height) / double($content_reqheight)}]
		place $container.scroll -relx 1 -y 0 -relheight 1 -anchor ne
		set content_reqwidth [winfo reqwidth $content]
		$container configure -width [expr {$content_reqwidth + $scrollbar_width}]
	}
	grid configure $container
}

proc scroll_scrolling_frame {container content op amount args} {
	upvar #0 scrolling_frame_offset($container) offset

	set content_reqheight [winfo reqheight $content]
	set container_height [winfo height $container]

	if {$op eq {moveto}} {
		set offset [expr {int($amount * $content_reqheight)}]
	} elseif {$op eq {scroll}} {
		if {$args eq {units}} {
			;# Scroll increment (20) should be an option for make_scrolling_frame
			set unit 20
		} elseif {$args eq {pages}} {
			set unit $container_height
		} elseif {[llength $args] != 1} {
			error {wrong # args: should be "... scroll number units|pages"}
		} else {
			error "bad argument \"$args\": must be units or pages"
		}
		set offset [expr {$offset + int($amount * $unit)}]
	} else {
		error "unknown option \"$op\": must be moveto or scroll"
	}

	configure_scrolling_frame $container $content
}

ttk::style theme use clam

canvas .content -width 200 -height 500 -highlightthickness 0 -bg #ca0
.content create line 0 0 199 500
.content create line 199 0 0 500
.content create text 100 30 -text {Tool palette}

grid [ttk::label .main -text {Main window area}] -row 0 -column 0
grid [make_scrolling_frame .container .content] -row 0 -column 1 -sticky nesw
grid columnconfigure . 0 -weight 1
grid rowconfigure . 0 -weight 1
wm minsize . 100 100
