#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam

pack [canvas .canvas] -fill both -expand true -side left
.canvas configure -background white -width 320 -height 160
pack [ttk::scrollbar .scroll -orient vertical] -fill y -side right
.scroll configure -command {.canvas yview}
.canvas configure -yscrollcommand {.scroll set}
.canvas configure -scrollregion {0 0 320 4000}

### Generate random graphics

proc randint {min max} {expr {int(rand()*($max-$min+1)) + $min}}

for {set lineno 0} {$lineno < 60} {incr lineno} {
	set coords [list]
	set y 0
	set num_points [randint 10 50]
	for {set i 0} {$i < $num_points} {incr i} {
		set x [randint 25 320]
		incr y [randint -10 [expr {(4000-$y)/($num_points-$i)*2}]]
		lappend coords $x $y
	}
	set r [randint 0 255]
	set g [randint 0 255]
	set b [randint 0 255]
	.canvas create line $coords \
		-fill [format "#%02x%02x%02x" $r $g $b] \
		-width [randint 1 6] \
		-joinstyle round -capstyle round
}

set textnumber 0
for {set y 20} {$y < 4000} {incr y 80} {
	.canvas create text 5 $y -text [incr textnumber] -anchor w -font TkHeadingFont
}

### Scrolling behavior bindings

.canvas configure -cursor fleur

bind .canvas <ButtonPress-1> {set ::last_mouse_y %y}
bind .canvas <B1-Motion> {canvas_drag %W %x %y}
bind .canvas <<WarpAfter>> {debug A}

set warp false
proc canvas_drag {canvas x y} {
	global warp
	if {$warp} {debug |; return}

	set dy [expr {$y - $::last_mouse_y}]
	lassign [$canvas yview] y_top y_bottom
	$canvas yview moveto [expr {$y_top - $dy / 4000.0}]
	set ::last_mouse_y $y
	update idletasks

	set height [winfo height $canvas]
	if {$y < 0} {
		incr y $height
		set warp true
		debug v
	} elseif {$y > $height} {
		incr y [expr {-$height}]
		set warp true
		debug ^
	} else {
		debug .
	}
	if {$warp} {
		event generate $canvas <ButtonPress-2> -x $x -y $y -warp true -when now
		set ::last_mouse_y $y
		debug W
		after 20 {set warp false}
	}
}

proc debug {c} {
	puts -nonewline $c
	flush stdout
}
