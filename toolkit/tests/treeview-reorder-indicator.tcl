#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam
. configure -bg #dcdad5
pack [ttk::treeview .tv -show {} -columns tagName -selectmode browse] -padx 6 -pady 6
.tv column tagName -width 120

foreach text {One Two Three Four Five Six Seven Eight} {
	.tv insert {} end -values [list $text]
}

proc make_listpos_indicator {treeview x y width} {
	if {! [winfo exists $treeview.indicator]} {
		frame $treeview.indicator -bg #6f9dc6
	}
	place $treeview.indicator -x $x -y $y -width $width -height 2
}

after 2000 {
	lassign [.tv bbox I003] x y width height
	make_listpos_indicator .tv $x [expr {$y - 1}] $width
	.tv configure -cursor fleur
}
bind Treeview <Motion> {}  ;# contains a routine that resets the cursor shape
