#!/usr/bin/wish8.5
# Copyright 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

source [file join [file dirname [info script]] .. msgline.tcl]

ttk::style theme use clam
ttk::style configure msgline.TButton -width 0 -padding {2 1}

image create photo White -width 24 -height 24
White put white -to 1 1 23 23

pack [ttk::button .b -text "Show message line" -command show_message_line]
pack [text .t -highlightthickness 0 -border 0 -background white] -padx 3 -pady 3 -fill both -expand true

proc show_message_line {} {
	msgline .t Attention: {This text is quite long and will wrap when the window's width is reduced.} {
		OK     0 White {.t insert end "OK pressed.\n"}
		No     0 White {.t insert end "No pressed.\n"}
		Cancel 0 White {.t insert end "Cancel pressed.\n"}
	}
	msgline_default .t Cancel
	msgline_grab .t
}
