#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam
. configure -bg #dcdad5
pack [ttk::treeview .tv -show {} -columns tagName -selectmode browse] \
	-side left -padx {6 0} -pady 6 -fill both -expand true
.tv column tagName -width 120
pack [ttk::scrollbar .scroll -orient vertical] \
	-side right -padx {2 6} -pady 6 -fill y -expand true
.tv configure -yscrollcommand {.scroll set}
.scroll configure -command {.tv yview}

foreach text {
	One Two Three Four Five Six Seven Eight Nine Ten
	Eleven Twelve Thirteen Fourteen Fifteen Sixteen Seventeen Eighteen Nineteen
} {
	.tv insert {} end -values [list "[incr i]. $text"]
}
foreach tens {Twenty Thirty Forty Fifty Sixty} {
	foreach ones {{} -one -two -three -four -five -six -seven -eight -nine} {
		.tv insert {} end -values [list "[incr i]. $tens$ones"]
	}
}


proc make_listpos_indicator {treeview x y width} {
	if {! [winfo exists $treeview.indicator]} {
		frame $treeview.indicator -bg #6f9dc6
	}
	place $treeview.indicator -x $x -y $y -width $width -height 2
}

bind .tv <B1-Motion> {listpos_motion %W %x %y}
bind .tv <ButtonRelease-1> {listpos_motion_finish %W %x %y}
# bind .tv <ButtonPress-2> {debug W%y}

set warp idle  ;# takes values: idle, before, now, after
set last_xy_before_warp [list 0 0]

proc listpos_motion {treeview x y} {
	global warp
	global last_xy_before_warp
	if {$warp eq "after"} {debug |; return}

	set height [winfo height $treeview]
	if {$warp eq "idle" && ($y < 2 || $y > $height-2)} {
		set warp before
		after 20 [list listpos_motion_warp $treeview]
		debug B
		return
	}
	if {$warp eq "before" && ($y < 2 || $y > $height-2)} {
		set last_xy_before_warp [list $x $y]
		return
	}

	set speedup 2
	if {$warp eq "now" && $y < 2} {
		set y [expr {($y - 2) * $speedup + 2}]
		set any_item [$treeview identify item 4 4]
		lassign [$treeview bbox $any_item] - - - item_height
		set steps_to_make_y_positive [expr {($y-2) / $item_height}]  ;# negative integer
		$treeview yview scroll $steps_to_make_y_positive units
		debug ^$steps_to_make_y_positive
		set dy [expr {-$steps_to_make_y_positive * $item_height}]
		set new_y [expr {$y + $dy}]
		update idletasks
		set warp after
		event generate $treeview <ButtonPress-2> -x $x -y $new_y -warp true -when now
		after 20 {set warp idle}
		return
	} elseif {$warp eq "now" && $y > $height-2} {
		set y [expr {($y - $height + 2) * $speedup + $height - 2}]
		set any_item [$treeview identify item 4 4]
		lassign [$treeview bbox $any_item] - - - item_height
		set steps_to_fit_y [expr {($y-$height+2) / $item_height + 1}]
		$treeview yview scroll $steps_to_fit_y units
		debug v$steps_to_fit_y
		set dy [expr {-$steps_to_fit_y * $item_height}]
		set new_y [expr {$y + $dy}]
		update idletasks
		set warp after
		event generate $treeview <ButtonPress-2> -x $x -y $new_y -warp true -when now
		after 20 {set warp idle}
		return
	} else {
		debug .
	}

	set hover_item [$treeview identify item $x $y]
	set bbox [$treeview bbox $hover_item]
	if {$bbox eq {}} {
		destroy $treeview.indicator
		return
	}
	lassign $bbox bbox_x bbox_y width height
	if {$y > $bbox_y + $height / 2} {
		# Show the indicator line just below the currently hovered item
		make_listpos_indicator $treeview $bbox_x [expr {$bbox_y + $height - 1}] $width
	} else {
		# Show the indicator line just above the currently hovered item
		make_listpos_indicator $treeview $bbox_x [expr {$bbox_y - 1}] $width
	}
	$treeview configure -cursor fleur
}

proc listpos_motion_warp {treeview} {
	global warp
	global last_xy_before_warp
	set warp now
	listpos_motion $treeview {*}$last_xy_before_warp
}

proc listpos_motion_finish {treeview x y} {
	destroy $treeview.indicator
	$treeview configure -cursor {}

	set hover_item [$treeview identify item $x $y]
	set bbox [$treeview bbox $hover_item]
	if {$bbox eq {}} return
	set hover_index [$treeview index $hover_item]
	set moved_item [$treeview selection]
	set moved_index [$treeview index $moved_item]

	if {$hover_index == $moved_index} return  ;# item moved onto itself

	set new_index $hover_index
	lassign $bbox bbox_x bbox_y width height
	if {$y < $bbox_y + $height / 2} {
		incr new_index -1
	}
	if {$hover_index < $moved_index} {
		incr new_index
	}
	$treeview move $moved_item {} $new_index
}

proc debug {c} {
	puts -nonewline $c
	flush stdout
}
