# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc set_radiobutton {button value on_value} {
	$button state [expr {$value eq $on_value ? {pressed} : {!pressed}}]
}
