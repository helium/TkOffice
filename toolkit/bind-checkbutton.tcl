# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc bind_checkbutton {checkbutton command} {
	$checkbutton configure -command [string map [list %W $checkbutton] $command]
}
