# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval menus {

proc addtag_menu_create {menupath} {
	ttk_menu $menupath -postcommand [list menus::addtag_menu_update $menupath]
	return $menupath
}

proc addtag_menu_update {menupath} {
	set tl [toplevel_for_menu $menupath]
	$menupath delete 0 end
	richtext::addtag_menu_populate $menupath [fix_toplevel_child $tl.editor.document]
}

proc removetag_menu_create {menupath} {
	ttk_menu $menupath -postcommand [list menus::removetag_menu_update $menupath]
	return $menupath
}

proc removetag_menu_update {menupath} {
	set tl [toplevel_for_menu $menupath]
	$menupath delete 0 end
	richtext::removetag_menu_populate $menupath [fix_toplevel_child $tl.editor.document]
}

}
