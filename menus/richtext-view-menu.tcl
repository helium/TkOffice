# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval menus {

proc richtext_view_menu_create {menupath} {
	ttk_menu $menupath
	$menupath add checkbutton -label [mc richtext_tag_panel] \
		-variable $menupath.tags_and_properties \
		-command [list menus::richtext_view_tag_panel $menupath] \
		-underline [mc richtext_tag_panel_underline]
	set ::$menupath.tags_and_properties 1

	return $menupath
}

proc richtext_view_tag_panel {menupath} {
	set tl [toplevel_for_menu $menupath]
	set editor [fix_toplevel_child $tl.editor]
	set state [get ::$menupath.tags_and_properties]
	if {$state} {
		richtext::show_tags_and_properties $editor
	} else {
		richtext::hide_tags_and_properties $editor
	}
}

}
