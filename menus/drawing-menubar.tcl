# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval menus {

proc drawing_menubar {menu} {
	$menu delete 0 end
	if {! [winfo exists $menu.file]} {
		file_menu_create $menu.file
	}
	if {! [winfo exists $menu.help]} {
		help_menu_create $menu.help
	}
	$menu add cascade -label [mc file] -underline [mc file_underline] -menu $menu.file
	$menu add cascade -label [mc help] -underline [mc help_underline] -menu $menu.help
}

}
