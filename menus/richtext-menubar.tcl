# Copyright 2021-2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval menus {

proc richtext_menubar {menu} {
	$menu delete 0 end
	if {! [winfo exists $menu.file]} {
		file_menu_create $menu.file
	}
	if {! [winfo exists $menu.edit]} {
		richtext_edit_menu_create $menu.edit
	}
	if {! [winfo exists $menu.addtag]} {
		addtag_menu_create $menu.addtag
	}
	if {! [winfo exists $menu.removetag]} {
		removetag_menu_create $menu.removetag
	}
	if {! [winfo exists $menu.richtext_view]} {
		richtext_view_menu_create $menu.richtext_view
	}
	if {! [winfo exists $menu.help]} {
		help_menu_create $menu.help
	}
	$menu add cascade -label [mc file] -underline [mc file_underline] -menu $menu.file
	$menu add cascade -label [mc edit] -underline [mc edit_underline] -menu $menu.edit
	$menu add cascade -label [mc addtag] -underline [mc addtag_underline] -menu $menu.addtag
	$menu add cascade -label [mc removetag] -underline [mc removetag_underline] -menu $menu.removetag
	$menu add cascade -label [mc view] -underline [mc view_underline] -menu $menu.richtext_view
	$menu add cascade -label [mc help] -underline [mc help_underline] -menu $menu.help
}

}
