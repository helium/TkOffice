# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval menus {

icons::load  help,16

proc help_menu_create {menupath} {
	ttk_menu $menupath
	set tl [toplevel_for_menu $menupath]
	$menupath add command -label [mc help_about] -command [list welcomescreen::about .about] \
		-underline [mc help_about_underline] \
		-compound left -image help,16

	return $menupath
}

}
