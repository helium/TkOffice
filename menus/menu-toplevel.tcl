# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval menus {

proc toplevel_for_menu {menu} {
	set widget $menu
	while {[winfo class $widget] eq {Menu}} {
		set widget [winfo parent $widget]
	}
	return [winfo toplevel $widget]
}

}
