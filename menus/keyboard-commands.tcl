# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval menus {

proc keyboard_commands {toplevel mode} {
	# $toplevel must be the window path of a toplevel window.
	switch $mode {
		drawing {
			file_menu_bind_keyboard_commands $toplevel
		}
		richtext {
			file_menu_bind_keyboard_commands $toplevel
		}
		none {
			file_menu_unbind_keyboard_commands $toplevel
		}
		default {error "bad mode \"$mode\": must be drawing, richtext, or none"}
	}
}

}
