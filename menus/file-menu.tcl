# Copyright 2021, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval menus {

variable filepath_for

icons::load  document-new,16  document-open,16  document-revert,16  document-save,16  document-save-as,16
icons::load  window-close,16  application-exit,16

proc file_menu_create {menupath} {
	variable filepath_for

	ttk_menu $menupath
	set tl [toplevel_for_menu $menupath]
	$menupath add command -label [mc file_new_richtext] -command [list menus::file_new_richtext $tl] \
		-underline [mc file_new_richtext_underline] \
		-compound left -image document-new,16
	$menupath add command -label [mc file_new_drawing] -command [list menus::file_new_drawing $tl] \
		-underline [mc file_new_drawing_underline] \
		-compound left -image document-new,16
	$menupath add separator
	$menupath add command -label [mc file_open] -command [list menus::file_open $tl] \
		-underline [mc file_open_underline] -accelerator [mc file_open_hotkey] \
		-compound left -image document-open,16
	$menupath add command -label [mc file_revert] -command [list menus::file_revert $tl] \
		-underline [mc file_revert_underline] \
		-compound left -image document-revert,16
	$menupath add separator
	$menupath add command -label [mc file_save] -command [list menus::file_save $tl] \
		-underline [mc file_save_underline] -accelerator [mc file_save_hotkey] \
		-compound left -image document-save,16
	$menupath add command -label [mc file_save_as] -command [list menus::file_save_as $tl] \
		-underline [mc file_save_as_underline] -accelerator [mc file_save_as_hotkey] \
		-compound left -image document-save-as,16
	$menupath add command -label [mc file_save_copy_as] -command [list menus::file_save_copy_as $tl] \
		-underline [mc file_save_copy_as_underline]
	$menupath add command -label [mc file_rename_and_save] \
		-command [list menus::file_rename_and_save $tl] \
		-underline [mc file_rename_and_save_underline]
	$menupath add separator
	$menupath add command -label [mc file_close] -command [list menus::file_close $tl] \
		-underline [mc file_close_underline] -accelerator [mc file_close_hotkey] \
		-compound left -image window-close,16
	$menupath add command -label [mc file_quit] -command [list menus::file_quit $tl] \
		-underline [mc file_quit_underline] -accelerator [mc file_quit_hotkey] \
		-compound left -image application-exit,16

	return $menupath
}

proc file_menu_bind_keyboard_commands {toplevel} {
	# The argument must be the window path of a toplevel window.
	bind $toplevel <Control-o> [list menus::file_open $toplevel]
	bind $toplevel <Control-s> [list menus::file_save $toplevel]
	bind $toplevel <Control-S> [list menus::file_save_as $toplevel]
	bind $toplevel <Control-w> [list menus::file_close $toplevel]
	bind $toplevel <Control-q> [list menus::file_quit $toplevel]
}

proc file_menu_unbind_keyboard_commands {toplevel} {
	# The argument must be the window path of a toplevel window.
	bind $toplevel <Control-o> {}
	bind $toplevel <Control-s> {}
	bind $toplevel <Control-S> {}
	bind $toplevel <Control-w> {}
	bind $toplevel <Control-q> {}
}

# Command implementations

proc file_new_richtext {toplevel} {
	variable filepath_for

	setup_editor $toplevel richtext
	set document [fix_toplevel_child $toplevel.editor.document]
	set filepath_for($document) {}
	undo::init_document [winfo parent $document]
	set_window_title $document
}

proc file_new_drawing {toplevel} {
	variable filepath_for

	setup_editor $toplevel drawing
	set document [fix_toplevel_child $toplevel.editor.document]
	set filepath_for($document) {}
	undo::init_document [winfo parent $document]
	set_window_title $document
}

proc file_open {toplevel} {
	variable filepath_for

	set doc [fix_toplevel_child $toplevel.editor.document]
	set filepath {}
	if {[info exists filepath_for($doc)]} {
		set filepath $filepath_for($doc)
	}
	set path [user_select_file $filepath open]
	if {$path eq {}} return
	read_file $path $toplevel
	undo::init_document [winfo parent $doc]
}

proc file_revert {toplevel} {
	variable filepath_for

	set doc [fix_toplevel_child $toplevel.editor.document]
	set path $filepath_for($doc)
	if {$path ne {}} {
		read_file $path $toplevel
	} else {
		switch [winfo class $doc] {
			Text {richtext::reset_widget $doc}
			Canvas {drawing::reset_widget $doc}
		}
	}
	# TODO: Undo handling should better store the revert in a group??
	undo::init_document [winfo parent $doc]
}

proc file_save {toplevel} {
	variable filepath_for

	set doc [fix_toplevel_child $toplevel.editor.document]
	set path $filepath_for($doc)
	if {$path eq {}} {
		file_save_as $toplevel
	} else {
		click_outside_handler  ;# removes e.g. the richtext tag-name entry popup
		write_file $path $toplevel
	}
}

proc file_save_as {toplevel {noremember {}}} {
	variable filepath_for

	set doc [fix_toplevel_child $toplevel.editor.document]
	set filepath $filepath_for($doc)
	set type [filetypes::type_from_widget $doc]
	set path [user_select_file $filepath save $type]
	if {$path eq {}} return
	write_file $path $toplevel $noremember
}

proc file_save_copy_as {toplevel} {
	file_save_as $toplevel -noremember
}

proc file_rename_and_save {toplevel} {
	variable filepath_for

	set doc [fix_toplevel_child $toplevel.editor.document]
	set filepath $filepath_for($doc)
	set type [filetypes::type_from_widget $doc]
	set path [user_select_file $filepath save $type]
	if {$path eq {}} return
	write_file $path $toplevel

	if {$path eq $filepath || $filepath eq {}} return
	file delete $filepath
}

proc file_close {toplevel} {
	set editor [fix_toplevel_child $toplevel.editor]
	undo::cleanup_document $editor
	destroy $editor
	pack [welcomescreen::create_gui .welcome] -fill both -expand true
	menus::keyboard_commands $toplevel none
	$toplevel configure -menu {}
	menubar_toggle_fix $toplevel  ;# work around the "stuck menubar" bug
	welcomescreen::set_window_title $toplevel
}

proc file_quit {toplevel} {
	exit 0
}

}
