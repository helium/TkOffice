# Copyright 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval menus {

icons::load  edit-undo,16  edit-redo,16

proc richtext_edit_menu_create {menupath} {
	ttk_menu $menupath
	$menupath delete 0 end
	$menupath add command -label [mc edit_undo] \
		-command [list menus::richtext_edit_undo $menupath] \
		-underline [mc edit_undo_underline] \
		-compound left -image edit-undo,16 \
		-accelerator [mc edit_undo_hotkey]
	$menupath add command -label [mc edit_redo] \
		-command [list menus::richtext_edit_redo $menupath] \
		-underline [mc edit_redo_underline] \
		-compound left -image edit-redo,16 \
		-accelerator [mc edit_redo_hotkey]
	# $menupath add command -label [
	$menupath configure -postcommand [list menus::richtext_edit_menu_update $menupath]

	return $menupath
}

proc richtext_edit_undo {menupath} {
	set tl [toplevel_for_menu $menupath]
	set editor [fix_toplevel_child $tl.editor]
	undo::undo $editor.document
}

proc richtext_edit_redo {menupath} {
	set tl [toplevel_for_menu $menupath]
	set editor [fix_toplevel_child $tl.editor]
	undo::redo $editor.document
}

proc richtext_edit_menu_update {menupath} {
	set tl [toplevel_for_menu $menupath]
	set editor [fix_toplevel_child $tl.editor]
	if {[undo::undo_is_possible $editor]} {
		set state normal
	} else {
		set state disabled
	}
	$menupath entryconfigure 0 -state $state
	if {[undo::redo_is_possible $editor]} {
		set state normal
	} else {
		set state disabled
	}
	$menupath entryconfigure 1 -state $state
}

}
