# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval menus {

proc setup_filepath_for {document} {
	variable filepath_for

	if {! [info exists filepath_for($document)]} {
		set filepath_for($document) {}
		bind $document <Destroy> [list menus::unset_filepath_for $document]
	}
}

proc unset_filepath_for {document} {
	variable filepath_for

	unset -nocomplain filepath_for($document)
}

proc setup_editor {toplevel type} {
	set editor [fix_toplevel_child $toplevel.editor]
	switch $type {
		richtext {set window_class Text}
		drawing {set window_class Canvas}
		default {error "bad type \"$type\": must be richtext or drawing"}
	}
	if {[winfo exists $editor] && [winfo class $editor.document] eq $window_class} {
		${type}::reset_widget $editor.document
		return
	}

	destroy $editor [fix_toplevel_child $toplevel.welcome]
	${type}::create_editor [ttk::frame $editor]
	setup_filepath_for $editor.document
	pack $editor -fill both -expand true

	set menubar [fix_toplevel_child $toplevel.menubar]
	menus::${type}_menubar $menubar
	$toplevel configure -menu $menubar
	menubar_toggle_fix $toplevel  ;# work around the "no menubar" bug
	menus::keyboard_commands $toplevel $type

	if {$type ne {richtext}} {
		destroy .richtext_properties
	}
}

proc set_window_title {document} {
	variable filepath_for

	set path $filepath_for($document)
	set tl [winfo toplevel $document]

	if {$path eq {}} {
		switch [filetypes::type_from_widget $document] {
			richtext {wm title $tl [mc window_title_new_richtext]}
			drawing {wm title $tl [mc window_title_new_drawing]}
		}
	} elseif {[string length $path] <= 64} {
		wm title $tl $path
	} else {
		wm title $tl [file tail $path]
	}
}

proc user_select_file {filepath mode {type {}}} {
	switch $mode {
		open {set cmd tk_getOpenFile}
		save {set cmd tk_getSaveFile}
		default {error "bad mode \"$mode\": must be open or save"}
	}
	set initialspec {}
	if {$filepath ne {}} {
		set initialspec [list -initialdir [file dirname $filepath] \
		                      -initialfile [file tail $filepath]]
	}
	set filetypes [list]
	lappend filetypes [list [mc filetype_tkoffice_all] {.tkt .tkg .tktext .tkdraw}]
	lappend filetypes [list [mc filetype_tktext] {.tkt .tktext}]
	lappend filetypes [list [mc filetype_tkdraw] {.tkg .tkdraw}]
	lappend filetypes [list [mc filetype_all] *]

	set defaultextension {}
	if {$mode eq {save}} {
		switch $type {
			richtext {set defaultextension [list -defaultextension .tkt]}
			drawing  {set defaultextension [list -defaultextension .tkg]}
		}
	}
	click_outside_handler  ;# removes e.g. the richtext tag-name entry popup
	return [$cmd {*}$initialspec -filetypes $filetypes {*}$defaultextension]
}

proc read_file {filepath toplevel} {
	variable filepath_for

	# May fail, then the standard Tk error dialog will show:
	set type [filetypes::type_from_file $filepath]
	switch $type {
		richtext - drawing {}
		default {error "could not determine magic-comment file type of \"$filepath\""}
	}
	setup_editor $toplevel $type
	set doc [fix_toplevel_child $toplevel.editor.document]
	${type}::load_widget_from_file $doc $filepath
	set filepath_for($doc) $filepath
	set_window_title $doc
}

proc write_file {filepath toplevel {noremember {}}} {
	variable filepath_for

	set doc [fix_toplevel_child $toplevel.editor.document]
	set type [filetypes::type_from_widget $doc]

	# May fail, then the standard Tk error dialog will show:
	set chan [open $filepath w]
	${type}::save_document_to_channel $doc $chan
	close $chan

	if {$noremember eq {-noremember}} return
	if {$noremember ne {}} {
		error "bad option \"$noremember\": must be -noremember or nothing"
	}
	set filepath_for($doc) $filepath
	set_window_title $doc
}

}
