The TkOffice package is licensed under the GNU General Public License,
version 3 or later.

As a further option, you may also apply the MIT or TCL licenses to some
individual files, namely those that deal with the TkOffice file formats
(Load, Save and prerequisites):
 * richtext/namespace.tcl
 * richtext/load.tcl
 * richtext/operations.tcl
 * richtext/save.tcl
 * drawing/load.tcl
 * drawing/scrollregion.tcl
 * drawing/save.tcl
 * common/font-normalization.tcl
 * common/file-types.tcl
See also the SPDX-License-Identifier comments in the individual files.

The icons used in these package have varying licenses. All files in
each subdirectory of icons/ have the same license, so the subdirectories
are listed here with the corresponding SPDX license identifiers:
 * gnome: GPL-2.0-only (source: Debian package "gnome-icon-theme", version 2.30.3-2)
 * Tango & Tango-renamed: CC-PDDC (source: Debian package "tango-icon-theme", version 0.8.90-5)
 * TclTk: TCL (source: Debian package "tk8.5-doc", version 8.5.17-1, extracted from anilabel.tcl)
 * TkOffice: GPL-3.0-or-later (source: own creation, based on Tango material which is public domain)
The GIF icons were created from PNG or SVG originals, with hand-edited
1-bit transparency. They are distributed under the same terms as their
PNG counterparts.
