# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval welcomescreen {

icons::load  tcl-powered-animated

proc about {toplevel} {
	if {[winfo exists $toplevel]} {
		raise $toplevel
		focus [focus -lastfor $toplevel]
		return
	}

	ttk_toplevel $toplevel
	focus $toplevel
	wm title $toplevel [mc about_window_title]
	wm resizable $toplevel no no

	tcl-powered-animated configure -format GIF
	label $toplevel.icon -image tcl-powered-animated

	set large_font [font configure TkCaptionFont]
	dict set large_font -size 20
	set small_font [font configure TkDefaultFont]
	dict set small_font -size [expr {[dict get $small_font -size] * 5 / 6}]

	ttk::label $toplevel.name -text [mc name_and_version] -font $large_font
	ttk::label $toplevel.description -text [mc description]
	ttk::label $toplevel.copyright -text [mc copyright] -font $small_font
	ttk::label $toplevel.license -text [mc license] -font $small_font -justify center
	ttk::button $toplevel.show_license -text [mc show_license]
	ttk::button $toplevel.dismiss -text [mc dismiss]

	grid $toplevel.icon -columnspan 2 -padx 16 -pady {12 8}
	grid $toplevel.name -columnspan 2 -padx 16 -pady {0 6}
	grid $toplevel.description -columnspan 2 -padx 16 -pady {0 8}
	grid $toplevel.copyright -columnspan 2 -padx 16 -pady {0 12}
	grid $toplevel.license -columnspan 2 -padx 16 -pady {0 12}
	grid $toplevel.show_license $toplevel.dismiss -padx 16 -pady {0 12}
	grid configure $toplevel.show_license -sticky w
	grid configure $toplevel.dismiss -sticky e

	$toplevel.show_license configure -command [list welcomescreen::show_license $toplevel]
	$toplevel.dismiss configure -command [list destroy $toplevel] -default active
	bind $toplevel.icon <Destroy> welcomescreen::animate_logo_stop
	animate_logo $toplevel.icon 150 1000
}

proc show_license {w} {
	help_window $w [mc license_window_title] [mc license_filepath]
}

proc animate_logo {label interval_ms pause_ms} {
	variable animation_callback

	set image [$label cget -image]
	# The easy way to animate a GIF!
	# Copied from the "Animated Labels" demo (anilabel.tcl, Tcl/Tk 8.5 distribution)
	set idx -1
	scan [$image cget -format] {GIF -index %d} idx
	if {[catch {
		# Note that we get an error if the index is out of range
		$image configure -format "GIF -index [incr idx]"
		set next_interval $interval_ms
	}]} then {
		$image configure -format "GIF -index 0"
		set next_interval $pause_ms
	}

	set animation_callback [after $next_interval welcomescreen::animate_logo $label $interval_ms $pause_ms]
}

proc animate_logo_stop {} {
	variable animation_callback
	if {[info exists animation_callback]} {
		after cancel $animation_callback
		unset animation_callback
	}
}

}
