# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval welcomescreen {

icons::load  document-open,48  document-new-richtext,48  document-new-drawing,48  help,16

proc create_gui {frame} {
	frame $frame -bg #95b77d  ;# olive (beige: #c7b586)

	label $frame.version -text [mc name_and_version]
	$frame.version configure -bg #95b77d -fg #a7cd8b -font {sans-serif -48 bold}
	grid $frame.version -sticky n -padx 12 -pady {30 10} -columnspan 5

	ttk::button $frame.open -text [mc open_document] -image document-open,48 -compound top \
		-default disabled -style welcomescreen.TButton
	ttk::button $frame.newtext -text [mc new_richtext] -image document-new-richtext,48 -compound top \
		-default disabled -style welcomescreen.TButton
	ttk::button $frame.newdrawing -text [mc new_drawing] -image document-new-drawing,48 -compound top \
		-default disabled -style welcomescreen.TButton
	grid x $frame.open $frame.newtext $frame.newdrawing x
	grid configure $frame.newtext -padx 6

	ttk::button $frame.info -image help,16 \
		-default disabled -style welcomescreen.TButton
	grid x x x $frame.info - -sticky se -padx {0 12} -pady {0 12}

	grid rowconfigure $frame {0 2} -weight 1 -uniform A
	grid columnconfigure $frame {0 4} -minsize 12 -weight 1 -uniform a

	set bg [$frame cget -background]
	foreach button {open newtext newdrawing info} {
		ttk_fixes::clam_corners $frame.$button $bg
	}

	bind_gui $frame
	return $frame
}

proc bind_gui {frame} {
	set tl [winfo toplevel $frame]
	$frame.open configure -command [list menus::file_open $tl]
	$frame.newtext configure -command [list menus::file_new_richtext $tl]
	$frame.newdrawing configure -command [list menus::file_new_drawing $tl]
	$frame.info configure -command [list welcomescreen::about .about]
}

proc set_window_title {toplevel} {
	wm title $toplevel [mc name]
}

}
