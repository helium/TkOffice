# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

namespace eval font {

proc simplify {fontdict} {
	set font [list]
	lappend font [dict get $fontdict -family]
	lappend font [dict get $fontdict -size]
	set weight [dict get $fontdict -weight]
	if {$weight ne {normal}} {
		lappend font $weight
	}
	set slant [dict get $fontdict -slant]
	if {$slant ne {roman}} {
		lappend font $slant
	}
	if {[dict get $fontdict -underline]} {
		lappend font underline
	}
	if {[dict get $fontdict -overstrike]} {
		lappend font overstrike
	}
	return $font
}

proc to_dict {font} {
	if {![string is list $font]} {
		error "fontspec \"$font\" is not a valid Tcl list"
	}
	if {[llength $font] < 2} {
		error "fontspec \"$font\" must contain at least a family and a size"
	}
	set fontdict [dict create \
		-family [lindex $font 0] -size [lindex $font 1] \
		-weight normal -slant roman -underline 0 -overstrike 0]

	foreach attr [lrange $font 2 end] {
		switch $attr {
			normal - bold {dict set fontdict -weight $attr}
			roman - italic {dict set fontdict -slant $attr}
			underline {dict set fontdict -underline 1}
			overstrike {dict set fontdict -overstrike 1}
			default {error "unknown font style \"$attr\""}
		}
	}
	return $fontdict
}

proc dict_size_to_px {fontdict} {
	set size [dict get $fontdict -size]
	if {$size > 0} {
		# Positive size numbers are values in pt, scale to px
		set size [expr {-round($size * [tk scaling])}]
		dict set fontdict -size $size
	}
	return $fontdict
}

}
