# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc screen_distance_parse {tk_pixel_spec} {
	set unit_char [string index $tk_pixel_spec end]
	if {$unit_char in {c m i p}} {
		set distance [string range $tk_pixel_spec 0 end-1]
		switch $unit_char {
			c {set unit cm}
			m {set unit mm}
			i {set unit in}
			p {set unit pt}
		}
	} else {
		set distance $tk_pixel_spec
		set unit px
	}
	if {![string is double $distance] || $distance eq {}} {
		error "invalid Tk_GetPixels value \"$tk_pixel_spec\""
	}
	return [list $distance $unit]
}

proc screen_distance_compose {distance unit} {
	if {![string is double $distance] || $distance eq {}} {
		error "distance \"$distance\" is not a number"
	}
	switch $unit {
		px {set unit_char {}}
		cm {set unit_char c}
		mm {set unit_char m}
		in {set unit_char i}
		pt {set unit_char p}
		default {error "unknown measurement unit \"$unit\""}
	}
	return $distance$unit_char
}

proc screen_distance_to_pixels {distance unit} {
	switch $unit {
		px {return $distance}
		cm {return [expr {$distance / 2.54 * 72.0 * [tk scaling]}]}
		mm {return [expr {$distance / 25.4 * 72.0 * [tk scaling]}]}
		in {return [expr {$distance * 72.0 * [tk scaling]}]}
		pt {return [expr {$distance * [tk scaling]}]}
		default {error "unknown measurement unit \"$unit\""}
	}
}

proc screen_distance_unit_convert {input input_unit} {
	if {$input_unit ni {px pt in mm cm}} {
		error "invalid input unit \"$input_unit\": must be one of px, pt, in, mm, cm"
	}
	set $input_unit $input
	if {$input_unit eq {px}} {set pt [expr {$px / [tk scaling]}]}
	if {$input_unit eq {cm}} {set in [expr {$cm / 2.54}]; set mm [expr {$cm * 10.0}]}
	if {$input_unit eq {mm}} {set in [expr {$mm / 25.4}]; set cm [expr {$mm * 0.1}]}
	if {$input_unit in {in cm mm}} {set pt [expr {$in * 72.0}]}
	if {$input_unit in {px pt}} {set in [expr {$pt / 72.0}]}
	if {$input_unit ne {px}} {set px [expr {$pt * [tk scaling]}]}
	if {$input_unit ni {cm mm}} {
		set cm [expr {$in * 2.54}]
		set mm [expr {$in * 25.4}]
	}
	set out [dict create]
	foreach unit {px pt in mm cm} {
		if {$unit eq $input_unit} continue
		set number [screen_distance_round_number [get $unit]]
		dict set out $unit [screen_distance_normalize_integer $number $unit]
	}
	return $out
}

proc screen_distance_round_number {number} {
	if {$number >= 0.0} {
		set sign [expr 1.0]
	} else {
		set number [expr {-$number}]
		set sign [expr -1.0]
	}

	if {$number > 30.0} {
		set delta 1.0
	} elseif {$number > 3.0} {
		set delta 0.1
	} elseif {$number > 0.3} {
		set delta 0.01
	} elseif {$number > 0.03} {
		set delta 0.001
	} elseif {$number > 0.003} {
		set delta 0.0001
	} else {
		# general case would be something similar to: set delta [expr {10**floor(log10($number)-2.0)}]
		# do not round without string manipulation, would lead to 0.03400000000001 artefacts
		return $number
	}

	set result [expr {$sign * round($number/$delta) * $delta}]
	if {$delta eq {1.0}} {
		# 23.566250000000003 -> 24
		return [format %.0f $result]
	} else {
		set postcomma_length [expr {[string length $delta] - 2}]
		return [format %.${postcomma_length}f $result]
	}
}

proc screen_distance_cleanup {number unit} {
	# Cleans up calculation results such that e.g.
	# 1280.1 + 0.1 = 1280.1999999999998 becomes 1280.2 again.
	set number [format %.10f $number]
	while {[string index $number end] eq "0"} {
		set number [string range $number 0 end-1]
	}
	return [screen_distance_normalize_integer $number $unit]
}

proc screen_distance_normalize_integer {number unit} {
	if {[string match {*.} $number]} {
		set number ${number}0
	} elseif {[string match {.*} $number]} {
		set number 0$number
	}
	if {$unit in {px pt mm}} {
		if {[string match {*.0} $number]} {
			set number [string range $number 0 end-2]
		}
	} else {
		if {![string match {*.*} $number]} {
			set number $number.0
		}
	}
	return $number
}
