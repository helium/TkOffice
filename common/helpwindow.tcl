# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc help_window {w title filepath} {
	if {[winfo exists $w.help]} {
		# TODO: test on window managers other than xfwm4
		raise $w.help
		focus [focus -lastfor $w.help]
		return
	}

	set helpwindow [ttk_toplevel $w.help]
	make_utility_window $helpwindow [wm group $w]
	wm title $helpwindow $title
	wm minsize $helpwindow 400 200
	wm geometry $helpwindow 600x400

	pack [ttk_text $helpwindow.helptext -width 0 -height 0 -wrap word] \
		-fill both -expand true -padx 5 -pady 6
	ttk_text_scrollbar $helpwindow.helptext on

	pack [ttk::button $helpwindow.dismiss -text [mc help_window_dismiss]] \
		-side right -padx 5 -pady {0 6}
	$helpwindow.dismiss configure -command [list destroy $helpwindow] -default active

	richtext::load_widget_from_file $helpwindow.helptext $filepath
	$helpwindow.helptext configure -state disabled
}
