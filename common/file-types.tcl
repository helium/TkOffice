# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

namespace eval filetypes {

variable identifiers
dict set identifiers {# TkOffice text document} richtext
dict set identifiers {# TkOffice vector drawing document} drawing

proc type_from_file {filepath} {
	variable identifiers

	set chan [open $filepath r]  ;# may raise an error
	gets $chan line
	close $chan

	if {[dict exists $identifiers $line]} {
		return [dict get $identifiers $line]
	} else {
		return {}
	}
}

proc type_from_widget {document} {
	switch [winfo class $document] {
		Text {return richtext}
		Canvas {return drawing}
		default {error "unsupported document widget of class [winfo class $doc], expected Text or Canvas"}
	}
}

}
