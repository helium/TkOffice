# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

# The procedures in this file update the properties GUI
# whenever the selection changes.

proc update_properties {canvas} {
	variable properties_frame_for

	set selection [get_selected $canvas]
	set types [object_types $canvas $selection]

	set prop_frame $properties_frame_for($canvas)
	update_generic_properties_gui $canvas $prop_frame $selection $types
	update_arc_properties_gui $canvas $prop_frame $selection $types
	update_polygonlike_properties_gui $canvas $prop_frame $selection $types
	update_text_properties_gui $canvas $prop_frame $selection $types

	toolbar_raiselower_buttons $canvas $selection
}

# Subprocedures for updating, canvas-dependent

proc get_selected {canvas} {
	$canvas find withtag {(selection && !mark) || item_being_created}
}

proc object_types {canvas items} {
	set ret [dict create]
	foreach item $items {
		dict incr ret [$canvas type $item]
	}
	return [dict keys $ret]
}

proc filter_by_type {canvas items allowed_types} {
	set ret [list]
	foreach item $items {
		if {[$canvas type $item] in $allowed_types} {
			lappend ret $item
		}
	}
	return $ret
}

proc get_string_property {canvas items propname} {
	set ret _NONE_  ;# never used in any relevant item property
	foreach item $items {
		catch {
			set prop [$canvas itemcget $item $propname]
			set ret [combine_string_property $ret $prop]
		}
	}
	return $ret
}

proc combine_string_property {accum prop} {
	if {$accum eq {_NONE_}} {
		return $prop
	} elseif {$prop ne $accum} {
		return _MULTIPLE_
	} else {
		return $accum
	}
}

proc combine_bool_property {accum prop} {
	combine_string_property $accum [expr {bool($prop)}]
}

# Special case since we want the -fill property of line objects to appear as outline
proc get_outline {canvas items} {
	set ret _NONE_
	foreach item $items {
		if {[$canvas type $item] eq {line}} {
			set prop [$canvas itemcget $item -fill]
		} else {
			if {[catch {
				set prop [$canvas itemcget $item -outline]
			}]} continue
		}
		set ret [combine_string_property $ret $prop]
	}
	return $ret
}

proc get_enum_property {canvas items propname} {
	set ret [dict create]
	foreach item $items {
		catch {dict set ret [$canvas itemcget $item $propname] 1}
	}
	return [dict keys $ret]
}

proc get_number_property {canvas items propname} {
	set min Inf
	set max -Inf
	foreach item $items {
		catch {
			set prop [$canvas itemcget $item $propname]
			set min [expr {min($min, $prop)}]
			set max [expr {max($max, $prop)}]
		}
	}
	return [finalize_number_property $min $max]
}

proc finalize_number_property {min max} {
	if {$min eq "Inf" && $max eq "-Inf"} {
		# No objects found supporting the property
		return {}
	} elseif {$min == $max} {
		# All objects have the same property value
		return $max
	} else {
		return [list $min $max]
	}
}

proc get_text_properties {canvas items} {
	set family _NONE_
	set size_min Inf
	set size_max -Inf
	set weight _NONE_
	set slant _NONE_
	set underline _NONE_
	set overstrike _NONE_

	foreach item $items {
		catch {
			set font [$canvas itemcget $item -font]
			set size_px [expr {-[dict get $font -size]}]

			set family [combine_string_property $family [dict get $font -family]]
			set size_min [expr {min($size_min, $size_px)}]
			set size_max [expr {max($size_max, $size_px)}]
			set weight [combine_string_property $weight [dict get $font -weight]]
			set slant [combine_string_property $slant [dict get $font -slant]]
			set underline [combine_bool_property $underline [dict get $font -underline]]
			set overstrike [combine_bool_property $overstrike [dict get $font -overstrike]]
		}
	}

	dict create -family $family \
		-size [finalize_number_property $size_min $size_max] \
		-weight $weight -slant $slant -underline $underline \
		-overstrike $overstrike
}

# Subprocedures manipulating only the widgets

proc update_color_label {label string} {
	if {$string in {_NONE_ _MULTIPLE_}} {
		$label configure -background {}
	} else {
		$label configure -background $string
	}
}

proc update_color_entry {entry string} {
	switch $string {
		_NONE_     {entry_set $entry {}}
		_MULTIPLE_ {entry_set $entry [mc multiple]}
		default    {entry_set $entry $string}
	}
	$entry selection clear
}

proc update_combobox {combobox string active_state} {
	if {$string eq {_NONE_}} {
		$combobox set {}
		$combobox configure -state disabled
	} elseif {$string eq {_MULTIPLE_}} {
		$combobox set [mc multiple]
		$combobox configure -state $active_state
	} else {
		$combobox set $string
		$combobox configure -state $active_state
	}
	$combobox selection clear
}

proc update_radiobutton {button bool} {
	if {$bool} {
		$button state pressed
	} else {
		$button state !pressed
	}
}

proc update_spinbox {spinbox numberlist} {
	switch [llength $numberlist] {
		0 {
			$spinbox set {}
			$spinbox configure -values {}
		} 1 {
			$spinbox set $numberlist
			$spinbox configure -values {}
		} 2 {
			set mc_multiple [mc multiple]
			$spinbox set $mc_multiple
			set values [linsert $numberlist 1 $mc_multiple]
			$spinbox configure -values $values
		}
	}
	$spinbox selection clear
}

proc update_checkbutton {checkbutton value offValue onValue} {
	global $checkbutton
	if {$value eq $onValue} {
		set $checkbutton 1
	} elseif {$value eq $offValue} {
		set $checkbutton 0
	} else {
		set $checkbutton 0  ;# If it is 1 at the time of unsetting, the
		unset $checkbutton  ;# "alternate" state is masked by "selected".
	}
}

proc enable_labelframe_cond {window bool} {
	if {$bool} {
		$window state !disabled
	} else {
		$window state disabled
	}
}

proc enable_widgets_cond {bool args} {
	if {$bool} {
		foreach widget $args {
			$widget configure -state normal
		}
	} else {
		foreach widget $args {
			$widget configure -state disabled
		}
	}
}

}
