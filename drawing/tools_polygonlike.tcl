# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

variable polygonlike_being_created

# Line tool

proc left_button_press_line {canvas x y} {
	polygonlike_click $canvas $x $y line
}

proc motion_line {canvas x y} {
	polygonlike_move_last_point $canvas $x $y
}

proc backspace_line {canvas x y} {
	polygonlike_delete_last_point $canvas
	polygonlike_move_last_point $canvas $x $y
}

proc right_button_press_line {canvas x y} {
	polygonlike_finish $canvas
}

proc escape_line {canvas} {
	polygonlike_abort $canvas
}

proc tool_leave_line {canvas} {
	polygonlike_abort $canvas
}

# Polygon tool

proc left_button_press_polygon {canvas x y} {
	polygonlike_click $canvas $x $y polygon -width 1 -outline black
}

proc motion_polygon {canvas x y} {
	polygonlike_move_last_point $canvas $x $y
}

proc backspace_polygon {canvas x y} {
	polygonlike_delete_last_point $canvas
	polygonlike_move_last_point $canvas $x $y
}

proc right_button_press_polygon {canvas x y} {
	polygonlike_finish $canvas
}

proc escape_polygon {canvas} {
	polygonlike_abort $canvas
}

proc tool_leave_polygon {canvas} {
	polygonlike_abort $canvas
}

# Generic procedures for Line and Polygon tool

proc polygonlike_is_being_created {canvas} {
	variable polygonlike_being_created
	info exists polygonlike_being_created($canvas)
}

proc polygonlike_create {canvas x y type args} {
	variable polygonlike_being_created

	if {[polygonlike_is_being_created $canvas]} return

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	$canvas create $type $x $y $x $y {*}$args -tags item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	set polygonlike_being_created($canvas) yes
}

proc polygonlike_delete_last_point {canvas} {
	if {! [polygonlike_is_being_created $canvas]} return

	set num_coords [$canvas index item_being_created end]
	if {$num_coords < 4} {
		polygonlike_abort $canvas
	}
	$canvas dchars item_being_created [expr {$num_coords - 2}] [expr {$num_coords - 1}]
}

proc polygonlike_add_point_as_last {canvas x y} {
	if {! [polygonlike_is_being_created $canvas]} return

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	$canvas insert item_being_created end [list $x $y]

	# Work around Tk bug #5fb8145997
	# (Canvas redraw region too small when inserting points to a smoothed line)
	if {[$canvas type item_being_created] eq {line} &&
	    [$canvas itemcget item_being_created -smooth] ne 0} {
		$canvas move item_being_created 0 0  ;# force redraw of entire item
	}
}

proc polygonlike_move_last_point {canvas x y} {
	if {! [polygonlike_is_being_created $canvas]} return
	focus $canvas
	polygonlike_delete_last_point $canvas
	polygonlike_add_point_as_last $canvas $x $y
}

proc polygonlike_finish {canvas} {
	variable polygonlike_being_created

	if {! [polygonlike_is_being_created $canvas]} return
	focus $canvas

	set type [$canvas type item_being_created]
	switch $type {
		line {set min 4}
		polygon {set min 6}
		default {error "unexpected object type '$type': should be line or polygon"}
	}
	if {[$canvas index item_being_created end] < $min} {
		# too few points in coordinate list
		polygonlike_abort $canvas
		return
	}

	$canvas dtag item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	unset polygonlike_being_created($canvas)
	update_scrollregion $canvas
}

proc polygonlike_abort {canvas} {
	variable polygonlike_being_created

	$canvas delete item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	unset -nocomplain polygonlike_being_created($canvas)
}

proc polygonlike_click {canvas x y type args} {
	focus $canvas
	if {[polygonlike_is_being_created $canvas]} {
		polygonlike_add_point_as_last $canvas $x $y
	} else {
		polygonlike_create $canvas $x $y $type {*}$args
	}
}

}
