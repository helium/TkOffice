# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

namespace eval drawing {

proc save_document_to_channel {w chan} {
	# TkOffice documents are encoded as UTF-8 with Unix (LF) line endings
	# on all platforms.
	fconfigure $chan -encoding utf-8 -translation lf

	puts $chan "# TkOffice vector drawing document"
	puts $chan ""
	save_widget_to_channel $w $chan
}

proc save_widget_to_channel {w chan {indent {}}} {
	# 0. Newline if indented
	if {$indent ne {}} {
		puts $chan ""
	}

	# 1. Background
	puts -nonewline $chan $indent
	puts $chan [list background [$w cget -background]]
	puts $chan ""  ;# 2 newlines to separate background from objects

	# 2. Individual objects
	foreach item [$w find withtag {!mark && !anchormark && !item_being_created}] {
		set cmd [list]
		lappend cmd [$w type $item]
		lappend cmd {*}[strip_coords [$w coords $item]]

		set config [dict create]
		foreach prop [$w itemconfigure $item] {
			set key [lindex $prop 0]
			set value [lindex $prop 4]
			set default [lindex $prop 3]
			if {$value in {0.0 0} && $default in {0.0 0}} continue
			if {$value ne $default || $key eq {-anchor} || $value eq {black}} {
				if {[string is double $value] && [string match *.0 $value]} {
					set value [string map {.0 {}} $value]
				}
				dict set config $key $value
			}
		}
		# Remove "selection" and "current" tags
		if {[dict exists $config -tags]} {
			set tags [dict get $config -tags]
			set pos [lsearch -exact $tags selection]
			if {$pos >= 0} {
				set tags [lreplace $tags $pos $pos]
			}
			set pos [lsearch -exact $tags current]
			if {$pos >= 0} {
				set tags [lreplace $tags $pos $pos]
			}
			if {$tags eq {}} {
				dict unset config -tags
			} else {
				dict set config -tags $tags
			}
		}

		if {[dict exists $config -font]} {
			dict set config -font [::font::simplify [dict get $config -font]]
		}

		# Make "-text" come first in attributes list
		if {! [dict exists $config -text]} {
			lappend cmd {*}$config
		} else {
			set text [dict get $config -text]
			dict unset config -text
			lappend cmd -text $text
			lappend cmd {*}$config
		}

		puts -nonewline $chan $indent
		puts $chan $cmd
	}
}

proc strip_coords {coords} {
	# Use integer representation for numbers like "432.0"
	set out [list]
	set map [list .0 {}]
	foreach coord $coords {
		if {[string match *.0 $coord]} {
			set coord [string map $map $coord]
		}
		lappend out $coord
	}
	return $out
}

}
