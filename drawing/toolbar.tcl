# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

proc toolbar_create {path -for canvas} {
	variable toolbar_for
	variable canvas_for_toolbar
	if {${-for} ne {-for}} {
		error "wrong second argument: must be toolbar_create path -for canvas"
	}

	ttk::frame $path
	set toolbar_for($canvas) $path
	set canvas_for_toolbar($path) $canvas

	grid [ttk::frame $path.tools] -sticky nwe
	# Item types not implemented: bitmap, image, window
	foreach tool {select points rectangle oval arc line polygon text} {
		set text [mc tool_$tool]
		grid [ttk::button $path.tools.$tool -text $text -style Toolbutton] -sticky nesw
		$path.tools.$tool configure -command [list drawing::tool_changed $canvas $tool]
	}
	grid columnconfigure $path.tools 0 -weight 1

	grid [ttk::separator $path.separator1 -orient horizontal] -sticky we -padx 2 -pady 2

	grid [ttk::frame $path.operations] -sticky nwe
	foreach {op command} {
		duplicate drawing::points_duplicate_selected
		midpoint  drawing::points_create_midpoints
	} {
		set text [mc toolbar_$op]
		grid [ttk::button $path.operations.$op -text $text -style Toolbutton -state disabled] -sticky nesw
		$path.operations.$op configure -command [list $command $canvas]
	}
	grid columnconfigure $path.operations 0 -weight 1

	grid [ttk::separator $path.separator2 -orient horizontal] -sticky we -padx 2 -pady 2

	grid [ttk::frame $path.raiselower] -sticky nwe
	foreach {op text command} {
		top    Top    drawing::raise_selection_to_top
		raise  Raise  drawing::raise_selection
		lower  Lower  drawing::lower_selection
		lowest Lowest drawing::lower_selection_to_lowest
	} {
		set text [mc toolbar_$op]
		grid [ttk::button $path.raiselower.$op -text $text -style Toolbutton -state disabled] -sticky nesw
		$path.raiselower.$op configure -command [list $command $canvas]
	}
	grid columnconfigure $path.raiselower 0 -weight 1

	bind $path <Destroy> [list drawing::toolbar_destroy $path $canvas]

	return $path
}

proc toolbar_destroy {path canvas} {
	variable toolbar_for
	variable canvas_for_toolbar

	unset toolbar_for($canvas)
	unset canvas_for_toolbar($path)
}

proc toolbar_tool_select {canvas new_tool} {
	variable toolbar_for
	set toolbar $toolbar_for($canvas)

	foreach toolbutton [winfo children $toolbar.tools] {
		if {[winfo name $toolbutton] eq $new_tool} {
			$toolbutton state selected
		} else {
			$toolbutton state !selected
		}
	}
}

proc toolbar_points_operation_buttons {canvas state} {
	variable toolbar_for
	set toolbar $toolbar_for($canvas)

	foreach button [winfo children $toolbar.operations] {
		$button configure -state $state
	}
}

proc toolbar_raiselower_buttons {canvas selection} {
	variable toolbar_for
	set toolbar $toolbar_for($canvas)

	if {$selection eq {}} {
		set state disabled
	} else {
		set state normal
	}
	foreach button [winfo children $toolbar.raiselower] {
		$button configure -state $state
	}
}

}
