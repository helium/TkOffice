# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

variable x_origin
variable y_origin

# Generic procedures for Rectangle, Oval, and Arc tools

proc rectanglelike_create {canvas x y type} {
	variable x_origin
	variable y_origin

	focus $canvas
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	$canvas create $type $x $y $x $y -tags item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	set x_origin($canvas) $x
	set y_origin($canvas) $y
}

proc rectanglelike_reshape {canvas x y} {
	variable x_origin
	variable y_origin

	# User has cancelled drawing (e.g. by Escape key)?
	if {! [info exists x_origin($canvas)]} return

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	$canvas coords item_being_created $x_origin($canvas) $y_origin($canvas) $x $y
}

proc rectanglelike_finish {canvas} {
	variable x_origin
	variable y_origin

	$canvas dtag item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	unset -nocomplain x_origin($canvas)
	unset -nocomplain y_origin($canvas)
	update_scrollregion $canvas
}

proc rectanglelike_cancel {canvas} {
	variable x_origin
	variable y_origin

	$canvas delete item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	unset -nocomplain x_origin($canvas)
	unset -nocomplain y_origin($canvas)
}

# Rectangle tool

proc left_button_press_rectangle {canvas x y} {
	rectanglelike_create $canvas $x $y rectangle
}

proc left_button_motion_rectangle {canvas x y} {
	rectanglelike_reshape $canvas $x $y
}

proc left_button_release_rectangle {canvas x y} {
	rectanglelike_finish $canvas
}

proc right_button_press_rectangle {canvas x y} {
	rectanglelike_cancel $canvas
}

proc escape_rectangle {canvas} {
	rectanglelike_cancel $canvas
}

# Oval tool

proc left_button_press_oval {canvas x y} {
	rectanglelike_create $canvas $x $y oval
}

proc left_button_motion_oval {canvas x y} {
	rectanglelike_reshape $canvas $x $y
}

proc left_button_release_oval {canvas x y} {
	rectanglelike_finish $canvas
}

proc right_button_press_oval {canvas x y} {
	rectanglelike_cancel $canvas
}

proc escape_oval {canvas} {
	rectanglelike_cancel $canvas
}

# Arc tool

proc left_button_press_arc {canvas x y} {
	rectanglelike_create $canvas $x $y arc
	$canvas itemconfigure item_being_created -extent 270.001
}

proc left_button_motion_arc {canvas x y} {
	rectanglelike_reshape $canvas $x $y
}

proc left_button_release_arc {canvas x y} {
	rectanglelike_finish $canvas
}

proc right_button_press_arc {canvas x y} {
	rectanglelike_cancel $canvas
}

proc escape_arc {canvas} {
	rectanglelike_cancel $canvas
}

}
