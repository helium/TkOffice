# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

# Configuration
variable points_dot_size 4
variable points_color1 black
variable points_color2 #777

variable prev_coords
variable single_point_moved
variable x_move_last
variable y_move_last

# Points tool

proc left_button_press_points {canvas x y} {
	variable single_point_moved

	focus $canvas
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	set potential_mark [points_mark_hit_at $canvas $x $y]
	set tags [$canvas gettags $potential_mark]
	if {$potential_mark ne {} && "mark" in $tags} {
		if {"selection" in $tags} {
			# point already selected
			move_start $canvas $x $y
			points_move_init $canvas
		} else {
			set single_point_moved($canvas) no
			set mark [lsearch -glob -inline $tags mark.*.*]
			points_deactivate_all $canvas
			points_activate_point $canvas $mark
			move_start $canvas $x $y
			points_move_init $canvas
		}
	} else {
		points_unmark_all $canvas
		points_mark_item $canvas [$canvas find withtag current]
	}
}

proc left_button_press_shift_points {canvas x y} {
	focus $canvas
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	set potential_mark [points_mark_hit_at $canvas $x $y]
	set tags [$canvas gettags $potential_mark]
	if {$potential_mark ne {} && "mark" in $tags} {
		set mark [lsearch -glob -inline $tags mark.*.*]
		if {"selection" ni $tags} {
			points_activate_point $canvas $mark
		} else {
			points_deactivate_point $canvas $mark
		}
	} else {
		set item [$canvas find withtag current]
		if {"selection" ni [$canvas gettags $item]} {
			points_mark_item $canvas $item
		} else {
			points_unmark_item $canvas $item
		}
	}
}

proc left_button_motion_points {canvas x y} {
	variable x_move_last
	variable y_move_last
	variable single_point_moved

	if {! [is_moving $canvas]} return

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	set dx [expr {$x - $x_move_last($canvas)}]
	set dy [expr {$y - $y_move_last($canvas)}]
	points_move_selected_coords $canvas $dx $dy
	set x_move_last($canvas) $x
	set y_move_last($canvas) $y

	if {[info exists single_point_moved($canvas)]} {
		set single_point_moved($canvas) yes
	}
}

proc left_button_release_points {canvas x y} {
	variable single_point_moved

	move_end $canvas
	points_move_deinit $canvas
	if {[info exists single_point_moved($canvas)]} {
		if {$single_point_moved($canvas)} {
			points_deactivate_all $canvas
		}
		unset single_point_moved($canvas)
	}
}

proc backspace_points {canvas} {
	points_delete_selected $canvas
}

proc tool_leave_points {canvas} {
	points_unmark_all $canvas
}

# Prerequisites for Points tool

proc points_mark_item {canvas item} {
	variable points_dot_size
	variable points_color1
	variable points_color2

	if {$item eq {}} return
	set tags [$canvas gettags $item]
	if {"mark" in $tags} return
	if {"selection" in $tags} return

	$canvas addtag selection withtag $item
	event generate $canvas <<SelectionUpdate>> -when head

	set s $points_dot_size
	set i 0
	foreach {x y} [$canvas coords $item] {
		set point_coords [list [expr {$x-$s}] [expr {$y-$s}] [expr {$x+$s}] [expr {$y+$s}]]
		$canvas create rectangle $point_coords -tags [list mark mark.$item mark.$item.$i] \
			-outline $points_color1 -outlinestipple gray50 -outlineoffset 0,0
		$canvas create rectangle $point_coords -tags [list mark mark.$item mark.$item.$i] \
			-outline $points_color2 -outlinestipple gray50 -outlineoffset 0,1
		incr i
	}
}

proc points_unmark_item {canvas item} {
	if {$item eq {}} return
	$canvas dtag $item selection
	$canvas delete withtag mark.$item
	event generate $canvas <<SelectionUpdate>> -when head
}

proc points_unmark_all {canvas} {
	$canvas dtag selection
	$canvas delete withtag mark
	event generate $canvas <<SelectionUpdate>> -when head
}

proc points_item_is_marked {canvas item} {
	if {$item eq {}} {return 0}
	expr {"selection" in [$canvas gettags $item]}
}

proc points_activate_point {canvas mark} {
	# "mark" argument must be of form mark.$item.$point e.g. mark.23.5
	$canvas addtag selection withtag $mark
	$canvas itemconfigure $mark -width 2
}

proc points_deactivate_point {canvas mark} {
	# "mark" argument must be of form mark.$item.$point e.g. mark.23.5
	$canvas dtag $mark selection
	$canvas itemconfigure $mark -width 1
}

proc points_deactivate_all {canvas} {
	$canvas dtag mark selection
	$canvas itemconfigure mark -width 1
}

proc points_are_activated {canvas} {
	expr {[$canvas find withtag {selection && mark}] ne {}}
}

proc points_move_init {canvas} {
	variable prev_coords

	foreach item [$canvas find withtag {selection && !mark}] {
		# This storage is required for "rectangle-like" objects (rectangle, oval, arc)
		# where the coordinate list may be reordered by the Tk canvas, namely if
		# the first point has larger x or y coordinate than the second point.
		dict set prev_coords($canvas) $item [$canvas coords $item]
	}
}

proc points_move_deinit {canvas} {
	variable prev_coords
	if {! [info exists prev_coords($canvas)]} return

	foreach item [$canvas find withtag {selection && !mark}] {
		if {[$canvas type $item] in {rectangle oval arc}} {
			# detect reordered coordinate list
			if {[$canvas coords $item] != [dict get $prev_coords($canvas) $item]} {
				# redraw marks, unselecting them as a side effect
				# (but if both of them were selected, we don't get here)
				points_unmark_item $canvas $item
				points_mark_item $canvas $item
			}
		}
	}
	unset -nocomplain prev_coords($canvas)
}

proc points_move_selected_coords {canvas dx dy} {
	variable prev_coords

	foreach item [$canvas find withtag {selection && !mark}] {
		set coords [dict get $prev_coords($canvas) $item]
		set prev_tag {}
		foreach mark [$canvas find withtag "selection && mark.$item"] {
			$canvas move $mark $dx $dy

			set tag [lsearch -glob -inline [$canvas gettags $mark] mark.*.*]
			# There are two rectangles for each point, don't move the point twice
			if {$tag eq $prev_tag} continue
			set prev_tag $tag

			set idx [lindex [split $tag .] end]  ;# e.g. 5 for mark.23.5
			set idx [expr {$idx*2}]
			lset coords $idx [expr {[lindex $coords $idx] + $dx}]
			incr idx
			lset coords $idx [expr {[lindex $coords $idx] + $dy}]
		}
		$canvas coords $item $coords  ;# apply changed coords
		dict set prev_coords($canvas) $item $coords
	}
}

proc points_selected_indices_of {canvas item} {
	set selected_indices [list]
	foreach mark [$canvas find withtag "selection && mark.$item"] {
		set tag [lsearch -glob -inline [$canvas gettags $mark] mark.*.*]
		# There are two "mark" rectangles for each point, keep both (cf. lsort -unique)
		lappend selected_indices [lindex [split $tag .] end]  ;# e.g. 5 for mark.23.5
	}
	return [lsort -integer -decreasing -unique $selected_indices]
}

proc points_delete_selected {canvas} {
	foreach item [$canvas find withtag {selection && !mark}] {
		set selected_indices [points_selected_indices_of $canvas $item]
		if {[llength [$canvas coords $item]] == 2*[llength $selected_indices]} {
			points_unmark_item $canvas $item
			$canvas delete $item
		} elseif {[points_ok_to_delete $canvas $item $selected_indices]} {
			points_unmark_item $canvas $item  ;# many vertex indices will differ after deleting some!
			foreach index $selected_indices {
				$canvas dchars $item [expr {2*$index}]
			}
			points_mark_item $canvas $item
		}
	}
}

proc points_ok_to_delete {canvas item selected_indices} {
	set type [$canvas type $item]
	set min_vertices [switch $type {
		rectangle - oval - arc {expr 2}
		line {expr 2}
		polygon {expr 3}
		text {expr 1}
	}]
	set vertices_present [expr {[llength [$canvas coords $item]] >> 1}]  ;# / 2
	set vertices_to_keep [expr {$vertices_present - [llength $selected_indices]}]
	expr {$vertices_to_keep >= $min_vertices}
}

proc points_duplicate_selected {canvas} {
	foreach item [$canvas find withtag {selection && !mark}] {
		if {[$canvas type $item] ni {line polygon}} continue
		set selected_indices [points_selected_indices_of $canvas $item]

		points_unmark_item $canvas $item
		set coords [$canvas coords $item]
		foreach index $selected_indices {
			set index2 [expr {2*$index}]
			$canvas insert $item $index2 [lrange $coords $index2 [expr {$index2 + 1}]]
		}
		points_mark_item $canvas $item
	}
}

proc points_create_midpoints {canvas} {
	foreach item [$canvas find withtag {selection && !mark}] {
		if {[$canvas type $item] ni {line polygon}} continue
		set selected_indices [points_selected_indices_of $canvas $item]

		set marks_present yes  ;# preserve mark selection state in case of a no-op
		set coords [$canvas coords $item]
		foreach index $selected_indices {
			if {($index-1) ni $selected_indices} continue  ;# need two adjacent vertices
			if {$marks_present} {
				points_unmark_item $canvas $item
				set marks_present no
			}
			set index2 [expr {2 * ($index-1)}]
			lassign [lrange $coords $index2 [expr {$index2 + 3}]] x1 y1 x2 y2
			set new_x [expr {0.5 * ($x1 + $x2)}]
			set new_y [expr {0.5 * ($y1 + $y2)}]
			$canvas insert $item [expr {$index2 + 2}] [list $new_x $new_y]
		}
		if {[$canvas type $item] eq "polygon"} {
			set last_coord [expr {([llength $coords] >> 1) - 1}]  ;# >>1 == /2
			if {$last_coord in $selected_indices && 0 in $selected_indices} {
				if {$marks_present} {
					points_unmark_item $canvas $item
					set marks_present no
				}
				lassign [lrange $coords 0 1] x1 y1
				lassign [lrange $coords end-1 end] x2 y2
				set new_x [expr {0.5 * ($x1 + $x2)}]
				set new_y [expr {0.5 * ($y1 + $y2)}]
				$canvas insert $item end [list $new_x $new_y]
			}
		}
		if {! $marks_present} {
			points_mark_item $canvas $item
		}
	}
}

proc points_mark_hit_at {canvas x y} {
	# $x and $y must have been processed by canvasx and canvasy, respectively.

	variable points_dot_size

	# As the point marks are realized as non-filled rectangles, we need to do an overlap search
	# in order to hit them also when the cursor is inside one of them (not on its outline).
	set s $points_dot_size
	set searchbox [list [expr {$x-$s}] [expr {$y-$s}] [expr {$x+$s}] [expr {$y+$s}]]
	set overlapping [$canvas find overlapping {*}$searchbox]
	set really_overlapping_rectangles [list]
	foreach item $overlapping {
		if {[$canvas type $item] eq "rectangle"} {
			lassign [$canvas coords $item] x1 y1 x2 y2
			if {$x1 <= $x && $x <= $x2  &&  $y1 <= $y && $y <= $y2} {
				lappend really_overlapping_rectangles $item
			}
		}
	}
	# Highest stacking order, since marks have been drawn last
	set potential_mark [lindex $really_overlapping_rectangles end]
	return $potential_mark
}

}
