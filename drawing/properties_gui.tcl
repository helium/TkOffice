# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

proc properties_frame_create {path -for canvas} {
	variable properties_frame_for
	variable canvas_for_properties_frame
	if {${-for} ne {-for}} {
		error "wrong second argument: must be properties_frame_create path -for canvas"
	}

	ttk::frame $path
	set properties_frame_for($canvas) $path
	set canvas_for_properties_frame($path) $canvas


	grid [properties_subframe_generic $path.generic $canvas] \
		-padx 3 -pady {3 0} -sticky we
	grid [properties_subframe_arc $path.arc $canvas] \
		-padx 3 -pady {6 0} -sticky we
	grid [properties_subframe_polygonlike $path.polygonlike $canvas] \
		-padx 3 -pady {6 0} -sticky we
	grid [properties_subframe_text $path.text $canvas] \
		-padx 3 -pady {6 0} -sticky we

	bind $canvas <<SelectionUpdate>> [list drawing::update_properties $canvas]
	bind $path <Destroy> [list drawing::properties_frame_destroy $path $canvas]

	set esc_binding [list focus $canvas]
	foreach frame [winfo children $path] {
		foreach widget [winfo children $frame] {
			if {[winfo class $widget] in {TCombobox TEntry TSpinbox}} {
				bind $widget <Escape> $esc_binding
			}
		}
	}

	return $path
}

proc properties_frame_destroy {path canvas} {
	variable properties_frame_for
	variable canvas_for_properties_frame

	unset properties_frame_for($canvas)
	unset canvas_for_properties_frame($path)
}

proc colorbutton_click {colorbutton canvas} {
	variable properties_frame_for

	if {[$colorbutton cget -state] eq {disabled}} return
	set old_color [$colorbutton cget -background]
	if {$old_color eq {}} {set old_color black}
	set properties_window [winfo toplevel $properties_frame_for($canvas)]
	return [tk_chooseColor -initialcolor $old_color -parent $properties_window]
}

proc set_from_spinbox {canvas spinbox propname {item_types {}}} {
	if {[catch {set_property $canvas $propname [$spinbox get] $item_types} result]} {
		statusbar_error $canvas $result
	} else {
		statusbar_restore $canvas
	}
}

proc set_from_combobox {canvas combobox propname {item_types {}}} {
	set_from_spinbox $canvas $combobox $propname $item_types
}

proc make_spinbox_valid {canvas spinbox propname {item_types {}}} {
	set selection [get_selected $canvas]
	if {$item_types ne {}} {
		set selection [filter_by_type $canvas $selection $item_types]
	}
	set value [get_number_property $canvas $selection $propname]
	update_spinbox $spinbox $value
	statusbar_restore_later $canvas
}

proc make_combobox_valid {canvas combobox active_state propname} {
	set selection [get_selected $canvas]
	set value [get_string_property $canvas $selection $propname]
	update_combobox $combobox $value $active_state
	statusbar_restore_later $canvas
}

}
