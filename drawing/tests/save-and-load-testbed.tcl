#!/usr/bin/wish8.5
# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Relative path to tclIndex - needs to be updated (add/remove some ..)
#  whenever this script is moved into a different hierarchy level
set dir [file join [file dirname [info script]] .. ..]
source [file join $dir tclIndex]

# Create the single-file drawing demo application
source [file join [file dirname [info script]] interactive-drawing.tcl]

# Build up GUI

ttk_useTheme clam
icons::load down,16 up,16 document-open,16 document-save-as,16

set w [ttk_toplevel .testbed]
wm title $w {Save/Load Testbed}
wm minsize $w 300 150
wm geometry $w 400x400

pack [ttk::frame $w.buttons] -side top
ttk_text $w.filedata -width 0 -height 10 -wrap word
ttk_text_scrollbar $w.filedata on
pack $w.filedata_frame -side bottom -fill both -expand true -padx 2 -pady {0 2}
ttk::button $w.save -text Save -image down,16 -compound left
ttk::button $w.load -text Load -image up,16 -compound left
pack $w.save -in $w.buttons -side left -padx {0 10} -pady 2
pack $w.load -in $w.buttons -side right -padx {10 0} -pady 2
foreach button [list $w.save $w.load] {
	$button configure -takefocus 0
}
bind .testbed <KeyPress-Alt_L> {
	foreach button [list .testbed.save .testbed.load] {
		$button configure -underline 0
	}
}
bind .testbed <KeyRelease-Alt_L> {
	foreach button [list .testbed.save .testbed.load] {
		$button configure -underline -1
	}
}
bind .testbed <Alt-KeyPress-s> {.testbed.save state pressed}
bind .testbed <Alt-KeyPress-l> {.testbed.load state pressed}
bind .testbed <Alt-KeyRelease-s> {.testbed.save state !pressed; .testbed.save invoke}
bind .testbed <Alt-KeyRelease-l> {.testbed.load state !pressed; .testbed.load invoke}

# Load File and Save File context menu for the text field

ttk_menu $w.filedata.contextmenu
$w.filedata.contextmenu add command -label "Load File" -accelerator Ctrl+O -command filedata_load_file \
	-image document-open,16 -compound left
$w.filedata.contextmenu add command -label "Save File" -accelerator Ctrl+S -command filedata_save_file \
	-image document-save-as,16 -compound left
bind $w.filedata <3> {tk_popup $w.filedata.contextmenu %X %Y}
bind $w.filedata <Menu> {
	tk_popup $w.filedata.contextmenu [expr {[winfo rootx $w.filedata]+20}] [expr {[winfo rooty $w.filedata]+20}]
}
bind $w.filedata <Control-o> {filedata_load_file; break}  ;# disable default "open new line" binding
bind $w.filedata <Control-s> filedata_save_file

# make this available for event bindings, where [info script] is empty
set examples_dir [file join [file dirname [info script]] save-and-load-examples]

proc filedata_load_file {} {
	set path [tk_getOpenFile -initialdir $::examples_dir -defaultextension .tkg \
		-filetypes {{{TkOffice vector drawing document} {.tkg .tkdraw}}} ]
	if {$path eq {}} return

	set chan [open $path]  ;# may fail, then the standard Tk error dialog will show
	.testbed.filedata delete 1.0 end
	.testbed.filedata insert end [read $chan]
	close $chan
}

proc filedata_save_file {} {
	set path [tk_getSaveFile -initialdir $::examples_dir -defaultextension .tkg \
		-filetypes {{{TkOffice vector drawing document} {.tkg .tkdraw}}} ]
	if {$path eq {}} return

	set chan [open $path w]  ; # may fail, then the standard Tk error dialog will show
	puts -nonewline $chan [.testbed.filedata get 1.0 end-1c]
	close $chan
}

# A "reflected channel" that writes into the text widget .filedata

namespace eval textchan {
	proc initialize {channelId mode} {
		if {$mode ne "write"} {
			error "bad mode \"$mode\": must be write"
		}
		.testbed.filedata delete 1.0 end
		return [list initialize finalize watch write]
	}
	proc finalize {channelId} {}
	proc watch {channelId eventspec} {}

	proc write {channelId data_bytes} {
		.testbed.filedata insert end [encoding convertfrom utf-8 $data_bytes]
		return [string length $data_bytes]
	}

	namespace export initialize finalize watch write
	namespace ensemble create
}

# Buttons that invoke the (de)serialization procedures under test

proc save {} {
	set chan [chan create write textchan]
	drawing::save_document_to_channel .canvas $chan
	close $chan
}

.testbed.save configure -command save

proc load_ {} {
	drawing::load_widget_from_string .canvas [.testbed.filedata get 1.0 end-1c]
}

.testbed.load configure -command load_
