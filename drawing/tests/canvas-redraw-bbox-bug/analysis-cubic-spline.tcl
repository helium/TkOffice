#!/usr/bin/wish
# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

ttk::style theme use clam
. configure -background #dcdad5
grid [canvas .canvas -background white -border 0 -highlightthickness 0] -row 0 -column 0 -sticky nesw
grid [ttk::scrollbar .vscroll -orient vertical] -row 0 -column 1 -sticky ns
grid [ttk::scrollbar .hscroll -orient horizontal] -row 1 -column 0 -sticky we
grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1
.canvas configure -xscrollcommand {.hscroll set} -yscrollcommand {.vscroll set}
.hscroll configure -command {.canvas xview}
.vscroll configure -command {.canvas yview}
.canvas configure -scrollregion {0 0 848 1392} -confine yes

proc guide_lines {args} {
	foreach {x1 y1 x2 y2} $args {
		.canvas create line $x1 $y1 $x2 $y2 -fill #bbb
	}
}

proc dot {x y size args} {
	.canvas create oval [expr {$x-$size}] [expr {$y-$size}] [expr {$x+$size}] [expr {$y+$size}] {*}$args
}

proc dot_userpoint {x y} {
	dot $x $y 3 -outline black -fill white
}

proc dot_userpoint_changed {x y} {
	dot $x $y 3 -outline black -fill black -width 1
}

proc dot_redraw_needed {x y} {
	dot $x $y 7 -outline {} -fill yellow
}

proc midpoint {destx_ desty_ Ax Ay Bx By} {
	upvar $destx_ destx $desty_ desty
	set destx [expr {0.5 * ($Ax + $Bx)}]
	set desty [expr {0.5 * ($Ay + $By)}]
}

proc move_points_by {dx dy args} {
	foreach {x_ y_} $args {
		upvar $x_ x $y_ y
		set x [expr {$x + $dx}]
		set y [expr {$y + $dy}]
	}
}

.canvas create text 26 32 -anchor w -text {Cubic Bézier splines (-smooth raw) in Tk} -font TkCaptionFont
.canvas create text 26 60 -anchor w -text Basics -font TkHeadingFont
.canvas create text 26 75 -anchor nw -width 500 -text \
{Every cubic bézier segment is defined by four control points A, B, C, D.
The line starts from point A in the direction AB, and it ends at point D coming from the direction CD.
The points B and C are generally not touched by the line.}

set Ax 560; set Ay 150; set Bx 590; set By 60; set Cx 680; set Cy 150; set Dx 710; set Dy 60
guide_lines $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy -smooth raw -fill #f44 -width 4
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy] {dot_userpoint $x $y}
.canvas create text [expr {$Ax - 5}] $Ay -text A -anchor e
.canvas create text [expr {$Bx - 5}] $By -text B -anchor e
.canvas create text [expr {$Cx + 5}] $Cy -text C -anchor w
.canvas create text [expr {$Dx + 5}] $Dy -text D -anchor w


.canvas create text 26 170 -anchor w -text {Line Items} -font TkHeadingFont
.canvas create text 26 185 -anchor nw -text \
{Tk line objects with -smooth raw should have 3n+1 coordinate points (n integer).
Then the first segment consists of the first four points (ABCD), the next of the fourth to the seventh point (DEFG) etc.}

set Ax 40; set Ay 330; set Bx 70; set By 245; set Cx 130; set Cy 330; set Dx 160; set Dy 245
set Ex 190; set Ey 330; set Fx 240; set Fy 330; set Gx 270; set Gy 245
guide_lines $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy -smooth raw -fill #fa4 -width 4  ;# First segment
.canvas create line $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy -smooth raw -fill #a4f -width 4  ;# Second segment
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy -smooth raw  ;# All segments
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy] {dot_userpoint $x $y}
.canvas create text $Ax [expr {$Ay + 5}] -text A -anchor n
.canvas create text $Bx [expr {$By - 4}] -text B -anchor s
.canvas create text $Cx [expr {$Cy + 5}] -text C -anchor n
.canvas create text $Dx [expr {$Dy - 4}] -text D -anchor s
.canvas create text $Ex [expr {$Ey + 5}] -text E -anchor n
.canvas create text $Fx [expr {$Fy + 5}] -text F -anchor n
.canvas create text $Gx [expr {$Gy - 4}] -text G -anchor s

.canvas create text 295 240 -anchor nw -width 200 -justify right -text \
{Continuously smooth curves are obtained only if a knot (D) and its adjacent control points (C, E) lie on a straight line:}

set Ax 510; set Ay 330; set Bx 540; set By 245; set Cx 580; set Cy 330
set Ex 620; set Ey 245; set Fx 700; set Fy 330; set Gx 730; set Gy 245
set Dx [expr {0.6*$Cx + 0.4*$Ex}]
set Dy [expr {0.6*$Cy + 0.4*$Ey}]
guide_lines $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy -smooth raw -fill #fa4 -width 4  ;# First segment
.canvas create line $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy -smooth raw -fill #a4f -width 4  ;# Second segment
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy -smooth raw  ;# All segments
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy] {dot_userpoint $x $y}
.canvas create text $Ax [expr {$Ay + 5}] -text A -anchor n
.canvas create text $Bx [expr {$By - 4}] -text B -anchor s
.canvas create text $Cx [expr {$Cy + 5}] -text C -anchor n
.canvas create text [expr {$Dx + 5}] $Dy -text D -anchor nw
.canvas create text $Ex [expr {$Ey - 4}] -text E -anchor s
.canvas create text $Fx [expr {$Fy + 5}] -text F -anchor n
.canvas create text $Gx [expr {$Gy - 4}] -text G -anchor s

.canvas create text 26 365 -anchor nw -text \
{When the "3n+1" rule is violated, Tk repeats the points starting from the first, until 3n+1 points are present.
See TkMakeRawCurve in generic/tkTrig.c:}
.canvas create text 40 405 -anchor nw -width 780 -text \
{"In the last two cases, 1 or 2 initial points from the first curve segment are reused as defining points also for the last curve segment. In the case of 3s input points, this will automatically close the curve."}

set Ax 50; set Ay 540; set Bx 80; set By 460; set Cx 160; set Cy 470; set Dx 185; set Dy 515; set Ex 130; set Ey 570
guide_lines $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Dx $Dy $Ex $Ey $Ax $Ay $Bx $By
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy -smooth raw -fill #fa4 -width 4  ;# First segment
.canvas create line $Dx $Dy $Ex $Ey $Ax $Ay $Bx $By -smooth raw -fill #4fa -width 4
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey -smooth raw  ;# The proof!
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey] {dot_userpoint $x $y}
.canvas create text [expr {$Ax - 5}] $Ay -text A -anchor e
.canvas create text [expr {$Bx + 5}] $By -text B -anchor w
.canvas create text [expr {$Cx + 5}] $Cy -text C -anchor w
.canvas create text [expr {$Dx + 5}] $Dy -text D -anchor w
.canvas create text [expr {$Ex - 5}] $Ey -text E -anchor e

.canvas create text 225 474 -anchor nw -width 500 -text \
{In this example, the points ABCDE are appended by the repeated points AB, such that the segments ABCD and DEAB result.
Such curves are not really useful, they probably only occur in the presence of programmer / user mistakes.}

.canvas create text 26 590 -anchor nw -width 810 -text \
{The case of 3n points, where only the starting point is repeated, can be used for "automatically" closing curves, when the mouse-cursor ("current" item) behavior of a polygon item is not wanted. However, this is somewhat unexpected, as such an "auto-close" feature is not present for the other two smoothing modes.}

.canvas create text 26 660 -anchor nw -width 810 -text \
{Insertion and deletion of points must only be solved efficiently for multiples of three points being inserted/deleted. Otherwise large parts of the curve need to be redrawn, as the roles of points (which points are the start/end points of a segment that lie on the curve) change for the whole rest of the curve:}

set Tx  36; set Ty 775
.canvas create text $Tx $Ty -anchor w -text {Before:}

set Ax  95; set Ay 810
set Bx 120; set By 745
set Cx 200; set Cy 810
set Ex 215; set Ey 745
set Fx 250; set Fy 745
set Gx 265; set Gy 810
set Hx 300; set Hy 810
set Ix 310; set Iy 745
set Kx 360; set Ky 810
set Lx 395; set Ly 810
set Mx 395; set My 745
midpoint Dx Dy $Cx $Cy $Ex $Ey
midpoint Jx Jy $Ix $Iy $Kx $Ky
set N1x 160; set N1y 745
set N2x 180; set N2y 810

guide_lines $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy
guide_lines $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy
guide_lines $Gx $Gy $Hx $Hy $Ix $Iy $Jx $Jy
guide_lines $Jx $Jy $Kx $Ky $Lx $Ly $Mx $My
set points [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy $Hx $Hy $Ix $Iy $Jx $Jy $Kx $Ky $Lx $Ly $Mx $My]
.canvas create line $points -smooth raw -fill #f44 -width 4 -splinesteps 48
foreach {x y} $points {dot_userpoint $x $y}

move_points_by 0 100 Ax Ay Bx By Cx Cy Dx Dy Ex Ey Fx Fy Gx Gy Hx Hy Ix Iy Jx Jy Kx Ky Lx Ly Mx My N1x N1y N2x N2y Tx Ty
.canvas create text $Tx $Ty -anchor w -text {After:}

guide_lines $Ax $Ay $Bx $By $N1x $N1y $N2x $N2y
guide_lines $N2x $N2y $Cx $Cy $Dx $Dy $Ex $Ey
guide_lines $Ex $Ey $Fx $Fy $Gx $Gy $Hx $Hy
guide_lines $Hx $Hy $Ix $Iy $Jx $Jy $Kx $Ky
guide_lines $Kx $Ky $Lx $Ly $Mx $My $Ax $Ay
set points [list $Ax $Ay $Bx $By $N1x $N1y $N2x $N2y $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy $Hx $Hy $Ix $Iy $Jx $Jy $Kx $Ky $Lx $Ly $Mx $My]
.canvas create line $points -smooth raw -fill #f44 -width 4 -splinesteps 48
set points [concat [lrange $points 0 3] [lrange $points 8 end]]
foreach {x y} $points {dot_userpoint $x $y}
dot_userpoint_changed $N1x $N1y
dot_userpoint_changed $N2x $N2y

.canvas create text 100 930 -anchor nw -text \
{2 black points inserted.
The whole line item needs to be redrawn.}

set Ax  510; set Ay  810
set Bx  550; set By  745
set N1x 580; set N1y 810
set N3x 620; set N3y 745
set Cx  650; set Cy  810
set Ex  680; set Ey  745
set Fx  740; set Fy  745
set Gx  750; set Gy  810
midpoint N2x N2y $N1x $N1y $N3x $N3y
midpoint Dx Dy $Cx $Cy $Ex $Ey

guide_lines $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy \
	-smooth raw -fill #f44 -width 4 -splinesteps 48
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy] {dot_userpoint $x $y}
.canvas create text [expr {$Ax - 5}] $Ay -text 0 -anchor ne
.canvas create text [expr {$Gx + 5}] $Gy -text {N−1} -anchor nw

move_points_by 0 100 Ax Ay Bx By N1x N1y N2x N2y N3x N3y Cx Cy Dx Dy Ex Ey Fx Fy Gx Gy
foreach {x y} [list $Ax $Ay $Bx $By $N1x $N1y $N2x $N2y $N3x $N3y $Cx $Cy $Dx $Dy] {dot_redraw_needed $x $y}
guide_lines $Ax $Ay $Bx $By $N1x $N1y $N2x $N2y $N2x $N2y $N3x $N3y $Cx $Cy $Dx $Dy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy
.canvas create line $Ax $Ay $Bx $By $N1x $N1y $N2x $N2y $N3x $N3y $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy \
	-smooth raw -fill #f44 -width 4 -splinesteps 48
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Gx $Gy] {dot_userpoint $x $y}
foreach {x y} [list $N1x $N1y $N2x $N2y $N3x $N3y] {dot_userpoint_changed $x $y}
.canvas create text [expr {$Ax - 5}] $Ay -text 0 -anchor ne
.canvas create text [expr {$N1x + 5}] $N1y -text {first=2} -anchor w
.canvas create text [expr {$N3x + 5}] $N3y -text {last=4} -anchor w
.canvas create text [expr {$Gx + 5}] $Gy -text {N+last−first} -anchor nw

.canvas create text 500 930 -anchor nw -text \
{3 black points inserted.
The yellow points need to be included for redrawing.}

.canvas create text 26 980 -anchor nw -text \
{When points are inserted/deleted in groups of three, only the adjacent segments must be redrawn.}
.canvas create text 26 1000 -anchor nw -text \
{First point to include: (first-1) - (first-1)%3
Last point to include: last + 3 - last%3.  If this expression exceeds the number of points, wrap around (degenerate curve!)}


.canvas create text 26 1070 -anchor w -text {Polygon Items} -font TkHeadingFont
.canvas create text 26 1085 -anchor nw -text \
{Tk polygon objects with -smooth raw should have 3n coordinate points (n integer).
A polygon ABCDEF corresponds to a line ABCDEFA (segments ABCD, DEFA).}

.canvas create text 26 1130 -anchor nw -width 360 -text \
{Similar behavior occurs when the "3n" rule is violated:
Polygon ABCD => Line ABCDA => Line ABCDAAB with segments ABCD, DAAB.}

set Ax 63; set Ay 1316; set Bx 63; set By 1204; set Cx 176; set Cy 1204; set Dx 176; set Dy 1316
guide_lines $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Dx $Dy $Ax $Ay
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy -smooth raw -fill #fa4 -width 4
.canvas create line $Dx $Dy $Ax $Ay $Ax $Ay $Bx $By -smooth raw -fill #4fa -width 4
.canvas create polygon $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy -smooth raw -fill {} -outline black -width 1  ;# The proof!
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy] {dot_userpoint $x $y}
.canvas create text [expr {$Ax - 5}] $Ay -text A -anchor e
.canvas create text [expr {$Bx - 5}] $By -text B -anchor e
.canvas create text [expr {$Cx + 5}] $Cy -text C -anchor w
.canvas create text [expr {$Dx + 5}] $Dy -text D -anchor w

.canvas create text 441 1130 -anchor nw -width 360 -text \
{Therefore, here too we should only optimize the insertion/deletion of groups of three points:}

set Ax 452; set Ay 1265; set Bx 452; set By 1175; set Cx 532; set Cy 1175; set Dx 547; set Dy 1206
set Ex 562; set Ey 1237; set Fx 562; set Fy 1265
set Nx 530; set Ny 1270; set Ox 520; set Oy 1240; set Px 452; set Py 1305
guide_lines $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy
guide_lines $Dx $Dy $Ex $Ey $Fx $Fy $Ax $Ay
.canvas create polygon $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy -smooth raw -fill {} -outline #f44 -width 4
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy] {dot_userpoint $x $y}
.canvas create text [expr {$Ax - 5}] $Ay -text 0 -anchor ne
.canvas create text [expr {$Fx + 5}] $Fy -text 5 -anchor nw

move_points_by 180 0 Ax Ay Bx By Cx Cy Dx Dy Ex Ey Fx Fy Nx Ny Ox Oy Px Py
foreach {x y} [list $Dx $Dy $Ex $Ey $Fx $Fy $Nx $Ny $Ox $Oy $Px $Py $Ax $Ay] {dot_redraw_needed $x $y}
guide_lines $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy
guide_lines $Dx $Dy $Ex $Ey $Fx $Fy $Nx $Ny
guide_lines $Nx $Ny $Ox $Oy $Px $Py $Ax $Ay
.canvas create polygon $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy $Nx $Ny $Ox $Oy $Px $Py \
	-smooth raw -fill {} -outline #f44 -width 4
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy] {dot_userpoint $x $y}
foreach {x y} [list $Nx $Ny $Ox $Oy $Px $Py] {dot_userpoint_changed $x $y}
.canvas create text [expr {$Ax - 5}] $Ay -text 0 -anchor ne
.canvas create text [expr {$Dx + 5}] $Dy -text 3 -anchor sw
.canvas create text [expr {$Fx + 5}] $Fy -text 5 -anchor nw
.canvas create text $Nx [expr {$Ny + 5}] -text {first=6} -anchor n
.canvas create text $Ox [expr {$Oy - 3}] -text 7 -anchor se
.canvas create text [expr {$Px + 5}] $Py -text {last=8} -anchor w

.canvas create text 441 1315 -anchor nw -width 360 -text \
{First point to include for redraw: (first-1) - (first-1)%3
Last point to include for redraw: last + 3 - last%3.  (If this equals the new number of points, also include the 0th point.)}

wm attributes . -zoomed 1