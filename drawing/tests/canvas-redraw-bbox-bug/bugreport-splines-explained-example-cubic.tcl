#!/usr/bin/wish
# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)
# Bug observed with Tk versions 8.5.19 and 8.6.8

pack [canvas .c -width 500 -height 350]

set coords [list 63 248  96 166  218 329  307 299  402 295  410 80  335 147  275 192  220 101  293 37]
.c create line $coords -smooth raw -width 3 -fill #0f0
.c create line $coords -smooth raw -tag myLine
update; after 2000
.c insert myLine 4 {157 247.5}
update; after 2000
.c move myLine 0 0
