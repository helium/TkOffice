#!/usr/bin/wish
# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

ttk::style theme use clam
. configure -background #dcdad5
grid [canvas .canvas -background white -border 0 -highlightthickness 0] -row 0 -column 0 -sticky nesw
grid [ttk::scrollbar .vscroll -orient vertical] -row 0 -column 1 -sticky ns
grid [ttk::scrollbar .hscroll -orient horizontal] -row 1 -column 0 -sticky we
grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1
.canvas configure -xscrollcommand {.hscroll set} -yscrollcommand {.vscroll set}
.hscroll configure -command {.canvas xview}
.vscroll configure -command {.canvas yview}
.canvas configure -scrollregion {0 0 848 1296} -confine yes

proc dot {x y size args} {
	.canvas create oval [expr {$x-$size}] [expr {$y-$size}] [expr {$x+$size}] [expr {$y+$size}] {*}$args
}

proc dot_userpoint {x y} {
	dot $x $y 3 -outline black -fill white
}

proc dot_userpoint_changed {x y} {
	dot $x $y 3 -outline #44f -fill white -width 2
}

proc dot_userpoint_removed {x y} {
	dot $x $y 3 -outline #f44 -fill white -width 2
}

proc dot_midpoint {x y} {
	dot $x $y 2 -outline black -fill black
}

proc dot_midpoint_changed {x y} {
	dot $x $y 2 -outline #44f -fill #44f
}

proc dot_midpoint_removed {x y} {
	dot $x $y 2 -outline #f44 -fill #f44
}

proc dot_redraw_needed {x y} {
	dot $x $y 7 -outline {} -fill yellow
}

proc midpoint {destx_ desty_ Ax Ay Bx By} {
	upvar $destx_ destx $desty_ desty
	set destx [expr {0.5 * ($Ax + $Bx)}]
	set desty [expr {0.5 * ($Ay + $By)}]
}

proc move_points_by {dx dy args} {
	foreach {x_ y_} $args {
		upvar $x_ x $y_ y
		set x [expr {$x + $dx}]
		set y [expr {$y + $dy}]
	}
}

.canvas create text 26 32 -anchor w -text {Quadratic Bézier splines (-smooth true) in Tk} -font TkCaptionFont
.canvas create text 26 60 -anchor w -text Basics -font TkHeadingFont
.canvas create text 26 75 -anchor nw -text \
{Every quadratic bézier segment is a parabola defined by three control points A, B, C.
The line starts from point A in the direction AB, and it ends at point C coming from the direction BC.
The second point B is generally not touched by the line (exception: all three points on a straight line).
The point on the line influenced largest by B is constructed as P = ¼A+½B+¼C:}

set Ax 31.0; set Ay 257.0; set Bx 109.0; set By 172.0; set Cx 153.0; set Cy 255.0
.canvas create line $Ax $Ay $Bx $By $Cx $Cy -smooth true -fill #f44 -width 4
.canvas create line $Ax $Ay $Bx $By $Cx $Cy
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy] {dot_userpoint $x $y}
midpoint ABx ABy $Ax $Ay $Bx $By
midpoint BCx BCy $Bx $By $Cx $Cy
.canvas create line $ABx $ABy $BCx $BCy -fill #888
dot_midpoint $ABx $ABy
dot_midpoint $BCx $BCy
midpoint Px Py $ABx $ABy $BCx $BCy
dot_midpoint $Px $Py
.canvas create text [expr {$Ax - 5}] $Ay -text A -anchor ne
.canvas create text $Bx [expr {$By - 3}] -text B -anchor s
.canvas create text [expr {$Cx + 5}] $Cy -text C -anchor nw
.canvas create text $ABx $ABy -text {½A+½B} -anchor se
.canvas create text $BCx $BCy -text {½B+½C} -anchor sw
.canvas create text $Px [expr {$Py + 3}] -text P -anchor n

set Ax 283.0; set Ay 296.0; set Bx 333.0; set By 170.0; set Cx 416.0; set Cy 301.0; set Dx 497.0; set Dy 173.0
set Ex 566.0; set Ey 297.0
midpoint Rx Ry $Bx $By $Cx $Cy
midpoint Sx Sy $Cx $Cy $Dx $Dy
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey -dash {2 4}
.canvas create line $Ax $Ay $Bx $By $Rx $Ry -smooth true -fill #fa4 -width 4
.canvas create line $Rx $Ry $Cx $Cy $Sx $Sy -smooth true -fill #4fa -width 4
.canvas create line $Sx $Sy $Dx $Dy $Ex $Ey -smooth true -fill #a4f -width 4
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey -smooth true  ;# The proof!
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey] {dot_userpoint $x $y}
foreach {x y} [list $Rx $Ry $Sx $Sy] {dot_midpoint $x $y}
.canvas create text $Ax [expr {$Ay + 5}] -text A -anchor n
.canvas create text $Bx [expr {$By - 4}] -text B -anchor s
.canvas create text $Cx [expr {$Cy + 5}] -text C -anchor n
.canvas create text $Dx [expr {$Dy - 4}] -text D -anchor s
.canvas create text $Ex [expr {$Ey + 5}] -text E -anchor n
.canvas create text [expr {$Rx - 3}] $Ry -text R -anchor ne
.canvas create text [expr {$Sx + 3}] $Sy -text S -anchor nw

.canvas create text 26 328 -anchor nw -width 700 -text \
{The figure at the right shows how a Tk line with more than three points (here: ABCDE) is decomposed into Bézier segments ABR, RCS, SDE, where R is the midpoint of BC, and S is the midpoint between C and D.}


.canvas create text 26 386 -anchor w -text {Insertion / Deletion of points} -font TkHeadingFont

.canvas create text 26 410 -anchor w -text {Points marked with yellow should be used for the redraw bounding box.}
.canvas create text 26 430 -anchor w -text {• Sequential build-up of a spline, adding points at its end:}

set Ax  30; set Ay 550; set Bx  80; set By 450
set Cx 130; set Cy 550; set Dx 180; set Dy 450
set Ex 230; set Ey 550; set Fx 280; set Fy 450
midpoint Px Py $Bx $By $Cx $Cy
midpoint Qx Qy $Cx $Cy $Dx $Dy
midpoint Rx Ry $Dx $Dy $Ex $Ey

dot_redraw_needed $Ax $Ay; dot_redraw_needed $Bx $By; dot_redraw_needed $Cx $Cy
.canvas create line $Ax $Ay $Bx $By -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Ax $Ay $Bx $By $Cx $Cy -smooth true -splinesteps 100 -width 2 -fill #44f
dot_userpoint $Ax $Ay; dot_userpoint $Bx $By; dot_userpoint_changed $Cx $Cy
.canvas create line [expr {$Cx+15}] 444 [expr {$Cx+15}] 560 -fill #ccc

move_points_by [expr {$Cx-$Ax+30}] 0 Ax Ay Bx By Cx Cy Dx Dy Ex Ey Fx Fy Px Py Qx Qy Rx Ry
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy] {dot_redraw_needed $x $y}
.canvas create line $Ax $Ay $Bx $By $Cx $Cy -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy -smooth true -splinesteps 100 -width 2 -fill #44f
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy] {dot_userpoint $x $y}
dot_userpoint_changed $Dx $Dy
dot_midpoint_changed $Px $Py
.canvas create line [expr {$Dx+15}] 444 [expr {$Dx+15}] 560 -fill #ccc

move_points_by [expr {$Dx-$Ax+30}] 0 Ax Ay Bx By Cx Cy Dx Dy Ex Ey Fx Fy Px Py Qx Qy Rx Ry
foreach {x y} [list $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey] {dot_redraw_needed $x $y}
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey -smooth true -splinesteps 100 -width 2 -fill #44f
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy] {dot_userpoint $x $y}
dot_userpoint_changed $Ex $Ey
dot_midpoint $Px $Py; dot_midpoint_changed $Qx $Qy
.canvas create line [expr {$Ex+15}] 444 [expr {$Ex+15}] 560 -fill #ccc

move_points_by [expr {$Ex-$Ax+30}] 0 Ax Ay Bx By Cx Cy Dx Dy Ex Ey Fx Fy Px Py Qx Qy Rx Ry
foreach {x y} [list $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy] {dot_redraw_needed $x $y}
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy -smooth true -splinesteps 100 -width 2 -fill #44f
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey] {dot_userpoint $x $y}
dot_userpoint_changed $Fx $Fy
dot_midpoint $Px $Py; dot_midpoint $Qx $Qy; dot_midpoint_changed $Rx $Ry
.canvas create text [expr {$Cx-8}] $Cy -anchor e -text i−3 -font TkHeadingFont
.canvas create text [expr {$Dx-8}] $Dy -anchor e -text i−2
.canvas create text [expr {$Ex-8}] $Ey -anchor e -text i−1
.canvas create text [expr {$Fx-8}] $Fy -anchor e -text i -font TkHeadingFont -fill #44f

.canvas create text 26 580 -anchor w -text {• Addition / removal at one place apart from the end:}
set Ax  80; set Ay 700; set Bx 180; set By 600
set Cx 230; set Cy 700; set Dx 280; set Dy 600
set Ex 330; set Ey 700
set Nx 150; set Ny 700
midpoint NBx NBy $Nx $Ny $Bx $By
midpoint BCx BCy $Bx $By $Cx $Cy
midpoint CDx CDy $Cx $Cy $Dx $Dy
foreach {x y} [list $Ax $Ay $Nx $Ny $Bx $By $Cx $Cy] {dot_redraw_needed $x $y}
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Ax $Ay $Nx $Ny $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey -smooth true -splinesteps 100 -width 2 -fill #44f
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey] {dot_userpoint $x $y}
dot_userpoint_changed $Nx $Ny
dot_midpoint_changed $NBx $NBy
dot_midpoint $BCx $BCy
dot_midpoint $CDx $CDy
.canvas create text [expr {$Ax-8}] $Ay -anchor e -text i−1
.canvas create text [expr {$Nx-8}] $Ny -anchor e -text i -font TkHeadingFont -fill #44f
.canvas create text [expr {$Bx-8}] $By -anchor e -text i+1
.canvas create text [expr {$Cx-8}] $Cy -anchor e -text i+2 -font TkHeadingFont

.canvas create line 380 570 380 720 -fill #ccc

.canvas create text 390 580 -anchor w -text {• Addition / removal at two places apart from the end:}
set Ax 410; set Ay 700; set Bx 460; set By 600
set Cx 610; set Cy 700; set Dx 660; set Dy 600
set Ex 710; set Ey 700
set Nx 530; set Ny 700
midpoint BNx BNy $Bx $By $Nx $Ny
midpoint NCx NCy $Nx $Ny $Cx $Cy
midpoint BCx BCy $Bx $By $Cx $Cy
midpoint CDx CDy $Cx $Cy $Dx $Dy
foreach {x y} [list $Ax $Ay $Bx $By $Nx $Ny $Cx $Cy $Dx $Dy] {dot_redraw_needed $x $y}
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Ax $Ay $Bx $By $Nx $Ny $Cx $Cy $Dx $Dy $Ex $Ey -smooth true -splinesteps 100 -width 2 -fill #44f
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey] {dot_userpoint $x $y}
dot_userpoint_changed $Nx $Ny
dot_midpoint_changed $BNx $BNy; dot_midpoint_changed $NCx $NCy
dot_midpoint_removed $BCx $BCy
dot_midpoint $CDx $CDy
.canvas create text [expr {$Ax+8}] $Ay -anchor w -text i−2 -font TkHeadingFont
.canvas create text [expr {$Bx+8}] $By -anchor w -text i−1
.canvas create text [expr {$Nx-8}] $Ny -anchor e -text i -font TkHeadingFont -fill #44f
.canvas create text [expr {$Cx+8}] $Cy -anchor w -text i+1
.canvas create text [expr {$Dx-8}] $Dy -anchor e -text i+2 -font TkHeadingFont

.canvas create text 26 735 -anchor w -text {• Addition / removal far inside the spline:}
set Ax  80; set Ay 860; set Bx 130; set By 760
set Cx 180; set Cy 860; set Dx 330; set Dy 760
set Ex 380; set Ey 860; set Fx 430; set Fy 760
set Nx 230; set Ny 770
midpoint BCx BCy $Bx $By $Cx $Cy
midpoint CDx CDy $Cx $Cy $Dx $Dy
midpoint DEx DEy $Dx $Dy $Ex $Ey
midpoint CNx CNy $Cx $Cy $Nx $Ny
midpoint NDx NDy $Nx $Ny $Dx $Dy
foreach {x y} [list $Bx $By $Cx $Cy $Nx $Ny $Dx $Dy $Ex $Ey] {dot_redraw_needed $x $y}
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Nx $Ny $Dx $Dy $Ex $Ey $Fx $Fy -smooth true -splinesteps 100 -width 2 -fill #44f
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey $Fx $Fy] {dot_userpoint $x $y}
dot_userpoint_changed $Nx $Ny
dot_midpoint $BCx $BCy
dot_midpoint_removed $CDx $CDy
dot_midpoint $DEx $DEy
dot_midpoint_changed $CNx $CNy
dot_midpoint_changed $NDx $NDy
.canvas create text [expr {$Bx-8}] $By -anchor e -text i−2 -font TkHeadingFont
.canvas create text [expr {$Cx-8}] $Cy -anchor e -text i−1
.canvas create text [expr {$Nx-8}] $Ny -anchor e -text i -font TkHeadingFont -fill #44f
.canvas create text [expr {$Dx+8}] $Dy -anchor w -text i+1
.canvas create text [expr {$Ex+8}] $Ey -anchor w -text i+2 -font TkHeadingFont

.canvas create text 470 820 -anchor w -width 350 -text \
{⇒ A neighborhood of two points in each direction needs to be added to the redraw bounding box.
If the point to be added/removed is the first or last point of the line, a third neighboring point must be added.}

.canvas create text 26 900 -anchor w -text {Movement of points} -font TkHeadingFont
.canvas create text 26 920 -anchor w -text {• Moving the point at the start of the line:}
set Aoldx  80; set Aoldy 1050; set Bx 130; set By 950
set Cx    180; set Cy    1050; set Dx 230; set Dy 950
set Ex    280; set Ey    1050
set Anewx  80; set Anewy  980
foreach {x y} [list $Aoldx $Aoldy $Anewx $Anewy $Bx $By $Cx $Cy] {dot_redraw_needed $x $y}
.canvas create line [expr {0.8*$Aoldx + 0.2*$Anewx}] [expr {0.8*$Aoldy + 0.2*$Anewy}] \
                    [expr {0.2*$Aoldx + 0.8*$Anewx}] [expr {0.2*$Aoldy + 0.8*$Anewy}] \
                    -arrow last -fill #888
.canvas create line $Aoldx $Aoldy $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Anewx $Anewy $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey -smooth true -splinesteps 100 -width 2 -fill #44f
dot_userpoint_removed $Aoldx $Aoldy
dot_userpoint_changed $Anewx $Anewy
foreach {x y} [list $Bx $By $Cx $Cy $Dx $Dy $Ex $Ey] {dot_userpoint $x $y}
midpoint BCx BCy $Bx $By $Cx $Cy
midpoint CDx CDy $Cx $Cy $Dx $Dy
dot_midpoint $BCx $BCy; dot_midpoint $CDx $CDy
.canvas create text [expr {$Aoldx-8}] [expr {0.5*($Aoldy+$Anewy)}] -anchor e -text i -font TkHeadingFont -fill #888
.canvas create text [expr {$Bx+8}] $By -anchor w -text i+1
.canvas create text [expr {$Cx+8}] $Cy -anchor w -text i+2 -font TkHeadingFont

.canvas create text 26 1100 -anchor w -width 300 -text {• Moving the point one position apart from the start of the line:}
set Ax  80; set Ay 1230; set Boldx 130; set Boldy 1130
set Cx 180; set Cy 1230; set Dx    230; set Dy    1130
set Ex 280; set Ey 1230
set Bnewx 130; set Bnewy 1200
foreach {x y} [list $Ax $Ay $Boldx $Boldy $Bnewx $Bnewy $Cx $Cy $Dx $Dy] {dot_redraw_needed $x $y}
.canvas create line [expr {0.8*$Boldx + 0.2*$Bnewx}] [expr {0.8*$Boldy + 0.2*$Bnewy}] \
                    [expr {0.2*$Boldx + 0.8*$Bnewx}] [expr {0.2*$Boldy + 0.8*$Bnewy}] \
                    -arrow last -fill #888
.canvas create line $Ax $Ay $Boldx $Boldy $Cx $Cy $Dx $Dy $Ex $Ey -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Ax $Ay $Bnewx $Bnewy $Cx $Cy $Dx $Dy $Ex $Ey -smooth true -splinesteps 100 -width 2 -fill #44f
dot_userpoint_removed $Boldx $Boldy
dot_userpoint_changed $Bnewx $Bnewy
foreach {x y} [list $Ax $Ay $Cx $Cy $Dx $Dy $Ex $Ey] {dot_userpoint $x $y}
midpoint BoldCx BoldCy $Boldx $Boldy $Cx $Cy
midpoint BnewCx BnewCy $Bnewx $Bnewy $Cx $Cy
midpoint CDx CDy $Cx $Cy $Dx $Dy
dot_midpoint_removed $BoldCx $BoldCy
dot_midpoint_changed $BnewCx $BnewCy
dot_midpoint $CDx $CDy
.canvas create text [expr {$Ax-8}] $Ay -anchor e -text i‒1
.canvas create text [expr {$Boldx-8}] [expr {0.5*($Boldy+$Bnewy)}] -anchor se -text i -font TkHeadingFont -fill #888
.canvas create text [expr {$Cx+8}] $Cy -anchor w -text i+1
.canvas create text [expr {$Dx+8}] $Dy -anchor w -text i+2 -font TkHeadingFont

.canvas create line 325 910 325 1240 -fill #ccc

.canvas create text 340 920 -anchor w -text {• Moving the point two positions apart from the start of the line:}
set Ax    380; set Ay    1050; set Bx 430; set By 950
set Coldx 480; set Coldy 1050; set Dx 530; set Dy 950
set Ex    580; set Ey    1050; set Fx 630; set Fy 950
set Cnewx 480; set Cnewy  980
foreach {x y} [list $Ax $Ay $Bx $By $Coldx $Coldy $Cnewx $Cnewy $Dx $Dy $Ex $Ey] {dot_redraw_needed $x $y}
.canvas create line [expr {0.8*$Coldx + 0.2*$Cnewx}] [expr {0.8*$Coldy + 0.2*$Cnewy}] \
                    [expr {0.2*$Coldx + 0.8*$Cnewx}] [expr {0.2*$Coldy + 0.8*$Cnewy}] \
                    -arrow last -fill #888
.canvas create line $Ax $Ay $Bx $By $Coldx $Coldy $Dx $Dy $Ex $Ey $Fx $Fy -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Ax $Ay $Bx $By $Cnewx $Cnewy $Dx $Dy $Ex $Ey $Fx $Fy -smooth true -splinesteps 100 -width 2 -fill #44f
dot_userpoint_removed $Coldx $Coldy
dot_userpoint_changed $Cnewx $Cnewy
foreach {x y} [list $Ax $Ay $Bx $By $Dx $Dy $Ex $Ey $Fx $Fy] {dot_userpoint $x $y}
midpoint BColdx BColdy $Bx $By $Coldx $Coldy
midpoint BCnewx BCnewy $Bx $By $Cnewx $Cnewy
midpoint ColdDx ColdDy $Coldx $Coldy $Dx $Dy
midpoint CnewDx CnewDy $Cnewx $Cnewy $Dx $Dy
midpoint DEx DEy $Dx $Dy $Ex $Ey
dot_midpoint_removed $BColdx $BColdy; dot_midpoint_removed $ColdDx $ColdDy
dot_midpoint_changed $BCnewx $BCnewy; dot_midpoint_changed $CnewDx $CnewDy
dot_midpoint $DEx $DEy
.canvas create text [expr {$Ax-8}] $Ay -anchor e -text i−2 -font TkHeadingFont
.canvas create text [expr {$Bx-8}] $By -anchor e -text i−1
.canvas create text [expr {$Coldx+8}] [expr {0.5*($Coldy+$Cnewy)}] -anchor sw -text i -font TkHeadingFont -fill #888
.canvas create text [expr {$Dx+8}] $Dy -anchor w -text i+1
.canvas create text [expr {$Ex+8}] $Ey -anchor w -text i+2 -font TkHeadingFont

.canvas create text 340 1100 -anchor w -text {• Moving a point far inside the line:}
set Ax 380; set Ay 1130; set Bx    430; set By    1230
set Cx 480; set Cy 1130; set Doldx 530; set Doldy 1230
set Ex 580; set Ey 1130; set Fx    630; set Fy    1230
set Gx 680; set Gy 1130
set Dnewx $Doldx; set Dnewy 1160
foreach {x y} [list $Bx $By $Cx $Cy $Doldx $Doldy $Dnewx $Dnewy $Ex $Ey $Fx $Fy] {dot_redraw_needed $x $y}
.canvas create line [expr {0.8*$Doldx + 0.2*$Dnewx}] [expr {0.8*$Doldy + 0.2*$Dnewy}] \
                    [expr {0.2*$Doldx + 0.8*$Dnewx}] [expr {0.2*$Doldy + 0.8*$Dnewy}] \
                    -arrow last -fill #888
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Doldx $Doldy $Ex $Ey $Fx $Fy $Gx $Gy -smooth true -splinesteps 100 -width 2 -fill #f44
.canvas create line $Ax $Ay $Bx $By $Cx $Cy $Dnewx $Dnewy $Ex $Ey $Fx $Fy $Gx $Gy -smooth true -splinesteps 100 -width 2 -fill #44f
dot_userpoint_removed $Doldx $Doldy
dot_userpoint_changed $Dnewx $Dnewy
foreach {x y} [list $Ax $Ay $Bx $By $Cx $Cy $Ex $Ey $Fx $Fy $Gx $Gy] {dot_userpoint $x $y}
midpoint BCx BCy $Bx $By $Cx $Cy
midpoint CDoldx CDoldy $Cx $Cy $Doldx $Doldy
midpoint CDnewx CDnewy $Cx $Cy $Dnewx $Dnewy
midpoint DoldEx DoldEy $Doldx $Doldy $Ex $Ey
midpoint DnewEx DnewEy $Dnewx $Dnewy $Ex $Ey
midpoint EFx EFy $Ex $Ey $Fx $Fy
dot_midpoint $BCx $BCy
dot_midpoint_removed $CDoldx $CDoldy; dot_midpoint_removed $DoldEx $DoldEy
dot_midpoint_changed $CDnewx $CDnewy; dot_midpoint_changed $DnewEx $DnewEy
dot_midpoint $EFx $EFy
.canvas create text [expr {$Bx-8}] $By -anchor e -text i−2 -font TkHeadingFont
.canvas create text [expr {$Cx-8}] $Cy -anchor e -text i−1
.canvas create text [expr {$Doldx+8}] [expr {0.5*($Doldy+$Dnewy)}] -anchor sw -text i -font TkHeadingFont -fill #888
.canvas create text [expr {$Ex+8}] $Ey -anchor w -text i+1
.canvas create text [expr {$Fx+8}] $Fy -anchor w -text i+2 -font TkHeadingFont

.canvas create text 26 1260 -anchor nw -width 820 -text \
{⇒ Only a neighborhood of two points in each direction needs to be added to the redraw bounding box. Note that the old location of the point being moved needs to be added as well, otherwise the old line may not be sufficiently cleared.}

wm attributes . -zoomed 1