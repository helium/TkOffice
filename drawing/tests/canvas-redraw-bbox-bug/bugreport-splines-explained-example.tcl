#!/usr/bin/wish
# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)
# Bug observed with Tk versions 8.5.19 and 8.6.8

wm resizable . 0 0
pack [canvas .c -width 500 -height 250]
pack [label .l]

proc mark_point {x y} {
	.c create oval [expr {$x-2}] [expr {$y-2}] [expr {$x+2}] [expr {$y+2}]
}

proc exec_and_show {args} {
	{*}$args
	.l configure -text $args
}

mark_point 40 200
mark_point 160 40
mark_point 280 200
# Both kinds of smoothing are affected, here we use "true" since it
# needs less points than "raw" for a demonstration.
exec_and_show .c create line 40 200 160 40 280 200 -smooth true -tag myLine

update
after 5000
mark_point 400 40
exec_and_show .c insert myLine end {400 40}

update
after 2000
exec_and_show .c move myLine 0 0
