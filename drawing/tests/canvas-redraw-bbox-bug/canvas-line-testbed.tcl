#!/usr/bin/wish
# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

ttk::style theme use clam
. configure -background #dcdad5

ttk::frame .canvasframe
canvas .canvas -highlightthickness 0 -width 500 -height 300 -background white -closeenough 6
ttk::scrollbar .vscroll -orient vertical
ttk::scrollbar .hscroll -orient horizontal
.canvas configure -xscrollcommand {.hscroll set}
.canvas configure -yscrollcommand {.vscroll set}
.hscroll configure -command {.canvas xview}
.vscroll configure -command {.canvas yview}
grid .canvas -in .canvasframe -row 0 -column 0 -sticky nesw
grid .vscroll -in .canvasframe -row 0 -column 1 -sticky ns
grid .hscroll -in .canvasframe -row 1 -column 0 -sticky we
grid rowconfigure .canvasframe 0 -weight 1
grid columnconfigure .canvasframe 0 -weight 1

ttk::frame .settings
grid [ttk::label .settings.original_l -text {Original Points:}] -sticky w
grid [ttk::spinbox .settings.original -width 3 -from 0 -to Inf -textvariable original_n] -sticky w
grid [ttk::label .settings.additional_l -text {Additional Points:}] -sticky w
grid [ttk::spinbox .settings.additional -width 3 -from 0 -to Inf -textvariable additional_n] -sticky w
grid [ttk::label .settings.modify_at_l -text {Insert/Modify at:}] -sticky w
grid [ttk::spinbox .settings.modify_at -width 3 -from 0 -to 0 -textvariable modify_at] -sticky w
grid [ttk::button .settings.randomize -text {Randomize Positions}] -sticky w -pady {6 3}
grid [ttk::label .settings.type_l -text {Item Type:}] -sticky w
grid [ttk::frame .settings.type_f] -sticky w
ttk::radiobutton .settings.type_line -text Line -variable type -value line
ttk::radiobutton .settings.type_polygon -text Polygon -variable type -value polygon
grid .settings.type_line .settings.type_polygon -in .settings.type_f
grid [ttk::label .settings.smoothing_l -text {Smoothing Type:}] -sticky w
grid [ttk::frame .settings.smoothing_f] -sticky w
ttk::radiobutton .settings.smoothing_no -text no -variable smoothing -value no
ttk::radiobutton .settings.smoothing_true -text true -variable smoothing -value true
ttk::radiobutton .settings.smoothing_raw -text raw -variable smoothing -value raw
grid .settings.smoothing_no .settings.smoothing_true .settings.smoothing_raw \
	-in .settings.smoothing_f
grid [ttk::label .settings.operation_l -text Operation:] -sticky w
grid [ttk::frame .settings.operation_f] -sticky w
ttk::radiobutton .settings.operation_insdel -text Insert/Delete -variable operation -value insdel
ttk::radiobutton .settings.operation_modify -text Modify -variable operation -value modify
grid .settings.operation_insdel .settings.operation_modify -in .settings.operation_f
grid [ttk::label .settings.arrows_l -text Arrows:] -sticky w
grid [ttk::frame .settings.arrows_f] -sticky w
ttk::checkbutton .settings.arrows_start -text Start -variable arrows_start -onvalue on -offvalue off
ttk::checkbutton .settings.arrows_end -text End -variable arrows_end -onvalue on -offvalue off
grid .settings.arrows_start .settings.arrows_end -in .settings.arrows_f

ttk::frame .codeframe
grid [ttk::label .codeframe.data_l -text {Test Data: (use Ctrl-Return to apply)}] -sticky w
grid [text .data -width 0 -height 3 -wrap word] -in .codeframe -sticky nesw
grid [ttk::label .codeframe.code_l -text {Minimal code:}] -sticky w
grid [text .code -width 0 -height 5 -wrap word] -in .codeframe -sticky nesw
grid columnconfigure .codeframe 0 -weight 1

grid .canvasframe -row 0 -column 0 -sticky nesw
grid .settings -row 0 -column 1 -sticky nesw -padx {6 3}
grid .codeframe -row 1 -column 0 -columnspan 2 -sticky nesw
grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1


# "Modify" (rchars) operation not supported
if {[string compare $tk_version 8.6] < 0} {
	.settings.operation_l configure -state disabled
	.settings.operation_insdel configure -state disabled
	.settings.operation_modify configure -state disabled
}

# Start-up settings
set original_n 3
set additional_n 1
set modify_at 1
set type line
set smoothing true
set operation insdel
set arrows_start off
set arrows_end off
# Start-up points
set original_coords {384 50 445 267 2 176}
set additional_coords {421 78}

proc update_from_settings {} {
	global original_n additional_n modify_at
	global original_coords additional_coords
	global type smoothing operation arrows_start arrows_end
	global arrow arrowshape
	global animation_state

	after cancel animate

	switch $operation insdel {
		if {$modify_at > $original_n} {
			set modify_at $original_n
		} elseif {$modify_at < 0} {
			set modify_at 0
		}
		.settings.modify_at configure -from 0 -to $original_n
	} modify {
		if {$additional_n > $original_n} {
			set additional_n $original_n
		}
		if {$modify_at < 0} {
			set modify_at 0
		}
		if {$original_n < $modify_at + $additional_n} {
			set modify_at [expr {$original_n - $additional_n}]
		}
		.settings.modify_at configure -from 0 -to [expr {$original_n - $additional_n}]
	} default {
		error "bad operation \"$operation\": must be insdel or modify"
	}
	coords_adjust original_coords $original_n
	coords_adjust additional_coords $additional_n

	set state [expr {$type eq {line} ? {normal} : {disabled}}]
	.settings.arrows_l configure -state $state
	.settings.arrows_start configure -state $state
	.settings.arrows_end configure -state $state

	if {$arrows_start && $arrows_end} {
		set arrow both
	} elseif {$arrows_start && !$arrows_end} {
		set arrow first
	} elseif {!$arrows_start && $arrows_end} {
		set arrow last
	} else {
		set arrow none
	}
	set arrowshape {20 30 10}

	.canvas delete all
	switch $type line {
		.canvas create line 0 0 0 0 -tag oldLine -fill #fa8 -width 4 \
			-smooth $smoothing -arrow $arrow -arrowshape $arrowshape
		line_coords oldLine $original_coords
		.canvas create line 0 0 0 0 -tag newLine -fill #8fa -width 4 \
			-smooth $smoothing -arrow $arrow -arrowshape $arrowshape
		line_coords newLine $original_coords
		.canvas create line 0 0 0 0 -tag animatedLine -fill #00f -width 4 \
			-smooth $smoothing -arrow $arrow -arrowshape $arrowshape
		line_coords animatedLine $original_coords
	} polygon {
		.canvas create polygon $original_coords -tag oldLine -fill {} \
			-outline #fa8 -width 4 -smooth $smoothing
		.canvas create polygon $original_coords -tag newLine -fill {} \
			-outline #8fa -width 4 -smooth $smoothing
		.canvas create polygon $original_coords -tag animatedLine -fill {} \
			-outline #00f -width 4 -smooth $smoothing
	} default {
		error "invalid type \"$type\": must be line or polygon"
	}
	switch $operation insdel {
		.canvas insert newLine [expr {2*$modify_at}] $additional_coords
	} modify {
		set last [expr {2*$modify_at + 2*$additional_n - 1}]
		.canvas rchars newLine [expr {2*$modify_at}] $last $additional_coords
	}
	set r 3
	for {set i 0} {$i < 2*$original_n} {incr i 2} {
		lassign [lrange $original_coords $i $i+1] x y
		.canvas create oval [expr {$x-$r}] [expr {$y-$r}] [expr {$x+$r}] [expr {$y+$r}] \
			-fill white -outline black -width 1 -tag mark.original.$i
	}
	for {set i 0} {$i < 2*$additional_n} {incr i 2} {
		lassign [lrange $additional_coords $i $i+1] x y
		.canvas create oval [expr {$x-$r}] [expr {$y-$r}] [expr {$x+$r}] [expr {$y+$r}] \
			-fill black -outline black -width 1 -tag mark.additional.$i
	}
	if {$operation eq {modify}} {
		for {set i [expr {2*$modify_at}]} {$i < 2*($modify_at+$additional_n)} {incr i 2} {
			.canvas itemconfigure mark.original.$i -fill #fa8 -outline black
		}
	}
	.canvas configure -scrollregion [.canvas bbox all] -confine on

	update_code

	set animation_state 1
	after 1000 animate
}

proc update_code {} {
	global original_coords additional_n additional_coords modify_at
	global type smoothing operation arrow arrowshape

	.code delete 1.0 end
	.code insert end "pack \[canvas .c -width [winfo width .canvas] -height [winfo height .canvas]\]\n"

	switch $type line {
		.code insert end ".c create line $original_coords -tag myLine -width 4 -smooth $smoothing"
		if {$arrow ne {none}} {
			.code insert end " -arrow $arrow -arrowshape {$arrowshape}"
		}
	} polygon {
		.code insert end ".c create polygon $original_coords -tag myLine -width 4 -smooth $smoothing"
		.code insert end " -fill {} -outline black"
	} default {
		error "invalid type \"$type\": must be line or polygon"
	}

	.code insert end \n
	set first [expr {2*$modify_at}]
	set last [expr {2*$modify_at + 2*$additional_n - 1}]
	switch $operation insdel {
		.code insert end ".c insert myLine $first {$additional_coords}\n"
		.code insert end ".c dchars myLine $first $last"
	} modify {
		.code insert end ".c rchars myLine $first $last {$additional_coords}\n"
		.code insert end ".c rchars myLine $first $last {[lrange $original_coords $first $last]}"
	}
}

proc update_data_text {} {
	global original_coords additional_coords modify_at
	global type smoothing operation arrow

	.data delete 1.0 end
	.data insert end "original $original_coords\n"
	.data insert end "additional $additional_coords\n"
	.data insert end "index $modify_at; splinetype $smoothing; operation $operation; type $type"
	if {$type eq {line}} {
		.data insert end "; arrows $arrow"
	}
}

proc coords_adjust {coords_ n} {
	upvar $coords_ coords
	# Adapt coordinate list $coords to changed number of points $n
	if {[llength $coords] > 2*$n} {
		set coords [lrange $coords 0 [expr {2*$n-1}]]
	} else {
		while {[llength $coords] < 2*$n} {
			lappend coords {*}[random_point]
		}
	}
}

proc random_point {} {
	set w [winfo width .canvas]
	set h [winfo height .canvas]
	set x [expr {int(rand()*$w)}]
	set y [expr {int(rand()*$h)}]
	return [list $x $y]
}

proc line_coords {tag coords} {
	# Custom version of ".canvas coords $tag $coords" that tolerates
	# coordinate lists of lengths less than 4
	.canvas dchars $tag 0 end
	.canvas insert $tag 0 $coords
}

proc randomize {} {
	global original_coords additional_coords
	# The proc update_from_settings already fills the coordinate lists with random points
	# if they are shorter than their "_n" variable. We just need to empty the lists here!
	set original_coords [list]
	set additional_coords [list]
	update_from_settings
	update_data_text
}
.settings.randomize configure -command randomize

# Animation
proc animate {} {
	global animation_state
	animate$animation_state
	incr animation_state
	if {$animation_state > 4} {set animation_state 1}
	after 1000 animate
}
proc animate1 {} {
	global operation modify_at additional_coords additional_n
	if {$additional_n == 0} return
	set first [expr {2*$modify_at}]
	set last [expr {2*$modify_at + 2*$additional_n - 1}]
	switch $operation insdel {
		.canvas insert animatedLine $first $additional_coords
	} modify {
		.canvas rchars animatedLine $first $last $additional_coords
	}
}
proc animate2 {} {
	# force refresh of whole bounding box
	.canvas move animatedLine 0 0
}
proc animate3 {} {
	global operation modify_at original_coords additional_n
	if {$additional_n == 0} return
	set first [expr {2*$modify_at}]
	set last [expr {2*$modify_at + 2*$additional_n - 1}]
	switch $operation insdel {
		.canvas dchars animatedLine $first $last
	} modify {
		.canvas rchars animatedLine $first $last [lrange $original_coords $first $last]
	}
}
proc animate4 {} {
	# force refresh of whole bounding box
	.canvas move animatedLine 0 0
}


# Load canvas contents at start-up
bind .canvas <Configure> {
	update_from_settings
	update_data_text
	bind .canvas <Configure> {}
}
# Reload canvas contents on settings changed
foreach spinbox {.settings.original .settings.additional .settings.modify_at} {
	foreach event {<<Increment>> <<Decrement>>} {
		bind $spinbox $event [list event generate $spinbox <<ValueChanged>> -when head]
	}
	foreach event {<Return> <FocusOut> <<ValueChanged>>} {
		bind $spinbox $event {update_from_settings; update_data_text}
	}
}
foreach button {
	.settings.type_line .settings.type_polygon
	.settings.smoothing_no .settings.smoothing_true .settings.smoothing_raw
	.settings.operation_insdel .settings.operation_modify
	.settings.arrows_start .settings.arrows_end
} {
	$button configure -command {update_from_settings; update_data_text}
}

######### Loading from the "data" text field #########

namespace eval dataformat {
	proc original {args} {
		global original_coords original_n
		set original_coords $args
		set original_n [expr {[llength $args] / 2}]
	}
	proc additional {args} {
		global additional_coords additional_n
		set additional_coords $args
		set additional_n [expr {[llength $args] / 2}]
	}
	proc index {idx} {
		global modify_at
		set modify_at $idx
	}
	proc type {t} {
		global type
		set type $t
	}
	proc splinetype {type} {
		global smoothing
		set smoothing $type
	}
	proc operation {op} {
		global operation
		set operation $op
	}
	proc arrows {arrow} {
		global arrows_start arrows_end
		switch $arrow {
			none  {set arrows_start off; set arrows_end off}
			first {set arrows_start on;  set arrows_end off}
			last  {set arrows_start off; set arrows_end on}
			both  {set arrows_start on;  set arrows_end on}
			default {error "invalid arrow spec \"$arrow\": must be none, first, last, or both"}
		}
	}
}

proc setup_interp {} {
	set interp [interp create -safe]
	# Also remove the "harmless" commands from the interpreter,
	# as they are not part of our data format
	foreach cmd [$interp eval {info commands}] {$interp hide $cmd}

	foreach cmd {original additional index type splinetype operation arrows} {
		$interp alias $cmd ::dataformat::$cmd
	}
	return $interp
}

proc update_from_data {} {
	set interp [setup_interp]
	$interp eval [.data get 1.0 end-1c]
	interp delete $interp
	update_from_settings
}
bind .data <FocusOut> update_from_data
bind .data <Control-Return> {update_from_data; break}

######### Moving points with the mouse #########

bind .canvas <Motion> {canvas_motion %x %y}
bind .canvas <ButtonPress-1> {canvas_buttonPress %x %y}
bind .canvas <B1-Motion> {canvas_buttonMotion %x %y}
bind .canvas <ButtonRelease-1> canvas_buttonRelease

proc canvas_motion {x y} {
	set x [.canvas canvasx $x]
	set y [.canvas canvasy $y]
	if {[find_marktag $x $y] eq {}} {
		.canvas configure -cursor arrow
	} else {
		.canvas configure -cursor fleur
	}
}

proc canvas_buttonPress {x y} {
	global move_x move_y move_tag
	set x [.canvas canvasx $x]
	set y [.canvas canvasy $y]
	set marktag [find_marktag $x $y]
	if {$marktag ne {}} {
		set move_x $x
		set move_y $y
		set move_tag $marktag
	}
}

proc canvas_buttonMotion {x y} {
	global move_x move_y move_tag
	global original_coords additional_coords

	if {![info exists move_x]} return
	set x [.canvas canvasx $x]
	set y [.canvas canvasy $y]
	set dx [expr {int($x - $move_x)}]
	set dy [expr {int($y - $move_y)}]
	set move_x $x
	set move_y $y
	.canvas move $move_tag $dx $dy
	set idx [lindex [split $move_tag .] end]
	if {[string match mark.original.* $move_tag]} {
		lassign [lrange $original_coords $idx $idx+1] x y
		lset original_coords $idx [expr {$x+$dx}]
		lset original_coords $idx+1 [expr {$y+$dy}]
	} elseif {[string match mark.additional.* $move_tag]} {
		lassign [lrange $additional_coords $idx $idx+1] x y
		lset additional_coords $idx [expr {$x+$dx}]
		lset additional_coords $idx+1 [expr {$y+$dy}]
	} else {
		return
	}
	update_from_settings
	update_data_text
}

proc canvas_buttonRelease {} {
	global move_x move_y move_tag
	unset -nocomplain move_x move_y move_tag
}

proc find_marktag {x y} {
	set tags [.canvas itemcget current -tags]
	set marktag [lsearch -glob -inline $tags mark.*]
	return $marktag
}
