#!/usr/bin/wish8.5
# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

### Demo for interactive drawing using the Tk Canvas widget, ###
### featuring multi-selection of objects and multi-movement  ###
### of points.                                               ###

# Implementation of canvas-less vertically scrolling frame

proc make_scrolling_frame {container content} {
	global scrolling_frame_offset

	ttk::frame $container
	ttk::scrollbar $container.scroll -orient vertical -command \
		[list scroll_scrolling_frame $container $content]
	set scrolling_frame_offset($container) 0
	place $content -in $container -x 0 -y 0 -relwidth 1 -width 0
	bind $container <Configure> +[list configure_scrolling_frame $container $content]
	bind $content <Configure> +[list configure_scrolling_frame $container $content]

	raise $content $container
	return $container
}

proc configure_scrolling_frame {container content} {
	upvar #0 scrolling_frame_offset($container) offset

	set content_reqheight [winfo reqheight $content]
	set container_height [winfo height $container]

	if {$container_height >= $content_reqheight} {
		place configure $content -relwidth 1 -width 0
		place configure $content -y 0
		$container.scroll set 0 1
		place forget $container.scroll
		$container configure -width [winfo reqwidth $content]
		set offset 0
	} else {
		set offset [expr {max(0, min($offset, $content_reqheight - $container_height))}]
		set scrollbar_width [winfo reqwidth $container.scroll]
		place configure $content -relwidth 1 -width [expr {-$scrollbar_width}]
		place configure $content -y [expr {-$offset}]
		$container.scroll set [expr {$offset / double($content_reqheight)}] \
			[expr {($offset + $container_height) / double($content_reqheight)}]
		place $container.scroll -relx 1 -y 0 -relheight 1 -anchor ne
		set content_reqwidth [winfo reqwidth $content]
		$container configure -width [expr {$content_reqwidth + $scrollbar_width}]
	}
	grid configure $container
}

proc scroll_scrolling_frame {container content op amount args} {
	upvar #0 scrolling_frame_offset($container) offset

	set content_reqheight [winfo reqheight $content]
	set container_height [winfo height $container]

	if {$op eq {moveto}} {
		set offset [expr {int($amount * $content_reqheight)}]
	} elseif {$op eq {scroll}} {
		if {$args eq {units}} {
			;# Scroll increment (20) should be an option for make_scrolling_frame
			set unit 20
		} elseif {$args eq {pages}} {
			set unit $container_height
		} elseif {[llength $args] != 1} {
			error {wrong # args: should be "... scroll number units|pages"}
		} else {
			error "bad argument \"$args\": must be units or pages"
		}
		set offset [expr {$offset + int($amount * $unit)}]
	} else {
		error "unknown option \"$op\": must be moveto or scroll"
	}

	configure_scrolling_frame $container $content
}

# GUI build-up

ttk::style theme use clam
. configure -bg #dcdad5
ttk::style layout ThreestateButton.TCheckbutton [ttk::style layout Toolbutton]
ttk::style configure ThreestateButton.TCheckbutton {*}[ttk::style configure Toolbutton]
ttk::style map ThreestateButton.TCheckbutton {*}[ttk::style map Toolbutton]
ttk::style map ThreestateButton.TCheckbutton \
	-relief {disabled flat selected sunken pressed sunken active raised alternate solid} \
	-background {disabled #dcdad5 pressed #bab5ab selected #bab5ab active #eeebe7} \
	-lightcolor {pressed #bab5ab selected #bab5ab} \
	-darkcolor {pressed #bab5ab selected #bab5ab}

grid [ttk::frame .tools_outer] -sticky n
grid [ttk::frame .tools] -in .tools_outer -sticky nwe
grid [ttk::button .tools.select -text Select -style Toolbutton -command {tool_changed select}] -sticky nesw
grid [ttk::button .tools.points -text Points -style Toolbutton -command {tool_changed points}] -sticky nesw
grid [ttk::button .tools.rectangle -text Rect -style Toolbutton -command {tool_changed rectangle}] -sticky nesw
grid [ttk::button .tools.oval -text Oval -style Toolbutton -command {tool_changed oval}] -sticky nesw
grid [ttk::button .tools.arc -text Arc -style Toolbutton -command {tool_changed arc}] -sticky nesw
grid [ttk::button .tools.line -text Line -style Toolbutton -command {tool_changed line}] -sticky nesw
grid [ttk::button .tools.polygon -text Poly -style Toolbutton -command {tool_changed polygon}] -sticky nesw
grid [ttk::button .tools.text -text Text -style Toolbutton -command {tool_changed text}] -sticky nesw
# Item types not implemented: bitmap, image, window
grid columnconfigure .tools 0 -weight 1
after idle {set ::activetool select; .tools.select invoke}

grid [ttk::separator .tools_outer.separator1 -orient horizontal] -sticky we -padx 2 -pady 2
grid [ttk::frame .operations] -in .tools_outer -sticky nwe
grid [ttk::button .operations.duplicate -text Dup -style Toolbutton -command {points_duplicate_selected .canvas} -state disabled] -sticky nesw
grid [ttk::button .operations.midpoint -text Mid -style Toolbutton -command {points_create_midpoints .canvas} -state disabled] -sticky nesw
grid columnconfigure .operations 0 -weight 1

grid [ttk::separator .tools_outer.separator2 -orient horizontal] -sticky we -padx 2 -pady 2
grid [ttk::frame .raiselower] -in .tools_outer -sticky nwe
grid [ttk::button .raiselower.top -text Top -style Toolbutton -command {raise_selection_to_top .canvas} -state disabled] -sticky nesw
grid [ttk::button .raiselower.raise -text Raise -style Toolbutton -command {raise_selection .canvas} -state disabled] -sticky nesw
grid [ttk::button .raiselower.lower -text Lower -style Toolbutton -command {lower_selection .canvas} -state disabled] -sticky nesw
grid [ttk::button .raiselower.lowest -text Lowest -style Toolbutton -command {lower_selection_to_lowest .canvas} -state disabled] -sticky nesw
grid columnconfigure .raiselower 0 -weight 1

grid [canvas .canvas -background white -closeenough 4] -row 0 -column 1 -sticky nesw
.canvas configure -highlightcolor #6f9dc6 -highlightbackground #dcdad5  ;# ttk::entry has the same colors in the "clam" theme
grid [ttk::scrollbar .xscroll -orient horizontal] -row 1 -column 1 -sticky we
grid [ttk::scrollbar .yscroll -orient vertical] -row 0 -column 2 -sticky ns
.canvas configure -xscrollcommand {.xscroll set} -yscrollcommand {.yscroll set}
.xscroll configure -command {.canvas xview}
.yscroll configure -command {.canvas yview}
ttk::frame .properties
grid [make_scrolling_frame .properties_scroll .properties] -row 0 -column 3 -rowspan 2 -sticky nesw
grid [ttk::frame .statusbar] -row 2 -column 0 -columnspan 4 -sticky we
grid [ttk::label .statusbar.message -text {}] -padx 2 -pady 3
set statusbar_for(.canvas) .statusbar

grid rowconfigure . 0 -weight 1
grid columnconfigure . 1 -weight 1

proc points_operation_buttons {state} {
	foreach button [winfo children .operations] {
		$button configure -state $state
	}
}

proc tool_changed {new_tool} {
	tool_leave .canvas $::activetool
	foreach toolbutton [winfo children .tools] {
		if {[winfo name $toolbutton] eq $new_tool} {
			$toolbutton state selected
		} else {
			$toolbutton state !selected
		}
	}
	set ::activetool $new_tool
	tool_enter .canvas $::activetool
}

proc update_scrollregion {canvas} {
	$canvas configure -scrollregion [$canvas bbox all] -confine off
}

# Bindings that call the appropriate tool-specific procs

bind .canvas <Motion> {motion %W %x %y}
bind .canvas <ButtonPress-1> {left_button_press %W %x %y}
bind .canvas <Shift-ButtonPress-1> {left_button_press_shift %W %x %y}
bind .canvas <B1-Motion> {left_button_motion %W %x %y}
bind .canvas <ButtonRelease-1> {left_button_release %W %x %y}
bind .canvas <ButtonPress-3> {right_button_press %W %x %y}
bind .canvas <Escape> {escape %W}
bind .canvas <BackSpace> {backspace %W %x %y}
bind .canvas <Delete> {delete_key %W}
bind .canvas <Left> {left_key %W}
bind .canvas <Right> {right_key %W}
bind .canvas <Control-v> {control_v %W}

bind .canvas <ButtonPress-2> {%W scan mark %x %y}
bind .canvas <B2-Motion> {%W scan dragto %x %y 1}

bind .canvas <KeyPress> {key_press %W %A}
# <Return> and <KP_Enter> would otherwise produce %A as \r instead of \n.
bind .canvas <Return> {key_press %W \n}
bind .canvas <KP_Enter> {key_press %W \n}
# Ignore all Alt, Meta, and Control keypresses unless explicitly bound.
# Otherwise, if a widget binding for one of these is defined, the
# <KeyPress> class binding will also fire and insert the character,
# which is wrong.
bind .canvas <Alt-KeyPress> {return}
bind .canvas <Meta-KeyPress> {return}
bind .canvas <Control-KeyPress> {return}
bind .canvas <Mod4-KeyPress> {return}
if {[tk windowingsystem] eq "aqua"} {
	bind .canvas <Command-KeyPress> {return}
}

proc tool_enter {canvas tool} {
	switch $::activetool {
		select {$canvas configure -cursor arrow}
		points {$canvas configure -cursor dotbox; points_operation_buttons normal}
		rectangle - oval - arc - line - polygon {$canvas configure -cursor crosshair}
		text {$canvas configure -cursor xterm}
	}
}

proc tool_leave {canvas tool} {
	switch $::activetool {
		select - points - line - polygon - text {tool_leave_$::activetool $canvas}
	}
}

proc motion {canvas x y} {
	switch $::activetool {
		line - polygon - text {motion_$::activetool $canvas $x $y}
	}
}

proc left_button_press {canvas x y} {
	switch $::activetool {
		select - points - rectangle - oval - arc - line - polygon - text {left_button_press_$::activetool $canvas $x $y}
	}
}

proc left_button_press_shift {canvas x y} {
	switch $::activetool {
		select - points {left_button_press_shift_$::activetool $canvas $x $y}
		rectangle - oval - arc - line - polygon {left_button_press_$::activetool $canvas $x $y}
	}
}

proc left_button_motion {canvas x y} {
	switch $::activetool {
		select - points - rectangle - oval - arc - text {left_button_motion_$::activetool $canvas $x $y}
		line - polygon {motion_$::activetool $canvas $x $y}
	}
}

proc left_button_release {canvas x y} {
	switch $::activetool {
		select - points - rectangle - oval - arc - text {left_button_release_$::activetool $canvas $x $y}
	}
}

proc right_button_press {canvas x y} {
	switch $::activetool {
		rectangle - oval - arc - line - polygon {right_button_press_$::activetool $canvas $x $y}
	}
}

proc escape {canvas} {
	switch $::activetool {
		select - rectangle - oval - arc - line - polygon - text {escape_$::activetool $canvas}
	}
}

proc backspace {canvas x y} {
	switch $::activetool {
		select - points - text {backspace_$::activetool $canvas}
		line - polygon {backspace_$::activetool $canvas $x $y}
	}
}

proc delete_key {canvas} {
	switch $::activetool {
		text {delete_key_$::activetool $canvas}
	}
}

proc left_key {canvas} {
	switch $::activetool {
		text {left_key_$::activetool $canvas}
	}
}

proc right_key {canvas} {
	switch $::activetool {
		text {right_key_$::activetool $canvas}
	}
}

proc key_press {canvas character} {
	switch $::activetool {
		text {key_press_$::activetool $canvas $character}
	}
}

proc control_v {canvas} {
	switch $::activetool {
		text {control_v_$::activetool $canvas}
	}
}

# Prerequisites for Select tool

set sel_dot_size 4
set sel_color1 black
set sel_color2 #777

proc select_draw_marks {canvas item} {
	set already_existing [expr {[$canvas find withtag mark.$item] ne {}}]
	lassign [$canvas bbox $item] x1 y1 x2 y2
	set size $::sel_dot_size
	foreach {rx1 ry1 rx2 ry2} [list \
		[expr {$x1 - $size}] [expr {$y1 - $size}] $x1 $y1 \
		[expr {$x1 - $size}] $y2 $x1 [expr {$y2 + $size}] \
		$x2 [expr {$y1 - $size}] [expr {$x2 + $size}] $y1 \
		$x2 $y2 [expr {$x2 + $size}] [expr {$y2 + $size}] \
	] dir {mark.nw mark.sw mark.ne mark.se} {
		if {$already_existing} {
			foreach mark [$canvas find withtag "mark.$item && $dir"] {
				$canvas coords $mark $rx1 $ry1 $rx2 $ry2
			}
			continue
		}
		# As Tk does not have "xor" fill mode, make a pattern that should be visible
		# on all possible backgrounds (except the very exact stipple we are creating).
		$canvas create rectangle $rx1 $ry1 $rx2 $ry2 -tags [list mark mark.$item $dir] \
			-fill $::sel_color1 -outline $::sel_color1 \
			-stipple gray50 -outlinestipple gray50 -offset 0,0 -outlineoffset 0,0
		$canvas create rectangle $rx1 $ry1 $rx2 $ry2 -tags [list mark mark.$item $dir] \
			-fill $::sel_color2 -outline $::sel_color2 \
			-stipple gray50 -outlinestipple gray50 -offset 0,1 -outlineoffset 0,1
	}
}

proc select_mark_item {canvas item} {
	if {$item eq {}} return
	if {"mark" in [$canvas gettags $item]} return

	$canvas addtag selection withtag $item
	event generate $canvas <<SelectionUpdate>> -when head
	select_draw_marks $canvas $item
}

proc select_unmark_item {canvas item} {
	if {$item eq {}} return
	$canvas dtag $item selection
	$canvas delete withtag mark.$item
	event generate $canvas <<SelectionUpdate>> -when head
}

proc select_unmark_all {canvas} {
	$canvas dtag selection
	$canvas delete withtag mark
	event generate $canvas <<SelectionUpdate>> -when head
}

proc select_toggle_mark_item {canvas item} {
	if {$item eq {}} return
	if {"selection" ni [$canvas gettags $item]} {
		select_mark_item $canvas $item
	} else {
		select_unmark_item $canvas $item
	}
}

proc select_delete_marked {canvas} {
	$canvas delete withtag selection
	$canvas delete withtag mark
	event generate $canvas <<SelectionUpdate>> -when head
	update_scrollregion $canvas
}

# Prerequisites for moving objects or points

proc move_start {canvas x y} {
	set ::x_move_last($canvas) $x
	set ::y_move_last($canvas) $y

	set ::oldcursor($canvas) [$canvas cget -cursor]
	if {$::oldcursor($canvas) ne "dotbox"} {
	# we want to keep the cursor for the points tool
		$canvas configure -cursor fleur
	}
}

proc is_moving {canvas} {
	info exists ::x_move_last($canvas)
}

proc move_end {canvas} {
	unset -nocomplain ::x_move_last($canvas)
	unset -nocomplain ::y_move_last($canvas)
	update_scrollregion $canvas

	if {[$canvas cget -cursor] eq "fleur"} {
		$canvas configure -cursor $::oldcursor($canvas)
		unset ::oldcursor($canvas)
	}
}

# Select tool

proc left_button_press_select {canvas x y} {
	focus $canvas
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	if {"selection" in [$canvas gettags current]} {
		move_start $canvas $x $y
	} else {
		select_unmark_all $canvas
		set items [$canvas find withtag current]
		select_mark_item $canvas $items
		if {$items ne {}} {
			move_start $canvas $x $y
		}
	}
}

proc left_button_motion_select {canvas x y} {
	if {! [is_moving $canvas]} return

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	set dx [expr {$x - $::x_move_last($canvas)}]
	set dy [expr {$y - $::y_move_last($canvas)}]
	$canvas move selection $dx $dy
	$canvas move mark $dx $dy
	set ::x_move_last($canvas) $x
	set ::y_move_last($canvas) $y
}

proc left_button_release_select {canvas x y} {
	move_end $canvas
}

proc left_button_press_shift_select {canvas x y} {
	focus $canvas
	select_toggle_mark_item $canvas [$canvas find withtag current]
}

proc backspace_select {canvas} {
	select_delete_marked $canvas
}

proc escape_select {canvas} {
	select_unmark_all $canvas
}

proc tool_leave_select {canvas} {
	select_unmark_all $canvas
}

# Prerequisites for Points tool

proc points_mark_item {canvas item} {
	if {$item eq {}} return
	set tags [$canvas gettags $item]
	if {"mark" in $tags} return
	if {"selection" in $tags} return

	$canvas addtag selection withtag $item
	event generate $canvas <<SelectionUpdate>> -when head

	set s $::sel_dot_size
	set i 0
	foreach {x y} [$canvas coords $item] {
		set point_coords [list [expr {$x-$s}] [expr {$y-$s}] [expr {$x+$s}] [expr {$y+$s}]]
		$canvas create rectangle $point_coords -tags [list mark mark.$item mark.$item.$i] \
			-outline $::sel_color1 -outlinestipple gray50 -outlineoffset 0,0
		$canvas create rectangle $point_coords -tags [list mark mark.$item mark.$item.$i] \
			-outline $::sel_color2 -outlinestipple gray50 -outlineoffset 0,1
		incr i
	}
}

proc points_unmark_item {canvas item} {
	if {$item eq {}} return
	$canvas dtag $item selection
	$canvas delete withtag mark.$item
	event generate $canvas <<SelectionUpdate>> -when head
}

proc points_unmark_all {canvas} {
	$canvas dtag selection
	$canvas delete withtag mark
	event generate $canvas <<SelectionUpdate>> -when head
}

proc points_item_is_marked {canvas item} {
	if {$item eq {}} {return 0}
	expr {"selection" in [$canvas gettags $item]}
}

proc points_activate_point {canvas mark} {
	# "mark" argument must be of form mark.$item.$point e.g. mark.23.5
	$canvas addtag selection withtag $mark
	$canvas itemconfigure $mark -width 2
}

proc points_deactivate_point {canvas mark} {
	# "mark" argument must be of form mark.$item.$point e.g. mark.23.5
	$canvas dtag $mark selection
	$canvas itemconfigure $mark -width 1
}

proc points_deactivate_all {canvas} {
	$canvas dtag mark selection
	$canvas itemconfigure mark -width 1
}

proc points_are_activated {canvas} {
	expr {[$canvas find withtag {selection && mark}] ne {}}
}

proc points_move_init {canvas} {
	foreach item [$canvas find withtag {selection && !mark}] {
		# This storage is required for "rectangle-like" objects (rectangle, oval, arc)
		# where the coordinate list may be reordered by the Tk canvas, namely if
		# the first point has larger x or y coordinate than the second point.
		dict set ::prev_coords($canvas) $item [$canvas coords $item]
	}
}

proc points_move_deinit {canvas} {
	if {! [info exists ::prev_coords($canvas)]} return

	foreach item [$canvas find withtag {selection && !mark}] {
		if {[$canvas type $item] in {rectangle oval arc}} {
			# detect reordered coordinate list
			if {[$canvas coords $item] != [dict get $::prev_coords($canvas) $item]} {
				# redraw marks, unselecting them as a side effect
				# (but if both of them were selected, we don't get here)
				points_unmark_item $canvas $item
				points_mark_item $canvas $item
			}
		}
	}
	unset -nocomplain ::prev_coords($canvas)
}

proc points_move_selected_coords {canvas dx dy} {
	foreach item [$canvas find withtag {selection && !mark}] {
		set coords [dict get $::prev_coords($canvas) $item]
		set prev_tag {}
		foreach mark [$canvas find withtag "selection && mark.$item"] {
			$canvas move $mark $dx $dy

			set tag [lsearch -glob -inline [$canvas gettags $mark] mark.*.*]
			# There are two rectangles for each point, don't move the point twice
			if {$tag eq $prev_tag} continue
			set prev_tag $tag

			set idx [lindex [split $tag .] end]  ;# e.g. 5 for mark.23.5
			set idx [expr {$idx*2}]
			lset coords $idx [expr {[lindex $coords $idx] + $dx}]
			incr idx
			lset coords $idx [expr {[lindex $coords $idx] + $dy}]
		}
		$canvas coords $item $coords  ;# apply changed coords
		dict set ::prev_coords($canvas) $item $coords
	}
}

proc points_selected_indices_of {canvas item} {
	set selected_indices [list]
	foreach mark [$canvas find withtag "selection && mark.$item"] {
		set tag [lsearch -glob -inline [$canvas gettags $mark] mark.*.*]
		# There are two "mark" rectangles for each point, keep both (cf. lsort -unique)
		lappend selected_indices [lindex [split $tag .] end]  ;# e.g. 5 for mark.23.5
	}
	return [lsort -integer -decreasing -unique $selected_indices]
}

proc points_delete_selected {canvas} {
	foreach item [$canvas find withtag {selection && !mark}] {
		set selected_indices [points_selected_indices_of $canvas $item]
		if {[llength [$canvas coords $item]] == 2*[llength $selected_indices]} {
			points_unmark_item $canvas $item
			$canvas delete $item
		} elseif {[points_ok_to_delete $canvas $item $selected_indices]} {
			points_unmark_item $canvas $item  ;# many vertex indices will differ after deleting some!
			foreach index $selected_indices {
				$canvas dchars $item [expr {2*$index}]
			}
			points_mark_item $canvas $item
		}
	}
}

proc points_ok_to_delete {canvas item selected_indices} {
	set type [$canvas type $item]
	set min_vertices [switch $type {
		rectangle - oval - arc {expr 2}
		line {expr 2}
		polygon {expr 3}
		text {expr 1}
	}]
	set vertices_present [expr {[llength [$canvas coords $item]] >> 1}]  ;# / 2
	set vertices_to_keep [expr {$vertices_present - [llength $selected_indices]}]
	expr {$vertices_to_keep >= $min_vertices}
}

proc points_duplicate_selected {canvas} {
	foreach item [$canvas find withtag {selection && !mark}] {
		if {[$canvas type $item] ni {line polygon}} continue
		set selected_indices [points_selected_indices_of $canvas $item]

		points_unmark_item $canvas $item
		set coords [$canvas coords $item]
		foreach index $selected_indices {
			set index2 [expr {2*$index}]
			$canvas insert $item $index2 [lrange $coords $index2 [expr {$index2 + 1}]]
		}
		points_mark_item $canvas $item
	}
}

proc points_create_midpoints {canvas} {
	foreach item [$canvas find withtag {selection && !mark}] {
		if {[$canvas type $item] ni {line polygon}} continue
		set selected_indices [points_selected_indices_of $canvas $item]

		set marks_present yes  ;# preserve mark selection state in case of a no-op
		set coords [$canvas coords $item]
		foreach index $selected_indices {
			if {($index-1) ni $selected_indices} continue  ;# need two adjacent vertices
			if {$marks_present} {
				points_unmark_item $canvas $item
				set marks_present no
			}
			set index2 [expr {2 * ($index-1)}]
			lassign [lrange $coords $index2 [expr {$index2 + 3}]] x1 y1 x2 y2
			set new_x [expr {0.5 * ($x1 + $x2)}]
			set new_y [expr {0.5 * ($y1 + $y2)}]
			$canvas insert $item [expr {$index2 + 2}] [list $new_x $new_y]
		}
		if {[$canvas type $item] eq "polygon"} {
			set last_coord [expr {([llength $coords] >> 1) - 1}]  ;# >>1 == /2
			if {$last_coord in $selected_indices && 0 in $selected_indices} {
				if {$marks_present} {
					points_unmark_item $canvas $item
					set marks_present no
				}
				lassign [lrange $coords 0 1] x1 y1
				lassign [lrange $coords end-1 end] x2 y2
				set new_x [expr {0.5 * ($x1 + $x2)}]
				set new_y [expr {0.5 * ($y1 + $y2)}]
				$canvas insert $item end [list $new_x $new_y]
			}
		}
		if {! $marks_present} {
			points_mark_item $canvas $item
		}
	}
}

proc points_mark_hit_at {canvas x y} {
	# $x and $y must have been processed by canvasx and canvasy, respectively.

	# As the point marks are realized as non-filled rectangles, we need to do an overlap search
	# in order to hit them also when the cursor is inside one of them (not on its outline).
	set s $::sel_dot_size
	set searchbox [list [expr {$x-$s}] [expr {$y-$s}] [expr {$x+$s}] [expr {$y+$s}]]
	set overlapping [$canvas find overlapping {*}$searchbox]
	set really_overlapping_rectangles [list]
	foreach item $overlapping {
		if {[$canvas type $item] eq "rectangle"} {
			lassign [$canvas coords $item] x1 y1 x2 y2
			if {$x1 <= $x && $x <= $x2  &&  $y1 <= $y && $y <= $y2} {
				lappend really_overlapping_rectangles $item
			}
		}
	}
	# Highest stacking order, since marks have been drawn last
	set potential_mark [lindex $really_overlapping_rectangles end]
	return $potential_mark
}

# Points tool

proc left_button_press_points {canvas x y} {
	focus $canvas
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	set potential_mark [points_mark_hit_at $canvas $x $y]
	set tags [$canvas gettags $potential_mark]
	if {$potential_mark ne {} && "mark" in $tags} {
		if {"selection" in $tags} {
			# point already selected
			move_start $canvas $x $y
			points_move_init $canvas
		} else {
			set ::single_point_moved($canvas) no
			set mark [lsearch -glob -inline $tags mark.*.*]
			points_deactivate_all $canvas
			points_activate_point $canvas $mark
			move_start $canvas $x $y
			points_move_init $canvas
		}
	} else {
		points_unmark_all $canvas
		points_mark_item $canvas [$canvas find withtag current]
	}
}

proc left_button_press_shift_points {canvas x y} {
	focus $canvas
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	set potential_mark [points_mark_hit_at $canvas $x $y]
	set tags [$canvas gettags $potential_mark]
	if {$potential_mark ne {} && "mark" in $tags} {
		set mark [lsearch -glob -inline $tags mark.*.*]
		if {"selection" ni $tags} {
			points_activate_point $canvas $mark
		} else {
			points_deactivate_point $canvas $mark
		}
	} else {
		set item [$canvas find withtag current]
		if {"selection" ni [$canvas gettags $item]} {
			points_mark_item $canvas $item
		} else {
			points_unmark_item $canvas $item
		}
	}
}

proc left_button_motion_points {canvas x y} {
	if {! [is_moving $canvas]} return

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	set dx [expr {$x - $::x_move_last($canvas)}]
	set dy [expr {$y - $::y_move_last($canvas)}]
	points_move_selected_coords $canvas $dx $dy
	set ::x_move_last($canvas) $x
	set ::y_move_last($canvas) $y

	if {[info exists ::single_point_moved($canvas)]} {
		set ::single_point_moved($canvas) yes
	}
}

proc left_button_release_points {canvas x y} {
	move_end $canvas
	points_move_deinit $canvas
	if {[info exists ::single_point_moved($canvas)]} {
		if {$::single_point_moved($canvas)} {
			points_deactivate_all $canvas
		}
		unset ::single_point_moved($canvas)
	}
}

proc backspace_points {canvas} {
	points_delete_selected $canvas
}

proc tool_leave_points {canvas} {
	points_unmark_all $canvas
	points_operation_buttons disabled
}

# Generic procedures for Rectangle and Oval tool

proc rectanglelike_create {canvas x y type} {
	focus $canvas
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	$canvas create $type $x $y $x $y -tags item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	set ::x_origin($canvas) $x
	set ::y_origin($canvas) $y
}

proc rectanglelike_reshape {canvas x y} {
	# User has cancelled drawing (e.g. by Escape key)?
	if {! [info exists ::x_origin($canvas)]} return

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	$canvas coords item_being_created $::x_origin($canvas) $::y_origin($canvas) $x $y
}

proc rectanglelike_finish {canvas} {
	$canvas dtag item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	unset -nocomplain ::x_origin($canvas)
	unset -nocomplain ::y_origin($canvas)
	update_scrollregion $canvas
}

proc rectanglelike_cancel {canvas} {
	$canvas delete item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	unset -nocomplain ::x_origin($canvas)
	unset -nocomplain ::y_origin($canvas)
}

# Rectangle tool

proc left_button_press_rectangle {canvas x y} {
	rectanglelike_create $canvas $x $y rectangle
}

proc left_button_motion_rectangle {canvas x y} {
	rectanglelike_reshape $canvas $x $y
}

proc left_button_release_rectangle {canvas x y} {
	rectanglelike_finish $canvas
}

proc right_button_press_rectangle {canvas x y} {
	rectanglelike_cancel $canvas
}

proc escape_rectangle {canvas} {
	rectanglelike_cancel $canvas
}

# Oval tool

proc left_button_press_oval {canvas x y} {
	rectanglelike_create $canvas $x $y oval
}

proc left_button_motion_oval {canvas x y} {
	rectanglelike_reshape $canvas $x $y
}

proc left_button_release_oval {canvas x y} {
	rectanglelike_finish $canvas
}

proc right_button_press_oval {canvas x y} {
	rectanglelike_cancel $canvas
}

proc escape_oval {canvas} {
	rectanglelike_cancel $canvas
}

# Arc tool

proc left_button_press_arc {canvas x y} {
	rectanglelike_create $canvas $x $y arc
	$canvas itemconfigure item_being_created -extent 270.001
}

proc left_button_motion_arc {canvas x y} {
	rectanglelike_reshape $canvas $x $y
}

proc left_button_release_arc {canvas x y} {
	rectanglelike_finish $canvas
}

proc right_button_press_arc {canvas x y} {
	rectanglelike_cancel $canvas
}

proc escape_arc {canvas} {
	rectanglelike_cancel $canvas
}

# Generic procedures for Line and Polygon tool

proc polygonlike_is_being_created {canvas} {
	info exists ::polygonlike_being_created($canvas)
}

proc polygonlike_create {canvas x y type args} {
	if {[polygonlike_is_being_created $canvas]} return

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	$canvas create $type $x $y $x $y {*}$args -tags item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	set ::polygonlike_being_created($canvas) yes
}

proc polygonlike_delete_last_point {canvas} {
	if {! [polygonlike_is_being_created $canvas]} return

	set num_coords [$canvas index item_being_created end]
	if {$num_coords < 4} {
		polygonlike_abort $canvas
	}
	$canvas dchars item_being_created [expr {$num_coords - 2}] [expr {$num_coords - 1}]
}

proc polygonlike_add_point_as_last {canvas x y} {
	if {! [polygonlike_is_being_created $canvas]} return

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	$canvas insert item_being_created end [list $x $y]

	# Work around Tk bug #5fb8145997
	# (Canvas redraw region too small when inserting points to a smoothed line)
	if {[$canvas type item_being_created] eq {line} &&
	    [$canvas itemcget item_being_created -smooth] ne 0} {
		$canvas move item_being_created 0 0  ;# force redraw of entire item
	}
}

proc polygonlike_move_last_point {canvas x y} {
	if {! [polygonlike_is_being_created $canvas]} return
	focus $canvas
	polygonlike_delete_last_point $canvas
	polygonlike_add_point_as_last $canvas $x $y
}

proc polygonlike_finish {canvas} {
	if {! [polygonlike_is_being_created $canvas]} return
	focus $canvas

	set type [$canvas type item_being_created]
	switch $type {
		line {set min 4}
		polygon {set min 6}
		default {error "unexpected object type '$type': should be line or polygon"}
	}
	if {[$canvas index item_being_created end] < $min} {
		# too few points in coordinate list
		polygonlike_abort $canvas
		return
	}

	$canvas dtag item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	unset ::polygonlike_being_created($canvas)
	update_scrollregion $canvas
}

proc polygonlike_abort {canvas} {
	$canvas delete item_being_created
	event generate $canvas <<SelectionUpdate>> -when head
	unset -nocomplain ::polygonlike_being_created($canvas)
}

proc polygonlike_click {canvas x y type args} {
	focus $canvas
	if {[polygonlike_is_being_created $canvas]} {
		polygonlike_add_point_as_last $canvas $x $y
	} else {
		polygonlike_create $canvas $x $y $type {*}$args
	}
}

# Line tool

proc left_button_press_line {canvas x y} {
	polygonlike_click $canvas $x $y line
}

proc motion_line {canvas x y} {
	polygonlike_move_last_point $canvas $x $y
}

proc backspace_line {canvas x y} {
	polygonlike_delete_last_point $canvas
	polygonlike_move_last_point $canvas $x $y
}

proc right_button_press_line {canvas x y} {
	polygonlike_finish $canvas
}

proc escape_line {canvas} {
	polygonlike_abort $canvas
}

proc tool_leave_line {canvas} {
	polygonlike_abort $canvas
}

# Polygon tool

proc left_button_press_polygon {canvas x y} {
	polygonlike_click $canvas $x $y polygon -width 1 -outline black
}

proc motion_polygon {canvas x y} {
	polygonlike_move_last_point $canvas $x $y
}

proc backspace_polygon {canvas x y} {
	polygonlike_delete_last_point $canvas
	polygonlike_move_last_point $canvas $x $y
}

proc right_button_press_polygon {canvas x y} {
	polygonlike_finish $canvas
}

proc escape_polygon {canvas} {
	polygonlike_abort $canvas
}

proc tool_leave_polygon {canvas} {
	polygonlike_abort $canvas
}

# Prerequisites for Text tool

set text_anchormark_size 2

proc text_anchormark_set {canvas x y} {
	set s $::text_anchormark_size
	set point_coords [list [expr {$x-$s}] [expr {$y-$s}] [expr {$x+$s}] [expr {$y+$s}]]
	set anchormarks [$canvas find withtag anchormark]
	if {$anchormarks ne {}} {
		foreach item $anchormarks {
			$canvas coords $item $point_coords
		}
	} else {
		$canvas create rectangle $point_coords -tags anchormark \
			-outline $::sel_color1 -outlinestipple gray50 -outlineoffset 0,0
		$canvas create rectangle $point_coords -tags anchormark \
			-outline $::sel_color2 -outlinestipple gray50 -outlineoffset 0,1
	}
}

proc text_anchormark_clear {canvas} {
	$canvas delete anchormark
}

proc text_anchormark_touching {canvas x y} {
	if {[$canvas find withtag anchormark] eq {}} {
		return false
	}
	lassign [$canvas coords anchormark] x1 y1 x2 y2
	expr {$x1 <= $x && $x <= $x2 && $y1 <= $y && $y <= $y2}
}

proc text_delete_if_empty {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	# Also true for empty string:
	if {[string is space [$canvas itemcget $text -text]]} {
		$canvas delete $text
		event generate $canvas <<SelectionUpdate>> -when head
	}
}

proc text_index_at {canvas textitem x y} {
	# The dependency of [$canvas index ... @x,y] on the scrollregion
	# (not just the canvasx,canvasy coordinate system) seems to be a Tk bug.
	set scrollregion [$canvas cget -scrollregion]
	if {$scrollregion ne {}} {
		lassign $scrollregion x1 y1
	} else {
		set x1 0
		set y1 0
	}
	$canvas index $textitem @[expr {$x-$x1}],[expr {$y-$y1}]
}

# Text tool

proc motion_text {canvas x y} {
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	if {[text_anchormark_touching $canvas $x $y]} {
		$canvas configure -cursor fleur
	} else {
		$canvas configure -cursor xterm
	}
}

proc left_button_press_text {canvas x y} {
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	focus $canvas
	set text [$canvas find withtag current]
	if {[text_anchormark_touching $canvas $x $y]} {
		$canvas focus {}  ;# remove blinking cursor
		move_start $canvas $x $y
	} elseif {$text ne {} && [$canvas type $text] eq "text"} {
		text_delete_if_empty $canvas
		text_anchormark_set $canvas {*}[$canvas coords $text]
		$canvas dtag selection
		$canvas addtag selection withtag $text
		event generate $canvas <<SelectionUpdate>> -when head
		$canvas focus $text
		$canvas icursor $text [text_index_at $canvas $text $x $y]
	} else {
		text_delete_if_empty $canvas
		text_anchormark_clear $canvas
		$canvas dtag selection

		set default_font [font configure TkDefaultFont]
		set text [$canvas create text $x $y -font $default_font -anchor w -tags selection]
		event generate $canvas <<SelectionUpdate>> -when head
		text_anchormark_set $canvas $x $y
		$canvas focus $text
	}
}

proc left_button_motion_text {canvas x y} {
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	if {[is_moving $canvas]} {
		set dx [expr {$x - $::x_move_last($canvas)}]
		set dy [expr {$y - $::y_move_last($canvas)}]
		$canvas move selection $dx $dy
		$canvas move anchormark $dx $dy
		set ::x_move_last($canvas) $x
		set ::y_move_last($canvas) $y
	}
}

proc left_button_release_text {canvas x y} {
	if {[is_moving $canvas]} {
		move_end $canvas
		$canvas focus selection
	}
}

proc key_press_text {canvas character} {
	if {$character eq {}} return
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	$canvas insert $text insert $character
	update_scrollregion $canvas
}

proc backspace_text {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	set index [$canvas index $text insert]
	if {$index < 1} return
	$canvas dchars $text [expr {$index-1}]
	update_scrollregion $canvas
}

proc delete_key_text {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	$canvas dchars $text insert
	update_scrollregion $canvas
}

proc left_key_text {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	set index [$canvas index $text insert]
	if {$index < 1} return
	$canvas icursor $text [expr {$index - 1}]
}

proc right_key_text {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	set index [$canvas index $text insert]
	$canvas icursor $text [expr {$index + 1}]
}

proc escape_text {canvas} {
	text_delete_if_empty $canvas
	text_anchormark_clear $canvas
	$canvas dtag selection
	event generate $canvas <<SelectionUpdate>> -when head
	$canvas focus {}
}

proc control_v_text {canvas} {
	if {[catch {clipboard get} clipboard]} {
		return  ;# There were no clipboard contents at all
	}
	if {$clipboard eq {}} return
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	$canvas insert $text insert $clipboard
	update_scrollregion $canvas
}

proc tool_leave_text {canvas} {
	text_delete_if_empty $canvas
	text_anchormark_clear $canvas
	$canvas dtag selection
	event generate $canvas <<SelectionUpdate>> -when head
	$canvas focus {}
}

# Operations changing the stacking order of items

proc raise_selection_to_top {canvas} {
	$canvas raise {(selection && !mark) || item_being_created}
}

proc raise_selection {canvas} {
	set searchSpec {(selection && !mark) || item_being_created}
	set selection [$canvas find withtag $searchSpec]
	if {$selection eq {}} return
	set topSelected [lindex $selection end]; unset selection

	# Try to skip "raise" operations that change nothing in the drawing:
	# Raise above an item that is actually overlapping the selection.
	set overlapping [$canvas find overlapping {*}[$canvas bbox $searchSpec]]
	set len [llength $overlapping]
	set pos [lsearch -exact $overlapping $topSelected]
	# The "while" loop is only necessary to skip items containing the "mark" tag.
	while {[incr pos] < $len} {
		set aboveThis [lindex $overlapping $pos]
		if {"mark" ni [$canvas gettags $aboveThis]} {
			$canvas raise $searchSpec $aboveThis
			break
		}
	}
}

proc lower_selection {canvas} {
	set searchSpec {(selection && !mark) || item_being_created}
	set selection [$canvas find withtag $searchSpec]
	if {$selection eq {}} return
	set lowestSelected [lindex $selection 0]; unset selection

	# Try to skip "lower" operations that change nothing in the drawing:
	# Lower below an item that is actually overlapping the selection.
	set overlapping [$canvas find overlapping {*}[$canvas bbox $searchSpec]]
	set pos [lsearch -exact $overlapping $lowestSelected]
	# The "while" loop is only necessary to skip items containing the "mark" tag.
	while {[incr pos -1] >= 0} {
		set belowThis [lindex $overlapping $pos]
		if {"mark" ni [$canvas gettags $belowThis]} {
			$canvas lower $searchSpec $belowThis
			break
		}
	}
}

proc lower_selection_to_lowest {canvas} {
	$canvas lower {(selection && !mark) || item_being_created}
}

proc update_raiselower_buttons {selection} {
	if {$selection eq {}} {
		set state disabled
	} else {
		set state normal
	}
	foreach button {top raise lower lowest} {
		.raiselower.$button configure -state $state
	}
}

##############################################################################

# Object properties GUI

set properties_frame_for(.canvas) .properties

grid [ttk::labelframe .properties.generic -text {Generic}] -padx 3 -pady {3 0} -sticky we
grid [ttk::labelframe .properties.arc -text {Arc Properties}] \
     -padx 3 -pady {6 0} -sticky we
grid [ttk::labelframe .properties.polygonlike -text {Line/Polygon Properties}] \
     -padx 3 -pady {6 0} -sticky we
grid [ttk::labelframe .properties.text -text {Text Properties}] \
     -padx 3 -pady {6 0} -sticky we

# ttk::entry widgets can only be set to a certain text by means of their textvariable,
# therefore we are auto-creating these variables here:
proc make_entry {w args} {
	set ::entry_var($w) {}
	ttk::entry $w {*}$args -textvariable entry_var($w)
	foreach event {<Return> <KP_Enter> <FocusOut>} {
		bind $w $event {event generate %W <<Entered>>}
	}
	return $w
}

proc entry_set {w string} {
	set ::entry_var($w) $string
}

# Generic properties
set f .properties.generic
grid columnconfigure $f 2 -weight 1

grid [ttk::label $f.fill_l -text {Fill: }] \
     [ttk::label $f.fill_color -background {} -text {      } -relief raised] \
     [make_entry $f.fill -width 10 -font TkFixedFont] \
     [ttk::button $f.fill_clear -text X -style Toolbutton] -sticky nsw -pady 2
grid configure $f.fill_color $f.fill -padx {0 3}
grid configure $f.fill -sticky nesw

grid [ttk::label $f.outline_l -text {Outline: }] \
     [ttk::label $f.outline_color -background {} -text {      } -relief raised] \
     [make_entry $f.outline -width 10 -font TkFixedFont] \
     [ttk::button $f.outline_clear -text X -style Toolbutton] -sticky nsw -pady 2
grid configure $f.outline_color $f.outline -padx {0 3}
grid configure $f.outline -sticky nesw

grid [ttk::label $f.width_l -text {Line width: }] \
     [ttk::spinbox $f.width -from 1 -to Inf -width 7] - - \
     -sticky nsw -pady 2

grid [ttk::label $f.dash_l -text {Dash pattern: }] \
     [ttk::combobox $f.dash -font TkFixedFont -width 0] - - \
     -sticky nsw -pady {10 2}
grid configure $f.dash -sticky nesw -padx {0 2}
$f.dash configure -values [list . , - _ -. -.. {- ..} {12 24} {36 24} {36 24 12 24}]

grid [ttk::label $f.dashoffset_l -text {Dash offset: }] \
     [ttk::spinbox $f.dashoffset -from -Inf -to Inf -width 7] - - \
     -sticky nsw -pady 2

# Arc properties
set f .properties.arc

grid [ttk::label $f.style_l -text {Type: }] \
     [ttk::frame $f.style_f] - - \
     -sticky nsw -pady 2
grid [ttk::button $f.style_arc -text Arc -style Toolbutton] \
     [ttk::button $f.style_chord -text Chord -style Toolbutton] \
     [ttk::button $f.style_pieslice -text Pieslice -style Toolbutton] \
     -in $f.style_f

grid [ttk::label $f.start_l -text {Start: }] \
     [ttk::spinbox $f.start -from -Inf -to Inf -width 7] - - \
     -sticky nsw -pady 2

grid [ttk::label $f.extent_l -text {Extent: }] \
     [ttk::spinbox $f.extent -from -Inf -to Inf -width 7] - - \
     -sticky nsw -pady 2

# Line/Polygon properties
set f .properties.polygonlike

grid [ttk::label $f.smooth_l -text {Smoothing: }] \
     [ttk::frame $f.smooth_f] - - - \
     -sticky nsw -pady 2
grid [ttk::button $f.smooth_none -text None -style Toolbutton] \
     [ttk::button $f.smooth_quadratic -text Quadratic -style Toolbutton] \
     [ttk::button $f.smooth_cubic -text Cubic -style Toolbutton] \
     -in $f.smooth_f

grid [ttk::label $f.splinesteps_l -text {Spline steps: }] \
     [ttk::spinbox $f.splinesteps -from 1 -to Inf -width 7] - - - \
     -sticky nsw -pady 2

grid [ttk::label $f.capstyle_l -text {Cap style: }] \
     [ttk::button $f.capstyle_butt -text Butt -style Toolbutton] \
     [ttk::button $f.capstyle_projecting -text Projecting -style Toolbutton] - \
     [ttk::button $f.capstyle_round -text Round -style Toolbutton] \
     -sticky nsw -pady {10 2}

grid [ttk::label $f.joinstyle_l -text {Join style: }] \
     [ttk::button $f.joinstyle_bevel -text Bevel -style Toolbutton] - \
     [ttk::button $f.joinstyle_miter -text Miter -style Toolbutton] \
     [ttk::button $f.joinstyle_round -text Round -style Toolbutton] \
     -sticky nsw -pady 2

# Text properties
set f .properties.text
grid columnconfigure $f 2 -weight 1

grid [ttk::label $f.font_l -text {Font: }] \
     [ttk::combobox $f.font -width 0] - \
     -sticky nsw -pady 2
grid configure $f.font -sticky nesw -padx {0 2}

grid [ttk::label $f.size_l -text {Size: }] \
     [ttk::spinbox $f.size -from 1 -to Inf -width 7] - \
     -sticky nsw -pady 2

grid [ttk::label $f.style_l -text {Style: }] \
     [ttk::frame $f.style_f] - \
     -sticky nsw -pady 2
grid [ttk::checkbutton $f.bold -text Bold] \
     [ttk::checkbutton $f.italic -text Italic] \
     [ttk::checkbutton $f.underline -text Underl.] \
     [ttk::checkbutton $f.overstrike -text Strike.] \
     -in $f.style_f -padx {0 2}
foreach b {bold italic underline overstrike} {
	$f.$b configure -style ThreestateButton.TCheckbutton
}

grid [ttk::label $f.justify_l -text {Justify: }] \
     [ttk::frame $f.justify_f] - \
     -sticky nsw -pady {10 2}
grid [ttk::button $f.justify_left -text Left -style Toolbutton] \
     [ttk::button $f.justify_center -text Center -style Toolbutton] \
     [ttk::button $f.justify_right -text Right -style Toolbutton] \
     -in $f.justify_f

grid [ttk::label $f.width_l -text {Wrap at: }] \
     [ttk::spinbox $f.width -from 0 -to Inf -width 7] \
     [ttk::button $f.width_clear -text X -style Toolbutton] \
     -sticky nsw -pady 2
grid configure $f.width_clear -padx {3 0}

grid [ttk::label $f.anchor_l -text {Anchor: }] \
     [ttk::frame $f.anchor_f] - \
     -sticky nw -pady {10 2}
grid configure $f.anchor_l -ipady 2
grid [ttk::button $f.anchor_nw -text NW -style Toolbutton] \
     [ttk::button $f.anchor_n -text N -style Toolbutton] \
     [ttk::button $f.anchor_ne -text NE -style Toolbutton] \
     -in $f.anchor_f -sticky we
grid [ttk::button $f.anchor_w -text W -style Toolbutton] \
     [ttk::button $f.anchor_center -text Center -style Toolbutton] \
     [ttk::button $f.anchor_e -text E -style Toolbutton] \
     -in $f.anchor_f -sticky we
grid [ttk::button $f.anchor_sw -text SW -style Toolbutton] \
     [ttk::button $f.anchor_s -text S -style Toolbutton] \
     [ttk::button $f.anchor_se -text SE -style Toolbutton] \
     -in $f.anchor_f -sticky we

unset f

# Status bar helpers

proc statusbar_message {canvas message} {
	set statusbar $::statusbar_for($canvas)
	unset -nocomplain ::oldstatusbar($statusbar)
	$statusbar.message configure -text $message
	after cancel statusbar_restore $canvas
}

proc statusbar_error {canvas message} {
	set statusbar $::statusbar_for($canvas)
	if {! [info exists ::oldstatusbar($statusbar)]} {
		set ::oldstatusbar($statusbar) [$statusbar.message cget -text]
	}
	$statusbar.message configure -text $message
	after cancel statusbar_restore $canvas
}

proc statusbar_restore {canvas} {
	set statusbar $::statusbar_for($canvas)
	if {[info exists ::oldstatusbar($statusbar)]} {
		$statusbar.message configure -text $::oldstatusbar($statusbar)
		unset ::oldstatusbar($statusbar)
	}
	after cancel statusbar_restore $canvas
}

proc statusbar_restore_later {canvas} {
	after cancel statusbar_restore $canvas
	after 2000 statusbar_restore $canvas
}

# Update the properties GUI whenever the selection changes

proc get_selected {canvas} {
	$canvas find withtag {(selection && !mark) || item_being_created}
}

proc object_types {canvas items} {
	set ret [dict create]
	foreach item $items {
		dict incr ret [$canvas type $item]
	}
	return [dict keys $ret]
}

proc list_contains_any_of {list keys} {
	set contains false
	foreach key $keys {
		set contains [expr {$contains || ($key in $list)}]
	}
	return $contains
}

proc filter_by_type {canvas items allowed_types} {
	set ret [list]
	foreach item $items {
		if {[$canvas type $item] in $allowed_types} {
			lappend ret $item
		}
	}
	return $ret
}

proc combine_string_property {accum prop} {
	if {$accum eq {_NONE_}} {
		return $prop
	} elseif {$prop ne $accum} {
		return _MULTIPLE_
	} else {
		return $accum
	}
}

proc combine_bool_property {accum prop} {
	combine_string_property $accum [expr {bool($prop)}]
}

proc get_string_property {canvas items propname} {
	set ret _NONE_  ;# never used in any relevant item property
	foreach item $items {
		catch {
			set prop [$canvas itemcget $item $propname]
			set ret [combine_string_property $ret $prop]
		}
	}
	return $ret
}

# Special case since we want the -fill property of line objects to appear as outline
proc get_outline {canvas items} {
	set ret _NONE_
	foreach item $items {
		if {[$canvas type $item] eq {line}} {
			set prop [$canvas itemcget $item -fill]
		} else {
			if {[catch {
				set prop [$canvas itemcget $item -outline]
			}]} continue
		}
		set ret [combine_string_property $ret $prop]
	}
	return $ret
}

proc update_color_label {label string} {
	if {$string in {_NONE_ _MULTIPLE_}} {
		$label configure -background {}
	} else {
		$label configure -background $string
	}
}

proc update_color_entry {entry string} {
	switch $string {
		_NONE_     {entry_set $entry {}}
		_MULTIPLE_ {entry_set $entry {multiple}}
		default    {entry_set $entry $string}
	}
	$entry selection clear
}

proc update_combobox {combobox string active_state} {
	if {$string eq {_NONE_}} {
		$combobox set {}
		$combobox configure -state disabled
	} elseif {$string eq {_MULTIPLE_}} {
		$combobox set "multiple"
		$combobox configure -state $active_state
	} else {
		$combobox set $string
		$combobox configure -state $active_state
	}
	$combobox selection clear
}

proc get_enum_property {canvas items propname} {
	set ret [dict create]
	foreach item $items {
		catch {dict set ret [$canvas itemcget $item $propname] 1}
	}
	return [dict keys $ret]
}

proc update_radiobutton {button bool} {
	if {$bool} {
		$button state pressed
	} else {
		$button state !pressed
	}
}

proc get_number_property {canvas items propname} {
	set min Inf
	set max -Inf
	foreach item $items {
		catch {
			set prop [$canvas itemcget $item $propname]
			set min [expr {min($min, $prop)}]
			set max [expr {max($max, $prop)}]
		}
	}
	return [finalize_number_property $min $max]
}

proc finalize_number_property {min max} {
	if {$min eq "Inf" && $max eq "-Inf"} {
		# No objects found supporting the property
		return {}
	} elseif {$min == $max} {
		# All objects have the same property value
		return $max
	} else {
		return [list $min $max]
	}
}

proc update_spinbox {spinbox numberlist} {
	switch [llength $numberlist] {
		0 {
			$spinbox set {}
			$spinbox configure -values {}
		} 1 {
			$spinbox set $numberlist
			$spinbox configure -values {}
		} 2 {
			$spinbox set "multiple"
			set values [linsert $numberlist 1 "multiple"]
			$spinbox configure -values $values
		}
	}
	$spinbox selection clear
}

proc enable_labelframe_cond {window bool} {
	if {$bool} {
		$window state !disabled
	} else {
		$window state disabled
	}
}

proc enable_widgets_cond {bool args} {
	if {$bool} {
		foreach widget $args {
			$widget configure -state normal
		}
	} else {
		foreach widget $args {
			$widget configure -state disabled
		}
	}
}

proc get_text_properties {canvas items} {
	set family _NONE_
	set size_min Inf
	set size_max -Inf
	set weight _NONE_
	set slant _NONE_
	set underline _NONE_
	set overstrike _NONE_

	foreach item $items {
		catch {
			set font [$canvas itemcget $item -font]
			set size_px [expr {-[dict get $font -size]}]

			set family [combine_string_property $family [dict get $font -family]]
			set size_min [expr {min($size_min, $size_px)}]
			set size_max [expr {max($size_max, $size_px)}]
			set weight [combine_string_property $weight [dict get $font -weight]]
			set slant [combine_string_property $slant [dict get $font -slant]]
			set underline [combine_bool_property $underline [dict get $font -underline]]
			set overstrike [combine_bool_property $overstrike [dict get $font -overstrike]]
		}
	}

	dict create -family $family \
		-size [finalize_number_property $size_min $size_max] \
		-weight $weight -slant $slant -underline $underline \
		-overstrike $overstrike
}

proc update_checkbutton {checkbutton value offValue onValue} {
	global $checkbutton
	if {$value eq $onValue} {
		set $checkbutton 1
	} elseif {$value eq $offValue} {
		set $checkbutton 0
	} else {
		set $checkbutton 0  ;# If it is 1 at the time of unsetting, the
		unset $checkbutton  ;# "alternate" state is masked by "selected".
	}
}

bind .canvas <<SelectionUpdate>> {update_properties %W}

proc update_properties {canvas} {
	set selection [get_selected $canvas]
	set types [object_types $canvas $selection]

	set prop_frame $::properties_frame_for($canvas)
	update_generic_properties_gui $canvas $prop_frame $selection $types
	update_arc_properties_gui $canvas $prop_frame $selection $types
	update_polygonlike_properties_gui $canvas $prop_frame $selection $types
	update_text_properties_gui $canvas $prop_frame $selection $types

	update_raiselower_buttons $selection
}

proc update_generic_properties_gui {canvas prop_frame selection types} {
	set f $prop_frame.generic
	enable_labelframe_cond $f [list_contains_any_of $types {arc line polygon oval rectangle text}]

	# We don't want to see -fill attributes of lines, see get_outline
	set selection_filtered [filter_by_type $canvas $selection {arc polygon oval rectangle text}]
	set fill [get_string_property $canvas $selection_filtered -fill]
	enable_widgets_cond [expr {$fill ne {_NONE_}}] \
		$f.fill_l $f.fill_color $f.fill $f.fill_clear
	update_color_label $f.fill_color $fill
	update_color_entry $f.fill $fill

	set outline [get_outline $canvas $selection]
	enable_widgets_cond [expr {$outline ne {_NONE_}}] \
		$f.outline_l $f.outline_color $f.outline $f.outline_clear
	update_color_label $f.outline_color $outline
	update_color_entry $f.outline $outline

	# We don't want to set -width attributes of texts and windows in the "Line width" entry:
	set selection_filtered [filter_by_type $canvas $selection {arc line polygon oval rectangle}]
	set width [get_number_property $canvas $selection_filtered -width]
	enable_widgets_cond [expr {$width ne {}}] \
		$f.width_l $f.width
	update_spinbox $f.width $width

	set dash [get_string_property $canvas $selection -dash]
	enable_widgets_cond [expr {$dash ne {_NONE_}}] \
		$f.dash_l $f.dash
	update_combobox $f.dash $dash normal

	set dashoffset [get_number_property $canvas $selection -dashoffset]
	enable_widgets_cond [expr {$dashoffset ne {}}] \
		$f.dashoffset_l $f.dashoffset
	update_spinbox $f.dashoffset $dashoffset
}

proc update_arc_properties_gui {canvas prop_frame selection types} {
	set f $prop_frame.arc
	enable_labelframe_cond $f [expr {"arc" in $types}]

	set style [get_enum_property $canvas $selection -style]
	enable_widgets_cond [expr {$style ne {}}] \
		$f.style_l $f.style_arc $f.style_chord $f.style_pieslice
	update_radiobutton $f.style_arc [expr {"arc" in $style}]
	update_radiobutton $f.style_chord [expr {"chord" in $style}]
	update_radiobutton $f.style_pieslice [expr {"pieslice" in $style}]

	set start [get_number_property $canvas $selection -start]
	enable_widgets_cond [expr {$start ne {}}] \
		$f.start_l $f.start
	update_spinbox $f.start $start

	set extent [get_number_property $canvas $selection -extent]
	enable_widgets_cond [expr {$extent ne {}}] \
		$f.extent_l $f.extent
	update_spinbox $f.extent $extent
}

proc update_polygonlike_properties_gui {canvas prop_frame selection types} {
	set f $prop_frame.polygonlike
	enable_labelframe_cond $f [list_contains_any_of $types {line polygon}]

	set smooth [get_enum_property $canvas $selection -smooth]
	# possible values: true 0 raw; all other representations are coerced by the canvas into these
	enable_widgets_cond [expr {$smooth ne {}}] \
		$f.smooth_l $f.smooth_none $f.smooth_quadratic $f.smooth_cubic
	update_radiobutton $f.smooth_none [expr {"0" in $smooth}]
	update_radiobutton $f.smooth_quadratic [expr {"true" in $smooth}]
	update_radiobutton $f.smooth_cubic [expr {"raw" in $smooth}]

	set splinesteps [get_number_property $canvas $selection -splinesteps]
	enable_widgets_cond [expr {$splinesteps ne {}}] \
		$f.splinesteps_l $f.splinesteps
	update_spinbox $f.splinesteps $splinesteps

	set capstyle [get_enum_property $canvas $selection -capstyle]
	enable_widgets_cond [expr {$capstyle ne {}}] \
		$f.capstyle_l $f.capstyle_butt $f.capstyle_projecting $f.capstyle_round
	update_radiobutton $f.capstyle_butt [expr {"butt" in $capstyle}]
	update_radiobutton $f.capstyle_projecting [expr {"projecting" in $capstyle}]
	update_radiobutton $f.capstyle_round [expr {"round" in $capstyle}]

	set joinstyle [get_enum_property $canvas $selection -joinstyle]
	enable_widgets_cond [expr {$joinstyle ne {}}] \
		$f.joinstyle_l $f.joinstyle_bevel $f.joinstyle_miter $f.joinstyle_round
	update_radiobutton $f.joinstyle_bevel [expr {"bevel" in $joinstyle}]
	update_radiobutton $f.joinstyle_miter [expr {"miter" in $joinstyle}]
	update_radiobutton $f.joinstyle_round [expr {"round" in $joinstyle}]
}

proc update_text_properties_gui {canvas prop_frame selection types} {
	set f $prop_frame.text
	set have_texts [expr {{text} in $types}]
	enable_labelframe_cond $f $have_texts
	enable_widgets_cond $have_texts \
		$f.font_l $f.font \
		$f.size_l $f.size \
		$f.style_l $f.bold $f.italic $f.underline $f.overstrike
	set selection [filter_by_type $canvas $selection text]

	set font [get_text_properties $canvas $selection]
	update_combobox $f.font [dict get $font -family] readonly
	update_spinbox $f.size [dict get $font -size]
	update_checkbutton $f.bold [dict get $font -weight] normal bold
	update_checkbutton $f.italic [dict get $font -slant] roman italic
	update_checkbutton $f.underline [dict get $font -underline] 0 1
	update_checkbutton $f.overstrike [dict get $font -overstrike] 0 1

	set justify [get_enum_property $canvas $selection -justify]
	enable_widgets_cond [expr {$justify ne {}}] \
		$f.justify_l $f.justify_left $f.justify_center $f.justify_right
	update_radiobutton $f.justify_left [expr {"left" in $justify}]
	update_radiobutton $f.justify_center [expr {"center" in $justify}]
	update_radiobutton $f.justify_right [expr {"right" in $justify}]

	set width [get_number_property $canvas $selection -width]
	enable_widgets_cond [expr {$width ne {}}] \
		$f.width_l $f.width $f.width_clear
	update_spinbox $f.width $width

	set anchor [get_enum_property $canvas $selection -anchor]
	enable_widgets_cond [expr {$anchor ne {}}] \
		$f.anchor_l $f.anchor_nw $f.anchor_n $f.anchor_ne \
		$f.anchor_w $f.anchor_center $f.anchor_e \
		$f.anchor_sw $f.anchor_s $f.anchor_se
	update_radiobutton $f.anchor_nw [expr {"nw" in $anchor}]
	update_radiobutton $f.anchor_n [expr {"n" in $anchor}]
	update_radiobutton $f.anchor_ne [expr {"ne" in $anchor}]
	update_radiobutton $f.anchor_w [expr {"w" in $anchor}]
	update_radiobutton $f.anchor_center [expr {"center" in $anchor}]
	update_radiobutton $f.anchor_e [expr {"e" in $anchor}]
	update_radiobutton $f.anchor_sw [expr {"sw" in $anchor}]
	update_radiobutton $f.anchor_s [expr {"s" in $anchor}]
	update_radiobutton $f.anchor_se [expr {"se" in $anchor}]

}

# Changes to properties GUI controls are applied to the selected objects

proc set_property {canvas propname value {item_types {}}} {
	if {$value eq {multiple}} return
	# We cannot just use
	#    $canvas itemconfigure {(selection && !mark) || item_being_created} $propname $value
	# since that would fail on the first item that does not support $propname.
	set selection [get_selected $canvas]
	if {$item_types ne {}} {
		set selection [filter_by_type $canvas $selection $item_types]
	}
	foreach item $selection {
		set code [catch {$canvas itemconfigure $item $propname $value} result]
		if {$code != 0 && ! [string match {unknown option *} $result]} {
			error $result  ;# probably $value is invalid
		}
	}
}

# Special case for avoiding -fill of line items
proc set_fill {canvas color} {
	if {$color eq {multiple}} return
	set selection [get_selected $canvas]
	foreach item $selection {
		if {[$canvas type $item] eq {line}} continue
		set code [catch {$canvas itemconfigure $item -fill $color} result]
		if {$code != 0 && ! [string match {unknown option *} $result]} {
			error $result  ;# probably $color is invalid
		}
	}
}

# Special case for setting -fill of line items
proc set_outline {canvas color} {
	if {$color eq {multiple}} return
	set selection [get_selected $canvas]
	foreach item $selection {
		if {[$canvas type $item] eq {line}} {
			set prop -fill
		} else {
			set prop -outline
		}
		set code [catch {$canvas itemconfigure $item $prop $color} result]
		if {$code != 0 && ! [string match {unknown option *} $result]} {
			error $result  ;# probably $color is invalid
		}
	}
}

proc set_and_update_fill {canvas color} {
	if {$color eq {multiple}} return
	if {[catch {set_fill $canvas $color} result]} {
		statusbar_error $canvas $result
		return
	} else {
		statusbar_restore $canvas
	}

	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.generic
	update_color_label $f.fill_color $color
	update_color_entry $f.fill $color
}

proc make_fill_entry_valid {canvas} {
	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.generic
	set selection_filtered \
		[filter_by_type $canvas [get_selected $canvas] {arc polygon oval rectangle text}]
	set fill [get_string_property $canvas $selection_filtered -fill]
	update_color_entry $f.fill $fill
	statusbar_restore_later $canvas
}

proc set_and_update_outline {canvas color} {
	if {$color eq {multiple}} return
	if {[catch {set_outline $canvas $color} result]} {
		statusbar_error $canvas $result
		return
	} else {
		statusbar_restore $canvas
	}

	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.generic
	update_color_label $f.outline_color $color
	update_color_entry $f.outline $color
}

proc make_outline_entry_valid {canvas} {
	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.generic
	set outline [get_outline $canvas [get_selected $canvas]]
	update_color_entry $f.outline $outline
	statusbar_restore_later $canvas
}

proc colorbutton_click {colorbutton canvas} {
	if {[$colorbutton cget -state] eq {disabled}} return
	set old_color [$colorbutton cget -background]
	if {$old_color eq {}} {set old_color black}
	set properties_window [winfo toplevel $::properties_frame_for($canvas)]
	return [tk_chooseColor -initialcolor $old_color -parent $properties_window]
}

proc fill_from_palette {colorbutton canvas} {
	set color [colorbutton_click $colorbutton $canvas]
	if {$color eq {}} return
	set_and_update_fill $canvas $color
}

proc outline_from_palette {colorbutton canvas} {
	set color [colorbutton_click $colorbutton $canvas]
	if {$color eq {}} return
	set_and_update_outline $canvas $color
}

proc bind_spinbox {spinbox script} {
	foreach event {<<Increment>> <<Decrement>>} {
		bind $spinbox $event [list event generate $spinbox <<ValueChanged>> -when head]
	}
	foreach event {<Return> <KP_Enter> <FocusOut> <<ValueChanged>>} {
		bind $spinbox $event $script
	}
}

proc set_from_spinbox {canvas spinbox propname {item_types {}}} {
	if {[catch {set_property $canvas $propname [$spinbox get] $item_types} result]} {
		statusbar_error $canvas $result
	} else {
		statusbar_restore $canvas
	}
}

proc make_spinbox_valid {canvas spinbox propname {item_types {}}} {
	set selection [get_selected $canvas]
	if {$item_types ne {}} {
		set selection [filter_by_type $canvas $selection $item_types]
	}
	set value [get_number_property $canvas $selection $propname]
	update_spinbox $spinbox $value
	statusbar_restore_later $canvas
}

proc bind_combobox {combobox script} {
	foreach event {<Return> <KP_Enter> <FocusOut> <<ComboboxSelected>>} {
		bind $combobox $event $script
	}
}

proc set_from_combobox {canvas combobox propname {item_types {}}} {
	set_from_spinbox $canvas $combobox $propname $item_types
}

proc make_combobox_valid {canvas combobox active_state propname} {
	set selection [get_selected $canvas]
	set value [get_string_property $canvas $selection $propname]
	update_combobox $combobox $value $active_state
	statusbar_restore_later $canvas
}

proc set_radiobutton {button value on_value} {
	$button state [expr {$value eq $on_value ? {pressed} : {!pressed}}]
}

proc update_bboxes {canvas types} {
	update_scrollregion $canvas
	if {$::activetool eq {select}} {
		set selection [filter_by_type $canvas [get_selected $canvas] $types]
		foreach item $selection {
			select_draw_marks $canvas $item
		}
	}
}

# Special procedures for font sub-attributes
proc set_font_property {canvas propname value} {
	if {$value eq {multiple}} return
	set selection [filter_by_type $canvas [get_selected $canvas] text]
	foreach text $selection {
		set font [$canvas itemcget $text -font]
		dict set font $propname $value
		$canvas itemconfigure $text -font $font  ;# may throw an error
	}
	update_bboxes $canvas text  ;# bounding box is very likely to change
}

proc set_font_size {canvas spinbox} {
	if {[catch {set_font_property $canvas -size [expr {-[$spinbox get]}]} result]} {
		statusbar_error $canvas $result
	} else {
		statusbar_restore $canvas
	}
}

proc make_fontsize_spinbox_valid {canvas spinbox} {
	set selection [filter_by_type $canvas [get_selected $canvas] text]
	set font [get_text_properties $canvas $selection]
	update_spinbox $spinbox [dict get $font -size]
	statusbar_restore_later $canvas
}

proc set_font_from_combobox {canvas combobox propname} {
	if {[catch {set_font_property $canvas $propname [$combobox get]} result]} {
		statusbar_error $canvas $result
	} else {
		statusbar_restore $canvas
	}
}

proc set_font_from_checkbutton {canvas checkbutton propname offValue onValue} {
	global $checkbutton
	if {! [info exists $checkbutton]} return  ;# "alternate" state
	set value [set $checkbutton]
	if {$value eq {0}} {
		set value $offValue
	} elseif {$value eq {1}} {
		set value $onValue
	} else {
		error "invalid value \"$value\" of checkbutton $checkbutton: expected 0 or 1"
	}
	if {[catch {set_font_property $canvas $propname $value} result]} {
		statusbar_error $canvas $result
	} else {
		statusbar_restore $canvas
	}
}

proc bind_checkbutton {checkbutton command} {
	$checkbutton configure -command [string map [list %W $checkbutton] $command]
}

proc clear_text_width {canvas} {
	set_property $canvas -width 0 text
	update_bboxes $canvas text

	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.text
	update_spinbox $f.width 0
}

# Generic Properties
set f .properties.generic
# Fill
bind $f.fill <<Entered>> {set_and_update_fill .canvas $entry_var(%W)}
bind $f.fill <FocusOut> {+make_fill_entry_valid .canvas}
bind $f.fill_color <ButtonPress> {fill_from_palette %W .canvas}
$f.fill_clear configure -command {set_and_update_fill .canvas {}}
# Outline
bind $f.outline <<Entered>> {set_and_update_outline .canvas $entry_var(%W)}
bind $f.outline <FocusOut> {+make_outline_entry_valid .canvas}
bind $f.outline_color <ButtonPress> {outline_from_palette %W .canvas}
$f.outline_clear configure -command {set_and_update_outline .canvas {}}
# Line width
bind_spinbox $f.width {set_from_spinbox .canvas %W -width {arc line polygon oval rectangle}
	update_bboxes .canvas {arc line polygon oval rectangle}}
bind $f.width <FocusOut> {+make_spinbox_valid .canvas %W -width {arc line polygon oval rectangle}}
# Dash pattern
bind_combobox $f.dash {set_from_combobox .canvas %W -dash}
bind $f.dash <FocusOut> {+make_combobox_valid .canvas %W normal -dash}
# Dash offset
bind_spinbox $f.dashoffset {set_from_spinbox .canvas %W -dashoffset}
bind $f.dashoffset <FocusOut> {+make_spinbox_valid .canvas %W -dashoffset}

# Arc Properties
set f .properties.arc
# Type
$f.style_arc configure -command {set_style .canvas arc}
$f.style_chord configure -command {set_style .canvas chord}
$f.style_pieslice configure -command {set_style .canvas pieslice}
proc set_style {canvas value} {
	set_property $canvas -style $value
	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.arc
	set_radiobutton $f.style_arc $value arc
	set_radiobutton $f.style_chord $value chord
	set_radiobutton $f.style_pieslice $value pieslice
	update_bboxes .canvas arc
}
# Start
bind_spinbox $f.start {set_from_spinbox .canvas %W -start; update_bboxes .canvas arc}
bind $f.start <FocusOut> {+make_spinbox_valid .canvas %W -start}
# Extent
bind_spinbox $f.extent {set_from_spinbox .canvas %W -extent; update_bboxes .canvas arc}
bind $f.extent <FocusOut> {+make_spinbox_valid .canvas %W -extent}

# Line/Polygon Properties
set f .properties.polygonlike
# Smoothing
$f.smooth_none configure -command {set_smooth .canvas 0}
$f.smooth_quadratic configure -command {set_smooth .canvas true}
$f.smooth_cubic configure -command {set_smooth .canvas raw}
proc set_smooth {canvas value} {
	set_property $canvas -smooth $value
	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.polygonlike
	set_radiobutton $f.smooth_none $value 0
	set_radiobutton $f.smooth_quadratic $value true
	set_radiobutton $f.smooth_cubic $value raw
}
# Spline steps
bind_spinbox $f.splinesteps {set_from_spinbox .canvas %W -splinesteps}
bind $f.splinesteps <FocusOut> {+make_spinbox_valid .canvas %W -splinesteps}
# Cap style
$f.capstyle_butt configure -command {set_capstyle .canvas butt}
$f.capstyle_projecting configure -command {set_capstyle .canvas projecting}
$f.capstyle_round configure -command {set_capstyle .canvas round}
proc set_capstyle {canvas value} {
	set_property $canvas -capstyle $value
	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.polygonlike
	set_radiobutton $f.capstyle_butt $value butt
	set_radiobutton $f.capstyle_projecting $value projecting
	set_radiobutton $f.capstyle_round $value round
}
# Join style
$f.joinstyle_bevel configure -command {set_joinstyle .canvas bevel}
$f.joinstyle_miter configure -command {set_joinstyle .canvas miter}
$f.joinstyle_round configure -command {set_joinstyle .canvas round}
proc set_joinstyle {canvas value} {
	set_property $canvas -joinstyle $value
	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.polygonlike
	set_radiobutton $f.joinstyle_bevel $value bevel
	set_radiobutton $f.joinstyle_miter $value miter
	set_radiobutton $f.joinstyle_round $value round
}

# Text Properties
set f .properties.text
# Font family
bind_combobox $f.font {set_font_from_combobox .canvas %W -family}
$f.font configure -values [concat monospace sans-serif serif [lsort [font families]]]
# Font size
bind_spinbox $f.size {set_font_size .canvas %W}
bind $f.size <FocusOut> {+make_fontsize_spinbox_valid .canvas %W}
# Font style
bind_checkbutton $f.bold {set_font_from_checkbutton .canvas %W -weight normal bold}
bind_checkbutton $f.italic {set_font_from_checkbutton .canvas %W -slant roman italic}
bind_checkbutton $f.underline {set_font_from_checkbutton .canvas %W -underline no yes}
bind_checkbutton $f.overstrike {set_font_from_checkbutton .canvas %W -overstrike no yes}
# Justification
$f.justify_left configure -command {set_justify .canvas left}
$f.justify_center configure -command {set_justify .canvas center}
$f.justify_right configure -command {set_justify .canvas right}
proc set_justify {canvas value} {
	set_property $canvas -justify $value
	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.text
	set_radiobutton $f.justify_left $value left
	set_radiobutton $f.justify_center $value center
	set_radiobutton $f.justify_right $value right
	# update_bboxes not required!
}
# Wrap width
bind_spinbox $f.width {set_from_spinbox .canvas %W -width text; update_bboxes .canvas text}
bind $f.width <FocusOut> {+make_spinbox_valid .canvas %W -width text}
$f.width_clear configure -command {clear_text_width .canvas}
# Anchor
$f.anchor_nw configure -command {set_anchor .canvas nw}
$f.anchor_n configure -command {set_anchor .canvas n}
$f.anchor_ne configure -command {set_anchor .canvas ne}
$f.anchor_w configure -command {set_anchor .canvas w}
$f.anchor_center configure -command {set_anchor .canvas center}
$f.anchor_e configure -command {set_anchor .canvas e}
$f.anchor_sw configure -command {set_anchor .canvas sw}
$f.anchor_s configure -command {set_anchor .canvas s}
$f.anchor_se configure -command {set_anchor .canvas se}
proc set_anchor {canvas value} {
	set_property $canvas -anchor $value text
	set prop_frame $::properties_frame_for($canvas)
	set f $prop_frame.text
	set_radiobutton $f.anchor_nw $value nw
	set_radiobutton $f.anchor_n $value n
	set_radiobutton $f.anchor_ne $value ne
	set_radiobutton $f.anchor_w $value w
	set_radiobutton $f.anchor_center $value center
	set_radiobutton $f.anchor_e $value e
	set_radiobutton $f.anchor_sw $value sw
	set_radiobutton $f.anchor_s $value s
	set_radiobutton $f.anchor_se $value se
	update_bboxes $canvas text
}

# Improve focus being kept on canvas / restored to canvas when pressing Escape

proc ttk::takefocus {w} {
	expr {[$w cget -style] ni {Toolbutton ThreestateButton.TCheckbutton} &&
	      [$w instate !disabled] && [winfo viewable $w]}
}

foreach class {TCombobox TEntry TSpinbox} {
	bind $class <Escape> {focus .canvas}
}

update idletasks
event generate .canvas <<SelectionUpdate>> -when tail
