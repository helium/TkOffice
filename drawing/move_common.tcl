# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

# Prerequisites for moving objects or points

variable x_move_last
variable y_move_last
variable oldcursor

proc move_start {canvas x y} {
	variable x_move_last
	variable y_move_last
	variable oldcursor

	set x_move_last($canvas) $x
	set y_move_last($canvas) $y

	set oldcursor($canvas) [$canvas cget -cursor]
	if {$oldcursor($canvas) ne "dotbox"} {
	# we want to keep the cursor for the points tool
		$canvas configure -cursor fleur
	}
}

proc is_moving {canvas} {
	variable x_move_last
	info exists x_move_last($canvas)
}

proc move_end {canvas} {
	variable x_move_last
	variable y_move_last
	variable oldcursor

	unset -nocomplain x_move_last($canvas)
	unset -nocomplain y_move_last($canvas)
	update_scrollregion $canvas

	if {[$canvas cget -cursor] eq "fleur"} {
		$canvas configure -cursor $oldcursor($canvas)
		unset oldcursor($canvas)
	}
}

}
