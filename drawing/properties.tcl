# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

proc set_property {canvas propname value {item_types {}}} {
	if {$value eq [mc multiple]} return
	# We cannot just use
	#    $canvas itemconfigure {(selection && !mark) || item_being_created} $propname $value
	# since that would fail on the first item that does not support $propname.
	set selection [get_selected $canvas]
	if {$item_types ne {}} {
		set selection [filter_by_type $canvas $selection $item_types]
	}
	foreach item $selection {
		set code [catch {$canvas itemconfigure $item $propname $value} result]
		if {$code != 0 && ! [string match {unknown option *} $result]} {
			error $result  ;# probably $value is invalid
		}
	}
}

# Same as update_scrollregion, but also updates the marks of the Select tool.
proc update_bboxes {canvas types} {
	variable activetool

	update_scrollregion $canvas
	if {$activetool($canvas) eq {select}} {
		set selection [filter_by_type $canvas [get_selected $canvas] $types]
		foreach item $selection {
			select_draw_marks $canvas $item
		}
	}
}

}
