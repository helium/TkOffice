# Copyright 2021, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

proc properties_subframe_polygonlike {f canvas} {
	ttk::labelframe $f -text [mc polygonlike_properties] -style Panel.TLabelframe

	grid [ttk::label $f.smooth_l -text [mc polygonlike_smooth]] \
		 [ttk::frame $f.smooth_f] - - - \
		 -sticky nsw -pady 2
	grid [ttk::button $f.smooth_none -text [mc polygonlike_smooth_none] -style Toolbutton] \
		 [ttk::button $f.smooth_quadratic -text [mc polygonlike_smooth_quadratic] -style Toolbutton] \
		 [ttk::button $f.smooth_cubic -text [mc polygonlike_smooth_cubic] -style Toolbutton] \
		 -in $f.smooth_f

	grid [ttk::label $f.splinesteps_l -text [mc polygonlike_splinesteps]] \
		 [ttk::spinbox $f.splinesteps -from 1 -to Inf -width 7] - - - \
		 -sticky nsw -pady 2

	grid [ttk::label $f.capstyle_l -text [mc polygonlike_capstyle]] \
		 [ttk::button $f.capstyle_butt -text [mc polygonlike_capstyle_butt] -style Toolbutton] \
		 [ttk::button $f.capstyle_projecting -text [mc polygonlike_capstyle_projecting] -style Toolbutton] - \
		 [ttk::button $f.capstyle_round -text [mc polygonlike_capstyle_round] -style Toolbutton] \
		 -sticky nsw -pady {10 2}

	grid [ttk::label $f.joinstyle_l -text [mc polygonlike_joinstyle]] \
		 [ttk::button $f.joinstyle_bevel -text [mc polygonlike_joinstyle_bevel] -style Toolbutton] - \
		 [ttk::button $f.joinstyle_miter -text [mc polygonlike_joinstyle_miter] -style Toolbutton] \
		 [ttk::button $f.joinstyle_round -text [mc polygonlike_joinstyle_round] -style Toolbutton] \
		 -sticky nsw -pady 2

	set_polygonlike_properties_gui_bindings $f $canvas
	return $f
}

proc set_polygonlike_properties_gui_bindings {f canvas} {
	# Smoothing
	$f.smooth_none configure -command [list drawing::polygonlike_set_smooth $canvas 0]
	$f.smooth_quadratic configure -command [list drawing::polygonlike_set_smooth $canvas true]
	$f.smooth_cubic configure -command [list drawing::polygonlike_set_smooth $canvas raw]
	# Spline steps
	bind_spinbox $f.splinesteps [list drawing::set_from_spinbox $canvas %W -splinesteps]
	bind $f.splinesteps <FocusOut> [list drawing::make_spinbox_valid $canvas %W -splinesteps]
	# Cap style
	$f.capstyle_butt configure -command [list drawing::polygonlike_set_capstyle $canvas butt]
	$f.capstyle_projecting configure -command [list drawing::polygonlike_set_capstyle $canvas projecting]
	$f.capstyle_round configure -command [list drawing::polygonlike_set_capstyle $canvas round]
	# Join style
	$f.joinstyle_bevel configure -command [list drawing::polygonlike_set_joinstyle $canvas bevel]
	$f.joinstyle_miter configure -command [list drawing::polygonlike_set_joinstyle $canvas miter]
	$f.joinstyle_round configure -command [list drawing::polygonlike_set_joinstyle $canvas round]
}

proc update_polygonlike_properties_gui {canvas prop_frame selection types} {
	set f $prop_frame.polygonlike
	enable_labelframe_cond $f [list_contains_any_of $types {line polygon}]

	set smooth [get_enum_property $canvas $selection -smooth]
	# possible values: true 0 raw; all other representations are coerced by the canvas into these
	enable_widgets_cond [expr {$smooth ne {}}] \
		$f.smooth_l $f.smooth_none $f.smooth_quadratic $f.smooth_cubic
	update_radiobutton $f.smooth_none [expr {"0" in $smooth}]
	update_radiobutton $f.smooth_quadratic [expr {"true" in $smooth}]
	update_radiobutton $f.smooth_cubic [expr {"raw" in $smooth}]

	set splinesteps [get_number_property $canvas $selection -splinesteps]
	enable_widgets_cond [expr {$splinesteps ne {}}] \
		$f.splinesteps_l $f.splinesteps
	update_spinbox $f.splinesteps $splinesteps

	set capstyle [get_enum_property $canvas $selection -capstyle]
	enable_widgets_cond [expr {$capstyle ne {}}] \
		$f.capstyle_l $f.capstyle_butt $f.capstyle_projecting $f.capstyle_round
	update_radiobutton $f.capstyle_butt [expr {"butt" in $capstyle}]
	update_radiobutton $f.capstyle_projecting [expr {"projecting" in $capstyle}]
	update_radiobutton $f.capstyle_round [expr {"round" in $capstyle}]

	set joinstyle [get_enum_property $canvas $selection -joinstyle]
	enable_widgets_cond [expr {$joinstyle ne {}}] \
		$f.joinstyle_l $f.joinstyle_bevel $f.joinstyle_miter $f.joinstyle_round
	update_radiobutton $f.joinstyle_bevel [expr {"bevel" in $joinstyle}]
	update_radiobutton $f.joinstyle_miter [expr {"miter" in $joinstyle}]
	update_radiobutton $f.joinstyle_round [expr {"round" in $joinstyle}]
}

proc polygonlike_set_smooth {canvas value} {
	variable properties_frame_for

	set_property $canvas -smooth $value
	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.polygonlike
	set_radiobutton $f.smooth_none $value 0
	set_radiobutton $f.smooth_quadratic $value true
	set_radiobutton $f.smooth_cubic $value raw
}

proc polygonlike_set_capstyle {canvas value} {
	variable properties_frame_for

	set_property $canvas -capstyle $value
	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.polygonlike
	set_radiobutton $f.capstyle_butt $value butt
	set_radiobutton $f.capstyle_projecting $value projecting
	set_radiobutton $f.capstyle_round $value round
}

proc polygonlike_set_joinstyle {canvas value} {
	variable properties_frame_for

	set_property $canvas -joinstyle $value
	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.polygonlike
	set_radiobutton $f.joinstyle_bevel $value bevel
	set_radiobutton $f.joinstyle_miter $value miter
	set_radiobutton $f.joinstyle_round $value round
}

}
