# Copyright 2021, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

proc properties_subframe_generic {f canvas} {
	ttk::labelframe $f -text [mc generic_properties] -style Panel.TLabelframe
	grid columnconfigure $f 2 -weight 1

	grid [ttk::label $f.fill_l -text [mc generic_fill]] \
		 [ttk::label $f.fill_color -background {} -text {      } -relief raised] \
		 [make_entry $f.fill -width 10 -font TkFixedFont] \
		 [ttk::button $f.fill_clear -text [mc cancel_button] -style Toolbutton] \
		 -sticky nsw -pady 2
	grid configure $f.fill_color $f.fill -padx {0 3}
	grid configure $f.fill -sticky nesw
	ttk_fixes::clam_corners $f.fill_color

	grid [ttk::label $f.outline_l -text [mc generic_outline]] \
		 [ttk::label $f.outline_color -background {} -text {      } -relief raised] \
		 [make_entry $f.outline -width 10 -font TkFixedFont] \
		 [ttk::button $f.outline_clear -text [mc cancel_button] -style Toolbutton] \
		 -sticky nsw -pady 2
	grid configure $f.outline_color $f.outline -padx {0 3}
	grid configure $f.outline -sticky nesw
	ttk_fixes::clam_corners $f.outline_color

	grid [ttk::label $f.width_l -text [mc generic_line_width]] \
		 [ttk::spinbox $f.width -from 1 -to Inf -width 7] - - \
		 -sticky nsw -pady 2

	grid [ttk::label $f.dash_l -text [mc generic_dash_pattern]] \
		 [ttk::combobox $f.dash -font TkFixedFont -width 0] - - \
		 -sticky nsw -pady {10 2}
	grid configure $f.dash -sticky nesw -padx {0 2}
	$f.dash configure -values [list . , - _ -. -.. {- ..} {12 24} {36 24} {36 24 12 24}]

	grid [ttk::label $f.dashoffset_l -text [mc generic_dash_offset]] \
		 [ttk::spinbox $f.dashoffset -from -Inf -to Inf -width 7] - - \
		 -sticky nsw -pady 2

	set_generic_properties_gui_bindings $f $canvas
	return $f
}

proc set_generic_properties_gui_bindings {f canvas} {
	# Fill
	bind $f.fill <<Entered>> "drawing::set_and_update_fill $canvas \$entry_var(%W)"
	bind $f.fill <FocusOut> [list +drawing::make_fill_entry_valid $canvas]
	bind $f.fill_color <ButtonPress> [list drawing::fill_from_palette %W $canvas]
	$f.fill_clear configure -command [list drawing::set_and_update_fill $canvas {}]
	# Outline
	bind $f.outline <<Entered>> "drawing::set_and_update_outline $canvas \$entry_var(%W)"
	bind $f.outline <FocusOut> [list +drawing::make_outline_entry_valid $canvas]
	bind $f.outline_color <ButtonPress> [list drawing::outline_from_palette %W $canvas]
	$f.outline_clear configure -command [list drawing::set_and_update_outline $canvas {}]
	# Line width
	bind_spinbox $f.width "
		drawing::set_from_spinbox $canvas %W -width {arc line polygon oval rectangle}
		drawing::update_bboxes $canvas {arc line polygon oval rectangle}
	"
	bind $f.width <FocusOut> [list +drawing::make_spinbox_valid $canvas %W -width {arc line polygon oval rectangle}]
	# Dash pattern
	bind_combobox $f.dash [list drawing::set_from_combobox $canvas %W -dash]
	bind $f.dash <FocusOut> [list +drawing::make_combobox_valid $canvas %W normal -dash]
	# Dash offset
	bind_spinbox $f.dashoffset [list drawing::set_from_spinbox $canvas %W -dashoffset]
	bind $f.dashoffset <FocusOut> [list +drawing::make_spinbox_valid $canvas %W -dashoffset]
}

proc update_generic_properties_gui {canvas prop_frame selection types} {
	set f $prop_frame.generic
	enable_labelframe_cond $f [list_contains_any_of $types {arc line polygon oval rectangle text}]

	# We don't want to see -fill attributes of lines, see get_outline
	set selection_filtered [filter_by_type $canvas $selection {arc polygon oval rectangle text}]
	set fill [get_string_property $canvas $selection_filtered -fill]
	enable_widgets_cond [expr {$fill ne {_NONE_}}] \
		$f.fill_l $f.fill_color $f.fill $f.fill_clear
	update_color_label $f.fill_color $fill
	update_color_entry $f.fill $fill

	set outline [get_outline $canvas $selection]
	enable_widgets_cond [expr {$outline ne {_NONE_}}] \
		$f.outline_l $f.outline_color $f.outline $f.outline_clear
	update_color_label $f.outline_color $outline
	update_color_entry $f.outline $outline

	# We don't want to set -width attributes of texts and windows in the "Line width" entry:
	set selection_filtered [filter_by_type $canvas $selection {arc line polygon oval rectangle}]
	set width [get_number_property $canvas $selection_filtered -width]
	enable_widgets_cond [expr {$width ne {}}] \
		$f.width_l $f.width
	update_spinbox $f.width $width

	set dash [get_string_property $canvas $selection -dash]
	enable_widgets_cond [expr {$dash ne {_NONE_}}] \
		$f.dash_l $f.dash
	update_combobox $f.dash $dash normal

	set dashoffset [get_number_property $canvas $selection -dashoffset]
	enable_widgets_cond [expr {$dashoffset ne {}}] \
		$f.dashoffset_l $f.dashoffset
	update_spinbox $f.dashoffset $dashoffset
}

# Subprocedures (see also properties.tcl, properties_gui.tcl)

proc fill_from_palette {colorbutton canvas} {
	set color [colorbutton_click $colorbutton $canvas]
	if {$color eq {}} return
	set_and_update_fill $canvas $color
}

proc outline_from_palette {colorbutton canvas} {
	set color [colorbutton_click $colorbutton $canvas]
	if {$color eq {}} return
	set_and_update_outline $canvas $color
}

proc make_fill_entry_valid {canvas} {
	variable properties_frame_for

	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.generic
	set selection_filtered \
		[filter_by_type $canvas [get_selected $canvas] {arc polygon oval rectangle text}]
	set fill [get_string_property $canvas $selection_filtered -fill]
	update_color_entry $f.fill $fill
	statusbar_restore_later $canvas
}

proc make_outline_entry_valid {canvas} {
	variable properties_frame_for

	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.generic
	set outline [get_outline $canvas [get_selected $canvas]]
	update_color_entry $f.outline $outline
	statusbar_restore_later $canvas
}

# Special case of set_property for avoiding -fill of line items
proc set_fill {canvas color} {
	if {$color eq [mc multiple]} return
	set selection [get_selected $canvas]
	foreach item $selection {
		if {[$canvas type $item] eq {line}} continue
		set code [catch {$canvas itemconfigure $item -fill $color} result]
		if {$code != 0 && ! [string match {unknown option *} $result]} {
			error $result  ;# probably $color is invalid
		}
	}
}

proc set_and_update_fill {canvas color} {
	variable properties_frame_for

	if {$color eq [mc multiple]} return
	if {[catch {set_fill $canvas $color} result]} {
		statusbar_error $canvas $result
		return
	} else {
		statusbar_restore $canvas
	}

	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.generic
	update_color_label $f.fill_color $color
	update_color_entry $f.fill $color
}

# Special case of set_property for setting -fill of line items
proc set_outline {canvas color} {
	if {$color eq [mc multiple]} return
	set selection [get_selected $canvas]
	foreach item $selection {
		if {[$canvas type $item] eq {line}} {
			set prop -fill
		} else {
			set prop -outline
		}
		set code [catch {$canvas itemconfigure $item $prop $color} result]
		if {$code != 0 && ! [string match {unknown option *} $result]} {
			error $result  ;# probably $color is invalid
		}
	}
}

proc set_and_update_outline {canvas color} {
	variable properties_frame_for

	if {$color eq [mc multiple]} return
	if {[catch {set_outline $canvas $color} result]} {
		statusbar_error $canvas $result
		return
	} else {
		statusbar_restore $canvas
	}

	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.generic
	update_color_label $f.outline_color $color
	update_color_entry $f.outline $color
}

}
