# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

variable statusbar_for
variable oldstatusbar

# GUI creation

proc statusbar_create {path -for canvas} {
	variable statusbar_for
	if {${-for} ne {-for}} {
		error "wrong second argument: must be statusbar_create path -for canvas"
	}

	ttk::frame $path
	grid [ttk::label $path.message -text {}] -padx 2 -pady 3
	set statusbar_for($canvas) $path
	bind $path <Destroy> [list drawing::statusbar_destroy $path $canvas]
	return $path
}

proc statusbar_destroy {path canvas} {
	variable statusbar_for

	unset statusbar_for($canvas)
}

# Status bar helpers

proc statusbar_message {canvas message} {
	variable statusbar_for
	variable oldstatusbar

	set statusbar $statusbar_for($canvas)
	unset -nocomplain oldstatusbar($statusbar)
	$statusbar.message configure -text $message
	after cancel drawing::statusbar_restore $canvas
}

proc statusbar_error {canvas message} {
	variable statusbar_for
	variable oldstatusbar

	set statusbar $statusbar_for($canvas)
	if {! [info exists oldstatusbar($statusbar)]} {
		set oldstatusbar($statusbar) [$statusbar.message cget -text]
	}
	$statusbar.message configure -text $message
	after cancel drawing::statusbar_restore $canvas
}

proc statusbar_restore {canvas} {
	variable statusbar_for
	variable oldstatusbar

	set statusbar $statusbar_for($canvas)
	if {[info exists oldstatusbar($statusbar)]} {
		$statusbar.message configure -text $oldstatusbar($statusbar)
		unset oldstatusbar($statusbar)
	}
	after cancel drawing::statusbar_restore $canvas
}

proc statusbar_restore_later {canvas} {
	after cancel drawing::statusbar_restore $canvas
	after 2000 drawing::statusbar_restore $canvas
}

}
