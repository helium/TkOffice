# Copyright 2021, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

proc properties_subframe_text {f canvas} {
	ttk::labelframe $f -text [mc text_properties] -style Panel.TLabelframe
	grid columnconfigure $f 2 -weight 1

	grid [ttk::label $f.font_l -text [mc text_font]] \
		 [ttk::combobox $f.font -width 0] - \
		 -sticky nsw -pady 2
	grid configure $f.font -sticky nesw -padx {0 2}

	grid [ttk::label $f.size_l -text [mc text_size]] \
		 [ttk::spinbox $f.size -from 1 -to Inf -width 7] - \
		 -sticky nsw -pady 2

	grid [ttk::label $f.style_l -text [mc text_style]] \
		 [ttk::frame $f.style_f] - \
		 -sticky nsw -pady 2
	grid [ttk::checkbutton $f.bold -text [mc text_style_bold]] \
		 [ttk::checkbutton $f.italic -text [mc text_style_italic]] \
		 [ttk::checkbutton $f.underline -text [mc text_style_underline]] \
		 [ttk::checkbutton $f.overstrike -text [mc text_style_overstrike]] \
		 -in $f.style_f -padx {0 2}
	foreach b {bold italic underline overstrike} {
		$f.$b configure -style ThreestateButton.TCheckbutton
	}

	grid [ttk::label $f.justify_l -text [mc text_justify]] \
		 [ttk::frame $f.justify_f] - \
		 -sticky nsw -pady {10 2}
	grid [ttk::button $f.justify_left -text [mc text_justify_left] -style Toolbutton] \
		 [ttk::button $f.justify_center -text [mc text_justify_center] -style Toolbutton] \
		 [ttk::button $f.justify_right -text [mc text_justify_right] -style Toolbutton] \
		 -in $f.justify_f

	grid [ttk::label $f.width_l -text [mc text_width]] \
		 [ttk::spinbox $f.width -from 0 -to Inf -width 7] \
		 [ttk::button $f.width_clear -text [mc cancel_button] -style Toolbutton] \
		 -sticky nsw -pady 2
	grid configure $f.width_clear -padx {3 0}

	grid [ttk::label $f.anchor_l -text [mc text_anchor]] \
		 [ttk::frame $f.anchor_f] - \
		 -sticky nw -pady {10 2}
	grid configure $f.anchor_l -ipady 2
	grid [ttk::button $f.anchor_nw -text [mc text_anchor_nw] -style Toolbutton] \
		 [ttk::button $f.anchor_n -text [mc text_anchor_n] -style Toolbutton] \
		 [ttk::button $f.anchor_ne -text [mc text_anchor_ne] -style Toolbutton] \
		 -in $f.anchor_f -sticky we
	grid [ttk::button $f.anchor_w -text [mc text_anchor_w] -style Toolbutton] \
		 [ttk::button $f.anchor_center -text [mc text_anchor_center] -style Toolbutton] \
		 [ttk::button $f.anchor_e -text [mc text_anchor_e] -style Toolbutton] \
		 -in $f.anchor_f -sticky we
	grid [ttk::button $f.anchor_sw -text [mc text_anchor_sw] -style Toolbutton] \
		 [ttk::button $f.anchor_s -text [mc text_anchor_s] -style Toolbutton] \
		 [ttk::button $f.anchor_se -text [mc text_anchor_se] -style Toolbutton] \
		 -in $f.anchor_f -sticky we

	set_text_properties_gui_bindings $f $canvas
	return $f
}

proc set_text_properties_gui_bindings {f canvas} {
	# Font family
	bind_combobox $f.font [list drawing::set_font_from_combobox $canvas %W -family]
	$f.font configure -values [concat monospace sans-serif serif [lsort -unique [font families]]]
	# Font size
	bind_spinbox $f.size [list drawing::set_font_size $canvas %W]
	bind $f.size <FocusOut> [list +drawing::make_fontsize_spinbox_valid $canvas %W]
	# Font style
	bind_checkbutton $f.bold [list drawing::set_font_from_checkbutton $canvas %W -weight normal bold]
	bind_checkbutton $f.italic [list drawing::set_font_from_checkbutton $canvas %W -slant roman italic]
	bind_checkbutton $f.underline [list drawing::set_font_from_checkbutton $canvas %W -underline no yes]
	bind_checkbutton $f.overstrike [list drawing::set_font_from_checkbutton $canvas %W -overstrike no yes]
	# Justification
	$f.justify_left configure -command [list drawing::text_set_justify $canvas left]
	$f.justify_center configure -command [list drawing::text_set_justify $canvas center]
	$f.justify_right configure -command [list drawing::text_set_justify $canvas right]
	# Wrap width
	bind_spinbox $f.width "
		drawing::set_from_spinbox $canvas %W -width text
		drawing::update_bboxes $canvas text
	"
	bind $f.width <FocusOut> [list +drawing::make_spinbox_valid $canvas %W -width text]
	$f.width_clear configure -command [list drawing::text_clear_width $canvas]
	# Anchor
	$f.anchor_nw configure -command [list drawing::text_set_anchor $canvas nw]
	$f.anchor_n configure -command [list drawing::text_set_anchor $canvas n]
	$f.anchor_ne configure -command [list drawing::text_set_anchor $canvas ne]
	$f.anchor_w configure -command [list drawing::text_set_anchor $canvas w]
	$f.anchor_center configure -command [list drawing::text_set_anchor $canvas center]
	$f.anchor_e configure -command [list drawing::text_set_anchor $canvas e]
	$f.anchor_sw configure -command [list drawing::text_set_anchor $canvas sw]
	$f.anchor_s configure -command [list drawing::text_set_anchor $canvas s]
	$f.anchor_se configure -command [list drawing::text_set_anchor $canvas se]
}

proc update_text_properties_gui {canvas prop_frame selection types} {
	set f $prop_frame.text
	set have_texts [expr {{text} in $types}]
	enable_labelframe_cond $f $have_texts
	enable_widgets_cond $have_texts \
		$f.font_l $f.font \
		$f.size_l $f.size \
		$f.style_l $f.bold $f.italic $f.underline $f.overstrike
	set selection [filter_by_type $canvas $selection text]

	set font [get_text_properties $canvas $selection]
	update_combobox $f.font [dict get $font -family] readonly
	update_spinbox $f.size [dict get $font -size]
	update_checkbutton $f.bold [dict get $font -weight] normal bold
	update_checkbutton $f.italic [dict get $font -slant] roman italic
	update_checkbutton $f.underline [dict get $font -underline] 0 1
	update_checkbutton $f.overstrike [dict get $font -overstrike] 0 1

	set justify [get_enum_property $canvas $selection -justify]
	enable_widgets_cond [expr {$justify ne {}}] \
		$f.justify_l $f.justify_left $f.justify_center $f.justify_right
	update_radiobutton $f.justify_left [expr {"left" in $justify}]
	update_radiobutton $f.justify_center [expr {"center" in $justify}]
	update_radiobutton $f.justify_right [expr {"right" in $justify}]

	set width [get_number_property $canvas $selection -width]
	enable_widgets_cond [expr {$width ne {}}] \
		$f.width_l $f.width $f.width_clear
	update_spinbox $f.width $width

	set anchor [get_enum_property $canvas $selection -anchor]
	enable_widgets_cond [expr {$anchor ne {}}] \
		$f.anchor_l $f.anchor_nw $f.anchor_n $f.anchor_ne \
		$f.anchor_w $f.anchor_center $f.anchor_e \
		$f.anchor_sw $f.anchor_s $f.anchor_se
	update_radiobutton $f.anchor_nw [expr {"nw" in $anchor}]
	update_radiobutton $f.anchor_n [expr {"n" in $anchor}]
	update_radiobutton $f.anchor_ne [expr {"ne" in $anchor}]
	update_radiobutton $f.anchor_w [expr {"w" in $anchor}]
	update_radiobutton $f.anchor_center [expr {"center" in $anchor}]
	update_radiobutton $f.anchor_e [expr {"e" in $anchor}]
	update_radiobutton $f.anchor_sw [expr {"sw" in $anchor}]
	update_radiobutton $f.anchor_s [expr {"s" in $anchor}]
	update_radiobutton $f.anchor_se [expr {"se" in $anchor}]

}

proc text_clear_width {canvas} {
	variable properties_frame_for

	set_property $canvas -width 0 text
	update_bboxes $canvas text

	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.text
	update_spinbox $f.width 0
}

proc text_set_justify {canvas value} {
	variable properties_frame_for

	set_property $canvas -justify $value
	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.text
	set_radiobutton $f.justify_left $value left
	set_radiobutton $f.justify_center $value center
	set_radiobutton $f.justify_right $value right
	# update_bboxes not required!
}

proc text_set_anchor {canvas value} {
	variable properties_frame_for

	set_property $canvas -anchor $value text
	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.text
	set_radiobutton $f.anchor_nw $value nw
	set_radiobutton $f.anchor_n $value n
	set_radiobutton $f.anchor_ne $value ne
	set_radiobutton $f.anchor_w $value w
	set_radiobutton $f.anchor_center $value center
	set_radiobutton $f.anchor_e $value e
	set_radiobutton $f.anchor_sw $value sw
	set_radiobutton $f.anchor_s $value s
	set_radiobutton $f.anchor_se $value se
	update_bboxes $canvas text
}

# Special procedures for font sub-attributes
# (special versions of those in properties.tcl, properties_gui.tcl)

proc set_font_property {canvas propname value} {
	if {$value eq [mc multiple]} return
	set selection [filter_by_type $canvas [get_selected $canvas] text]
	foreach text $selection {
		set font [$canvas itemcget $text -font]
		dict set font $propname $value
		$canvas itemconfigure $text -font $font  ;# may throw an error
	}
	update_bboxes $canvas text  ;# bounding box is very likely to change
}

proc set_font_size {canvas spinbox} {
	if {[catch {set_font_property $canvas -size [expr {-[$spinbox get]}]} result]} {
		statusbar_error $canvas $result
	} else {
		statusbar_restore $canvas
	}
}

proc set_font_from_combobox {canvas combobox propname} {
	if {[catch {set_font_property $canvas $propname [$combobox get]} result]} {
		statusbar_error $canvas $result
	} else {
		statusbar_restore $canvas
	}
}

proc set_font_from_checkbutton {canvas checkbutton propname offValue onValue} {
	global $checkbutton
	if {! [info exists $checkbutton]} return  ;# "alternate" state
	set value [set $checkbutton]
	if {$value eq {0}} {
		set value $offValue
	} elseif {$value eq {1}} {
		set value $onValue
	} else {
		error "invalid value \"$value\" of checkbutton $checkbutton: expected 0 or 1"
	}
	if {[catch {set_font_property $canvas $propname $value} result]} {
		statusbar_error $canvas $result
	} else {
		statusbar_restore $canvas
	}
}

proc make_fontsize_spinbox_valid {canvas spinbox} {
	set selection [filter_by_type $canvas [get_selected $canvas] text]
	set font [get_text_properties $canvas $selection]
	update_spinbox $spinbox [dict get $font -size]
	statusbar_restore_later $canvas
}

}
