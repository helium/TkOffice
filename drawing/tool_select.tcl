# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

# Configuration
variable sel_dot_size 4
variable sel_color1 black
variable sel_color2 #777

variable x_move_last
variable y_move_last

# Select tool

proc left_button_press_select {canvas x y} {
	focus $canvas
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	if {"selection" in [$canvas gettags current]} {
		move_start $canvas $x $y
	} else {
		select_unmark_all $canvas
		set items [$canvas find withtag current]
		select_mark_item $canvas $items
		if {$items ne {}} {
			move_start $canvas $x $y
		}
	}
}

proc left_button_motion_select {canvas x y} {
	variable x_move_last
	variable y_move_last

	if {! [is_moving $canvas]} return

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]
	set dx [expr {$x - $x_move_last($canvas)}]
	set dy [expr {$y - $y_move_last($canvas)}]
	$canvas move selection $dx $dy
	$canvas move mark $dx $dy
	set x_move_last($canvas) $x
	set y_move_last($canvas) $y
}

proc left_button_release_select {canvas x y} {
	move_end $canvas
}

proc left_button_press_shift_select {canvas x y} {
	focus $canvas
	select_toggle_mark_item $canvas [$canvas find withtag current]
}

proc backspace_select {canvas} {
	select_delete_marked $canvas
}

proc escape_select {canvas} {
	select_unmark_all $canvas
}

proc tool_leave_select {canvas} {
	select_unmark_all $canvas
}

# Prerequisites for Select tool

proc select_draw_marks {canvas item} {
	variable sel_dot_size
	variable sel_color1
	variable sel_color2

	set already_existing [expr {[$canvas find withtag mark.$item] ne {}}]
	lassign [$canvas bbox $item] x1 y1 x2 y2
	set size $sel_dot_size
	foreach {rx1 ry1 rx2 ry2} [list \
		[expr {$x1 - $size}] [expr {$y1 - $size}] $x1 $y1 \
		[expr {$x1 - $size}] $y2 $x1 [expr {$y2 + $size}] \
		$x2 [expr {$y1 - $size}] [expr {$x2 + $size}] $y1 \
		$x2 $y2 [expr {$x2 + $size}] [expr {$y2 + $size}] \
	] dir {mark.nw mark.sw mark.ne mark.se} {
		if {$already_existing} {
			foreach mark [$canvas find withtag "mark.$item && $dir"] {
				$canvas coords $mark $rx1 $ry1 $rx2 $ry2
			}
			continue
		}
		# As Tk does not have "xor" fill mode, make a pattern that should be visible
		# on all possible backgrounds (except the very exact stipple we are creating).
		$canvas create rectangle $rx1 $ry1 $rx2 $ry2 -tags [list mark mark.$item $dir] \
			-fill $sel_color1 -outline $sel_color1 \
			-stipple gray50 -outlinestipple gray50 -offset 0,0 -outlineoffset 0,0
		$canvas create rectangle $rx1 $ry1 $rx2 $ry2 -tags [list mark mark.$item $dir] \
			-fill $sel_color2 -outline $sel_color2 \
			-stipple gray50 -outlinestipple gray50 -offset 0,1 -outlineoffset 0,1
	}
}

proc select_mark_item {canvas item} {
	if {$item eq {}} return
	if {"mark" in [$canvas gettags $item]} return

	$canvas addtag selection withtag $item
	event generate $canvas <<SelectionUpdate>> -when head
	select_draw_marks $canvas $item
}

proc select_unmark_item {canvas item} {
	if {$item eq {}} return
	$canvas dtag $item selection
	$canvas delete withtag mark.$item
	event generate $canvas <<SelectionUpdate>> -when head
}

proc select_unmark_all {canvas} {
	$canvas dtag selection
	$canvas delete withtag mark
	event generate $canvas <<SelectionUpdate>> -when head
}

proc select_toggle_mark_item {canvas item} {
	if {$item eq {}} return
	if {"selection" ni [$canvas gettags $item]} {
		select_mark_item $canvas $item
	} else {
		select_unmark_item $canvas $item
	}
}

proc select_delete_marked {canvas} {
	$canvas delete withtag selection
	$canvas delete withtag mark
	event generate $canvas <<SelectionUpdate>> -when head
	update_scrollregion $canvas
}

# Behavior when pasting

proc clipboard_paste_before_select {canvas} {
	select_unmark_all $canvas
}

proc clipboard_paste_after_select {canvas} {
	foreach item [$canvas find withtag !__before_pasting] {
		select_mark_item $canvas $item
	}
}

}
