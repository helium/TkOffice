# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

# Operations changing the stacking order of items

proc raise_selection_to_top {canvas} {
	$canvas raise {(selection && !mark) || item_being_created}
}

proc raise_selection {canvas} {
	set searchSpec {(selection && !mark) || item_being_created}
	set selection [$canvas find withtag $searchSpec]
	if {$selection eq {}} return
	set topSelected [lindex $selection end]; unset selection

	# Try to skip "raise" operations that change nothing in the drawing:
	# Raise above an item that is actually overlapping the selection.
	set overlapping [$canvas find overlapping {*}[$canvas bbox $searchSpec]]
	set len [llength $overlapping]
	set pos [lsearch -exact $overlapping $topSelected]
	# The "while" loop is only necessary to skip items containing the "mark" tag.
	while {[incr pos] < $len} {
		set aboveThis [lindex $overlapping $pos]
		if {"mark" ni [$canvas gettags $aboveThis]} {
			$canvas raise $searchSpec $aboveThis
			break
		}
	}
}

proc lower_selection {canvas} {
	set searchSpec {(selection && !mark) || item_being_created}
	set selection [$canvas find withtag $searchSpec]
	if {$selection eq {}} return
	set lowestSelected [lindex $selection 0]; unset selection

	# Try to skip "lower" operations that change nothing in the drawing:
	# Lower below an item that is actually overlapping the selection.
	set overlapping [$canvas find overlapping {*}[$canvas bbox $searchSpec]]
	set pos [lsearch -exact $overlapping $lowestSelected]
	# The "while" loop is only necessary to skip items containing the "mark" tag.
	while {[incr pos -1] >= 0} {
		set belowThis [lindex $overlapping $pos]
		if {"mark" ni [$canvas gettags $belowThis]} {
			$canvas lower $searchSpec $belowThis
			break
		}
	}
}

proc lower_selection_to_lowest {canvas} {
	$canvas lower {(selection && !mark) || item_being_created}
}

}
