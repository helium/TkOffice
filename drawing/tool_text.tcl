# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

# Configuration
variable text_anchormark_size 2
variable text_anchormark_color1 black
variable text_anchormark_color2 #777

# Text tool

proc motion_text {canvas x y} {
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	if {[text_anchormark_touching $canvas $x $y]} {
		$canvas configure -cursor fleur
	} else {
		$canvas configure -cursor xterm
	}
}

proc left_button_press_text {canvas x y} {
	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	focus $canvas
	set text [$canvas find withtag current]
	if {[text_anchormark_touching $canvas $x $y]} {
		$canvas focus {}  ;# remove blinking cursor
		move_start $canvas $x $y
	} elseif {$text ne {} && [$canvas type $text] eq "text"} {
		text_delete_if_empty $canvas
		text_anchormark_set $canvas {*}[$canvas coords $text]
		$canvas dtag selection
		$canvas addtag selection withtag $text
		event generate $canvas <<SelectionUpdate>> -when head
		$canvas focus $text
		$canvas icursor $text [text_index_at $canvas $text $x $y]
	} else {
		text_delete_if_empty $canvas
		text_anchormark_clear $canvas
		$canvas dtag selection

		set default_font [dict create \
			-family sans-serif -size [expr {-round(10 * [tk scaling])}] \
			-weight normal -slant roman -underline 0 -overstrike 0]
		set text [$canvas create text $x $y -font $default_font -anchor w -tags selection]
		event generate $canvas <<SelectionUpdate>> -when head
		text_anchormark_set $canvas $x $y
		$canvas focus $text
	}
}

proc left_button_motion_text {canvas x y} {
	variable x_move_last
	variable y_move_last

	set x [$canvas canvasx $x]
	set y [$canvas canvasy $y]

	if {[is_moving $canvas]} {
		set dx [expr {$x - $x_move_last($canvas)}]
		set dy [expr {$y - $y_move_last($canvas)}]
		$canvas move selection $dx $dy
		$canvas move anchormark $dx $dy
		set x_move_last($canvas) $x
		set y_move_last($canvas) $y
	}
}

proc left_button_release_text {canvas x y} {
	if {[is_moving $canvas]} {
		move_end $canvas
		$canvas focus selection
	}
}

proc key_press_text {canvas character} {
	if {$character eq {}} return
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	$canvas insert $text insert $character
	update_scrollregion $canvas
}

proc backspace_text {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	set index [$canvas index $text insert]
	if {$index < 1} return
	$canvas dchars $text [expr {$index-1}]
	update_scrollregion $canvas
}

proc delete_key_text {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	$canvas dchars $text insert
	update_scrollregion $canvas
}

proc left_key_text {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	set index [$canvas index $text insert]
	if {$index < 1} return
	$canvas icursor $text [expr {$index - 1}]
}

proc right_key_text {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	if {$text ne [$canvas focus]} return

	set index [$canvas index $text insert]
	$canvas icursor $text [expr {$index + 1}]
}

proc escape_text {canvas} {
	text_delete_if_empty $canvas
	text_anchormark_clear $canvas
	$canvas dtag selection
	event generate $canvas <<SelectionUpdate>> -when head
	$canvas focus {}
}

proc clipboard_paste_tool_text {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {} || $text ne [$canvas focus]} {
		clipboard_paste $canvas
		return
	}

	if {[catch {
		set types [clipboard get -type TARGETS]
	}]} {
		return
	}
	if {"UTF8_STRING" in $types} {
		set clipboard [clipboard get -type UTF8_STRING]
	} elseif {"STRING" in $types} {
		set clipboard [clipboard get -type STRING]
	} else {
		return
	}

	if {$clipboard eq {}} return
	$canvas insert $text insert $clipboard
	update_scrollregion $canvas
}

proc tool_leave_text {canvas} {
	text_delete_if_empty $canvas
	text_anchormark_clear $canvas
	$canvas dtag selection
	event generate $canvas <<SelectionUpdate>> -when head
	$canvas focus {}
}

# Prerequisites for Text tool

proc text_anchormark_set {canvas x y} {
	variable text_anchormark_size
	variable text_anchormark_color1
	variable text_anchormark_color2

	set s $text_anchormark_size
	set point_coords [list [expr {$x-$s}] [expr {$y-$s}] [expr {$x+$s}] [expr {$y+$s}]]
	set anchormarks [$canvas find withtag anchormark]
	if {$anchormarks ne {}} {
		foreach item $anchormarks {
			$canvas coords $item $point_coords
		}
	} else {
		$canvas create rectangle $point_coords -tags anchormark \
			-outline $text_anchormark_color1 -outlinestipple gray50 -outlineoffset 0,0
		$canvas create rectangle $point_coords -tags anchormark \
			-outline $text_anchormark_color2 -outlinestipple gray50 -outlineoffset 0,1
	}
}

proc text_anchormark_clear {canvas} {
	$canvas delete anchormark
}

proc text_anchormark_touching {canvas x y} {
	if {[$canvas find withtag anchormark] eq {}} {
		return false
	}
	lassign [$canvas coords anchormark] x1 y1 x2 y2
	expr {$x1 <= $x && $x <= $x2 && $y1 <= $y && $y <= $y2}
}

proc text_delete_if_empty {canvas} {
	set text [$canvas find withtag selection]
	if {$text eq {}} return
	# Also true for empty string:
	if {[string is space [$canvas itemcget $text -text]]} {
		$canvas delete $text
		event generate $canvas <<SelectionUpdate>> -when head
	}
}

proc text_index_at {canvas textitem x y} {
	# The dependency of [$canvas index ... @x,y] on the scrollregion
	# (not just the canvasx,canvasy coordinate system) seems to be a Tk bug.
	set scrollregion [$canvas cget -scrollregion]
	if {$scrollregion ne {}} {
		lassign $scrollregion x1 y1
	} else {
		set x1 0
		set y1 0
	}
	$canvas index $textitem @[expr {$x-$x1}],[expr {$y-$y1}]
}

}
