# Copyright 2021, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

proc properties_subframe_arc {f canvas} {
	ttk::labelframe $f -text [mc arc_properties] -style Panel.TLabelframe

	grid [ttk::label $f.style_l -text [mc arc_type]] \
		 [ttk::frame $f.style_f] - - \
		 -sticky nsw -pady 2
	grid [ttk::button $f.style_arc -text [mc arc_type_arc] -style Toolbutton] \
		 [ttk::button $f.style_chord -text [mc arc_type_chord] -style Toolbutton] \
		 [ttk::button $f.style_pieslice -text [mc arc_type_pieslice] -style Toolbutton] \
		 -in $f.style_f

	grid [ttk::label $f.start_l -text [mc arc_start]] \
		 [ttk::spinbox $f.start -from -Inf -to Inf -width 7] - - \
		 -sticky nsw -pady 2

	grid [ttk::label $f.extent_l -text [mc arc_extent]] \
		 [ttk::spinbox $f.extent -from -Inf -to Inf -width 7] - - \
		 -sticky nsw -pady 2

	set_arc_properties_gui_bindings $f $canvas
	return $f
}

proc set_arc_properties_gui_bindings {f canvas} {
	# Type
	$f.style_arc configure -command [list drawing::arc_set_style $canvas arc]
	$f.style_chord configure -command [list drawing::arc_set_style $canvas chord]
	$f.style_pieslice configure -command [list drawing::arc_set_style $canvas pieslice]
	# Start
	bind_spinbox $f.start "
		drawing::set_from_spinbox $canvas %W -start
		drawing::update_bboxes $canvas arc
	"
	bind $f.start <FocusOut> [list +drawing::make_spinbox_valid $canvas %W -start]
	# Extent
	bind_spinbox $f.extent "
		drawing::set_from_spinbox $canvas %W -extent
		drawing::update_bboxes $canvas arc
	"
	bind $f.extent <FocusOut> [list +drawing::make_spinbox_valid $canvas %W -extent]
}

proc update_arc_properties_gui {canvas prop_frame selection types} {
	set f $prop_frame.arc
	enable_labelframe_cond $f [expr {"arc" in $types}]

	set style [get_enum_property $canvas $selection -style]
	enable_widgets_cond [expr {$style ne {}}] \
		$f.style_l $f.style_arc $f.style_chord $f.style_pieslice
	update_radiobutton $f.style_arc [expr {"arc" in $style}]
	update_radiobutton $f.style_chord [expr {"chord" in $style}]
	update_radiobutton $f.style_pieslice [expr {"pieslice" in $style}]

	set start [get_number_property $canvas $selection -start]
	enable_widgets_cond [expr {$start ne {}}] \
		$f.start_l $f.start
	update_spinbox $f.start $start

	set extent [get_number_property $canvas $selection -extent]
	enable_widgets_cond [expr {$extent ne {}}] \
		$f.extent_l $f.extent
	update_spinbox $f.extent $extent
}

proc arc_set_style {canvas value} {
	variable properties_frame_for

	set_property $canvas -style $value
	set prop_frame $properties_frame_for($canvas)
	set f $prop_frame.arc
	set_radiobutton $f.style_arc $value arc
	set_radiobutton $f.style_chord $value chord
	set_radiobutton $f.style_pieslice $value pieslice
	update_bboxes $canvas arc
}

}
