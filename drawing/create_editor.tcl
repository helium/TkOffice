# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

proc create_editor {f} {
	# The argument $f should be a frame, ttk::frame or toplevel.

	foreach child {document toolbar xscroll yscroll properties properties_scroll statusbar} {
		set $child [fix_toplevel_child $f.$child]
	}

	grid [canvas $document -background white -closeenough 4] \
		-row 0 -column 1 -sticky nesw
	grid [toolbar_create $toolbar -for $document] \
		-row 0 -column 0 -sticky n
	grid [ttk::scrollbar $xscroll -orient horizontal] \
		-row 1 -column 1 -sticky we
	grid [ttk::scrollbar $yscroll -orient vertical] \
		-row 0 -column 2 -sticky ns
	$document configure -xscrollcommand [list $xscroll set] \
		-yscrollcommand [list $yscroll set]
	$xscroll configure -command [list $document xview]
	$yscroll configure -command [list $document yview]

	drawing::properties_frame_create $properties -for $document
	grid [make_scrolling_frame $properties_scroll $properties] \
		-row 0 -column 3 -rowspan 2 -sticky nesw
	grid [drawing::statusbar_create $statusbar -for $document] \
		-row 2 -column 0 -columnspan 4 -sticky we

	grid rowconfigure $f 0 -weight 1
	grid columnconfigure $f 1 -weight 1

	highlight_setup $document
	drawing::set_canvas_bindings $document

	set ::drawing::activetool($document) select
	$toolbar.tools.select invoke

	bind $document <Configure> {
		bind %W <Configure> {}
		event generate %W <<SelectionUpdate>> -when tail
	}
}

}
