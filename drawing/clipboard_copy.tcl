# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

proc copy_selection {w} {
	set tag_expr {selection && !mark}
	if {[$w find withtag $tag_expr] ne {}} {
		copy_to_clipboard $w $tag_expr
	}
}

proc cut_selection {w} {
	set tag_expr {selection && !mark}
	if {[$w find withtag $tag_expr] ne {}} {
		copy_to_clipboard $w $tag_expr
		$w delete withtag $tag_expr
		$w delete withtag mark  ;# these marks belong to selected (and thus deleted) objects
		event generate $w <<SelectionUpdate>> -when head
		update_scrollregion $w
	}
}

proc copy_to_clipboard {w tag_expr} {
	clipboard clear

	# 1. Individual objects
	foreach item [$w find withtag $tag_expr] {
		set cmd [list]
		lappend cmd [$w type $item]
		lappend cmd {*}[strip_coords [$w coords $item]]

		set config [dict create]
		foreach prop [$w itemconfigure $item] {
			set key [lindex $prop 0]
			set value [lindex $prop 4]
			set default [lindex $prop 3]
			if {$value in {0.0 0} && $default in {0.0 0}} continue
			if {$value ne $default || $key eq {-anchor} || $value eq {black}} {
				if {[string is double $value] && [string match *.0 $value]} {
					set value [string map {.0 {}} $value]
				}
				dict set config $key $value
			}
		}
		# Remove "selection" and "current" tags
		if {[dict exists $config -tags]} {
			set tags [dict get $config -tags]
			set pos [lsearch -exact $tags selection]
			if {$pos >= 0} {
				set tags [lreplace $tags $pos $pos]
			}
			set pos [lsearch -exact $tags current]
			if {$pos >= 0} {
				set tags [lreplace $tags $pos $pos]
			}
			if {$tags eq {}} {
				dict unset config -tags
			} else {
				dict set config -tags $tags
			}
		}

		if {[dict exists $config -font]} {
			dict set config -font [::font::simplify [dict get $config -font]]
		}

		# Make "-text" come first in attributes list
		if {! [dict exists $config -text]} {
			lappend cmd {*}$config
		} else {
			set text [dict get $config -text]
			dict unset config -text
			lappend cmd -text $text
			lappend cmd {*}$config
		}

		clipboard append -type TkOfficeVectorDrawing $cmd
		clipboard append -type TkOfficeVectorDrawing \n
	}
}

}
