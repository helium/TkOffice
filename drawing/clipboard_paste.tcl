# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

proc clipboard_paste {w} {
	if {[catch {
		set types [clipboard get -type TARGETS]
	}]} {
		return false
	}

	if {"TkOfficeVectorDrawing" in $types} {
		clipboard_paste_drawing $w [clipboard get -type TkOfficeVectorDrawing]
	} else {
		return false
	}
	return true
}

proc clipboard_paste_drawing_main {w data} {
	set interp [setup_fileformat_interp $w]
	$interp eval $data  ;# main action
	interp delete $interp
	update_scrollregion $w
}

proc clipboard_paste_drawing {w data {same_place no}} {
	variable activetool

	set tool $activetool($w)
	switch $tool {
		select {clipboard_paste_before_$tool $w}
	}
	$w addtag __before_pasting all

	clipboard_paste_drawing_main $w $data

	handle_pasted_items_outside_visible_region $w !__before_pasting $same_place

	switch $tool {
		select {clipboard_paste_after_$tool $w}
	}
	$w dtag __before_pasting
}

proc handle_pasted_items_outside_visible_region {canvas pasted_tags same_place} {
	# Determine the currently visible area of a canvas.
	# Note that xview and yview would be unreliable: when a canvas is scrolled
	# far left away from its scrollregion, [xview] returns {0.0 0.0} in all cases!
	set width [winfo width $canvas]
	set height [winfo height $canvas]
	set left [$canvas canvasx 0]
	set right [$canvas canvasx $width]
	set top [$canvas canvasy 0]
	set bottom [$canvas canvasy $height]

	lassign [$canvas cget -scrollregion] s_left s_top s_right s_bottom
	lassign [$canvas bbox $pasted_tags] x1 y1 x2 y2
	if {$x1 > $right || $x2 < $left || $y1 > $bottom || $y2 < $top} {
		# Pasted elements would be completely invisible, move them or change view!
		set pasted_xmean [expr {($x1 + $x2) * 0.5}]
		set pasted_ymean [expr {($y1 + $y2) * 0.5}]
		set window_xmean [expr {($left + $right) * 0.5}]
		set window_ymean [expr {($top + $bottom) * 0.5}]

		set dx [expr {$window_xmean - $pasted_xmean}]
		set dy [expr {$window_ymean - $pasted_ymean}]
		if {$x1 <= $right && $x2 >= $left} {set dx 0}
		if {$y1 <= $bottom && $y2 >= $top} {set dy 0}

		if {!$same_place} {
			$canvas move $pasted_tags $dx $dy
		} else {
			# $left / ($s_right-$s_left) is close to the previous xview.
			$canvas xview moveto [expr {($left - $dx) / double($s_right-$s_left)}]
			$canvas yview moveto [expr {($top - $dy) / double($s_bottom-$s_top)}]
		}
	}
}

}
