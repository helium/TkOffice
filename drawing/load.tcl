# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

namespace eval drawing {

proc reset_widget {w} {
	$w delete all
	$w configure -background white

	# Scroll such that (x=0,y=0) is at the top left corner
	$w configure -scrollregion {}
	$w xview moveto 0
	$w yview moveto 0
}

proc setup_fileformat_interp {w} {
	set interp [interp create -safe]
	# Also remove the "harmless" commands from the interpreter,
	# as they are not part of the file format without macros
	foreach cmd [$interp eval {info commands}] {$interp hide $cmd}

	foreach cmd {background arc line oval polygon rectangle text} {
		$interp alias $cmd ::drawing::fileformat::$cmd $w
	}
	return $interp
}

proc load_widget_from_string {w data} {
	reset_widget $w
	set interp [setup_fileformat_interp $w]
	$interp eval $data  ;# main action
	interp delete $interp
	update_scrollregion $w
}

proc load_widget_from_file {w filePath} {
	reset_widget $w
	set interp [setup_fileformat_interp $w]

	# The "source" command always reads files using the current system
	# encoding. Temporarily set that to utf-8 as TkOffice documents are
	# UTF8-encoded on all platforms.
	set system_encoding [encoding system]
	encoding system utf-8
	$interp invokehidden source $filePath
	encoding system $system_encoding

	interp delete $interp
	update_scrollregion $w
}

namespace eval fileformat {

	proc background {w color} {
		$w configure -background $color
	}

	proc arc {w args} {
		$w create arc {*}$args
	}

	proc line {w args} {
		$w create line {*}$args
	}

	proc oval {w args} {
		$w create oval {*}$args
	}

	proc polygon {w args} {
		$w create polygon {*}$args
	}

	proc rectangle {w args} {
		$w create rectangle {*}$args
	}

	proc text {w args} {
		# Force all font specifications into key-value format:
		set pos [lsearch -all -exact $args "-font"]
		if {$pos ne {}} {
			# support command lines that mention "-font" multiple times
			set pos [lindex $pos end]
			# access the list element after "-font"
			incr pos
			if {$pos < [llength $args]} {
				set font [lindex $args $pos]
				lset args $pos [::font::dict_size_to_px [::font::to_dict $font]]
			}
		}

		$w create text {*}$args
	}
}

}
