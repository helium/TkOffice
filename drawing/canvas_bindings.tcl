# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval drawing {

# Bindings that call the appropriate tool-specific procs

proc set_canvas_bindings {canvas} {
	bind $canvas <Motion> {drawing::motion %W %x %y}
	bind $canvas <ButtonPress-1> {drawing::left_button_press %W %x %y}
	bind $canvas <Shift-ButtonPress-1> {drawing::left_button_press_shift %W %x %y}
	bind $canvas <B1-Motion> {drawing::left_button_motion %W %x %y}
	bind $canvas <ButtonRelease-1> {drawing::left_button_release %W %x %y}
	bind $canvas <ButtonPress-3> {drawing::right_button_press %W %x %y}
	bind $canvas <Escape> {drawing::escape %W}
	bind $canvas <BackSpace> {drawing::backspace %W %x %y}
	bind $canvas <Delete> {drawing::delete_key %W}
	bind $canvas <Left> {drawing::left_key %W}
	bind $canvas <Right> {drawing::right_key %W}

	bind $canvas <ButtonPress-2> {%W scan mark %x %y}
	bind $canvas <B2-Motion> {%W scan dragto %x %y 1}

	bind $canvas <<Cut>> {drawing::cut_selection %W}
	bind $canvas <<Copy>> {drawing::copy_selection %W}
	bind $canvas <<Paste>> {drawing::clipboard_paste_tool %W}

	bind $canvas <KeyPress> {drawing::key_press %W %A}
	# <Return> and <KP_Enter> would otherwise produce %A as \r instead of \n.
	bind $canvas <Return> {drawing::key_press %W \n}
	bind $canvas <KP_Enter> {drawing::key_press %W \n}
	# Ignore all Alt, Meta, and Control keypresses unless explicitly bound.
	# Otherwise, if a widget binding for one of these is defined, the
	# <KeyPress> class binding will also fire and insert the character,
	# which is wrong.
	bind $canvas <Alt-KeyPress> {return}
	bind $canvas <Meta-KeyPress> {return}
	bind $canvas <Control-KeyPress> {return}
	bind $canvas <Mod4-KeyPress> {return}
	if {[tk windowingsystem] eq "aqua"} {
		bind $canvas <Command-KeyPress> {return}
	}
}

proc tool_changed {canvas new_tool} {
	variable activetool

	tool_leave $canvas $activetool($canvas)
	toolbar_tool_select $canvas $new_tool
	set activetool($canvas) $new_tool
	tool_enter $canvas $new_tool
}

proc tool_enter {canvas tool} {
	switch $tool {
		select {$canvas configure -cursor arrow}
		points {$canvas configure -cursor dotbox}
		rectangle - oval - arc - line - polygon {$canvas configure -cursor crosshair}
		text {$canvas configure -cursor xterm}
	}
	if {$tool eq {points}} {
		toolbar_points_operation_buttons $canvas normal
	} else {
		toolbar_points_operation_buttons $canvas disabled
	}
}

proc tool_leave {canvas tool} {
	switch $tool {
		select - points - line - polygon - text {tool_leave_$tool $canvas}
	}
}

proc motion {canvas x y} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		line - polygon - text {motion_$tool $canvas $x $y}
	}
}

proc left_button_press {canvas x y} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		select - points - rectangle - oval - arc - line - polygon - text {left_button_press_$tool $canvas $x $y}
	}
}

proc left_button_press_shift {canvas x y} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		select - points {left_button_press_shift_$tool $canvas $x $y}
		rectangle - oval - arc - line - polygon {left_button_press_$tool $canvas $x $y}
	}
}

proc left_button_motion {canvas x y} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		select - points - rectangle - oval - arc - text {left_button_motion_$tool $canvas $x $y}
		line - polygon {motion_$tool $canvas $x $y}
	}
}

proc left_button_release {canvas x y} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		select - points - rectangle - oval - arc - text {left_button_release_$tool $canvas $x $y}
	}
}

proc right_button_press {canvas x y} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		rectangle - oval - arc - line - polygon {right_button_press_$tool $canvas $x $y}
	}
}

proc escape {canvas} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		select - rectangle - oval - arc - line - polygon - text {escape_$tool $canvas}
	}
}

proc backspace {canvas x y} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		select - points - text {backspace_$tool $canvas}
		line - polygon {backspace_$tool $canvas $x $y}
	}
}

proc delete_key {canvas} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		text {delete_key_$tool $canvas}
	}
}

proc left_key {canvas} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		text {left_key_$tool $canvas}
	}
}

proc right_key {canvas} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		text {right_key_$tool $canvas}
	}
}

proc key_press {canvas character} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		text {key_press_$tool $canvas $character}
	}
}

proc clipboard_paste_tool {canvas} {
	variable activetool
	set tool $activetool($canvas)

	switch $tool {
		text {clipboard_paste_tool_$tool $canvas}
		default {clipboard_paste $canvas}
	}
}

}
