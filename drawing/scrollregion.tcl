# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

namespace eval drawing {

proc update_scrollregion {canvas} {
	$canvas configure -scrollregion [$canvas bbox all] -confine off
}

}
