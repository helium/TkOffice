#!/usr/bin/wish8.5
# Copyright 2023 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam

pack [ttk::panedwindow .pw -orient horizontal] -fill both -expand true
.pw add [ttk::frame .pw.canvasframe]
pack [ttk::frame .pw.canvasframe.toolbar] -side left -fill y
pack [ttk::radiobutton .pw.canvasframe.toolbar.add -style Toolbutton -text Add] -fill x
pack [ttk::radiobutton .pw.canvasframe.toolbar.delete -style Toolbutton -text Delete] -fill x
pack [ttk::radiobutton .pw.canvasframe.toolbar.move -style Toolbutton -text Move] -fill x
pack [canvas .pw.canvasframe.canvas -background white] -fill both -expand true

.pw add [ttk::labelframe .pw.undoframe -text "Undo list:"]
pack [ttk::treeview .pw.undolist -show tree] -in .pw.undoframe -fill both -expand true
.pw.undolist tag configure inactive -foreground grey
.pw add [ttk::labelframe .pw.redoframe -text "Redo list:"]
pack [ttk::treeview .pw.redolist -show tree] -in .pw.redoframe -fill both -expand true


foreach radiobutton {add delete move} {
	.pw.canvasframe.toolbar.$radiobutton configure -variable tool_mode -value $radiobutton
}

trace add variable tool_mode write tool_changed

proc tool_changed {args} {
	global tool_mode
	set canvas .pw.canvasframe.canvas
	switch $tool_mode add {
		bindtags $canvas CanvasAddMode
		$canvas configure -cursor arrow
	} delete {
		bindtags $canvas CanvasDeleteMode
		$canvas configure -cursor X_cursor
	} move {
		bindtags $canvas CanvasMoveMode
		$canvas configure -cursor fleur
	}
}

proc rectangle_coords_from_xy {x y} {
	list [expr {$x-30}] [expr {$y-25}] [expr {$x+30}] [expr {$y+25}]
}

bind CanvasAddMode <Enter> {canvas_addmode_enter %W %x %y}
proc canvas_addmode_enter {canvas x y} {
	$canvas create rectangle [rectangle_coords_from_xy $x $y] \
		-tag PreviewRectangle -outline #a88 -outlinestipple gray50
}
bind CanvasAddMode <Leave> {canvas_addmode_leave %W}
proc canvas_addmode_leave {canvas} {
	$canvas delete PreviewRectangle
}
bind CanvasAddMode <Motion> {canvas_addmode_motion %W %x %y}
proc canvas_addmode_motion {canvas x y} {
	$canvas coords PreviewRectangle [rectangle_coords_from_xy $x $y]
}
bind CanvasAddMode <ButtonPress> {canvas_addmode_buttonpress %W %x %y}
proc canvas_addmode_buttonpress {canvas x y} {
	global created_items_counter
	incr created_items_counter
	set r [expr {int(rand()*200)}]
	set g [expr {int(rand()*200)}]
	set b [expr {int(rand()*200)}]
	set color [format "#%02x%02x%02x" $r $g $b]
	undoable_edit_step Create $x $y $color $created_items_counter
}

bind CanvasDeleteMode <ButtonPress> {canvas_deletemode_buttonpress %W %x %y}
proc canvas_deletemode_buttonpress {canvas x y} {
	set tag [lindex [$canvas gettags current] 0]
	set number [string map {item# {}} $tag]
	if {$number eq {}} return
	undoable_edit_step Delete $number
}

bind CanvasMoveMode <ButtonPress-1> {canvas_movemode_buttonpress %W %x %y}
proc canvas_movemode_buttonpress {canvas x y} {
	global moved_item
	global moved_orig_xy
	global moved_prev_mouse_xy

	set tag [lindex [$canvas gettags current] 0]
	set moved_item $tag
	set moved_prev_mouse_xy [list $x $y]
	if {$moved_item eq {}} return

	lassign [$canvas find withtag $tag] rectangle text
	lassign [$canvas coords $text] x y
	set x [expr {int($x)}]
	set y [expr {int($y)}]
	set moved_orig_xy [list $x $y]
}
bind CanvasMoveMode <B1-Motion> {canvas_movemode_motion %W %x %y}
proc canvas_movemode_motion {canvas x y} {
	global moved_item
	global moved_orig_xy
	global moved_prev_mouse_xy

	if {$moved_item eq {}} return
	lassign $moved_prev_mouse_xy prev_x prev_y
	$canvas move $moved_item [expr {$x-$prev_x}] [expr {$y-$prev_y}]
	set moved_prev_mouse_xy [list $x $y]
}
bind CanvasMoveMode <ButtonRelease-1> {canvas_movemode_buttonrelease %W}
proc canvas_movemode_buttonrelease {canvas} {
	global moved_item
	global moved_orig_xy

	if {$moved_item eq {}} return
	lassign [canvas_movemode_move_back $canvas] dx dy
	set number [string map {item# {}} $moved_item]
	set moved_item {}
	undoable_edit_step Move $number $dx $dy
}
bind . <Escape> {canvas_movemode_escape .pw.canvasframe.canvas}
proc canvas_movemode_escape {canvas} {
	global moved_item
	global moved_orig_xy

	if {$moved_item eq {}} return
	canvas_movemode_move_back $canvas
	set moved_item {}
}

proc canvas_movemode_move_back {canvas} {
	global moved_item
	global moved_orig_xy

	if {$moved_item eq {}} return
	lassign [$canvas find withtag $moved_item] rectangle text
	lassign [$canvas coords $text] x y
	lassign $moved_orig_xy orig_x orig_y
	set dx [expr {int($x-$orig_x)}]
	set dy [expr {int($y-$orig_y)}]
	$canvas move $moved_item [expr {-$dx}] [expr {-$dy}]

	return [list $dx $dy]
}


proc Create {x y color number} {
	set canvas .pw.canvasframe.canvas
	$canvas create rectangle [rectangle_coords_from_xy $x $y] \
		-outline {} -fill $color -tag item#$number
	$canvas create text $x $y -fill white -text $number -tag item#$number
	return [list Delete $number]
}

proc Delete {number} {
	set canvas .pw.canvasframe.canvas
	lassign [$canvas find withtag item#$number] rectangle text
	set color [$canvas itemcget $rectangle -fill]
	lassign [$canvas coords $text] x y
	set x [expr {int($x)}]
	set y [expr {int($y)}]
	$canvas delete item#$number
	return [list Create $x $y $color $number]
}

proc Move {number dx dy} {
	set canvas .pw.canvasframe.canvas
	$canvas move item#$number $dx $dy
	return [list Move $number [expr {-$dx}] [expr {-$dy}]]
}


set undo_list [list]
set undo_pos 0
set redo_list [list]

proc undoable_edit_step {command args} {
	global undo_list undo_pos redo_list

	# Do not bloat the undo/redo lists on repetitive
	# "do manually" - undo - "do the same manually"
	if {[lindex $redo_list end] eq [list $command {*}$args]} {
		redo
		return
	}

	if {$undo_pos < [llength $undo_list]} {
		lappend undo_list [concat Group [lreverse $redo_list]]
		set undo_pos [llength $undo_list]
		set redo_list [list]
	}

	puts -nonewline "DO: $command $args"
	set opposite [$command {*}$args]
	puts "  OPPOSITE: $opposite"

	lappend undo_list $opposite
	incr undo_pos

	update_undo_redo_display
}

bind . <Control-z> undo
bind . <Control-Z> redo
bind . <Control-y> redo

proc undo {} {
	global undo_list undo_pos redo_list

	set entry [lindex $undo_list $undo_pos-1]
	if {$entry eq {}} return
	puts -nonewline "UNDO: $entry"
	set opposite [{*}$entry]  ;# execute $entry
	puts "  OPPOSITE: $opposite"

	lappend redo_list $opposite
	incr undo_pos -1

	update_undo_redo_display
}

proc redo {} {
	global undo_list undo_pos redo_list

	set entry [lindex $redo_list end]
	if {$entry eq {}} return
	puts -nonewline "REDO: $entry"
	set opposite [{*}$entry]  ;# execute $entry
	puts -nonewline "  OPPOSITE: $opposite"

	set redo_list [lrange $redo_list 0 end-1]
	incr undo_pos
	puts "  SHOULD MATCH: [lindex $undo_list $undo_pos-1]"

	update_undo_redo_display
}

proc Group {args} {
	set opposites [list]
	foreach entry $args {
		lappend opposites [{*}$entry]  ;# execute $entry
	}
	return [concat Group [lreverse $opposites]]
}


proc update_undo_redo_display {} {
	global undo_list undo_pos redo_list

	update_treeview .pw.undolist $undo_list $undo_pos
	update_treeview .pw.redolist $redo_list [llength $redo_list]
}

proc update_treeview {treeview list valid_entries} {
	$treeview delete [$treeview children {}]
	set tag {}
	foreach item $list {
		incr items_processed
		if {$items_processed == $valid_entries+1} {
			set tag inactive
		}
		treeview_insert_recursive $treeview {} $item $tag
	}
}

proc treeview_insert_recursive {treeview parent item tag} {
	if {[lindex $item 0] ne "Group"} {
		$treeview insert $parent end -text $item -tag $tag
	} else {
		set sub_parent [$treeview insert $parent end -text Group -tag $tag -open true]
		foreach item [lrange $item 1 end] {
			treeview_insert_recursive $treeview $sub_parent $item $tag
		}
	}
}
