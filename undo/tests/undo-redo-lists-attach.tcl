#!/usr/bin/wish8.5
# Copyright 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam
. configure -background #dcdad5

### Test GUI ###

pack [ttk::frame .selector_frame] -pady {0 3}
pack [ttk::label .selector_label -text "Editor: "] -in .selector_frame -side left
pack [ttk::combobox .selector] -in .selector_frame -side left
pack [ttk::panedwindow .pw -orient horizontal] -fill both -expand true
.pw add [ttk::labelframe .pw.undoframe -text "Undo list:"] -weight 1
pack [ttk::treeview .pw.undolist -show tree] -in .pw.undoframe -fill both -expand true
.pw.undolist tag configure inactive -foreground grey
.pw add [ttk::labelframe .pw.redoframe -text "Redo list:"] -weight 1
pack [ttk::treeview .pw.redolist -show tree] -in .pw.redoframe -fill both -expand true

### Test GUI updater ###

proc update_undo_redo_display {undo_list undo_pos redo_list} {
	update_treeview .pw.undolist $undo_list $undo_pos
	update_treeview .pw.redolist $redo_list [llength $redo_list]
}

proc update_treeview {treeview list valid_entries} {
	$treeview delete [$treeview children {}]
	set tag {}
	foreach item $list {
		incr items_processed
		if {$items_processed == $valid_entries+1} {
			set tag inactive
		}
		treeview_insert_recursive $treeview {} $item $tag
	}
}

proc treeview_insert_recursive {treeview parent item tag} {
	if {[lindex $item 0] ne "undo::Group"} {
		$treeview insert $parent end -text [string map [list \n \u23CE] $item] -tag $tag
	} else {
		set sub_parent [$treeview insert $parent end -text Group -tag $tag -open true]
		foreach item [lrange $item 1 end] {
			treeview_insert_recursive $treeview $sub_parent $item $tag
		}
	}
}

### Attach to other process ###

set target [lindex $argv 0]
if {$target eq {}} {
	set target Main.tcl
}
if {$target ni [winfo interps]} {
	puts "Target '$target' not found!"
	if {[llength [winfo interps]] > 1} {
		puts "Start the TkOffice application first, or specify a different Tk target"
		puts "as argument. Currently available Tk applications are:"
		foreach application [winfo interps] {
			if {$application ne [tk appname]} {
				puts "  $application"
			}
		}
	} else {
		puts "Start the TkOffice application first."
	}
	exit 1
}

proc remote_fetch_keys {} {
	global target

	set keys [send $target {array names undo::undo_list_for}]
	.selector configure -values $keys
	if {[.selector get] eq {}} {
		.selector set [lindex $keys 0]
		if {[lindex $keys 0] ne {}} {
			remote_fetch [.selector get]
		}
	} elseif {[.selector get] ni $keys} {
		.selector set {}
		foreach treeview {.pw.undolist .pw.redolist} {
			$treeview delete [$treeview children {}]
		}
	}
}

proc remote_fetch {key} {
	global target

	if {$key ne [.selector get]} return
	if {$key ni [send $target array names undo::undo_list_for]} return
	set undo_list [send $target "get undo::undo_list_for(${key})"]
	set undo_pos [send $target "get undo::undo_pos_for(${key})"]
	set redo_list [send $target "get undo::redo_list_for(${key})"]
	update_undo_redo_display $undo_list $undo_pos $redo_list
}

send $target {
	namespace eval ::UNDOREMOTE {}
	proc ::UNDOREMOTE::trace_array {name1 name2 op} {
		after cancel ::UNDOREMOTE::array_changed
		after idle ::UNDOREMOTE::array_changed
		foreach var {undo_list_for undo_pos_for redo_list_for} {
			trace remove variable ::undo::${var}($name2) {write unset} [list ::UNDOREMOTE::trace_item $name2]
			if {$op eq {write}} {
				trace add variable ::undo::${var}($name2) {write unset} [list ::UNDOREMOTE::trace_item $name2]
			}
		}
	}
	proc ::UNDOREMOTE::array_changed {} {
		send undo-redo-lists-attach.tcl remote_fetch_keys
	}
	trace add variable ::undo::undo_list_for write ::UNDOREMOTE::trace_array

	proc ::UNDOREMOTE::trace_item {key name1 name2 op} {
		after cancel send undo-redo-lists-attach.tcl remote_fetch $key
		after idle send undo-redo-lists-attach.tcl remote_fetch $key
	}
	# Initialize if an editor is already open
	foreach key [array names ::undo::undo_list_for] {
		::UNDOREMOTE::trace_array ::undo::undo_list_for $key write
	}
}
wm protocol . WM_DELETE_WINDOW {
	if {$target in [winfo interps]} {
		send $target {
			foreach var {undo_list_for redo_list_for undo_pos_for} {
				trace remove variable ::undo::$var write ::UNDOREMOTE::trace_array
				foreach key [array names ::undo::$var] {
					trace remove variable ::undo::${var}($key) {write unset} [list ::UNDOREMOTE::trace_item $key]
				}
			}
			namespace delete ::UNDOREMOTE
		}
	}
	destroy .
}
