#!/usr/bin/wish8.5
# Copyright 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam

### Test GUI ###

pack [ttk::panedwindow .pw -orient horizontal] -fill both -expand true
.pw add [text .t]

.pw add [ttk::labelframe .pw.undoframe -text "Undo list:"]
pack [ttk::treeview .pw.undolist -show tree] -in .pw.undoframe -fill both -expand true
.pw.undolist tag configure inactive -foreground grey
.pw add [ttk::labelframe .pw.redoframe -text "Redo list:"]
pack [ttk::treeview .pw.redolist -show tree] -in .pw.redoframe -fill both -expand true


### Text Editing Bindings ###

bindtags .t {UndoText . all}

bind UndoText <1> {
	tk::TextButton1 %W %x %y
	%W tag remove sel 0.0 end
}
bind UndoText <B1-Motion> {
	set tk::Priv(x) %x
	set tk::Priv(y) %y
	tk::TextSelectTo %W %x %y
}
bind UndoText <Double-1> {
	set tk::Priv(selectMode) word
	tk::TextSelectTo %W %x %y
	catch {%W mark set insert sel.first}
}
bind UndoText <Triple-1> {
	set tk::Priv(selectMode) line
	tk::TextSelectTo %W %x %y
	catch {%W mark set insert sel.first}
}
bind UndoText <Shift-1> {
	tk::TextResetAnchor %W @%x,%y
	set tk::Priv(selectMode) char
	tk::TextSelectTo %W %x %y
}
bind UndoText <Double-Shift-1>	{
	set tk::Priv(selectMode) word
	tk::TextSelectTo %W %x %y 1
}
bind UndoText <Triple-Shift-1>	{
	set tk::Priv(selectMode) line
	tk::TextSelectTo %W %x %y
}
bind UndoText <B1-Leave> {
	set tk::Priv(x) %x
	set tk::Priv(y) %y
	tk::TextAutoScan %W
}
bind UndoText <B1-Enter> {
	tk::CancelRepeat
}
bind UndoText <ButtonRelease-1> {
	tk::CancelRepeat
}
# An operation that moves the insert mark without making it
# one end of the selection
bind UndoText <Control-1> {
	%W mark set insert @%x,%y
}
# stop an accidental double click triggering <Double-Button-1>
bind UndoText <Double-Control-1> { # nothing }
# stop an accidental movement triggering <B1-Motion>
bind UndoText <Control-B1-Motion> { # nothing }
bind UndoText <Left> {
	tk::TextSetCursor %W insert-1displayindices
}
bind UndoText <Right> {
	tk::TextSetCursor %W insert+1displayindices
}
bind UndoText <Up> {
	tk::TextSetCursor %W [tk::TextUpDownLine %W -1]
}
bind UndoText <Down> {
	tk::TextSetCursor %W [tk::TextUpDownLine %W 1]
}
bind UndoText <Shift-Left> {
	tk::TextKeySelect %W [%W index {insert - 1displayindices}]
}
bind UndoText <Shift-Right> {
	tk::TextKeySelect %W [%W index {insert + 1displayindices}]
}
bind UndoText <Shift-Up> {
	tk::TextKeySelect %W [tk::TextUpDownLine %W -1]
}
bind UndoText <Shift-Down> {
	tk::TextKeySelect %W [tk::TextUpDownLine %W 1]
}
bind UndoText <Control-Left> {
	tk::TextSetCursor %W [tk::TextPrevPos %W insert tcl_startOfPreviousWord]
}
bind UndoText <Control-Right> {
	tk::TextSetCursor %W [tk::TextNextWord %W insert]
}
bind UndoText <Control-Up> {
	tk::TextSetCursor %W [tk::TextPrevPara %W insert]
}
bind UndoText <Control-Down> {
	tk::TextSetCursor %W [tk::TextNextPara %W insert]
}
bind UndoText <Shift-Control-Left> {
	tk::TextKeySelect %W [tk::TextPrevPos %W insert tcl_startOfPreviousWord]
}
bind UndoText <Shift-Control-Right> {
	tk::TextKeySelect %W [tk::TextNextWord %W insert]
}
bind UndoText <Shift-Control-Up> {
	tk::TextKeySelect %W [tk::TextPrevPara %W insert]
}
bind UndoText <Shift-Control-Down> {
	tk::TextKeySelect %W [tk::TextNextPara %W insert]
}
bind UndoText <Prior> {
	tk::TextSetCursor %W [tk::TextScrollPages %W -1]
}
bind UndoText <Shift-Prior> {
	tk::TextKeySelect %W [tk::TextScrollPages %W -1]
}
bind UndoText <Next> {
	tk::TextSetCursor %W [tk::TextScrollPages %W 1]
}
bind UndoText <Shift-Next> {
	tk::TextKeySelect %W [tk::TextScrollPages %W 1]
}
bind UndoText <Control-Prior> {
	%W xview scroll -1 page
}
bind UndoText <Control-Next> {
	%W xview scroll 1 page
}

bind UndoText <Home> {
	tk::TextSetCursor %W {insert display linestart}
}
bind UndoText <Shift-Home> {
	tk::TextKeySelect %W {insert display linestart}
}
bind UndoText <End> {
	tk::TextSetCursor %W {insert display lineend}
}
bind UndoText <Shift-End> {
	tk::TextKeySelect %W {insert display lineend}
}
bind UndoText <Control-Home> {
	tk::TextSetCursor %W 1.0
}
bind UndoText <Control-Shift-Home> {
	tk::TextKeySelect %W 1.0
}
bind UndoText <Control-End> {
	tk::TextSetCursor %W {end - 1 indices}
}
bind UndoText <Control-Shift-End> {
	tk::TextKeySelect %W {end - 1 indices}
}

bind UndoText <Tab> {
	if {[%W cget -state] eq "normal"} {
		TextInsert %W \t
		focus %W
		break
	}
}
bind UndoText <Shift-Tab> {
	# Needed only to keep <Tab> binding from triggering;  doesn't
	# have to actually do anything.
	break
}
bind UndoText <Control-Tab> {
	focus [tk_focusNext %W]
}
bind UndoText <Control-Shift-Tab> {
	focus [tk_focusPrev %W]
}
bind UndoText <Return> {
    TextInsert %W \n
}
bind UndoText <Delete> {
	if {[tk::TextCursorInSelection %W]} {
		if {[%W compare sel.first != end-1c]} {
			undoable_edit_step Delete sel.first sel.last
		} else {
			# avoid no-change Delete action
			%W tag remove sel 1.0 end
		}
	} elseif {[%W compare end != insert+1c]} {
		undoable_edit_step Delete insert
		undo_join_enable
	}
	%W see insert
}
bind UndoText <BackSpace> {
	if {[tk::TextCursorInSelection %W]} {
		if {[%W compare sel.first != end-1c]} {
			undoable_edit_step Delete sel.first sel.last
		} else {
			# avoid no-change Delete action
			%W tag remove sel 1.0 end
		}
	} elseif {[%W compare insert != 1.0]} {
		undoable_edit_step Delete insert-1c
		undo_join_enable
	}
	%W see insert
}

bind UndoText <Control-a> {
	%W tag add sel 1.0 end
}
bind UndoText <Control-A> {
	%W tag remove sel 1.0 end
}

bind UndoText <KeyPress> {
	TextInsert %W %A
}

# Ignore all Alt, Meta, and Control keypresses unless explicitly bound.
# Otherwise, if a widget binding for one of these is defined, the
# <KeyPress> class binding will also fire and insert the character,
# which is wrong.  Ditto for <Escape>.

bind UndoText <Alt-KeyPress> {# nothing }
bind UndoText <Meta-KeyPress> {# nothing}
bind UndoText <Control-KeyPress> {# nothing}
bind UndoText <Escape> {# nothing}
bind UndoText <KP_Enter> {# nothing}
if {[tk windowingsystem] eq "aqua"} {
	bind UndoText <Command-KeyPress> {# nothing}
}

# Derived from ::tk::TextInsert
proc TextInsert {w s} {
	if {$s eq "" || [$w cget -state] eq "disabled"} {
		return
	}
	if {[::tk::TextCursorInSelection $w]} {
		undoable_edit_step Replace sel.first sel.last $s
	} else {
		undoable_edit_step Insert insert $s
		undo_join_enable
	}
	$w see insert
}


### Undo/Redo Content Procedures ###

proc Insert {index chars} {
	set index [.t index $index]
	.t mark set ::undo::insert_mark $index
	.t insert $index $chars
	set end_index [.t index ::undo::insert_mark]
	.t mark unset ::undo::insert_mark
	return [list Delete $index $end_index]
}

proc Delete {index1 {index2 {}}} {
	set index1 [.t index $index1]
	if {$index2 ne {}} {
		set index2 [.t index $index2]
	} else {
		set index2 [.t index "$index1 + 1 c"]
	}
	set text [.t get $index1 [final_linebreak_fix $index2]]
	.t delete $index1 $index2
	return [list Insert $index1 $text]
}

proc Replace {index1 index2 chars} {
	set index1 [.t index $index1]
	set index2 [.t index $index2]
	set text [.t get $index1 [final_linebreak_fix $index2]]
	.t mark set ::undo::replace_mark $index1
	.t replace $index1 $index2 $chars
	set end_index [.t index ::undo::replace_mark]
	.t mark unset ::undo::replace_mark
	return [list Replace $index1 $end_index $text]
}

proc final_linebreak_fix {index} {
	if {[.t compare $index == end]} {
		return [.t index end-1c]
	} else {
		return $index
	}
}


### Undo/Redo Action Joining ###

proc undo_try_join_richtext {prev_command command} {
	lassign $prev_command prevAction prevArg1 prevArg2
	lassign $command action arg1 arg2

	if {$action == "Delete" && $prevAction == "Delete"} {
		# new range start == previous range end?
		if {$arg1 == $prevArg2} {
			set fromLine [lindex [split $prevArg1 .] 0]
			set toLine [lindex [split $arg2 .] 0]
			if {$fromLine == $toLine} {
				# delete from previous range start to new range end
				return [list Delete $prevArg1 $arg2]
			}
		}
	} elseif {$action == "Insert" && $prevAction == "Insert"} {
		# This case is relevant for the Delete key:
		# prev = {Insert 1.0 a}, command = {Insert 1.0 b}, return {Insert 1.0 ab}
		if {$arg1 eq $prevArg1} {
			if {$arg2 ne "\n" && $prevArg2 ne "\n"} {
				return [list Insert $arg1 $prevArg2$arg2]
			}
		}
		# This case is relevant for the Backspace key:
		# prev = {Insert 1.8 b}, command = {Insert 1.7 a}, return {Insert 1.7 ab}
		set col [lindex [split $arg1 .] 1]
		set prevCol [lindex [split $prevArg1 .] 1]
		set len [string length $arg2]
		if {$col + $len == $prevCol} {
			if {[string index $prevArg2 0] ne "\n"} {
				return [list Insert $arg1 $arg2$prevArg2]
			}
		}
	}
	return {}
}

proc undo_join_enable {} {
	global undo_joinable
	
	set undo_joinable yes
}

proc undo_join_disable {} {
	global undo_joinable
	
	set undo_joinable no
}


### Selection and Cursor Position Update ###

namespace eval undo_selection {}

proc undo_selection::Insert {index chars} {
	set len [string length $chars]
	set index2 [.t index "$index + $len chars"]
	.t tag add sel $index $index2
	.t mark set insert $index2
}

proc undo_selection::Delete {index1 index2} {
	.t mark set insert $index1
}

proc undo_selection::Replace {index1 index2 chars} {
	set len [string length $chars]
	set index2b [.t index "$index1 + $len chars"]
	.t tag add sel $index1 $index2b
	.t mark set insert $index2b
}

proc undo_selection::Group {args} {
}

proc undo_selection_richtext_init {} {
	.t tag remove sel 1.0 end
}


### Undo/Redo Mechanism ###

set undo_list [list]
set undo_pos 0
set undo_joinable no
set redo_list [list]

proc undoable_edit_step {command args} {
	global undo_list undo_pos undo_joinable redo_list

	# Do not bloat the undo/redo lists on repetitive
	# "do manually" - undo - "do the same manually"
	if {[lindex $redo_list end] eq [list $command {*}$args]} {
		redo
		return
	}

	if {$undo_pos < [llength $undo_list]} {
		lappend undo_list [concat Group [lreverse $redo_list]]
		set undo_pos [llength $undo_list]
		set redo_list [list]
	}

	puts -nonewline "DO: $command $args"
	set opposite [$command {*}$args]
	puts "  OPPOSITE: $opposite"

	set joined {}
	if {$undo_joinable} {
		set joined [undo_try_join_richtext [lindex $undo_list end] $opposite]
	}
	if {$joined eq {}} {
		lappend undo_list $opposite
		incr undo_pos
	} else {
		lset undo_list end $joined
	}

	update_undo_redo_display
}

bind . <Control-z> undo
bind . <Control-Z> redo
bind . <Control-y> redo

proc undo {} {
	global undo_list undo_pos redo_list

	set entry [lindex $undo_list $undo_pos-1]
	if {$entry eq {}} return
	undo_join_disable
	puts -nonewline "UNDO: $entry"
	undo_selection_richtext_init
	set opposite [{*}$entry]  ;# execute $entry
	{*}::undo_selection::$entry  ;# execute $entry in undo_selection namespace
	puts "  OPPOSITE: $opposite"

	lappend redo_list $opposite
	incr undo_pos -1

	update_undo_redo_display
}

proc redo {} {
	global undo_list undo_pos redo_list

	set entry [lindex $redo_list end]
	if {$entry eq {}} return
	undo_join_disable
	puts -nonewline "REDO: $entry"
	undo_selection_richtext_init
	set opposite [{*}$entry]  ;# execute $entry
	{*}::undo_selection::$entry  ;# execute $entry in undo_selection namespace
	puts -nonewline "  OPPOSITE: $opposite"

	set redo_list [lrange $redo_list 0 end-1]
	incr undo_pos
	puts "  SHOULD MATCH: [lindex $undo_list $undo_pos-1]"

	update_undo_redo_display
}

proc Group {args} {
	set opposites [list]
	foreach entry $args {
		lappend opposites [{*}$entry]  ;# execute $entry
		{*}::undo_selection::$entry  ;# execute $entry in undo_selection namespace
	}
	return [concat Group [lreverse $opposites]]
}


proc update_undo_redo_display {} {
	global undo_list undo_pos redo_list

	update_treeview .pw.undolist $undo_list $undo_pos
	update_treeview .pw.redolist $redo_list [llength $redo_list]
}

proc update_treeview {treeview list valid_entries} {
	$treeview delete [$treeview children {}]
	set tag {}
	foreach item $list {
		incr items_processed
		if {$items_processed == $valid_entries+1} {
			set tag inactive
		}
		treeview_insert_recursive $treeview {} $item $tag
	}
}

proc treeview_insert_recursive {treeview parent item tag} {
	if {[lindex $item 0] ne "Group"} {
		$treeview insert $parent end -text [string map [list \n \u23CE] $item] -tag $tag
	} else {
		set sub_parent [$treeview insert $parent end -text Group -tag $tag -open true]
		foreach item [lrange $item 1 end] {
			treeview_insert_recursive $treeview $sub_parent $item $tag
		}
	}
}
