# Copyright 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval undo {

variable undo_list_for
variable undo_pos_for
variable undo_joinable_for
variable redo_list_for

proc init_document {document} {
	variable undo_list_for
	variable undo_pos_for
	variable undo_joinable_for
	variable redo_list_for

	set undo_list_for($document) [list]
	set undo_pos_for($document) 0
	set undo_joinable_for($document) no
	set redo_list_for($document) [list]
}

proc cleanup_document {document} {
	variable undo_list_for
	variable undo_pos_for
	variable undo_joinable_for
	variable redo_list_for

	unset undo_list_for($document)
	unset undo_pos_for($document)
	unset undo_joinable_for($document)
	unset redo_list_for($document)
}

proc undoable_edit_step {widgetpath command args} {
	variable undo_list_for
	variable undo_pos_for
	variable undo_joinable_for
	variable redo_list_for

	set document [document_from_widget $widgetpath]
	set widget [widget_tail $widgetpath]

	upvar 0 undo_list_for($document) undo_list
	upvar 0 undo_pos_for($document) undo_pos
	upvar 0 undo_joinable_for($document) undo_joinable
	upvar 0 redo_list_for($document) redo_list

	# Do not bloat the undo/redo lists on repetitive
	# "do manually" - undo - "do the same manually"
	if {[lindex $redo_list end] eq [list $widget $command {*}$args]} {
		redo $widgetpath
		return
	}

	if {$undo_pos < [llength $undo_list]} {
		lappend undo_list [concat undo::Group [lreverse $redo_list]]
		set undo_pos [llength $undo_list]
		set redo_list [list]
	}

	set type [filetypes::type_from_widget $widgetpath]
	set opposite [${type}::undoable::$command $widgetpath {*}$args]
	set opposite [concat_widget_opposite $widget $opposite]

	set joined {}
	if {$undo_joinable} {
		set joined [${type}::undo_try_join [lindex $undo_list end] $opposite]
	}
	if {$joined eq {}} {
		lappend undo_list $opposite
		incr undo_pos
	} else {
		lset undo_list end $joined
	}
}

proc undo {widgetpath} {
	variable undo_list_for
	variable undo_pos_for
	variable redo_list_for

	set document [document_from_widget $widgetpath]

	upvar 0 undo_list_for($document) undo_list
	upvar 0 undo_pos_for($document) undo_pos
	upvar 0 redo_list_for($document) redo_list

	set entry [lindex $undo_list $undo_pos-1]
	if {$entry eq {}} return
	join_disable $widgetpath
	[filetypes::type_from_widget $widgetpath]::undo_selection_init $widgetpath
	set opposite [execute_entry $entry $document]

	lappend redo_list $opposite
	incr undo_pos -1
}

proc redo {widgetpath} {
	variable undo_list_for
	variable undo_pos_for
	variable redo_list_for

	set document [document_from_widget $widgetpath]

	upvar 0 undo_list_for($document) undo_list
	upvar 0 undo_pos_for($document) undo_pos
	upvar 0 redo_list_for($document) redo_list

	set entry [lindex $redo_list end]
	if {$entry eq {}} return
	join_disable $widgetpath
	[filetypes::type_from_widget $widgetpath]::undo_selection_init $widgetpath
	set opposite [execute_entry $entry $document]

	set redo_list [lrange $redo_list 0 end-1]
	incr undo_pos
	if {$opposite ne [lindex $undo_list $undo_pos-1]} {
		puts stderr "WARNING: detected corrupt undo/redo: {$opposite} must match {[lindex $undo_list $undo_pos-1]}"
	}
}

proc Group {document args} {
	set opposites [list]
	foreach entry $args {
		lappend opposites [execute_entry $entry $document]
	}
	return [concat undo::Group [lreverse $opposites]]
}

proc execute_entry {entry document} {
	if {[lindex $entry 0] ne "undo::Group"} {
		set args [lassign $entry widget command]
		set widgetpath [widgetpath_from_widget $widget $document]
		set type [filetypes::type_from_widget $widgetpath]
		set opposite [${type}::undoable::$command $widgetpath {*}$args]
		set opposite [concat_widget_opposite $widget $opposite]
		${type}::undo_selection::$command $widgetpath {*}$args
	} else {
		set opposite [undo::Group $document {*}[lrange $entry 1 end]]
	}
	return $opposite
}

proc document_from_widget {widgetpath} {
	winfo parent $widgetpath
}

proc widget_tail {widgetpath} {
	lindex [split $widgetpath .] end
}

proc widgetpath_from_widget {widget document} {
	return $document.$widget
}

proc concat_widget_opposite {widget opposite} {
	if {[lindex $opposite 0] ne "undo::Group"} {
		set new_opposite [concat $widget $opposite]
	} else {
		set new_opposite [list undo::Group]
		foreach part [lrange $opposite 1 end] {
			lappend new_opposite [concat_widget_opposite $widget $part]
		}
	}
	return $new_opposite
}

proc join_enable {widgetpath} {
	variable undo_joinable_for

	set document [document_from_widget $widgetpath]
	set undo_joinable_for($document) yes
}

proc join_disable {widgetpath} {
	variable undo_joinable_for

	set document [document_from_widget $widgetpath]
	set undo_joinable_for($document) no
}

proc undo_is_possible {document} {
	variable undo_pos_for

	return [expr {$undo_pos_for($document) > 0}]
}

proc redo_is_possible {document} {
	variable redo_list_for

	return [expr {$redo_list_for($document) ne {}}]
}

}
