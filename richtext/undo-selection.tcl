# Copyright 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Using the text selection for showing the changes introduced by Undo/Redo operations.

namespace eval richtext::undo_selection {

proc Insert {widget index args} {
	set len 0
	foreach {chars tags} $args {
		incr len [string length $chars]
	}
	set index2 [$widget index "$index + $len chars"]
	$widget tag add sel $index $index2
	$widget mark set insert $index2
}

proc Delete {widget index1 {index2 {}}} {
	$widget mark set insert $index1
}

proc Replace {widget index1 index2 args} {
	set len 0
	foreach {chars tags} $args {
		incr len [string length $chars]
	}
	set index2b [$widget index "$index1 + $len chars"]
	$widget tag add sel $index1 $index2b
	$widget mark set insert $index2b
}

proc TagRange {widget tag remove_from remove_to args} {
	if {[$widget tag cget $tag -background] eq {}} {
		$widget tag add sel $remove_from $remove_to
	}
}

proc TagConfigure {widget tag option value} {}
proc TagRename {widget old_tag new_tag} {}
proc TagCreate {widget tag above_tag options ranges} {}
proc TagDelete {widget args} {}
proc TagReorder {widget op moved_tags reference_tag {order_to_restore {}}} {}
proc WidgetConfigure {widget option value} {}
proc WidgetUnconfigure {widget option} {}
# Not used because ClipboardPaste is never part of an Undo/Redo history:
# proc ClipboardPaste {widget from_index to_index tags_created} {}

}

namespace eval richtext {

proc undo_selection_init {widget} {
	$widget tag remove sel 1.0 end
}

}
