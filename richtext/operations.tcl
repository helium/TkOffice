# Copyright 2021-2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

namespace eval richtext {

init_namespace

proc configure_default {w} {
	variable allowed_config

	# Reset widget options to defaults
	foreach option $allowed_config {
		$w configure $option [lindex [$w configure $option] 3]
	}
}

proc config_of_widget {w} {
	variable config_of_widget
	variable allowed_config

	if ![info exists config_of_widget($w)] {
		# Try to recover user-set config from the widget by taking
		# values that differ from the widget's defaults.
		# This omits options explicitly set to the default values.
		set config [dict create]
		foreach option $allowed_config {
			set cfg [$w configure $option]
			set default [lindex $cfg 3]
			set value [lindex $cfg 4]
			if {$default ne $value} {
				dict set config $option $value
			}
		}
		set config_of_widget($w) $config
		return $config
	}
	return $config_of_widget($w)
}

proc config_of_tag {w tag} {
	set config [dict create]
	foreach prop [$w tag configure $tag] {
		set key [lindex $prop 0]
		set value [lindex $prop 4]
		if {$value ne {}} {
			dict set config $key $value
		}
	}
	return $config
}

proc tag_names_for_ranges {w args} {
	if {$args eq {} || ([llength $args] & 1) != 0} {
		error "bad ranges list: must be start end ?start2 end2 ...?"
	}
	set tags [list]
	foreach {start end} $args {
		foreach tag [$w tag names $start] {
			if {$tag ni $tags} {lappend tags $tag}
		}
		foreach {tagonoff tag index} [$w dump -tag $start $end] {
			if {$tagonoff ne "tagon"} continue
			if {$tag ni $tags} {lappend tags $tag}
		}
	}
	# Iterate again over [tag names] to ensure correct ordering by priority
	# (as [$w tag names] would do for the whole text widget)
	set ret [list]
	foreach tag [$w tag names] {
		if {$tag ni $tags} continue
		lappend ret $tag
	}
	return $ret
}

proc tag_reconfigure {w tag args} {
	# Reset the tag to contain only defaults
	foreach prop [$w tag configure $tag] {
		set option [lindex $prop 0]
		$w tag configure $tag $option {}
	}
	if {$args ne {}} {
		$w tag configure $tag {*}$args
	}
}

proc tag_option_set {w tag option value} {
# Use $value = {} to unset an option
	$w tag configure $tag $option $value
}

proc widget_option_set {w option value} {
	variable config_of_widget
	$w configure $option $value
	dict set config_of_widget($w) $option $value
}

proc widget_option_unset {w option} {
	variable config_of_widget
	$w configure $option [lindex [$w configure $option] 3]
	dict unset config_of_widget($w) $option
}

proc tag_rename {w old_tag new_tag} {
	set tag_names [$w tag names]
	set old_tag_pos [lsearch -exact $tag_names $old_tag]
	if {$old_tag_pos == -1} {error "tag \"$old_tag\" not found"}

	$w tag configure $new_tag  ;# makes sure it exists at all
	foreach prop [$w tag configure $old_tag] {
		set key [lindex $prop 0]
		set value [lindex $prop 4]
		if {$value ne {}} {
			$w tag configure $new_tag $key $value
		}
	}
	set ranges [$w tag ranges $old_tag]
	if {$ranges ne {}} {
		$w tag add $new_tag {*}$ranges
	}
	$w tag delete $old_tag

	# restore priority (without this, would be always be highest = last-in-list)
	if {$old_tag_pos < [llength $tag_names]-1} {
		$w tag lower $new_tag [lindex $tag_names [expr {$old_tag_pos+1}]]
	}
}

proc tag_ranges_within {w tag index1 index2} {
	set out [list]
	# Normalize indices
	set index1 [$w index $index1]
	set index2 [$w index $index2]

	if {$tag in [$w tag names $index1]} {
		set range [$w tag prevrange $tag "$index1 + 1 c"]
		# There must exist such a range that contains $index1 since the character
		# at $index1 is tagged. Generally that range starts before $index1, but
		# we are not interested in that part, therefore modify it:
		lset range 0 $index1
	} else {
		set range [$w tag nextrange $tag $index1 $index2]
	}

	while {$range ne {}} {
		lappend out {*}$range
		set range_end_idx [lindex $range 1]
		set range [$w tag nextrange $tag $range_end_idx $index2]
	}

	# The last range possibly reaches beyond $index2, therefore modify it:
	if {$out ne {} && [$w compare [lindex $out end] > $index2]} {
		lset out end $index2
	}

	return $out
}

proc to_text_taglist_pairs {w from_index to_index} {
# Produces "chars tagList chars tagList" data as used for $w insert and $w replace
	set end_indices [list]
	foreach {key value index} [$w dump -tag $from_index $to_index] {
		if {$value eq "sel"} continue
		if {$index ne [lindex $end_indices end] && $index ne $from_index} {
			lappend end_indices $index
		}
	}
	if {[lindex $end_indices end] ne $to_index} {
		lappend end_indices $to_index
	}

	set start_index $from_index
	set out [list]
	foreach end_index $end_indices {
		set tags [$w tag names $start_index]
		lremoveitem_nocomplain tags sel
		lappend out [$w get $start_index $end_index]
		lappend out $tags
		set start_index $end_index
	}
	return $out
}

}
