# Copyright 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

init_namespace

proc undo_try_join {prev_command command} {
	set prevArg2ff [lassign $prev_command prevWidget prevAction prevArg1]
	set prevArg2 [lindex $prevArg2ff 0]
	set arg2ff [lassign $command widget action arg1]
	set arg2 [lindex $arg2ff 0]

	if {$widget ne $prevWidget} {
		return {}
	}

	if {$action eq "Delete" && $prevAction eq "Delete"} {
		# new range start == previous range end?
		if {$arg1 eq $prevArg2} {
			set fromLine [lindex [split $prevArg1 .] 0]
			set toLine [lindex [split $arg2 .] 0]
			if {$fromLine == $toLine} {
				# delete from previous range start to new range end
				return [list $widget Delete $prevArg1 $arg2]
			}
		}
	} elseif {$action eq "Insert" && $prevAction eq "Insert"} {
		# This case is relevant for the Delete key:
		# prev = {Insert 1.0 a}, command = {Insert 1.0 b}, return {Insert 1.0 ab}
		if {$arg1 eq $prevArg1} {
			if {$arg2 ne "\n" && $prevArg2 ne "\n"} {
				return [list $widget Insert $arg1 {*}[concat_text_tags_lists $prevArg2ff $arg2ff]]
			}
		}
		# This case is relevant for the Backspace key:
		# prev = {Insert 1.8 b}, command = {Insert 1.7 a}, return {Insert 1.7 ab}
		set col [lindex [split $arg1 .] 1]
		set prevCol [lindex [split $prevArg1 .] 1]
		set len 0
		foreach {text tags} $arg2ff {
			incr len [string length $text]
		}
		if {$col + $len == $prevCol} {
			if {[string index $prevArg2 0] ne "\n"} {
				return [list $widget Insert $arg1 {*}[concat_text_tags_lists $arg2ff $prevArg2ff]]
			}
		}
	} elseif {$action eq "TagRename" && $prevAction eq "TagDelete"} {
		# Do not create an individual Undo entry for the initial naming of a tag
		if {$arg2 eq $prevArg1} {
			return [list $widget TagDelete $arg1]
		}
	}
	return {}
}

proc concat_text_tags_lists {listA listB} {
	set lenA [llength $listA]
	set lenA_except_last [expr {($lenA - 1) & ~1}]
	set out [lrange $listA 0 [expr {$lenA_except_last - 1}]]
	lassign [lrange $listA $lenA_except_last end] textA tagsA

	set listB_remainder [lassign $listB textB tagsB]
	if {$tagsA eq $tagsB} {
		lappend out $textA$textB $tagsA
	} else {
		lappend out $textA $tagsA $textB $tagsB
	}
	return [concat $out $listB_remainder]
}

}
