# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

proc size_entry_create {frame property args} {
	variable entry_last_value

	if {[llength $args]&1 != 0} {
		error "number of extra arguments must be even, have [llength $args]"
	}
	set label {UNDEFINED LABEL}
	set from 0
	set to Inf
	set units {px cm mm in pt}
	set pady 2
	foreach {key value} $args {
		if {$key in {-label -from -to -units -pady}} {
			set [string range $key 1 end] $value
		} else {
			error "unknown property \"$key\": must be -label, -from, -to, -units, or -pady"
		}
	}

	grid \
		[ttk::label $frame.${property}_l -text $label] \
		[ttk::spinbox $frame.$property -from $from -to $to -width 4] \
		[ttk::combobox $frame.${property}_unit -state readonly -width 3 -values $units] \
		[ttk::button $frame.${property}_clear -text [mc cancel_button] -style Toolbutton] \
		-sticky nsw -pady $pady
	grid configure $frame.$property $frame.${property}_unit -padx {0 3}
	grid configure $frame.$property -sticky nesw

	set fine_binding [list richtext::size_entry_set_increment $frame $property true]
	set coarse_binding [list richtext::size_entry_set_increment $frame $property false]
	bind $frame.$property <ButtonPress-1> {focus_intercept_click %W}
	bind $frame.$property <ButtonPress-1> +$fine_binding
	bind $frame.$property <Control-Button-1> {focus_intercept_click %W}
	bind $frame.$property <Control-Button-1> \
		+[list richtext::size_entry_spinbox_control_click $frame $property %x %y]
	bind $frame.$property <Key-Up> $fine_binding
	bind $frame.$property <Key-Down> $fine_binding
	bind $frame.$property <Control-Key-Up> $coarse_binding
	bind $frame.$property <Control-Key-Down> $coarse_binding
	# Let the default unit show up on empty spinbox also on Tab traversal
	bind $frame.$property <FocusIn> [list richtext::size_entry_get_unit $frame $property]

	foreach event {<<Increment>> <<Decrement>>} dir {+1 -1} {
		bind $frame.$property $event \
			[list richtext::size_entry_spin $frame $property $dir]
		bind $frame.$property $event +break
	}
	ttk::bindMouseWheel $frame.$property \
		[list richtext::size_entry_mousewheel $frame $property true]
	bindControlMouseWheel $frame.$property \
		[list richtext::size_entry_mousewheel $frame $property false]

	set binding [list richtext::size_entry_number_changed $frame $property]
	bind_spinbox $frame.$property $binding
	bind $frame.$property <<FocusOutPrepare>> $binding
	bind $frame.$property <Escape> {richtext::size_entry_escape %W}
	bind $frame.$property <<PasteSelection>> break

	bind $frame.${property}_unit <<ComboboxSelected>> \
		[list richtext::size_entry_unit_changed $frame $property]
	bind $frame.${property}_unit <ButtonPress-1> {focus_intercept_click %W}
	ttk::bindMouseWheel $frame.${property}_unit {focus_intercept_mousewheel %W}

	$frame.${property}_clear configure -command \
		[list richtext::size_entry_clear $frame $property]

	set entry_last_value($frame.$property) {}
	bind $frame.$property <Destroy> {richtext::size_entry_destroy_spinbox %W}
}

proc size_entry_destroy_spinbox {w} {
	variable entry_last_value
	variable last_validation_attempt

	unset entry_last_value($w)
	unset -nocomplain last_validation_attempt($w)
}

proc size_entry_set_spinbox {w value} {
	variable entry_last_value

	$w set $value
	set entry_last_value($w) $value
}

proc size_entry_update {frame property options} {
	if {[dict exists $options -$property]} {
		set size [dict get $options -$property]
		lassign [screen_distance_parse $size] number unit
		size_entry_set_spinbox $frame.$property $number
		$frame.${property}_unit set $unit
		$frame.${property}_clear state !disabled
	} else {
		size_entry_clear $frame $property -nochange
	}
}

proc size_entry_number_changed {frame property} {
	variable entry_last_value
	variable last_validation_attempt

	set number [$frame.$property get]
	if {$number eq {}} {
		size_entry_clear $frame $property
		return
	}
	if {[string is double $number] && $number ne {}} {
		set unit [size_entry_get_unit $frame $property]
		if {$number < [$frame.$property cget -from]} {
			set number [$frame.$property cget -from]
		} elseif {$number > [$frame.$property cget -to]} {
			set number [$frame.$property cget -to]
		}
		set number [screen_distance_cleanup $number $unit]
		$frame.$property set $number
		set new_size [screen_distance_compose $number $unit]
		tag_list_property_set $frame -$property $new_size
		$frame.${property}_clear state !disabled
		size_entry_becomes_valid $frame.$property
		set entry_last_value($frame.$property) $number
	} elseif {![info exists last_validation_attempt($frame.$property)]} {
		# Spinbox just turned invalid
		size_entry_becomes_invalid $frame.$property
		focus $frame.$property  ;# work against <FocusOut>
		set last_validation_attempt($frame.$property) $number
	} elseif {$last_validation_attempt($frame.$property) eq $number} {
		# Spinbox was already invalid and has not changed, restore
		size_entry_set_spinbox $frame.$property $entry_last_value($frame.$property)
		size_entry_becomes_valid $frame.$property
		if {$entry_last_value($frame.$property) eq {}} {
			$frame.${property}_unit set {}
		}
	} else {
		# Spinbox was already invalid but the user tried another value, keep Invalid status
		focus $frame.$property  ;# work against <FocusOut>
		set last_validation_attempt($frame.$property) $number
	}
}

proc size_entry_becomes_invalid {w} {
	switch [$w cget -style] {
		{} {$w configure -style Invalid.TSpinbox}
		Modern.TSpinbox {$w configure -style Invalid.Modern.TSpinbox}
	}
}

proc size_entry_becomes_valid {w} {
	variable last_validation_attempt

	switch [$w cget -style] {
		Invalid.TSpinbox {$w configure -style {}}
		Invalid.Modern.TSpinbox {$w configure -style Modern.TSpinbox}
	}
	unset -nocomplain last_validation_attempt($w)
}

proc size_entry_unit_changed {frame property} {
	if {[$frame.${property}_unit current] == -1} return
	set unit [$frame.${property}_unit get]
	set number [$frame.$property get]
	if {$number eq {}} return
	set number [screen_distance_cleanup $number $unit]
	$frame.$property set $number
	set new_size [screen_distance_compose $number $unit]
	tag_list_property_set $frame -$property $new_size
	$frame.${property}_clear state !disabled
}

proc size_entry_clear {frame property {-nochange {}}} {
	size_entry_set_spinbox $frame.$property {}
	$frame.${property}_unit set {}
	$frame.${property}_clear state disabled
	if {${-nochange} ne {-nochange}} {
		tag_list_property_set $frame -$property {} -unset
	}
	size_entry_becomes_valid $frame.$property
}

proc size_entry_escape {spinbox} {
	variable entry_last_value

	if {[$spinbox get] eq $entry_last_value($spinbox)} {
		event generate $spinbox <<Escape>>
	} else {
		$spinbox set $entry_last_value($spinbox)
		size_entry_becomes_valid $spinbox
	}
}

proc size_entry_spin {frame property steps} {
	variable entry_last_value
	variable last_validation_attempt

	set w $frame.$property

	set value [$w get]
	if {$value eq {}} {
		set new_value 0
	} elseif {![string is double $value]} {
		if {![info exists last_validation_attempt($frame.$property)]} {
			# Spinbox just turned invalid
			size_entry_becomes_invalid $frame.$property
			set last_validation_attempt($frame.$property) $value
			return
		} elseif {$last_validation_attempt($frame.$property) ne $value} {
			# Spinbox was already invalid but the user tried another value, keep Invalid status
			set last_validation_attempt($frame.$property) $value
			return
		} else {
			# Spinbox was already invalid and has not changed, restore
			set new_value $entry_last_value($w)
			if {$new_value eq {}} {
				set new_value 0
			}
		}
	} else {
		set increment [$w cget -increment]
		set from [$w cget -from]
		set to [$w cget -to]
		set new_value [expr {$value + $steps * $increment}]
		if {$new_value < $from} {
			set new_value $from
		} elseif {$new_value > $to} {
			set new_value $to
		}
	}
	set unit [size_entry_get_unit $frame $property]
	size_entry_set_spinbox $w [screen_distance_cleanup $new_value $unit]
	set script [list richtext::size_entry_select_all_on_focus $w]
	after cancel $script
	after 2 $script

	invoke_spinbox $w
}

proc size_entry_select_all_on_focus {w} {
	if {[focus] eq $w} {
		ttk::spinbox::SelectAll $w
	}
}

proc size_entry_mousewheel {frame property fine neg_steps} {
	$frame.$property instate disabled {return -code break}
	size_entry_set_increment $frame $property $fine
	size_entry_spin $frame $property [expr {-$neg_steps}]
	return -code break
}

proc size_entry_set_increment {frame property fine} {
	$frame.$property instate disabled {return}
	set unit [size_entry_get_unit $frame $property]
	if {$fine} {
		switch $unit {
			cm {set step_size 0.1}
			in {set step_size 0.125}
			mm - pt - px {set step_size 1}
		}
	} else {
		switch $unit {
			cm - in {set step_size 1.0}
			mm - px {set step_size 10}
			pt {set step_size 12}
		}
	}
	$frame.$property configure -increment $step_size
}

proc size_entry_spinbox_control_click {frame property x y} {
	set w $frame.$property
	if {[$w instate disabled]} { return }
	size_entry_set_increment $frame $property false
	switch -glob -- [$w identify $x $y] {
		*textarea {
			$w icursor @$x
			focus $w
		} default {
			ttk::spinbox::Press $w $x $y
		}
	}
	return -code break
}

proc size_entry_get_unit {frame property} {
	if {[$frame.${property}_unit current] == -1} {
		$frame.${property}_unit current 0
	}
	return [$frame.${property}_unit get]
}

}
