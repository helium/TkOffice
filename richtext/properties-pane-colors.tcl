# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

proc properties_pane_colors {f mode} {
	ttk::labelframe $f -text [mc property_pane_colors_title_tag_mode] -style Panel.TLabelframe
	grid columnconfigure $f 2 -weight 1

	# Properties for tags and whole text widget:
	color_entry_create $f background -label [mc property_background]
	color_entry_create $f foreground -label [mc property_foreground]

	switch $mode widget {
		$f configure -text [mc property_pane_colors_title_widget_mode]

		# Properties for whole text widget only:
		color_entry_create $f selectbackground -label [mc property_selectbackground] -pady {10 2}
		color_entry_create $f inactiveselectbackground -label [mc property_inactiveselectbackground]
		color_entry_create $f selectforeground -label [mc property_selectforeground]
		color_entry_create $f insertbackground -label [mc property_insertbackground] -pady {10 2}

	} tag {
		$f configure -text [mc property_pane_colors_title_tag_mode]

		# Properties for tags only:
		combobox_entry_create $f bgstipple -label [mc property_bgstipple] \
			-values [list {} gray75 gray50 gray25 gray12] -width 6 -pady {10 2} \
			-gridmode colorpane

		combobox_entry_create $f relief -label [mc property_relief] \
			-values [list flat raised sunken groove ridge solid] -width 6 \
			-gridmode colorpane

		size_entry_create $f borderwidth -label [mc property_border] -units {px pt mm}
		# Modify widget layout to fit into Colors panel
		set info [grid info $f.borderwidth]
		grid forget $f.borderwidth $f.borderwidth_unit
		grid [ttk::frame $f.borderwidth_f] -row [dict get $info -row] -column 2 -columnspan 2 \
			-pady [dict get $info -pady] -sticky nsw
		grid $f.borderwidth $f.borderwidth_unit -in $f.borderwidth_f -padx {0 3} -sticky nsw
		lower $f.borderwidth_f  ;# otherwise it obscures the contained widgets created earlier
	} default {
		error "invalid mode \"$mode\": must be tag or widget"
	}

	return $f
}

proc properties_pane_colors_update {f mode options} {
	color_entry_update $f background $options
	color_entry_update $f foreground $options

	switch $mode widget {
		color_entry_update $f selectbackground $options
		color_entry_update $f inactiveselectbackground $options
		color_entry_update $f selectforeground $options
		color_entry_update $f insertbackground $options
	} tag {
		combobox_entry_update $f bgstipple $options
		combobox_entry_update $f relief $options
		size_entry_update $f borderwidth $options
	}
}

proc properties_pane_colors_update_single {f option value {-unset {}}} {
	if {${-unset} ne {-unset}} {
		set opt_list [list $option $value]
	} else {
		set opt_list [list]
	}
	switch $option {
		"-background" { color_entry_update $f background $opt_list }
		"-foreground" { color_entry_update $f foreground $opt_list }
		"-selectbackground" { color_entry_update $f selectbackground $opt_list }
		"-inactiveselectbackground" { color_entry_update $f inactiveselectbackground $opt_list }
		"-selectforeground" { color_entry_update $f selectforeground $opt_list }
		"-insertbackground" { color_entry_update $f insertbackground $opt_list }
		"-bgstipple" { combobox_entry_update $f bgstipple $opt_list }
		"-relief" { combobox_entry_update $f relief $opt_list }
		"-borderwidth" { size_entry_update $f borderwidth $opt_list }
	}
}

}
