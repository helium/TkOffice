# Copyright 2022-2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

init_namespace

variable associated_richtext
variable config_of_widget
variable tag_list_disable_refresh  ;# Used to prevent refreshing an invisible tag list

icons::load  list-add,16  list-remove,16  up,16  down,16

proc tag_list_create {path -for textwidget} {
	variable associated_richtext
	variable tag_list_disable_refresh

	set associated_richtext($path) $textwidget
	if {${-for} ne {-for}} {
		error "wrong second argument: must be tag_list_create path -for textwidget"
	}

	ttk::frame $path
	grid rowconfigure $path 1 -weight 1
	grid columnconfigure $path 0 -weight 1

	grid [ttk::label $path.title -text [mc tag_list_title]] \
		-sticky nw -pady {0 3}

	grid [ttk_treeview $path.treeview] -sticky nesw
	$path.treeview configure -show {} -columns tagName -selectmode browse
	$path.treeview column tagName -width 135
	if {{ItalicDefaultFont} ni [font names]} {
		font create ItalicDefaultFont {*}[font configure TkDefaultFont] \
			-slant italic
	}
	$path.treeview tag configure entire_text -font ItalicDefaultFont
	make_entry $path.treeview.entry
	ttk_treeview_place_scrollbar [ttk::scrollbar $path.treeview.scroll -orient v]

	grid [ttk::button $path.add -text [mc tag_add] -image list-add,16 -compound left -default disabled] -pady {3 0}
	grid [ttk::button $path.remove -text [mc tag_remove] -image list-remove,16 -compound left -default disabled] -pady 1
	grid [ttk::button $path.up -text [mc tag_up] -image up,16 -compound left -default disabled] -pady {6 1}
	grid [ttk::button $path.down -text [mc tag_down] -image down,16 -compound left -default disabled] -pady {0 3}
	foreach btn {add remove up down} {
		$path.$btn configure -style LeftAlignedImage.TButton
		grid configure $path.$btn -sticky nesw
	}

	foreach window [winfo children $path] {
		grid configure $window -padx {3 0}
	}

	# Behaviour bindings
	bind $textwidget <<PropertyRefresh>> [list richtext::tag_list_refresh $path]
	set tag_list_disable_refresh($path) no
	bind $path <Destroy> [list richtext::tag_list_destroy $path]
	tag_list_refresh $path

	bind $path.treeview <ButtonPress-1> {focus_intercept_click %W}

	doubleclick_workaround::add_to_widget $path.treeview
	bind $path.treeview <<TreeviewSelect>> [list richtext::tag_list_select $path]
	bind $path.treeview <<DoubleClick>> [list richtext::tag_list_doubleclick $path %x %y]

	bind $path.treeview.entry <Escape> [list richtext::tag_list_hide_entry $path]
	set binding [list richtext::tag_list_rename_from_entry $path]
	foreach event {<Return> <KP_Enter> <Key-Tab> <<PrevWindow>> <Key-F10> <Alt-Key>} {
		bind $path.treeview.entry $event $binding
	}

	treeview_enable_multi_selection $path.treeview
	treeview_make_reorderable $path.treeview
	bind $path.treeview <<MoveUp>> [list richtext::tag_list_reorder_tags $path lower %d]
	bind $path.treeview <<MoveDown>> [list richtext::tag_list_reorder_tags $path raise %d]
	bind $path.treeview <<MoveUpMultiple>> [list richtext::tag_list_reorder_tags $path lower %d]
	bind $path.treeview <<MoveDownMultiple>> [list richtext::tag_list_reorder_tags $path raise %d]

	$path.add configure -command [list richtext::tag_list_add_tag $path]
	$path.remove configure -command [list richtext::tag_list_remove_tag $path]
	$path.up configure -command [list richtext::tag_list_lower_tag $path]
	$path.down configure -command [list richtext::tag_list_raise_tag $path]

	foreach window [winfo children $path] {
		bind $window <Escape> [list focus $textwidget]
	}

	return $path
}

proc tag_list_link_to_panel {w panel} {
	variable associated_panel
	set associated_panel($w) $panel
}

proc tag_list_unlink_panel {w} {
	variable associated_panel
	unset -nocomplain associated_panel($w)
}

proc tag_list_destroy {w} {
	variable associated_richtext
	variable associated_panel
	variable tag_list_disable_refresh

	set textwidget $associated_richtext($w)
	if {[winfo exists $textwidget]} {
		bind $textwidget <<PropertyRefresh>> {}
	}
	bind $w <Destroy> {}
	unset associated_richtext($w)
	unset tag_list_disable_refresh($w)

	if {[info exists associated_panel($w)]} {
		properties_frame_unlink_tag_list $associated_panel($w)
		unset associated_panel($w)
	}
}

proc tag_list_refresh {w} {
	variable associated_richtext
	variable tag_list_disable_refresh

	if {$tag_list_disable_refresh($w)} return
	set text $associated_richtext($w)

	set selected_tags [list]
	foreach item [$w.treeview selection] {
		lappend selected_tags [lindex [$w.treeview item $item -values] 0]
	}

	$w.treeview delete [$w.treeview children {}]
	set tags [list entire_text reorder::always_first]
	set first_item [
		$w.treeview insert {} end -values [list [mc entire_text]] -tags $tags
	]
	foreach tag [$text tag names] {
		if {$tag eq "sel"} continue
		set item [$w.treeview insert {} end -values [list $tag]]
		if {$tag in $selected_tags} {
			$w.treeview selection add $item
		}
	}

	if {[$w.treeview selection] eq {}} {
		toolkit::treeview_multi_selection::selection_set $w.treeview $first_item
	}
	toolkit::treeview_multi_selection::forget_anchors $w.treeview
	$w.treeview see [lindex [$w.treeview selection] 0]
}

proc tag_list_lookup {richtext} {
	variable associated_richtext

	set taglist {}
	# Find the tag list to which $richtext is associated
	foreach {taglist_ richtext_} [array get associated_richtext] {
		if {$richtext_ eq $richtext} {
			set taglist $taglist_
			break
		}
	}
	return $taglist
}

proc tag_list_refresh_from_undo {richtext} {
	set taglist [tag_list_lookup $richtext]
	if {$taglist ne {}} {
		tag_list_refresh $taglist
	}
}

proc tag_list_disable_refresh {w} {
	variable tag_list_disable_refresh

	set tag_list_disable_refresh($w) yes
}

proc tag_list_enable_refresh {w} {
	variable tag_list_disable_refresh

	set tag_list_disable_refresh($w) no
	richtext::tag_list_refresh $w
}

proc tag_list_select {w} {
	after cancel richtext::tag_list_select_actual $w
	after idle richtext::tag_list_select_actual $w
}

proc tag_list_select_actual {w} {
	variable associated_panel
	variable associated_richtext
	set text $associated_richtext($w)

	if {![info exists associated_panel($w)]} return
	set panel $associated_panel($w)

	set selected_items [$w.treeview selection]
	if {[llength $selected_items] != 1} {
		properties_frame_set_enabled $panel no
	} else {
		properties_frame_set_enabled $panel yes
		if {[$w.treeview tag has entire_text $selected_items]} {
			properties_frame_update $panel widget [config_of_widget $text]
			set button_state disabled
		} else {
			set tag [lindex [$w.treeview item $selected_items -values] 0]
			properties_frame_update $panel tag [config_of_tag $text $tag]
			set button_state normal
		}
		foreach button [list $w.remove $w.up $w.down] {
			$button configure -state $button_state
		}
	}
}

proc tag_list_show_entry_for {w item} {
	variable entry_for_item

	update idletasks  ;# make bbox valid
	lassign [$w.treeview bbox $item] x y width height
	place $w.treeview.entry -x $x -y $y -width $width -height $height
	entry_set $w.treeview.entry [lindex [$w.treeview item $item -values] 0]
	$w.treeview.entry selection range 0 end
	set entry_for_item($w.treeview) $item
	focus $w.treeview.entry
	click_outside_register $w.treeview.entry
	bind $w.treeview.entry <<ClickOutside>> [list richtext::tag_list_rename_from_entry $w]
}

proc tag_list_hide_entry {w} {
	variable entry_for_item

	place forget $w.treeview.entry
	unset -nocomplain entry_for_item($w.treeview)
}

proc tag_list_rename_from_entry {w} {
	variable entry_for_item
	variable associated_richtext

	if {![info exists entry_for_item($w.treeview)]} {
		# Prevent error on fast clicks such as triple-clicking
		tag_list_hide_entry $w
		return
	}
	set old_tag [lindex [$w.treeview item $entry_for_item($w.treeview) -values] 0]
	set new_tag [$w.treeview.entry get]
	if {$new_tag ne $old_tag && $new_tag ni {{} sel}} {
		undo::undoable_edit_step $associated_richtext($w) TagRename $old_tag $new_tag
		$w.treeview item $entry_for_item($w.treeview) -values [list $new_tag]
	}
	tag_list_hide_entry $w
}

proc tag_list_rename_from_undo {richtext old_tag new_tag} {
	set taglist [tag_list_lookup $richtext]
	if {$taglist eq {}} return

	# Find the tag list item matching $old_tag, change it to $new_tag
	set item [tag_list_item_for_tag $taglist $old_tag]
	if {$item eq {}} return
	$taglist.treeview item $item -values [list $new_tag]
}

proc tag_list_doubleclick {w x y} {
	variable associated_richtext

	if {[llength [$w.treeview selection]] != 1} {
		return
	}
	set item [$w.treeview identify item $x $y]
	if {$item != {} && ![$w.treeview tag has entire_text $item]} {
		tag_list_show_entry_for $w $item
		undo::join_disable $associated_richtext($w)
	}
}

proc tag_list_add_tag {w} {
	variable associated_richtext
	set text $associated_richtext($w)

	# Find a tag name in the form of "tag#42" (translated)
	set tags [$text tag names]
	set name_pattern [mc new_tag_name_pattern]
	while {[set new_tag [string map [list {$i} [incr i]] $name_pattern]] in $tags} {}
	$text tag configure $new_tag

	set selected_item [lindex [$w.treeview selection] end]
	if {$selected_item eq {}} {
		set selected_item [lindex [$w.treeview children {}] end]
	}

	# Create tag in text widget
	if {![$w.treeview tag has entire_text $selected_item]} {
		# Tag just above selected tag entry
		lassign [$w.treeview item $selected_item -values] selected_tag_name
		undo::undoable_edit_step $text TagCreate $new_tag $selected_tag_name {} {}
	} else {
		# Tag only above "entire text" entry = lowest priority
		undo::undoable_edit_step $text TagCreate $new_tag {} {} {}
	}
	undo::join_enable $text

	set item [tag_list_item_for_tag $w $new_tag]
	if {$item ne {}} {
		tag_list_show_entry_for $w $item
	}
}

proc tag_list_add_from_undo {richtext tag above_tag} {
	set w [tag_list_lookup $richtext]

	if {$above_tag eq {}} {
		set new_item [$w.treeview insert {} 1 -values [list $tag]]
	} else {
		set item [tag_list_item_for_tag $w $above_tag]
		if {$item ne {}} {
			set new_pos [$w.treeview index $item]
			incr new_pos
			set new_item [$w.treeview insert {} $new_pos -values [list $tag]]
		}
	}
	if {![info exists new_item]} {error "Tag '$above_tag' not found in treeview"}

	toolkit::treeview_multi_selection::selection_set $w.treeview $new_item
	$w.treeview see $new_item
}

proc tag_list_remove_tag {w} {
	variable associated_richtext
	set text $associated_richtext($w)

	set tags_to_delete [list]
	set selected_items [$w.treeview selection]
	foreach selected_item $selected_items {
		if {[$w.treeview tag has entire_text $selected_item]} continue
		lassign [$w.treeview item $selected_item -values] tag_name
		lappend tags_to_delete $tag_name
	}

	if {[$w.treeview tag has entire_text [lindex $selected_items 0]]} {
		# entire_text entry stays selected
		set sel_item [lindex $selected_items 0]
		# Prevent deletion of entire_text entry
		set selected_items [lrange $selected_items 1 end]
	} else {
		set sel_item [$w.treeview next [lindex $selected_items end]]
		if {$sel_item eq {}} {
			set sel_item [$w.treeview prev [lindex $selected_items 0]]
		}
	}
	lassign [$w.treeview item $sel_item -values] sel_tag
	undo::undoable_edit_step $text TagDelete {*}$tags_to_delete  ;# this refreshes the treeview
	set item [tag_list_item_for_tag $w $sel_tag]
	if {$item ne {}} {
		toolkit::treeview_multi_selection::selection_set $w.treeview $item
	}
}

proc tag_list_reorder_tags {w op data} {
	variable associated_richtext
	set text $associated_richtext($w)

	if {$op ni {lower raise}} {error "invalid op $op: must be lower or raise"}

	lassign $data moved_items relative_item
	lassign [$w.treeview item $relative_item -values] relative_tag
	set moved_tags [list]
	foreach moved_item $moved_items {
		lassign [$w.treeview item $moved_item -values] moved_tag
		lappend moved_tags $moved_tag
	}
	# Cannot do this directly, as the treeview-reorder-drag code has not yet finished.
	# TODO: maybe this can be solved by letting treeview-reorder-drag generate events with -when tail
	after idle [list undo::undoable_edit_step $text TagReorder $op $moved_tags $relative_tag]
}

proc tag_list_lower_tag {w} {
	toolkit::treeview_reorder::move_selection_up_by_one $w.treeview
}

proc tag_list_raise_tag {w} {
	toolkit::treeview_reorder::move_selection_down_by_one $w.treeview
}

proc tag_list_item_for_tag {w tag} {
	set values_to_find [list $tag]
	foreach item [$w.treeview children {}] {
		if {[$w.treeview item $item -values] eq $values_to_find} {
			return $item
		}
	}
	return {}
}

proc tag_list_select_tags {richtext args} {
	set w [tag_list_lookup $richtext]
	set selection [list]
	foreach tag $args {
		lappend selection [tag_list_item_for_tag $w $tag]
	}
	if {[$w.treeview selection] ne $selection} {
		toolkit::treeview_multi_selection::selection_set $w.treeview $selection
	}
}

proc tag_list_select_entire_text_entry {richtext} {
	set w [tag_list_lookup $richtext]
	set selection [lindex [$w.treeview children {}] 0]
	if {[$w.treeview selection] ne $selection} {
		$w.treeview selection set $selection
	}
}

# Properties panels call this function to apply settings to the
# currently selected tag / whole widget

proc tag_list_property_set {panel option new_value {-unset {}}} {
	variable taglist_for_properties_frame
	variable associated_richtext

	set properties_frame [winfo parent [winfo parent $panel]]
	set taglist $taglist_for_properties_frame($properties_frame)
	set richtext $associated_richtext($taglist)

	set selected_items $::ttk_fixes::selection_of_treeview($taglist.treeview)
	if {[llength $selected_items] != 1} {
		error "tried to set property for multi-selection"
	} else {
		if {[$taglist.treeview tag has entire_text $selected_items]} {
			if {${-unset} eq {-unset}} {
				if {[dict exists [config_of_widget $richtext] $option]} {
					undo::undoable_edit_step $richtext WidgetUnconfigure $option
				}
			} else {
				set config_of_widget [config_of_widget $richtext]
				set config_exists [dict exists $config_of_widget $option]
				if {!$config_exists || [dict get $config_of_widget $option] ne $new_value} {
					undo::undoable_edit_step $richtext WidgetConfigure $option $new_value
				}
			}
		} else {
			set tag [lindex [$taglist.treeview item $selected_items -values] 0]
			if {[$richtext tag cget $tag $option] ne $new_value} {
				undo::undoable_edit_step $richtext TagConfigure $tag $option $new_value
			}
		}
	}
}

}
