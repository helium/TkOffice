# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

init_namespace

proc clipboard_paste {w {-plaintext {}}} {
	if {[catch {
		set types [clipboard get -type TARGETS]
	}]} {
		return false
	}
	if {${-plaintext} ne "-plaintext" && "TkOfficeRichText" in $types} {
		clipboard_paste_richtext $w [clipboard get -type TkOfficeRichText]
	} elseif {"UTF8_STRING" in $types} {
		clipboard_paste_plaintext $w [clipboard get -type UTF8_STRING]
	} elseif {"STRING" in $types} {
		clipboard_paste_plaintext $w [clipboard get -type STRING]
	} else {
		return false
	}
	return true
}

proc clipboard_paste_plaintext {w text} {
	set tags [tags_surrounding_cursor $w]
	undo::undoable_edit_step $w Insert insert $text $tags
}

proc clipboard_paste_richtext {w data} {
	set tags [tags_surrounding_cursor $w]
	set all_tags_before [$w tag names]

	set begin_index [$w index insert]
	clipboard_paste_richtext_main $w $data
	set end_index [$w index insert]

	set tags_pasted [tag_names_for_ranges $w $begin_index $end_index]
	foreach tag $tags {
		# Apply to the pasted text each tag that surrounded the cursor
		# before pasting, but only if the pasted text does not contain
		# that tag at all.
		if {$tag ni $tags_pasted} {
			$w tag add $tag $begin_index $end_index
		}
	}

	set tags_created [list]
	foreach tag $tags_pasted {
		if {$tag ni $all_tags_before} {
			lappend tags_created $tag
		}
	}
	undo::undoable_edit_step $w ClipboardPaste $begin_index $end_index $tags_created
}

proc clipboard_paste_richtext_main {w data} {
	set interp [setup_fileformat_interp $w]
	fileformat::_initialize $w
	$interp eval $data  ;# main action
	fileformat::_finalize $w
	interp delete $interp
	$w tag raise sel
	event generate $w <<PropertyRefresh>>
}

proc tags_surrounding_cursor {w} {
	set tags_after_cursor [$w tag names insert]
	set tags_before_cursor [$w tag names insert-1c]
	set tags [list]
	foreach tag $tags_after_cursor {
		if {$tag in $tags_before_cursor} {
			lappend tags $tag
		}
	}
	return $tags
}

}
