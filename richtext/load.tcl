# Copyright 2020-2023 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

namespace eval richtext {

init_namespace

variable config_of_widget


proc reset_widget {w} {
	variable config_of_widget

	# Reset widget options to defaults
	configure_default $w
	set config_of_widget($w) [dict create]

	# Reset contents
	$w delete 1.0 end
	$w tag delete {*}[$w tag names]
	$w mark unset {*}[$w mark names]

	event generate $w <<PropertyRefresh>>
}

proc setup_fileformat_interp {w} {
	set interp [interp create -safe]
	# Also remove the "harmless" commands from the interpreter,
	# as they are not part of the file format without macros
	foreach cmd [$interp eval {info commands}] {$interp hide $cmd}

	foreach cmd {config tag tagon tagoff text newline} {
		$interp alias $cmd ::richtext::fileformat::$cmd $w
	}
	return $interp
}

proc load_widget_from_string {w data} {
	reset_widget $w
	set interp [setup_fileformat_interp $w]
	fileformat::_initialize $w
	$interp eval $data  ;# main action
	fileformat::_finalize $w
	interp delete $interp
	$w tag raise sel
	event generate $w <<PropertyRefresh>>
}

proc load_widget_from_file {w filePath} {
	reset_widget $w
	set interp [setup_fileformat_interp $w]
	fileformat::_initialize $w

	# The "source" command always reads files using the current system
	# encoding. Temporarily set that to utf-8 as TkOffice documents are
	# UTF8-encoded on all platforms.
	set system_encoding [encoding system]
	encoding system utf-8
	$interp invokehidden source $filePath
	encoding system $system_encoding

	fileformat::_finalize $w
	interp delete $interp
	$w tag raise sel
	event generate $w <<PropertyRefresh>>
}


namespace eval fileformat {

	variable active_tags

	proc _initialize {w} {
		variable active_tags
		set active_tags($w) [list]
	}

	proc _finalize {w} {
		variable active_tags
		unset active_tags($w)
	}

	proc config {w args} {
		namespace upvar [namespace parent] allowed_config allowed_config
		namespace upvar [namespace parent] config_of_widget config_of_widget

		if {[llength $args]&1 != 0} {
			error "number of arguments must be even, have [llength $args] arguments"
		}
		foreach {option value} $args {
			if {$option ni $allowed_config} {
				error "option \"$option\" not available for config command"
			}
			$w configure $option $value
			dict set config_of_widget($w) $option $value
		}
	}

	proc tag {w tagName args} {
		# If the tag already exists, do nothing (only important for clipboard_paste)
		if {$tagName ni [$w tag names]} {
			$w tag configure $tagName {*}$args
		}
	}

	proc tagon {w tagName} {
		variable active_tags
		if {$tagName ni $active_tags($w)} {
			# We need to insert the new tag at the _beginning_ of the list
			# handed to "$w insert", since "$w dump" outputs "tagon" entries
			# in the _reverse_ order - the tag added earliest gives the
			# last "tagon" entry and the first "tagoff" entry.
			set active_tags($w) [linsert $active_tags($w) 0 $tagName]
		}
	}

	proc tagoff {w tagName} {
		variable active_tags
		if {$tagName in $active_tags($w)} {
			# lremoveitem active_tags($w) $tagName
			set idx [lsearch -exact $active_tags($w) $tagName]
			set active_tags($w) [lreplace $active_tags($w) $idx $idx]
		}
	}

	proc text {w text} {
		variable active_tags
		# Insert at the insertion cursor (important for clipboard_paste)
		$w insert insert $text $active_tags($w)
	}

	proc newline {w} {
		variable active_tags
		$w insert insert \n $active_tags($w)
	}
}

}
