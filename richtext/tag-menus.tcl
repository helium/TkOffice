# Copyright 2021-2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

# Add/Remove Tag submenus
# (applicable for context menus, menubuttons, or the menubar)

proc addtag_menu_populate {menu text} {
	foreach tag [$text tag names] {
		if {$tag eq "sel"} continue
		$menu add command -label $tag -command [list richtext::addtag_menu_action $text $tag]
	}
	if {[$menu index end] eq {none}} {
		$menu add command -label [mc addtag_no_tags_in_document] -state disabled
	}
}

proc addtag_menu_action {text tag} {
	set selection_ranges [$text tag ranges sel]
	if {$selection_ranges ne {}} {
		# Special treatment for selections with multiple parts (list length > 2):
		# Preserve tag ranges in "gaps" (that are not selected now).
		set preserve_ranges [list]
		foreach {index1 index2} [lrange $selection_ranges 1 end-1] {
			lappend preserve_ranges {*}[richtext::tag_ranges_within $text $tag $index1 $index2]
		}
		# Undoable version of [$text tag add $tag {*}$selection_ranges]
		undo::undoable_edit_step $text TagRange $tag sel.first sel.last {*}$preserve_ranges {*}$selection_ranges
	}
}

proc removetag_menu_populate {menu text} {
	set selection_ranges [$text tag ranges sel]
	if {$selection_ranges eq {}} {
		set tags [$text tag names insert]
	} else {
		set tags [tag_names_for_ranges $text {*}$selection_ranges]
	}
	foreach tag $tags {
		if {$tag eq "sel"} continue
		$menu add command -label $tag -command [list richtext::removetag_menu_action $text $tag]
	}
	if {[$menu index end] eq {none}} {
		$menu add command -label [mc removetag_no_tags_in_selection] -state disabled
	}
}

proc removetag_menu_action {text tag} {
	set selection_ranges [$text tag ranges sel]
	if {$selection_ranges ne {}} {
		# Special treatment for selections with multiple parts (list length > 2):
		# Preserve tag ranges in "gaps" (that are not selected now).
		set preserve_ranges [list]
		foreach {index1 index2} [lrange $selection_ranges 1 end-1] {
			lappend preserve_ranges {*}[richtext::tag_ranges_within $text $tag $index1 $index2]
		}
		# Undoable version of [$text tag remove $tag {*}$selection_ranges]
		undo::undoable_edit_step $text TagRange $tag sel.first sel.last {*}$preserve_ranges
	}
}

}
