# Copyright 2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

namespace eval richtext {

namespace import ::msgcat::mc

variable allowed_config [list \
	-background -foreground -font -selectbackground -selectforeground \
	-inactiveselectbackground -insertbackground \
	-spacing1 -spacing2 -spacing3 -tabs -tabstyle -padx -pady]


# This procedure does nothing, but calling it makes sure
# the file containing it is sourced via tclIndex.
proc init_namespace {} {}

}
