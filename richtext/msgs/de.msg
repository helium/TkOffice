# Copyright 2020-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {
::msgcat::mcflmset {

new_tag_name_pattern {Stil#$i}

entire_text {Ganzer Text}
removetag_no_tags_in_selection {Auswahl enthält keine Stile}
addtag_no_tags_in_document {Dokument enthält keine Stile}

tag_list_title {Text-Stile}
tag_add {Erstellen}
tag_remove {Entfernen}
tag_up {Prio. senken}
tag_down {Prio. erhöhen}

property_pane_colors_title_tag_mode {Farben und Muster}
property_pane_colors_title_widget_mode {Farben}
property_background {Hintergrund: }
property_foreground {Textfarbe: }
property_bgstipple {Hintergrundmuster: }
property_relief {Rahmenart: }
property_border {Rahmendicke: }
property_selectbackground {Auswahl-Hintergrund: }
property_inactiveselectbackground {Auswahl-HG inaktiv: }
property_selectforeground {Auswahl-Textfarbe: }
property_insertbackground {Textcursor: }

property_pane_font_title {Schrift}
property_font_family {Schriftart: }
property_font_size {Größe: }
property_font_style {Stil: }
property_font_bold {Fett}
property_font_italic {Kursiv}
property_font_underline {Unter.}
property_font_overstrike {Durch.}
property_underline {Unterstreichen: }
property_overstrike {Durchstreichen: }
property_off {Aus}
property_on {Ein}
property_offset {Hoch-/Tiefstellen: }

property_pane_horizontal_title {Horizontal}
property_tabs {Tabulatoren: }
property_tabstyle {Tab-Verhalten: }
property_tabstyle_tabular {Tabelle}
property_tabstyle_wordprocessor {Klassisch}
property_justify {Ausrichtung: }
property_justify_left {Links}
property_justify_center {Zentriert}
property_justify_right {Rechts}
property_lmargin1 {Einzug 1. Zeile: }
property_lmargin2 {Einzug andere: }
property_rmargin {Rechter Rand: }
property_wrap {Umbruch: }
property_wrap_word {Am Wortende}
property_wrap_char {Jedes Zeichen}
property_wrap_none {Kein Umbruch}
property_padx {Extra-Abstand: }

property_pane_vertical_title {Vertikale Abstände}
property_spacing1 {Über Absatz: }
property_spacing2 {Im Absatz: }
property_spacing3 {Unter Absatz: }
property_pady {Extra-Abstand: }

tabs_unset {ungesetzt}
tab_type_left {Links}
tab_type_center {Zentriert}
tab_type_right {Rechts}
tab_type_numeric {Dezimal}
tab_remove {Entfernen}
}
}
