# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

proc properties_frame_create {path -for textwidget} {
	variable properties_frame_for
	variable textwidget_for_properties_frame
	if {${-for} ne {-for}} {
		error "wrong second argument: must be properties_frame_create path -for textwidget"
	}

	ttk::frame $path
	set properties_frame_for($textwidget) $path
	set textwidget_for_properties_frame($path) $textwidget

	foreach mode {widget tag} {
		ttk::frame $path.$mode
		grid [properties_pane_colors $path.$mode.colors $mode] -padx 3 -pady {3 0} -sticky we
		grid [properties_pane_font $path.$mode.font $mode] -padx 3 -pady {6 0} -sticky we
		grid [properties_pane_horizontal $path.$mode.horizontal $mode] -padx 3 -pady {6 0} -sticky we
		grid [properties_pane_vertical $path.$mode.vertical $mode] -padx 3 -pady {6 0} -sticky we
		grid columnconfigure $path.$mode 0 -weight 1
	}

	bind $path <Destroy> [list richtext::properties_frame_destroy $path $textwidget]

	set esc_binding [list focus $textwidget]
	foreach frame [concat [winfo children $path.widget] [winfo children $path.tag]] {
		foreach widget [winfo children $frame] {
			switch [winfo class $widget] TCombobox {
				bind $widget <Escape> $esc_binding
			} TEntry - TSpinbox - Text {
				# Virtual event "Escape with no changes" of tabstop editor etc.
				bind $widget <<Escape>> $esc_binding
			}
		}
	}

	return $path
}

proc properties_frame_link_to_tag_list {path tag_list} {
	variable taglist_for_properties_frame

	set taglist_for_properties_frame($path) $tag_list
}

proc properties_frame_unlink_tag_list {path} {
	variable  taglist_for_properties_frame

	unset -nocomplain taglist_for_properties_frame($path)
}

proc properties_frame_destroy {path textwidget} {
	variable properties_frame_for
	variable textwidget_for_properties_frame
	variable taglist_for_properties_frame
	variable old_mode
	variable old_bindtags

	unset old_mode($path)
	unset properties_frame_for($textwidget)
	unset textwidget_for_properties_frame($path)
	if {[info exists taglist_for_properties_frame($path)]} {
		tag_list_unlink_panel $taglist_for_properties_frame($path)
		unset taglist_for_properties_frame($path)
	}

	foreach frame [winfo children $path] {
		foreach child [winfo children $frame] {
			unset -nocomplain old_bindtags($child)
		}
	}
}

proc properties_frame_set_mode {path mode} {
	variable old_mode

	if {[info exists old_mode($path)] && $mode eq $old_mode($path)} return

	switch $mode widget {
		pack forget $path.tag
		pack $path.widget -fill both -expand true
	} tag {
		pack forget $path.widget
		pack $path.tag -fill both -expand true
	}
	set old_mode($path) $mode
}

proc properties_frame_set_enabled {path enabled} {
	variable old_bindtags

	if {$enabled} {
		set state !disabled
	} else {
		set state disabled
	}
	foreach frame [concat [winfo children $path.widget] [winfo children $path.tag]] {
		$frame state $state
		foreach child [winfo children $frame] {
			if {[winfo class $child] ne {Text}} {
				$child state $state
			} else {
				if {$enabled} {
					if {[info exists old_bindtags($child)]} {
						bindtags $child $old_bindtags($child)
						unset old_bindtags($child)
					}
				} else {
					if {![info exists old_bindtags($child)]} {
						set old_bindtags($child) [bindtags $child]
						bindtags $child DisabledText
					}
				}
			}
		}
	}
}

proc properties_frame_update {path mode options} {
	properties_frame_set_mode $path $mode
	properties_pane_colors_update $path.$mode.colors $mode $options
	properties_pane_font_update $path.$mode.font $mode $options
	properties_pane_horizontal_update $path.$mode.horizontal $mode $options
	properties_pane_vertical_update $path.$mode.vertical $mode $options
}

proc properties_frame_update_from_undo {textwidget mode option value {-unset {}}} {
	variable properties_frame_for

	# For $mode == "tag", this procedure expects that the tag being changed is selected
	# in the tag list view. It does not check for that but just updates the property
	# controls associated with $option.

	set path $properties_frame_for($textwidget)
	properties_pane_colors_update_single $path.$mode.colors $option $value ${-unset}
	properties_pane_font_update_single $path.$mode.font $mode $option $value ${-unset}
	properties_pane_horizontal_update_single $path.$mode.horizontal $option $value ${-unset}
	properties_pane_vertical_update_single $path.$mode.vertical $option $value ${-unset}
}

}
