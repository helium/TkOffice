# Copyright 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext::undoable {

proc Insert {widget index args} {
	set index [$widget index $index]
	$widget mark set undo::insert_mark $index
	$widget insert $index {*}$args
	set end_index [$widget index undo::insert_mark]
	$widget mark unset undo::insert_mark
	$widget see $index
	$widget see $end_index
	return [list Delete $index $end_index]
}

proc Delete {widget index1 {index2 {}}} {
	set index1 [$widget index $index1]
	if {$index2 ne {}} {
		set index2 [$widget index $index2]
	} else {
		set index2 [$widget index "$index1 + 1 c"]
	}
	set index2_fix [richtext::final_linebreak_fix $widget $index2]
	set text_and_tags [richtext::to_text_taglist_pairs $widget $index1 $index2_fix]
	$widget delete $index1 $index2
	$widget see $index1
	return [list Insert $index1 {*}$text_and_tags]
}

proc Replace {widget index1 index2 args} {
	set index1 [$widget index $index1]
	set index2 [$widget index $index2]
	set index2_fix [richtext::final_linebreak_fix $widget $index2]
	set text_and_tags [richtext::to_text_taglist_pairs $widget $index1 $index2_fix]
	$widget mark set undo::replace_mark $index1
	$widget replace $index1 $index2 {*}$args
	set end_index [$widget index undo::replace_mark]
	$widget mark unset undo::replace_mark
	$widget see $index1
	$widget see $end_index
	return [list Replace $index1 $end_index {*}$text_and_tags]
}

# Example: TagRange .editor highlight 2.32 2.56 2.42 2.46 2.50 2.54
# -> remove tag "highlight" from indices 2.32 up to 2.56
# -> add tag "highlight" to indices 2.42 up to 2.46
# -> add tag "highlight" to indices 2.50 up to 2.54

proc TagRange {widget tag remove_from remove_to args} {
	set remove_from [$widget index $remove_from]
	set remove_to [$widget index $remove_to]
	set prev_tag_ranges [richtext::tag_ranges_within $widget $tag $remove_from $remove_to]
	$widget tag remove $tag $remove_from $remove_to
	if {$args ne {}} {
		$widget tag add $tag {*}$args
	}
	return [list TagRange $tag $remove_from $remove_to {*}$prev_tag_ranges]
}

proc TagConfigure {widget tag option value} {
	set old_value [$widget tag cget $tag $option]
	$widget tag configure $tag $option $value
	richtext::tag_list_select_tags $widget $tag
	set unset [expr {$value eq {} ? {-unset} : {}}]
	richtext::properties_frame_update_from_undo $widget tag $option $value $unset
	return [list TagConfigure $tag $option $old_value]
}

proc TagRename {widget old_tag new_tag} {
	richtext::tag_rename $widget $old_tag $new_tag
	richtext::tag_list_rename_from_undo $widget $old_tag $new_tag
	richtext::tag_list_select_tags $widget $new_tag
	return [list TagRename $new_tag $old_tag]
}

proc TagCreate {widget tag above_tag options ranges} {
	# $above_tag may be empty ($tag is lowest in priority)
	$widget tag configure $tag {*}$options
	if {$ranges ne {}} {
		$widget tag add $tag {*}$ranges
	}
	if {$above_tag ne {}} {
		$widget tag raise $tag $above_tag
	} else {
		$widget tag lower $tag
	}
	richtext::tag_list_add_from_undo $widget $tag $above_tag
	return [list TagDelete $tag]
}

proc TagDelete {widget args} {
	set all_tags [$widget tag names]

	foreach tag $args {
		set options [richtext::config_of_tag $widget $tag]
		set ranges [$widget tag ranges $tag]

		set pos [lsearch -exact $all_tags $tag]
		if {$pos <= 0} {
			set above_tag {}
		} else {
			set above_tag [lindex $all_tags $pos-1]
		}

		$widget tag delete $tag
		lappend opposite [list TagCreate $tag $above_tag $options $ranges]
	}
	richtext::tag_list_refresh_from_undo $widget
	if {[llength $opposite] == 1} {
		return [lindex $opposite 0]
	} else {
		return [concat undo::Group $opposite]
	}
}

proc TagReorder {widget op moved_tags reference_tag {order_to_restore {}}} {
	if {$order_to_restore ne {}} {
		foreach tag $order_to_restore {
			if {[info exists ref_tag]} {
				$widget tag raise $tag $ref_tag
			}
			set ref_tag $tag
		}

		richtext::tag_list_refresh_from_undo $widget
		richtext::tag_list_select_tags $widget {*}$moved_tags

		# The original (now opposite) operation was stored in the arguments.
		return [list TagReorder $op $moved_tags $reference_tag]
	}

	set prev_tags [$widget tag names]
	if {$op ni {lower raise}} {
		error "invalid operation '$op': must be lower or raise"
	}
	set remaining_moved_tags [lassign $moved_tags first_moved_tag]
	$widget tag $op $first_moved_tag $reference_tag

	# Make remaining items follow the first item without gaps
	set ref_tag $first_moved_tag
	foreach tag $remaining_moved_tags {
		$widget tag raise $tag $ref_tag
		set ref_tag $tag
	}

	richtext::tag_list_refresh_from_undo $widget
	richtext::tag_list_select_tags $widget {*}$moved_tags

	# Generate the opposite action

	# Assuming that $moved_tags are ordered by priority, it suffices to
	# track the first and the last one. ($pos_first and $pos_last may be
	# identical if there is only one item in $moved_tags.)
	set pos_first [lsearch -exact $prev_tags [lindex $moved_tags 0]]
	set pos_last [lsearch -exact $prev_tags [lindex $moved_tags end]]

	if {$pos_last - $pos_first == [llength $moved_tags] - 1} {
		# A contiguous range of tags was moved
		switch $op raise {
			set ref_tag [lindex $prev_tags $pos_last+1]
			return [list TagReorder lower $moved_tags $ref_tag]
		} lower {
			set ref_tag [lindex $prev_tags $pos_first-1]
			return [list TagReorder raise $moved_tags $ref_tag]
		}
	} else {
		# Restoring a set of disparate tag ranges cannot be expressed
		# by a single "lower" or "raise" operation. Therefore, store the
		# whole sequence of tags (omitting non-changed entries at the
		# beginning and the end) as an extra argument for TagReorder.
		# The remaining arguments are used for remembering the current
		# action on Redo.
		set new_tags [$widget tag names]
		set num_tags [llength $new_tags]
		for {set i 0} {$i < $num_tags} {incr i} {
			if {[lindex $prev_tags $i] ne [lindex $new_tags $i]} break
		}
		set first_changed $i
		for {set i [expr {$num_tags - 1}]} {$i >= 0} {incr i -1} {
			if {[lindex $prev_tags $i] ne [lindex $new_tags $i]} break
		}
		set last_changed $i
		set order_to_restore [lrange $prev_tags $first_changed $last_changed]
		return [list TagReorder $op $moved_tags $reference_tag $order_to_restore]
	}
}

proc WidgetConfigure {widget option value} {
	set old_config [richtext::config_of_widget $widget]
	richtext::widget_option_set $widget $option $value
	richtext::tag_list_select_entire_text_entry $widget
	richtext::properties_frame_update_from_undo $widget widget $option $value
	if {[dict exists $old_config $option]} {
		return [list WidgetConfigure $option [dict get $old_config $option]]
	} else {
		return [list WidgetUnconfigure $option]
	}
}

proc WidgetUnconfigure {widget option} {
	set old_config [richtext::config_of_widget $widget]
	richtext::widget_option_unset $widget $option
	richtext::tag_list_select_entire_text_entry $widget
	richtext::properties_frame_update_from_undo $widget widget $option {} -unset
	return [list WidgetConfigure $option [dict get $old_config $option]]
}

# Unlike the other actions, this one serves for merely documenting a change that
# already happened to the text widget. ClipboardPaste will never be part of an
# Undo or Redo list, as there is no procedure here that returns it as an
# "opposite" action.

proc ClipboardPaste {widget from_index to_index tags_created} {
	set delete_op [list Delete $from_index $to_index]
	if {$tags_created eq {}} {
		return $delete_op
	} else {
		set tag_delete_op [concat TagDelete $tags_created]
		return [list undo::Group $delete_op $tag_delete_op]
	}
}

}

namespace eval richtext {

proc final_linebreak_fix {widget index} {
	if {[$widget compare $index == end]} {
		return [$widget index end-1c]
	} else {
		return $index
	}
}

}
