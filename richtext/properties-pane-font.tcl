# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

proc properties_pane_font {f mode} {
	variable entry_last_value

	ttk::labelframe $f -text [mc property_pane_font_title] -style Panel.TLabelframe
	grid columnconfigure $f 2 -weight 1

	# Properties for tags and whole text widget:
	grid [ttk::label $f.font_family_l -text [mc property_font_family]] \
	     [ttk::combobox $f.font_family -state readonly -width 0] \
	     - \
	     [ttk::button $f.font_clear -text [mc cancel_button] -style Toolbutton] \
	     -sticky nsw -pady 2
	grid configure $f.font_family -padx {0 3} -sticky nesw

	grid [ttk::label $f.font_size_l -text [mc property_font_size]] \
	     [ttk::spinbox $f.font_size -from 1 -to Inf -width 4] \
	     [ttk::combobox $f.font_size_unit -state readonly -width 3] \
	     ^ \
	     -sticky nsw -pady 2
	grid configure $f.font_size $f.font_size_unit -padx {0 3}
	grid configure $f.font_size -sticky nesw
	$f.font_size_unit configure -values {px pt}

	grid [ttk::label $f.font_style_l -text [mc property_font_style]] \
	     [ttk::frame $f.font_style_f] \
	     - \
	     ^ \
	     -sticky nsw -pady 2
	grid configure $f.font_style_f -padx {0 3}

	ttk::checkbutton $f.font_style_bold -text [mc property_font_bold] \
		-style Toolbutton -variable $f.font_style_bold -onvalue bold -offvalue normal
	ttk::checkbutton $f.font_style_italic -text [mc property_font_italic] \
		-style Toolbutton -variable $f.font_style_italic -onvalue italic -offvalue roman
	set ::$f.font_style_bold normal
	set ::$f.font_style_italic roman
	grid $f.font_style_bold $f.font_style_italic -in $f.font_style_f -padx {0 2}

	switch $mode widget {
	} tag {
		# Properties for tags only:
		bool_entry_create $f underline -label [mc property_underline]
		bool_entry_create $f overstrike -label [mc property_overstrike]
		size_entry_create $f offset -label [mc property_offset] \
			-from -Inf -to Inf
	} default {
		error "invalid mode \"$mode\": must be tag or widget"
	}

	$f.font_family configure -values \
		[concat monospace sans-serif serif [lsort -unique [font families]]]

	set binding [list richtext::properties_pane_font_apply $f $mode]
	bind $f.font_family <<ComboboxSelected>> $binding
	bind $f.font_family <ButtonPress-1> {focus_intercept_click %W}
	ttk::bindMouseWheel $f.font_family {focus_intercept_mousewheel %W}

	foreach event {<<Increment>> <<Decrement>>} dir {+1 -1} {
		bind $f.font_size $event \
			[list richtext::font_size_entry_spin $f $mode $dir]
		bind $f.font_size $event +break
	}
	ttk::bindMouseWheel $f.font_size \
		[list richtext::font_size_entry_mousewheel $f $mode]
	set binding_checked [list richtext::font_size_number_changed $f $mode]
	bind_spinbox $f.font_size $binding_checked
	bind $f.font_size <<FocusOutPrepare>> $binding_checked
	bind $f.font_size <Escape> {richtext::size_entry_escape %W}
	bind $f.font_size <ButtonPress-1> {+focus_intercept_click %W}
	bind $f.font_size <<PasteSelection>> break

	set entry_last_value($f.font_size) {}
	bind $f <Destroy> {richtext::properties_pane_font_destroy %W}

	bind $f.font_size_unit <<ComboboxSelected>> \
		[list richtext::font_size_unit_changed $f $mode]
	bind $f.font_size_unit <ButtonPress-1> {focus_intercept_click %W}
	ttk::bindMouseWheel $f.font_size_unit {focus_intercept_mousewheel %W}

	$f.font_style_bold configure -command $binding
	$f.font_style_italic configure -command $binding
	$f.font_clear configure -command [list richtext::properties_pane_font_clear $f $mode]

	foreach cb [list $f.font_style_bold $f.font_style_italic] {
		bind $cb <B1-Enter> {focus_intercept_click %W}  ;# This binding triggers before ButtonPress!
		bind $cb <ButtonPress-1> {focus_intercept_click %W -noupdate}
		bind $cb <ButtonRelease-1> {focus_intercept_click %W -noupdate}
	}

	return $f
}

proc properties_pane_font_destroy {f} {
	variable entry_last_value
	variable last_validation_attempt

	unset entry_last_value($f.font_size)
	unset -nocomplain last_validation_attempt($f.font_size)
}

proc properties_pane_font_update_common {f mode font_value} {
	variable entry_last_value

	set fontspec [font::to_dict $font_value]
	$f.font_family set [dict get $fontspec -family]
	set size [dict get $fontspec -size]
	if {$size > 0} {
		set entry_last_value($f.font_size) $size
		$f.font_size set $size
		$f.font_size_unit set pt
	} else {
		set entry_last_value($f.font_size) [expr {-$size}]
		$f.font_size set [expr {-$size}]
		$f.font_size_unit set px
	}
	set ::$f.font_style_bold [dict get $fontspec -weight]
	set ::$f.font_style_italic [dict get $fontspec -slant]
	$f.font_clear state !disabled
}

proc properties_pane_font_update {f mode options} {
	variable entry_last_value

	if {[dict exists $options -font]} {
		properties_pane_font_update_common $f $mode [dict get $options -font]
	} else {
		properties_pane_font_clear $f $mode -noapply
	}

	switch $mode tag {
		bool_entry_update $f underline $options
		bool_entry_update $f overstrike $options
		size_entry_update $f offset $options
	}
}

proc properties_pane_font_update_single {f mode option value {-unset {}}} {
	if {${-unset} ne {-unset}} {
		set opt_list [list $option $value]
	} else {
		set opt_list [list]
	}
	switch $option {
		"-font" {
			if {${-unset} ne {-unset}} {
				properties_pane_font_update_common $f $mode $value
			} else {
				properties_pane_font_clear $f $mode -noapply
			}
		}
		"-underline"  { bool_entry_update $f underline $opt_list }
		"-overstrike" { bool_entry_update $f overstrike $opt_list }
		"-offset"     { size_entry_update $f offset $opt_list }
	}
}

proc properties_pane_font_apply {f mode} {
	variable textwidget_for_properties_frame

	# Initialize fontspec from whole-widget setting (uses default if never configured)
	set properties_frame [winfo parent [winfo parent $f]]
	set richtext $textwidget_for_properties_frame($properties_frame)
	set default_font [$richtext cget -font]
	if {$default_font in [font names]} {
		# $default_font is usually TkFixedFont if never configured
		set fontspec [font configure $default_font]
	} elseif {[dict exists $default_font -family]} {
		set fontspec $default_font
	} else {
		set fontspec [font::to_dict $default_font]
	}

	# Apply defaults if entries are missing
	if {[$f.font_family get] eq {}} {
		$f.font_family set [dict get $fontspec -family]
	}
	if {[$f.font_size_unit get] eq {}} {
		if {[dict get $fontspec -size] > 0} {
			$f.font_size_unit set pt
		} else {
			$f.font_size_unit set px
		}
	}
	if {[$f.font_size get] eq {}} {
		set default_size [dict get $fontspec -size]
		switch [$f.font_size_unit get] px {
			if {$default_size < 0} {
				$f.font_size set [expr {-$default_size}]
			} else {
				$f.font_size set [expr {round($default_size * [tk scaling])}]
			}
		} pt {
			if {$default_size > 0} {
				$f.font_size set $default_size
			} else {
				$f.font_size set [expr {round(-$default_size / [tk scaling])}]
			}
		}
	}

	# Main part
	switch [$f.font_size_unit get] px {
		set size -[$f.font_size get]
	} pt {
		set size [$f.font_size get]
	}
	dict set fontspec -family [$f.font_family get]
	dict set fontspec -size $size
	dict set fontspec -weight [get ::$f.font_style_bold]
	dict set fontspec -slant [get ::$f.font_style_italic]
	dict set fontspec -underline 0
	dict set fontspec -overstrike 0

	tag_list_property_set $f -font [font::simplify $fontspec]
	$f.font_clear state !disabled
}

proc properties_pane_font_clear {f mode {-noapply {}}} {
	variable entry_last_value

	$f.font_family set {}
	$f.font_size set {}
	set entry_last_value($f.font_size) {}
	$f.font_size_unit set {}
	set ::$f.font_style_bold normal
	set ::$f.font_style_italic roman
	$f.font_clear state disabled

	if {${-noapply} ne {-noapply}} {
		tag_list_property_set $f -font {} -unset
	}
}

proc font_size_entry_set {f value} {
	variable entry_last_value

	$f.font_size set $value
	set entry_last_value($f.font_size) $value
}

proc font_size_entry_spin {f mode steps} {
	variable entry_last_value
	variable last_validation_attempt

	set spinbox $f.font_size
	set value [$spinbox get]
	if {$value eq {}} {
		properties_pane_font_apply $f $mode  ;# load default from widget
		return
	} elseif {!([string is integer $value] && $value > 0)} {
		if {![info exists last_validation_attempt($spinbox)]} {
			# Spinbox just turned invalid
			size_entry_becomes_invalid $spinbox
			set last_validation_attempt($spinbox) $value
			return
		} elseif {$last_validation_attempt($spinbox) ne $value} {
			# Spinbox was already invalid but the user tried another value, keep Invalid status
			set last_validation_attempt($spinbox) $value
			return
		} else {
			# Spinbox was already invalid and has not changed, restore
			set new_value $entry_last_value($w)
			if {$new_value eq {}} {
				properties_pane_font_apply $f $mode  ;# load default from widget
			}
			return
		}
	}
	set new_value [expr {$value + $steps}]
	if {$new_value <= 0} {
		set new_value 1
	}
	$spinbox set $new_value
	set script [list richtext::size_entry_select_all_on_focus $spinbox]
	after cancel $script
	after 2 $script

	properties_pane_font_apply $f $mode
}

proc font_size_entry_mousewheel {f mode neg_steps} {
	if {! [catch {focus_intercept_click $f.font_size}]} {
		font_size_entry_spin $f $mode [expr {-$neg_steps}]
	}
	return -code break
}

proc font_size_number_changed {f mode} {
	variable entry_last_value
	variable last_validation_attempt

	set spinbox $f.font_size
	set number [$spinbox get]
	if {$number eq $entry_last_value($spinbox)} return

	if {[string is integer $number] && $number ne {} && $number > 0} {
		set number [expr {$number}]
		$spinbox set $number
		properties_pane_font_apply $f $mode
		size_entry_becomes_valid $spinbox
		set entry_last_value($spinbox) $number
	} elseif {![info exists last_validation_attempt($spinbox)]} {
		# Spinbox just turned invalid
		size_entry_becomes_invalid $spinbox
		focus $spinbox  ;# work against <FocusOut>
		set last_validation_attempt($spinbox) $number
	} elseif {$last_validation_attempt($spinbox) eq $number} {
		# Spinbox was already invalid and has not changed, restore
		$spinbox set $entry_last_value($spinbox)
		size_entry_becomes_valid $spinbox
		if {$entry_last_value($spinbox) eq {}} {
			properties_pane_font_clear $f $mode -noapply
		}
	} else {
		# Spinbox was already invalid but the user tried another value, keep Invalid status
		focus $spinbox  ;# work against <FocusOut>
		set last_validation_attempt($spinbox) $number
	}
}

proc font_size_unit_changed {f mode} {
	properties_pane_font_apply $f $mode
}

}
