#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Relative path to tclIndex - needs to be updated (add/remove some ..)
#  whenever this script is moved into a different hierarchy level
set dir [file join [file dirname [info script]] .. ..]
source [file join $dir tclIndex]

ttk_useTheme clam

icons::load  tab-center,13  tab-numeric,13  tab-left,13  tab-right,13

proc configure_tabs {w} {
	if {[winfo width $w] <= 1} return
	set right_end [expr {
		[winfo width $w] - 2*([$w cget -padx] + [$w cget -highlightthickness] + [$w cget -border])
	}]
	$w configure -tabs [list 5 [expr {25 + [font measure TkDefaultFont 00000]}] numeric $right_end right]
}

pack [ttk_text .t -font TkDefaultFont -width 20 -height 6 -padx 0 -wrap none -spacing1 2 -spacing3 2] \
	-fill both -expand true -padx 3 -pady 3
bind .t <Configure> {configure_tabs %W}
.t tag configure highlight -background #ffa
.t insert end "\t"
.t image create end -image tab-left,13
.t insert end "\t2.3 cm\n"
.t insert end "\t"
.t image create end -image tab-left,13
.t insert end "\t4.3 cm\t \u25be \n"
.t tag add highlight {insert - 1 line} insert
.t insert end "\t"
.t image create end -image tab-center,13
.t insert end "\t131.53 pt\n"
.t insert end "\t"
.t image create end -image tab-numeric,13
.t insert end "\t10 cm\n"
.t insert end "\t"
.t image create end -image tab-right,13
.t insert end "\t12.5 cm\n"
.t insert end "\t\t"


event generate .t <Configure> -when tail
