#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Relative path to tclIndex - needs to be updated (add/remove some ..)
#  whenever this script is moved into a different hierarchy level
set dir [file join [file dirname [info script]] .. ..]
source [file join $dir tclIndex]

# Set up the GUI translations
package require msgcat
namespace import ::msgcat::mc
foreach namespace {common menus richtext} {
	msgcat::mcload [file join $dir $namespace msgs]
}

# Build up GUI

wm state . withdrawn
ttk_useTheme clam

icons::load  list-add,16  list-remove,16  up,16  down,16

grid [text .document -wrap word] \
	-row 0 -column 0 -sticky nesw
grid [ttk::scrollbar .scroll -orient vertical] \
	-row 0 -column 1 -sticky ns
grid [ttk::scrollbar .hscroll -orient horizontal] \
	-row 1 -column 0 -sticky we
.document configure -yscrollcommand [list .scroll set]
.scroll configure -command [list .document yview]
.document configure -xscrollcommand [list .hscroll set]
.hscroll configure -command [list .document xview]

grid [ttk::frame .tags] \
	-row 0 -column 2 -rowspan 2 -sticky nesw -padx {3 0} -pady 3
grid rowconfigure .tags 1 -weight 1
grid columnconfigure .tags 0 -weight 1

grid [ttk::label .tags.title -text Styles:] -sticky nw -pady {0 3}

grid [ttk_treeview .tags.treeview] -sticky nesw
.tags.treeview configure -show {} -columns tagName -selectmode browse
.tags.treeview column tagName -width 120
font create ItalicFont {*}[font configure TkDefaultFont] -slant italic
.tags.treeview tag configure entire_text -font ItalicFont
.tags.treeview insert {} end -values {{Entire Text}} -tag entire_text
.tags.treeview insert {} end -values heading1
.tags.treeview insert {} end -values heading2
.tags.treeview insert {} end -values italic
.tags.treeview insert {} end -values link

grid [ttk::button .tags.add -text Add -image list-add,16 -compound left -default disabled] -pady {3 0}
grid [ttk::button .tags.remove -text Remove -image list-remove,16 -compound left -default disabled] -pady 1
grid [ttk::button .tags.up -text "Lower Priority" -image up,16 -compound left -default disabled] -pady {6 1}
grid [ttk::button .tags.down -text "Higher Priority" -image down,16 -compound left -default disabled]
foreach button {add remove up down} {
	.tags.$button configure -style LeftAlignedImage.TButton
	grid configure .tags.$button -sticky nesw
}

ttk::frame .properties
grid [richtext::properties_pane_colors .properties.fc] \
	-padx 3 -pady {3 0} -sticky we
grid [richtext::properties_pane_font .properties.ff] \
	-padx 3 -pady {6 0} -sticky we
grid [richtext::properties_pane_horizontal .properties.fh] \
	-padx 3 -pady {6 0} -sticky we
grid [richtext::properties_pane_vertical .properties.fv] \
	-padx 3 -pady {6 0} -sticky we

grid [make_scrolling_frame .properties_scroll .properties] \
	-row 0 -column 3 -rowspan 2 -sticky nesw

grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1

highlight_setup .document
richtext::set_event_bindings .document

bind .tags.treeview <<TreeviewSelect>> update_treeview

proc update_treeview {} {
	if {[.tags.treeview selection] eq "I001"} {
		update_mode widget
	} else {
		update_mode tag
	}
}

proc update_mode {mode} {
	global old_mode
	if {$mode ne $old_mode} {
		richtext::properties_pane_colors_mode .properties.fc $mode
		richtext::properties_pane_font_mode .properties.ff $mode
		richtext::properties_pane_horizontal_mode .properties.fh $mode
		richtext::properties_pane_vertical_mode .properties.fv $mode
		set old_mode $mode
	}
}
set old_mode ""
.tags.treeview selection set I001

update idletasks
wm state . normal
