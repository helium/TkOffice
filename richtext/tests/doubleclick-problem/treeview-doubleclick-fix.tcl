#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

proc click_workaround {w x y} {
	variable last_x
	variable last_y
	variable last_milliseconds

	set now [clock milliseconds]

	if {[info exists last_x($w)] && [info exists last_y($w)]
	&& [info exists last_milliseconds($w)]
	&& abs($x - $last_x($w)) <= 5 && abs($y - $last_y($w)) <= 5
	&& $now - $last_milliseconds($w) <= 500} {
		event generate $w <<DoubleClick>>
		set code break
	} else {
		set code ok
	}

	set last_x($w) $x
	set last_y($w) $y
	set last_milliseconds($w) $now

	return -code $code
}

proc click_workaround_keypress {w keysym} {
	variable last_milliseconds

	# The original <Double-ButtonPress> still fires on a click-keypress-click
	# sequence when the keypress is a modifier key (exception: AltGr = ISO_Level3_Shift).
	# Emulate that here:
	# (TODO: Keys on non-PC keyboards, such as Meta or Command (Mac).)
	if {$keysym in {Shift_L Shift_R Control_L Control_R Alt_L Alt_R Super_L Super_R}} return

	unset -nocomplain last_milliseconds($w)
}

ttk::style theme use clam
. configure -bg #dcdad5

grid [ttk::treeview .treeview] \
	-row 0 -column 0 -sticky nesw -padx 3 -pady 3
grid [ttk::labelframe .props -text Properties] \
	-row 0 -column 1 -sticky nesw -padx 3 -pady 3

# Create widgets for properties frame
grid [ttk::label .props.a_label -text {Property A: }] \
     [ttk::spinbox .props.a_spinbox] \
     [ttk::combobox .props.a_combobox -state readonly -width 3] \
     -sticky nsw -pady 2
ttk::label .props.b_label -text {Property BBB: }
ttk::spinbox .props.b_spinbox
ttk::combobox .props.b_combobox -state readonly -width 3
ttk::label .props.c_label -text {Property CC: }
ttk::spinbox .props.c_spinbox
ttk::combobox .props.c_combobox -state readonly -width 3

.treeview configure -show {}
.treeview configure -columns {mytext}
.treeview insert {} end -values {Hello}
.treeview insert {} end -values {Tcl}
.treeview insert {} end -values {Tk}

bind .treeview <Button-1> {click_workaround %W %x %y}
bind .treeview <KeyPress> {click_workaround_keypress %W %K}
bind .treeview <<DoubleClick>> {puts "[clock milliseconds]: Double-click"}

proc toggle_mode {} {
	puts "[clock milliseconds]: toggle_mode"
	global mode
	if {$mode eq {show}} {
		grid forget .props.b_label .props.b_spinbox .props.b_combobox
		grid forget .props.c_label .props.c_spinbox .props.c_combobox
		set mode hide
	} else {
		grid .props.b_label .props.b_spinbox .props.b_combobox -sticky nsw -pady 2
		grid .props.c_label .props.c_spinbox .props.c_combobox -sticky nsw -pady 2
		set mode show
	}
}
set mode hide

# Double-clicking on treeview entries (prints to console) works as intended.
bind .treeview <<TreeviewSelect>> {toggle_mode}
