#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

ttk::style theme use clam
. configure -bg #dcdad5

grid [ttk::treeview .treeview] \
	-row 0 -column 0 -sticky nesw -padx 3 -pady 3
grid [ttk::labelframe .props -text Properties] \
	-row 0 -column 1 -sticky nesw -padx 3 -pady 3

# Create widgets for properties frame
grid [ttk::label .props.a_label -text {Property A: }] \
     [ttk::spinbox .props.a_spinbox] \
     [ttk::combobox .props.a_combobox -state readonly -width 3] \
     -sticky nsw -pady 2
ttk::label .props.b_label -text {Property BBB: }
ttk::spinbox .props.b_spinbox
ttk::combobox .props.b_combobox -state readonly -width 3

.treeview configure -show {}
.treeview configure -columns {mytext}
.treeview insert {} end -values {Hello}
.treeview insert {} end -values {Tcl}
.treeview insert {} end -values {Tk}
bind .treeview <Double-Button-1> {puts "[clock milliseconds]: Double-click"}

proc toggle_mode {} {
	puts "[clock milliseconds]: toggle_mode"
	global mode
	if {$mode eq {show}} {
		grid forget .props.b_label .props.b_spinbox .props.b_combobox
		set mode hide
	} else {
		grid .props.b_label .props.b_spinbox .props.b_combobox -sticky nsw -pady 2
		set mode show
	}
}
set mode hide

# Double-clicking on treeview entries (prints to console) work only when
# on the first click the "grid forget" branch of toggle_mode was executed.
# The double-click binding is never invoked in the other (grid widgets) case.
bind .treeview <<TreeviewSelect>> {toggle_mode}
