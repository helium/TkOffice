#!/usr/bin/wish8.5
# Copyright 2020-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Relative path to tclIndex - needs to be updated (add/remove some ..)
#  whenever this script is moved into a different hierarchy level
set dir [file join [file dirname [info script]] .. ..]
source [file join $dir tclIndex]
source [file join [file dirname [info script]] sourced properties-editor-simple.tcl]

# Set up the GUI translations
package require msgcat
namespace import ::msgcat::mc
foreach namespace {common} {
	msgcat::mcload [file join $dir $namespace msgs]
}

# Build up GUI

ttk_useTheme clam
icons::load down,16 up,16 document-open,16 document-save-as,16

ttk::panedwindow .pw
ttk::notebook .pw.nb
ttk::frame .text_and_buttons
pack [ttk::frame .text_and_buttons.top] -side top
pack [ttk::frame .text_and_buttons.bottom] -side bottom

ttk_text .t -height 7 -wrap word  ;# Main text widget, which contains formatted text
ttk_text .commands -height 10  ;# Commands to apply to the text widget directly
ttk_text .dump -height 10      ;# Contains data of ".t dump"
ttk_text .tags -height 10      ;# Contains tag data of .t
# Exchanges data with .t by means of the load/store procedures under test:
ttk_text .filedata -height 10

foreach text {.t .commands .dump .tags .filedata} {
	ttk_text_scrollbar $text on
	$text configure -width 0
}

wm minsize . 300 330
wm geometry . 580x800

ttk::button .execute -text Execute -image down,16 -compound left
ttk::button .analyze -text Analyze -image up,16 -compound left
ttk::button .save -text Save -image down,16 -compound left
ttk::button .load -text Load -image up,16 -compound left

pack .t_frame -in .text_and_buttons -fill both -expand true -padx 2 -pady 2
pack .execute -in .text_and_buttons.top -side left -padx {0 10} -pady 2
pack .analyze -in .text_and_buttons.top -side right -padx {10 0} -pady 2
pack .save -in .text_and_buttons.bottom -side left -padx {0 10} -pady 2
pack .load -in .text_and_buttons.bottom -side right -padx {10 0} -pady 2

.pw add .pw.nb -weight 1
.pw.nb add .commands_frame -text Commands
.pw.nb add .dump_frame -text Dump
.pw.nb add .tags_frame -text Tags
.pw add .text_and_buttons -weight 1
.pw add .filedata_frame -weight 1
pack .pw -fill both -expand true

foreach w {.pw.nb .execute .analyze .save .load} {
	$w configure -takefocus 0
}
bind . <KeyPress-Alt_L> {
	foreach button {.execute .analyze .save .load} {
		$button configure -underline 0
	}
	foreach tab {0 1 2} {
		.pw.nb tab $tab -underline 0
	}
}
bind . <KeyRelease-Alt_L> {
	foreach button {.execute .analyze .save .load} {
		$button configure -underline -1
	}
	foreach tab {0 1 2} {
		.pw.nb tab $tab -underline -1
	}
}
bind . <Alt-KeyPress-e> {.execute state pressed}
bind . <Alt-KeyPress-a> {.analyze state pressed}
bind . <Alt-KeyPress-s> {.save state pressed}
bind . <Alt-KeyPress-l> {.load state pressed}
bind . <Alt-KeyRelease-e> {.execute state !pressed; .execute invoke}
bind . <Alt-KeyRelease-a> {.analyze state !pressed; .analyze invoke}
bind . <Alt-KeyRelease-s> {.save state !pressed; .save invoke}
bind . <Alt-KeyRelease-l> {.load state !pressed; .load invoke}
bind . <Alt-KeyPress-c> {.pw.nb select 0; focus .commands}
bind . <Alt-KeyPress-d> {.pw.nb select 1}
bind . <Alt-KeyPress-t> {.pw.nb select 2}


# Directly implemented commands (that do not use the procedures under test)

proc execute {} {
	richtext::reset_widget .t
	unset -nocomplain richtext::config_of_widget(.t)

	set interp [interp create -safe]
	$interp alias .t .t
	$interp eval [.commands get 1.0 end-1c]
	interp delete $interp

	event generate .t <<PropertyRefresh>>
}

.execute configure -command execute
bind .execute <ButtonPress> {.pw.nb select 0}


proc analyze_dump {key value index} {
	.dump insert end [list $key $value $index]\n
}

proc analyze {} {
	.dump delete 1.0 end
	.t dump -command analyze_dump 1.0 end-1c

	.tags delete 1.0 end
	foreach tag [.t tag names] {
		.tags insert end "$tag  "
		foreach prop [.t tag configure $tag] {
			set key [lindex $prop 0]
			set value [lindex $prop 4]
			if {$value ne {}} {
				.tags insert end " $key $value"
			}
		}
		.tags insert end \n
	}
	.tags delete end-1c  ;# remove trailing newline
}

.analyze configure -command analyze

bind .analyze <ButtonPress> {
	if {[.pw.nb index current] == 0} {
		.pw.nb select 1
	}
}


# Load File and Save File context menus for the relevant text fields

ttk_menu .commands.contextmenu
.commands.contextmenu add command -label "Load File" -accelerator Ctrl+O -command commands_load_file \
	-image document-open,16 -compound left
.commands.contextmenu add command -label "Save File" -accelerator Ctrl+S -command commands_save_file \
	-image document-save-as,16 -compound left
bind .commands <3> {tk_popup .commands.contextmenu %X %Y}
bind .commands <Menu> {
	tk_popup .commands.contextmenu [expr {[winfo rootx .commands]+20}] [expr {[winfo rooty .commands]+20}]
}
bind .commands <Control-o> {commands_load_file; break}  ;# disable default "open new line" binding
bind .commands <Control-s> commands_save_file

ttk_menu .filedata.contextmenu
.filedata.contextmenu add command -label "Load File" -accelerator Ctrl+O -command filedata_load_file \
	-image document-open,16 -compound left
.filedata.contextmenu add command -label "Save File" -accelerator Ctrl+S -command filedata_save_file \
	-image document-save-as,16 -compound left
bind .filedata <3> {tk_popup .filedata.contextmenu %X %Y}
bind .filedata <Menu> {
	tk_popup .filedata.contextmenu [expr {[winfo rootx .filedata]+20}] [expr {[winfo rooty .filedata]+20}]
}
bind .filedata <Control-o> {filedata_load_file; break}  ;# disable default "open new line" binding
bind .filedata <Control-s> filedata_save_file

# make this available for event bindings, where [info script] is empty
set examples_dir [file join [file dirname [info script]] save-and-load-examples]

proc commands_load_file {} {
	set path [tk_getOpenFile -initialdir $::examples_dir -defaultextension .tcl \
		-filetypes {{{Tcl script} {.tcl}}} ]
	if {$path eq {}} return

	set chan [open $path]  ;# may fail, then the standard Tk error dialog will show
	.commands delete 1.0 end
	.commands insert end [read $chan]
	close $chan
}

proc commands_save_file {} {
	set path [tk_getSaveFile -initialdir $::examples_dir -defaultextension .tcl \
		-filetypes {{{Tcl script} {.tcl}}} ]
	if {$path eq {}} return

	set chan [open $path w]  ; # may fail, then the standard Tk error dialog will show
	puts -nonewline $chan [.commands get 1.0 end-1c]
	close $chan
}

proc filedata_load_file {} {
	set path [tk_getOpenFile -initialdir $::examples_dir -defaultextension .tkt \
		-filetypes {{{TkOffice text document} {.tkt .tktext}}} ]
	if {$path eq {}} return

	set chan [open $path]  ;# may fail, then the standard Tk error dialog will show
	.filedata delete 1.0 end
	.filedata insert end [read $chan]
	close $chan
}

proc filedata_save_file {} {
	set path [tk_getSaveFile -initialdir $::examples_dir -defaultextension .tkt \
		-filetypes {{{TkOffice text document} {.tkt .tktext}}} ]
	if {$path eq {}} return

	set chan [open $path w]  ; # may fail, then the standard Tk error dialog will show
	puts -nonewline $chan [.filedata get 1.0 end-1c]
	close $chan
}


# A "reflected channel" that writes into the text widget .filedata

namespace eval textchan {
	proc initialize {channelId mode} {
		if {$mode ne "write"} {
			error "bad mode \"$mode\": must be write"
		}
		.filedata delete 1.0 end
		return [list initialize finalize watch write]
	}
	proc finalize {channelId} {}
	proc watch {channelId eventspec} {}

	proc write {channelId data_bytes} {
		.filedata insert end [encoding convertfrom utf-8 $data_bytes]
		return [string length $data_bytes]
	}

	namespace export initialize finalize watch write
	namespace ensemble create
}


# Buttons that invoke the (de)serialization procedures under test

proc save {} {
	set chan [chan create write textchan]
	richtext::save_document_to_channel .t $chan
	close $chan
}

.save configure -command save

proc load_ {} {
	richtext::load_widget_from_string .t [.filedata get 1.0 end-1c]
}

.load configure -command load_


# Right-click on the rich text invokes the property editor

bind .t <3> {
	if {![winfo exists .props]} {
		richtext::properties_editor_simple .props .t
	} else {
		# TODO: test on window managers other than xfwm4
		raise .props
		focus [focus -lastfor .props]
	}
}
