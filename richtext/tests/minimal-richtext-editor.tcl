#!/usr/bin/wish8.5
# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Relative path to tclIndex - needs to be updated (add/remove some ..)
#  whenever this script is moved into a different hierarchy level
set dir [file join [file dirname [info script]] .. ..]
source [file join $dir tclIndex]
source [file join [file dirname [info script]] sourced properties-editor-simple.tcl]

package require msgcat
namespace eval richtext {namespace import ::msgcat::mc}
proc mc {args} {namespace eval richtext [concat mc $args]}
msgcat::mcload [file join [file dirname [info script]] .. msgs]
msgcat::mcload [file join [file dirname [info script]] .. .. common msgs]
namespace eval richtext {
	::msgcat::mcmset de {
		minimal_richtext_editor_window_title {TkOffice - Minimaler Richtext-Editor}
		menu_add_tag_to_text {Tag hinzufügen}
		menu_remove_tag_from_text {Tag entfernen}
		menu_property_editor {Stil-Eigenschaften…}
		menu_file_open {Datei öffnen…}
		menu_file_save {Speichern}
		menu_file_save_as {Speichern unter…}
		menu_file_new {Neues Dokument}
		menu_file_open_hotkey {Strg+O}
		menu_file_save_hotkey {Strg+S}
		menu_file_save_as_hotkey {Umschalt+Strg+S}
		filetype_tktext {TkOffice-Textdokument}
		filetype_all {Alle Dateien}
	}
	# Unicode characters like … don't seem to work when the program is started with
	# an environment variable LANG="". (LANG=en.UTF-8 would however work.)
	::msgcat::mcmset {} {
		minimal_richtext_editor_window_title {TkOffice - Minimal Rich-Text Editor}
		menu_add_tag_to_text {Add Tag}
		menu_remove_tag_from_text {Remove Tag}
		menu_property_editor {Style Properties...}
		menu_file_open {Open File...}
		menu_file_save {Save}
		menu_file_save_as {Save As...}
		menu_file_new {New Document}
		menu_file_open_hotkey {Ctrl+O}
		menu_file_save_hotkey {Ctrl+S}
		menu_file_save_as_hotkey {Shift+Ctrl+S}
		filetype_tktext {TkOffice text document}
		filetype_all {All files}
	}
}


# Build up GUI

ttk_useTheme clam

icons::load  list-add,16  list-remove,16  document-properties,16
icons::load  document-new,16  document-open,16  document-save,16  document-save-as,16

wm geometry . 640x440
wm minsize . 160 64

pack [text .document -wrap word -width 0 -height 0 -border 0 -highlightthickness 0] \
	-fill both -expand true -side left
pack [ttk::scrollbar .scroll] -side right -fill y
.document configure -yscrollcommand [list .scroll set]
.scroll configure -command [list .document yview]

ttk_menu .contextmenu
bind . <3> {tk_popup .contextmenu %X %Y}
bind . <Menu> {
	tk_popup .contextmenu [expr {[winfo rootx .]+20}] [expr {[winfo rooty .]+20}]
}
ttk_menu .contextmenu.addtag -postcommand {addtag_menu_refresh .contextmenu.addtag .document}
ttk_menu .contextmenu.removetag -postcommand {removetag_menu_refresh .contextmenu.removetag .document}
.contextmenu add cascade -label [mc menu_add_tag_to_text] -compound left -image list-add,16 \
	-menu .contextmenu.addtag
.contextmenu add cascade -label [mc menu_remove_tag_from_text] -compound left -image list-remove,16 \
	-menu .contextmenu.removetag
.contextmenu add command -label [mc menu_property_editor] -compound left -image document-properties,16 \
	-command [list show_properties_editor .props .document]
.contextmenu add separator
.contextmenu add command -label [mc menu_file_open] -accelerator [mc menu_file_open_hotkey] \
	-compound left -image document-open,16 -command [list file_open .document]
.contextmenu add command -label [mc menu_file_save] -accelerator [mc menu_file_save_hotkey] \
	-compound left -image document-save,16 -command [list file_save .document]
.contextmenu add command -label [mc menu_file_save_as] -accelerator [mc menu_file_save_as_hotkey] \
	-compound left -image document-save-as,16 -command [list file_save_as .document]
.contextmenu add command -label [mc menu_file_new] \
	-compound left -image document-new,16 -command [list file_new .document]

bind . <Control-o> [list file_open .document]
bind . <Control-s> [list file_save .document]
bind . <Control-S> [list file_save_as .document]

proc show_properties_editor {window text} {
	if {![winfo exists $window]} {
		richtext::properties_editor_simple .props .document
	} else {
		# TODO: test on window managers other than xfwm4
		raise .props
		focus [focus -lastfor .props]
	}
}


# Tag operations: see also tag-menus.tcl

proc addtag_menu_refresh {menu text} {
	$menu delete 0 end
	richtext::addtag_menu_populate $menu $text
}

proc removetag_menu_refresh {menu text} {
	$menu delete 0 end
	richtext::removetag_menu_populate $menu $text
}


# File operations

variable filepath_for
set filepath_for(.document) {}

proc set_window_title {path} {
	if {$path eq {}} {
		wm title . [mc minimal_richtext_editor_window_title]
	} elseif {[string length $path] <= 64} {
		wm title . $path
	} else {
		wm title . [file tail $path]
	}
}
set_window_title $filepath_for(.document)

proc file_open {w} {
	variable filepath_for

	if {$filepath_for($w) eq {}} {
		set initialspec {}
	} else {
		set initialspec [list -initialdir [file dirname $filepath_for($w)] \
		                      -initialfile [file tail $filepath_for($w)]]
	}
	set path [tk_getOpenFile {*}$initialspec -defaultextension .tkt \
		-filetypes [list [list [mc filetype_tktext] {.tkt .tktext}] \
		            [list [mc filetype_all] *]] ]
	if {$path eq {}} return

	# May fail, then the standard Tk error dialog will show:
	richtext::load_widget_from_file $w $path
	set filepath_for($w) $path
	set_window_title $path
}

proc file_save {w} {
	variable filepath_for

	if {$filepath_for($w) eq {}} {
		file_save_as $w
		return
	}

	# May fail, then the standard Tk error dialog will show:
	set chan [open $filepath_for($w) w]
	richtext::save_document_to_channel .document $chan
	close $chan
}

proc file_save_as {w} {
	variable filepath_for

	set initialspec [list -initialdir [file dirname $filepath_for($w)] \
	                      -initialfile [file tail $filepath_for($w)]]
	set path [tk_getSaveFile {*}$initialspec -defaultextension .tkt \
		-filetypes [list [list [mc filetype_tktext] {.tkt .tktext}] \
		            [list [mc filetype_all] *]] ]
	if {$path eq {}} return

	# May fail, then the standard Tk error dialog will show:
	set chan [open $path w]
	richtext::save_document_to_channel .document $chan
	close $chan

	set filepath_for($w) $path
	set_window_title $path
}

proc file_new {w} {
	variable filepath_for

	richtext::reset_widget $w
	set filepath_for($w) {}
	set_window_title {}
}
