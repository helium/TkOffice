#!/usr/bin/tclsh8.5
# Copyright 2020 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc richtext {param value content} {
	puts "Richtext $param $value START"
	puts <<<$content>>>
	eval $content
	puts "Richtext $param $value STOP"
}
proc text {content} {
	puts "Text <$content>"
}

set script ""
append script [list text Hallo]
append script \n
append script [list richtext -width 80]
append script " {"
append script \n\t
append script [list richtext -height 2]
append script " {"
append script \n\t\t
append script [list text \}]
append script \n\t\t
append script [list text "look, Ma! a \{ list \] with `\" weird characters \} in it"]
append script \n\t\t
append script [list text "look, Ma! a \{\{ list with `\" weird characters \} in it"]
append script \n\t\}\n
append script "}\n"
puts "script = <$script> evaluating..."
eval $script
