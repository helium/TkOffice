.t insert end {Weird " char[act[er$ are ] no problem. }
.t insert end "It is only about braces and backslashes.
"
.t insert end "{ The list operator resorts to backslashes here {}
"
.t insert end "{ Braces are kept if they are balanced {}}
"
.t insert end {My sound program is C:\Windows\System32\SNDREC32.EXE}
.t insert end { - this is the standard way how backslashes appear.
}
.t insert end "Backslashes \\ together with unbalanced {{ braces }!"
.t insert end "

"
.t insert end "Some nice Tcl script, balanced as a whole, but each line"
.t insert end " contains unbalanced braces on itself:

"
.t insert end {proc putargs {$args} {
	foreach arg $args {
		puts "argument [incr i] = '$arg'"
	}
}
}