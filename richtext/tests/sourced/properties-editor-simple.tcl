# Copyright 2020-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

package require msgcat


msgcat::mcload [file join [file dirname [info script]] msgs]

namespace eval richtext {

::msgcat::mcmset {} {
	property_editor_window_title {Style Properties}

	entire_text_label {Entire text:}
	entire_text_accel_key <Alt-e>
	entire_text_accel_underline 0

	tag_name_heading {Tag}
	tag_name_heading_width 100
	tag_properties_heading {Properties}
	tag_properties_heading_width 200

	tag_name_label {Tag:}
	tag_name_accel_key <Alt-t>
	tag_name_accel_underline 0
	tag_properties_label {Properties:}
	tag_properties_accel_key <Alt-r>
	tag_properties_accel_underline 1

	property_help_window_title {Help: Style Properties}

	error_ill_formatted_options {Error: ill-formatted options list}
	error_bad_option {Error: bad option "%s"}
	error_from_tk {Error: %s}
}

::msgcat::mcmset de {
	property_editor_window_title {Stil-Eigenschaften}

	entire_text_label {Ganzer Text:}
	entire_text_accel_key <Alt-g>
	entire_text_accel_underline 0

	tag_name_heading {Tag}
	tag_properties_heading {Eigenschaften}

	tag_name_label {Tag:}
	tag_name_accel_key <Alt-t>
	tag_name_accel_underline 0
	tag_properties_label {Eigensch.:}
	tag_properties_accel_key <Alt-e>
	tag_properties_accel_underline 0

	property_help_window_title {Hilfe: Stil-Eigenschaften}

	error_ill_formatted_options {Fehler: Ungültige Optionsliste}
	error_bad_option {Fehler: Ungültige Option "%s"}
	error_from_tk {Fehler: %s}
}

::msgcat::mcset {} property_help_filepath \
	[file join [file dirname [info script]] help properties-editor.ROOT.tkt]

::msgcat::mcset de property_help_filepath \
	[file join [file dirname [info script]] help properties-editor.de.tkt]


init_namespace

variable associated_richtext

icons::load  view-refresh,16  up,16  down,16  list-add,16  list-remove,16  help,16
ttk_configure_styles

# Creates a properties editor toplevel with a given window path
# $toplevel (must not exist) for the text widget $textwidget.
proc properties_editor_simple {toplevel textwidget} {
	variable associated_richtext

	ttk_toplevel $toplevel
	make_utility_window $toplevel [winfo toplevel $textwidget]
	focus $toplevel
	set associated_richtext($toplevel) $textwidget
	wm title $toplevel [mc property_editor_window_title]

	grid [ttk::label $toplevel.widget_config_title -text [mc entire_text_label]] \
		-row 0 -column 0 -columnspan 2 -sticky nw -padx {5 2} -pady {6 0}
	grid [ttk_text $toplevel.widget_config -width 0 -height 4 -wrap word] \
		-row 0 -column 2 -sticky nesw -padx {0 5} -pady {6 0}
	ttk_text_scrollbar $toplevel.widget_config on
	bind $toplevel.widget_config <Tab> {focus [tk_focusNext %W]; break}

	set buttonf $toplevel.button_frame
	ttk::frame $buttonf
	grid [ttk::button $buttonf.refresh -image view-refresh,16 -default disabled]
	grid [ttk::button $buttonf.add -image list-add,16 -default disabled] -pady {16 0}
	grid [ttk::button $buttonf.remove -image list-remove,16 -default disabled]
	grid [ttk::button $buttonf.up -image up,16 -default disabled] -pady {16 0}
	grid [ttk::button $buttonf.down -image down,16 -default disabled]
	grid [ttk::button $buttonf.help -image help,16 -default disabled] -pady {16 0}
	grid anchor $buttonf center
	grid $buttonf -row 1 -column 0 -sticky nesw -padx {5 0} -pady 5

	set table $toplevel.tag_table
	ttk_treeview $table -show headings -selectmode browse -columns {Name Config}
	$table column Name -minwidth 40 -width [mc tag_name_heading_width] -stretch no
	$table column Config -minwidth 40 -width [mc tag_properties_heading_width] -stretch yes
	$table heading Name -text [mc tag_name_heading] -anchor w
	$table heading Config -text [mc tag_properties_heading] -anchor w

	grid $table -row 1 -column 1 -columnspan 2 -sticky nesw -padx 5 -pady 5

	grid [ttk::label $toplevel.tag_name_title -text [mc tag_name_label]] \
		-row 2 -column 0 -columnspan 2 -sticky nw -padx {5 2} -pady 0
	grid [ttk::entry $toplevel.tag_name -font TkFixedFont] \
		-row 2 -column 2 -sticky we -padx {0 5} -pady 0

	grid [ttk::label $toplevel.tag_props_title -text [mc tag_properties_label]] \
		-row 3 -column 0 -columnspan 2 -sticky nw -padx {5 2} -pady 5
	grid [ttk_text $toplevel.tag_properties -width 0 -height 4 -wrap word] \
		-row 3 -column 2 -sticky nesw -padx {0 5} -pady 5
	ttk_text_scrollbar $toplevel.tag_properties on
	bind $toplevel.tag_properties <Tab> {focus [tk_focusNext %W]; break}

	grid columnconfigure $toplevel 2 -weight 1
	grid rowconfigure $toplevel 1 -weight 1

	update idletasks
	ttk_treeview_place_scrollbar [ttk::scrollbar $table.scrollbar -orient vertical]
	wm minsize $toplevel [winfo width $toplevel] [winfo height $toplevel]

	# Alt-key navigation
	# TODO: Macintosh has a different key than Alt_L for this, if at all!
	bind $toplevel <KeyPress-Alt_L> [list richtext::properties_editor_simple_accelkey $toplevel]
	bind $toplevel <KeyRelease-Alt_L> [list richtext::properties_editor_simple_accelkey_off $toplevel]
	bind $toplevel [mc entire_text_accel_key] [list focus $toplevel.widget_config]
	bind $toplevel [mc tag_name_accel_key] [list focus $toplevel.tag_name]
	bind $toplevel [mc tag_properties_accel_key] [list focus $toplevel.tag_properties]

	# Property editor behaviour

	bind $textwidget <<PropertyRefresh>> [list richtext::properties_editor_refresh $toplevel]
	bind $toplevel <Destroy> [list richtext::properties_editor_destroy $toplevel]
	properties_editor_refresh $toplevel

	set script [list richtext::properties_editor_textwidget_config $toplevel]
	bind $toplevel.widget_config <FocusOut> +$script
	append script "\nbreak"
	bind $toplevel.widget_config <Return> $script
	bind $toplevel.widget_config <KP_Enter> $script

	bind $toplevel.tag_table <<TreeviewSelect>> [list richtext::properties_editor_select_tag $toplevel]
	bind $toplevel.tag_table <ButtonPress> [list focus %W]

	set script [list richtext::properties_editor_tag_rename $toplevel]
	bind $toplevel.tag_name <FocusOut> $script
	bind $toplevel.tag_name <Return> $script
	bind $toplevel.tag_name <KP_Enter> $script

	set script [list richtext::properties_editor_tag_reconfigure $toplevel]
	bind $toplevel.tag_properties <FocusOut> +$script
	append script "\nbreak"
	bind $toplevel.tag_properties <Return> $script
	bind $toplevel.tag_properties <KP_Enter> $script

	$toplevel.button_frame.refresh configure -command [list richtext::properties_editor_refresh $toplevel]
	$toplevel.button_frame.add configure -command [list richtext::properties_editor_tag_add $toplevel]
	$toplevel.button_frame.remove configure -command [list richtext::properties_editor_tag_delete $toplevel]
	$toplevel.button_frame.up configure -command [list richtext::properties_editor_tag_lower $toplevel]
	$toplevel.button_frame.down configure -command [list richtext::properties_editor_tag_raise $toplevel]
	$toplevel.button_frame.help configure -command [list richtext::properties_editor_help $toplevel]
	bind $toplevel.tag_table <Control-Up> [list richtext::properties_editor_tag_lower $toplevel]\nbreak
	bind $toplevel.tag_table <Control-Down> [list richtext::properties_editor_tag_raise $toplevel]\nbreak

	bind $toplevel <Escape> [list focus $textwidget]

	return $toplevel
}

proc properties_editor_simple_reassociate {toplevel textwidget} {
	variable associated_richtext

	if {$associated_richtext($toplevel) eq $textwidget} return
	set associated_richtext($toplevel) $textwidget
	bind $toplevel <Escape> [list focus $textwidget]
	make_utility_window $toplevel [winfo toplevel $textwidget]
	properties_editor_refresh $toplevel
}

proc properties_editor_simple_accelkey {toplevel} {
	$toplevel.widget_config_title configure -underline [mc entire_text_accel_underline]
	$toplevel.tag_name_title configure -underline [mc tag_name_accel_underline]
	$toplevel.tag_props_title configure -underline [mc tag_properties_accel_underline]
}

proc properties_editor_simple_accelkey_off {toplevel} {
	$toplevel.widget_config_title configure -underline -1
	$toplevel.tag_name_title configure -underline -1
	$toplevel.tag_props_title configure -underline -1
}

proc properties_editor_destroy {toplevel} {
	variable associated_richtext

	set textwidget $associated_richtext($toplevel)
	bind $textwidget <<PropertyRefresh>> {}
	bind $toplevel <Destroy> {}
	unset associated_richtext($toplevel)
}

proc properties_editor_refresh {w} {
	variable associated_richtext
	set text $associated_richtext($w)

	# 1. Config options of whole widget
	set config [config_of_widget $text]
	$w.widget_config delete 1.0 end
	$w.widget_config insert end $config
	$w.widget_config configure -background [lindex [$w.widget_config configure -background] 3]

	# 2. Tag attributes
	$w.tag_table delete [$w.tag_table children {}]
	foreach tag [$text tag names] {
		if {$tag eq "sel"} continue
		set config [config_of_tag $text $tag]
		$w.tag_table insert {} end -values [list $tag $config]
	}
	# The following is tested to work even if [... children ...] returns empty list.
	set first_item [lindex [$w.tag_table children {}] 0]
	$w.tag_table selection set $first_item
	$w.tag_table see $first_item
}

proc properties_editor_textwidget_config {w} {
	variable allowed_config
	variable associated_richtext
	variable config_of_widget

	set config [$w.widget_config get 1.0 end-1c]
	if {!([string is list $config] && (([llength $config] & 1) == 0))} {
		$w.widget_config configure -background #fcc
		tk_messageBox -icon error -message [mc error_ill_formatted_options]
		return
	}
	foreach {option value} $config {
		if {$option ni $allowed_config} {
			$w.widget_config configure -background #fcc
			tk_messageBox -icon error -message [mc error_bad_option $option]
			return
		}
	}
	set text $associated_richtext($w)

	# Reset widget options to defaults
	configure_default $text

	# Apply new configuration
	if {[catch {$text configure {*}$config} result]} {
		$w.widget_config configure -background #fcc
		tk_messageBox -icon error -message [mc error_from_tk $result]
	} else {
		$w.widget_config configure -background [lindex [$w.widget_config configure -background] 3]
		set config_of_widget($text) $config
	}
}

proc properties_editor_select_tag {w} {
	set selected_item [$w.tag_table selection]
	$w.tag_table see $selected_item
	lassign [$w.tag_table item $selected_item -values] tag_name tag_properties
	$w.tag_name delete 0 end
	$w.tag_name insert end $tag_name
	$w.tag_name configure -style TEntry  ;# remove possible red colour from earlier errors
	$w.tag_properties delete 1.0 end
	$w.tag_properties insert end $tag_properties
	$w.tag_properties configure -background [lindex [$w.tag_properties configure -background] 3]

	if {[$w.tag_table selection] ne {}} {
		$w.tag_name configure -state normal
		$w.tag_properties configure -state normal
	} else {
		$w.tag_name configure -state disabled
		$w.tag_properties configure -state disabled
	}
}

proc properties_editor_tag_rename {w} {
	variable associated_richtext
	set text $associated_richtext($w)

	set selected_item $::ttk_fixes::selection_of_treeview($w.tag_table)
	if {$selected_item eq {}} return
	lassign [$w.tag_table item $selected_item -values] old_tag_name tag_config
	set new_tag_name [$w.tag_name get]
	if {$new_tag_name eq $old_tag_name} return
	if {$new_tag_name in [$text tag names]} {
		# Can't rename to an already existing tag, must delete that first
		$w.tag_name configure -style red.TEntry
		return
	}
	$w.tag_name configure -style TEntry

	tag_rename $text $old_tag_name $new_tag_name

	$w.tag_table item $selected_item -values [list $new_tag_name $tag_config]
}

proc properties_editor_tag_reconfigure {w} {
	variable associated_richtext
	set text $associated_richtext($w)

	set selected_item $::ttk_fixes::selection_of_treeview($w.tag_table)
	if {$selected_item eq {}} return
	lassign [$w.tag_table item $selected_item -values] tag_name
	set config [$w.tag_properties get 1.0 end-1c]

	if {!([string is list $config] && (([llength $config] & 1) == 0))} {
		$w.tag_properties configure -background #fcc
		tk_messageBox -icon error -message [mc error_ill_formatted_options]
		return
	}
	if {[catch {tag_reconfigure $text $tag_name {*}$config} result]} {
		$w.tag_properties configure -background #fcc
		tk_messageBox -icon error -message [mc error_from_tk $result]
		return
	}
	$w.tag_properties configure -background [lindex [$w.tag_properties configure -background] 3]

	$w.tag_table item $selected_item -values [list $tag_name $config]
}

proc properties_editor_tag_add {w} {
	variable associated_richtext
	set text $associated_richtext($w)

	# Find a tag name in the form of "tag#42"
	set tags [$text tag names]
	while {[set new_tag "tag#[incr i]"] in $tags} {}
	$text tag configure $new_tag

	set selected_item $::ttk_fixes::selection_of_treeview($w.tag_table)
	if {$selected_item eq {}} {
		set new_pos 0
		$text tag raise sel  ;# makes sure the selection background colour is visible
	} else {
		set new_pos [$w.tag_table index $selected_item]
		incr new_pos

		# Adjust tag priority in text widget
		lassign [$w.tag_table item $selected_item -values] selected_tag_name
		$text tag raise $new_tag $selected_tag_name
	}
	set new_item [$w.tag_table insert {} $new_pos -values [list $new_tag {}]]
	$w.tag_table selection set {}  ;# makes sure the tag gets selected if there have been no tags in the list
	$w.tag_table selection set $new_item
}

proc properties_editor_tag_delete {w} {
	variable associated_richtext
	set text $associated_richtext($w)

	set selected_item $::ttk_fixes::selection_of_treeview($w.tag_table)
	if {$selected_item eq {}} return
	lassign [$w.tag_table item $selected_item -values] tag_name

	$text tag delete $tag_name

	set sel_item [$w.tag_table next $selected_item]
	if {$sel_item eq {}} {
		set sel_item [$w.tag_table prev $selected_item]
	}
	$w.tag_table delete $selected_item
	$w.tag_table selection set $sel_item  ;# ok even if empty list
}

proc properties_editor_tag_lower {w} {
	variable associated_richtext
	set text $associated_richtext($w)

	set selected_item $::ttk_fixes::selection_of_treeview($w.tag_table)
	set prev_item [$w.tag_table prev $selected_item]
	if {$prev_item eq {}} return

	lassign [$w.tag_table item $selected_item -values] tag_name
	lassign [$w.tag_table item $prev_item -values] prev_tag_name
	$text tag lower $tag_name $prev_tag_name

	set selected_item_pos [$w.tag_table index $selected_item]
	$w.tag_table move $selected_item {} [expr {$selected_item_pos - 1}]
}

proc properties_editor_tag_raise {w} {
	variable associated_richtext
	set text $associated_richtext($w)

	set selected_item $::ttk_fixes::selection_of_treeview($w.tag_table)
	set next_item [$w.tag_table next $selected_item]
	if {$next_item eq {}} return

	lassign [$w.tag_table item $selected_item -values] tag_name
	lassign [$w.tag_table item $next_item -values] next_tag_name
	$text tag raise $tag_name $next_tag_name

	set selected_item_pos [$w.tag_table index $selected_item]
	$w.tag_table move $selected_item {} [expr {$selected_item_pos + 1}]
}

proc properties_editor_help {w} {
	help_window $w [mc property_help_window_title] [mc property_help_filepath]
}

}
