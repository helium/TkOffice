#!/usr/bin/wish8.5
# Copyright 2020-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Relative path to tclIndex - needs to be updated (add/remove some ..)
#  whenever this script is moved into a different hierarchy level
set dir [file join [file dirname [info script]] .. ..]
source [file join $dir tclIndex]
source [file join [file dirname [info script]] sourced properties-editor-simple.tcl]

## Set up the GUI translations
package require msgcat
namespace import ::msgcat::mc
foreach namespace {common} {
	msgcat::mcload [file join $dir $namespace msgs]
}

# Build up GUI

ttk_useTheme clam

pack [text .t]
.t configure -font {sans-serif -12} -spacing1 3 -spacing3 3 -padx 20 -pady 10
.t tag configure blue -foreground blue
.t tag configure yellow -background #ff0
.t tag configure orange -background #f80
.t insert end {blue text} blue
.t insert end { }
.t insert end {yellow } yellow
.t insert end both {yellow orange}
.t insert end { orange} orange

richtext::properties_editor_simple .prop .t
