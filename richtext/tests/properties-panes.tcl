#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Relative path to tclIndex - needs to be updated (add/remove some ..)
#  whenever this script is moved into a different hierarchy level
set dir [file join [file dirname [info script]] .. ..]
source [file join $dir tclIndex]

# Set up the GUI translations
package require msgcat
namespace import ::msgcat::mc
foreach namespace {common menus richtext} {
	msgcat::mcload [file join $dir $namespace msgs]
}

# Build up GUI

wm state . withdrawn
ttk_useTheme clam

pack [ttk::frame .mode_f]
pack [ttk::label .mode_l -text {Mode: }] -in .mode_f -side left
pack [ttk::radiobutton .mode_tag -text {Tag} -variable mode -value tag -command update_mode] \
	-in .mode_f -side left
pack [ttk::radiobutton .mode_widget -text {Widget} -variable mode -value widget -command update_mode] \
	-in .mode_f -side left
pack [richtext::properties_pane_colors .fc] -fill both -expand true -padx 2 -pady 2
pack [richtext::properties_pane_font .ff] -fill both -expand true -padx 2 -pady 2
pack [richtext::properties_pane_horizontal .fh] -fill both -expand true -padx 2 -pady 2
pack [richtext::properties_pane_vertical .fv] -fill both -expand true -padx 2 -pady 2

proc update_mode {} {
	global mode old_mode
	if {$mode ne $old_mode} {
		richtext::properties_pane_colors_mode .fc $mode
		richtext::properties_pane_font_mode .ff $mode
		richtext::properties_pane_horizontal_mode .fh $mode
		richtext::properties_pane_vertical_mode .fv $mode
		set old_mode $mode
		update idletasks
		wm minsize . [winfo reqwidth .] [winfo reqheight .]
	}
}
set mode widget
set old_mode ""
update_mode

update idletasks
wm state . normal
