#!/usr/bin/wish8.5
# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

ttk::style theme use clam
. configure -bg #dcdad5

grid columnconfigure . 0 -weight 1
grid rowconfigure . 0 -weight 1

grid [ttk::panedwindow .pw -orient horizontal] -sticky nesw
.pw add [ttk::frame .pw.listframe]
.pw add [ttk::frame .pw.textframe]
foreach frame {.pw.listframe .pw.textframe} {
	grid columnconfigure $frame 0 -weight 1
	grid rowconfigure $frame 0 -weight 1
}

listbox .types -listvariable Types -activestyle none -selectmode single \
	-exportselection 0
grid .types -in .pw.listframe -sticky nesw
grid [text .content -state disabled] -in .pw.textframe -sticky nesw
set Content {}

proc update_content {} {
	global Content
	set selection [.types curselection]
	if {$selection eq {}} {
		set new_content {}
	} else {
		set new_content [clipboard get -type [.types get $selection]]
	}
	if {$Content eq $new_content} return
	set Content $new_content
	.content configure -state normal
	.content delete 1.0 end
	.content insert end $Content
	.content configure -state disabled
}
bind .types <<ListboxSelect>> update_content

proc update_types {} {
	global Types
	if {[catch {set Types [clipboard get -type TARGETS]}]} {
		set Types [list]
	}
	update_content
	after 500 update_types
}
update_types
