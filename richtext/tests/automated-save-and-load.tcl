#!/usr/bin/tclsh8.5
# Copyright 2020-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

package require Tk

# Relative path to tclIndex - needs to be updated (add/remove some ..)
#  whenever this script is moved into a different hierarchy level
set dir [file join [file dirname [info script]] .. ..]
source [file join $dir tclIndex]

text .t  ;# do not display, only use it as a "data structure"

namespace eval stringchan {
	proc initialize {channelId mode} {
		global stringchan_text
		if {$mode ne "write"} {
			error "bad mode \"$mode\": must be write"
		}
		set stringchan_text {}
		return [list initialize finalize watch write]
	}
	proc finalize {channelId} {}
	proc watch {channelId eventspec} {}

	proc write {channelId data_bytes} {
		global stringchan_text
		append stringchan_text [encoding convertfrom utf-8 $data_bytes]
		return [string length $data_bytes]
	}

	namespace export initialize finalize watch write
	namespace ensemble create
}

proc apply_tcl {filepath} {
	richtext::reset_widget .t
	unset -nocomplain richtext::config_of_widget(.t)

	set interp [interp create -safe]
	$interp alias .t .t
	$interp invokehidden source $filepath
	interp delete $interp
}


set testsdir [file join [file dirname [info script]] save-and-load-examples]

foreach tcl_file [glob [file join $testsdir *.tcl]] {
	puts -nonewline "Testing [file tail $tcl_file]:  "
	flush stdout
	set tkt_file [string replace $tcl_file end-3 end .tkt]

	# 1. .tcl -> .tkt conversion, compare with actual .tkt file
	apply_tcl $tcl_file

	set chan [chan create write stringchan]
	richtext::save_document_to_channel .t $chan
	close $chan

	set chan [open $tkt_file r]
	set tkt_data [read $chan]
	close $chan

	if {$stringchan_text eq $tkt_data} {
		puts -nonewline "tcl>tkt OK  "
	} else {
		puts -nonewline "tcl>tkt \x1b\[1;31mFAIL\x1b\[0m  "
	}

	# 2. .tkt -> text -> .tkt roundtrip
	richtext::load_widget_from_file .t $tkt_file

	set chan [chan create write stringchan]
	richtext::save_document_to_channel .t $chan
	close $chan

	if {$stringchan_text eq $tkt_data} {
		puts "tkt>tkt OK"
	} else {
		puts "tkt>tkt \x1b\[1;31mFAIL\x1b\[0m"
	}
}

exit 0
