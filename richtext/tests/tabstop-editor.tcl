#!/usr/bin/wish8.5
# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

# Relative path to tclIndex - needs to be updated (add/remove some ..)
#  whenever this script is moved into a different hierarchy level
set dir [file join [file dirname [info script]] .. ..]
source [file join $dir tclIndex]

# Set up the GUI translations
package require msgcat
namespace import ::msgcat::mc
foreach namespace {common menus richtext} {
	msgcat::mcload [file join $dir $namespace msgs]
}

# Build up GUI

wm state . withdrawn
ttk_useTheme clam

grid [make_entry .tabspec -width 40 -font TkFixedFont] - \
	-padx 3 -pady 3
grid [ttk::button .set -text Set] [ttk::button .get -text Get] \
	-padx 3 -pady {0 3}
grid [richtext::tabstop_editor_create .ted] - \
	-padx 3 -pady {0 3}

.set configure -command set_tabs_from_entry
proc set_tabs_from_entry {} {
	global tabs_at_last_event
	set tabspec [.tabspec get]
	richtext::tabstop_editor_set .ted $tabspec
	puts "Set tabs from entry: <$tabspec>"
	set tabs_at_last_event $tabspec
}
bind .tabspec <Return> {.set state pressed; .set invoke}
bind .tabspec <KP_Enter> {.set state pressed; .set invoke}
.set invoke

.get configure -command {entry_set .tabspec [richtext::tabstop_editor_get .ted]}
bind .tabspec <Shift-Return> {.get state pressed; .get invoke}
bind .tabspec <Shift-KP_Enter> {.get state pressed; .get invoke}

bind .tabspec <KeyRelease-Return> {.set state !pressed; .get state !pressed}
bind .tabspec <KeyRelease-KP_Enter> {.set state !pressed; .get state !pressed}

toplevel .playground
pack [text .playground.text -font TkDefaultFont] -fill both -expand true
.playground.text insert end "1\t2\t3\t4\t5\n11\t22\t33\t44\t55"

update idletasks
wm state . normal

proc periodic_check {w} {
	global tabs_at_last_event
	set new_tabs [richtext::tabstop_editor_get $w]
	if {$new_tabs ne $tabs_at_last_event} {
		puts "!!! Periodic check: <$new_tabs>"
		set tabs_at_last_event $new_tabs
	}
	after 100 periodic_check $w
}
periodic_check .ted

proc tabs_edited {w} {
	global tabs_at_last_event
	set tabs_at_last_event [richtext::tabstop_editor_get $w]
	puts "<<TabsEdited>>: <$tabs_at_last_event>"
	.playground.text configure -tabs $tabs_at_last_event
}
bind .ted <<TabsEdited>> {tabs_edited %W}
