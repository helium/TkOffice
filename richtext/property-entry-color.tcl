# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

proc color_entry_create {frame property args} {
	if {[llength $args]&1 != 0} {
		error "number of extra arguments must be even, have [llength $args]"
	}
	set label {UNDEFINED LABEL}
	set pady 2
	foreach {key value} $args {
		if {$key in {-label -pady}} {
			set [string range $key 1 end] $value
		} else {
			error "unknown property \"$key\": must be -label or -pady"
		}
	}

	grid \
		[ttk::label $frame.${property}_l -text $label] \
		[ttk::label $frame.${property}_color -background {} -text {      } -relief raised] \
		[make_entry $frame.$property -width 10 -font TkFixedFont] \
		[ttk::button $frame.${property}_clear -text [mc cancel_button] -style Toolbutton] \
		-sticky nsw -pady $pady
	grid configure $frame.${property}_color $frame.$property -padx {0 3}
	grid configure $frame.${property}_color -sticky nse
	grid configure $frame.$property -sticky nesw

	bind $frame.${property}_color <ButtonPress> [list richtext::color_entry_button $frame $property]
	set binding [list richtext::color_entry_text $frame $property]
	bind $frame.$property <<Entered>> $binding
	bind $frame.$property <<FocusOutPrepare>> $binding
	bind $frame.$property <Escape> [list richtext::color_entry_escape $frame $property]
	bind $frame.$property <Destroy> {unset -nocomplain richtext::last_validation_attempt(%W)}
	$frame.${property}_clear configure -command [list richtext::color_entry_clear $frame $property]

	ttk_fixes::clam_corners $frame.${property}_color
}

proc color_entry_update {frame property options} {
	if {[dict exists $options -$property]} {
		set color [dict get $options -$property]
		$frame.${property}_color configure -background $color
		entry_set $frame.$property $color
		$frame.${property}_clear state !disabled
	} else {
		$frame.${property}_color configure -background {}
		entry_set $frame.$property {}
		$frame.${property}_clear state disabled
	}
}

proc color_entry_becomes_valid {entry} {
	variable last_validation_attempt

	$entry configure -style {}  ;# remove Invalid status
	unset -nocomplain last_validation_attempt($entry)
}

proc color_entry_button {frame property} {
	$frame.${property}_color instate disabled {return}

	if {[catch [list focus_intercept_click $frame.${property}_color]]} return
	click_outside_handler  ;# would otherwise not trigger until the modal dialog is closed

	set old_color [$frame.${property}_color cget -background]
	if {$old_color eq {}} {set old_color black}
	set new_color [tk_chooseColor -initialcolor $old_color -parent [winfo toplevel $frame]]
	if {$new_color eq {}} return

	tag_list_property_set $frame -$property $new_color

	$frame.${property}_color configure -background $new_color
	entry_set $frame.$property $new_color
	color_entry_becomes_valid $frame.$property

	$frame.${property}_clear state !disabled
}

proc color_entry_text {frame property} {
	variable last_validation_attempt

	set old_color [$frame.${property}_color cget -background]
	set new_color [$frame.$property get]
	if {$old_color eq {} && $new_color eq {}} {
		# Do not assign an empty color even to properties that support that.
		# As this procedure is also called when the entry widget loses focus,
		# it is usually not the user's intention to change the property.
		# Assignment of an empty color to -inactiveselectbackground and
		# -selectforeground requires first setting a non-empty color and then
		# deleting the entry text manually (not by pressing the Clear button).
		color_entry_becomes_valid $frame.$property
		return
	}
	set code [catch {$frame.${property}_color configure -background $new_color}]
	if {$code == 0} {
		color_entry_becomes_valid $frame.$property
		set code [catch {tag_list_property_set $frame -$property $new_color}]
		if {$code == 0} {
			$frame.${property}_clear state !disabled
		} elseif {$new_color eq {}} {
			# We get here when $property does not support assigning an empty color.
			# So we act as if the Clear button had been pressed.
			tag_list_property_set $frame -$property {} -unset
			$frame.${property}_clear state disabled
		} else {
			# This should never happen
			error "failed to apply valid color $new_color to richtext property $property"
		}
	} elseif {![info exists last_validation_attempt($frame.$property)]} {
		# Entry just turned invalid
		$frame.$property configure -style Invalid.TEntry
		focus $frame.$property  ;# work against <FocusOut>
		set last_validation_attempt($frame.$property) $new_color
	} elseif {$last_validation_attempt($frame.$property) eq $new_color} {
		# Entry was already invalid and has not changed, restore
		entry_set $frame.$property $old_color
		color_entry_becomes_valid $frame.$property
	} else {
		# Entry was already invalid but the user tried another value, keep Invalid status
		focus $frame.$property  ;# work against <FocusOut>
		set last_validation_attempt($frame.$property) $new_color
	}
}

proc color_entry_clear {frame property} {
	tag_list_property_set $frame -$property {} -unset

	$frame.${property}_color configure -background {}
	entry_set $frame.$property {}
	color_entry_becomes_valid $frame.$property
	$frame.${property}_clear state disabled
}

proc color_entry_escape {frame property} {
	variable last_validation_attempt

	set prev_color [$frame.${property}_color cget -background]
	if {[$frame.$property get] eq $prev_color} {
		event generate $frame.$property <<Escape>>
	} else {
		entry_set $frame.$property $prev_color
		color_entry_becomes_valid $frame.$property
	}
}

}
