# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

init_namespace

variable clipboard_temp_text


proc copy_selection {w} {
	if {[$w tag ranges sel] ne {}} {
		copy_to_clipboard $w sel.first sel.last
	}
}

proc cut_selection {w} {
	if {[$w tag ranges sel] ne {}} {
		copy_to_clipboard $w sel.first sel.last
		undo::undoable_edit_step $w Delete sel.first sel.last
	}
}

proc copy_to_clipboard {w index1 index2} {
	variable clipboard_temp_text {}

	clipboard clear

	# 0. Plain-text clipboard
	clipboard append [$w get $index1 $index2]

	# 1. Tag attributes
	set tags_written no
	foreach tag [tag_names_for_ranges $w $index1 $index2] {
		if {$tag eq "sel"} continue
		set config [config_of_tag $w $tag]
		clipboard append -type TkOfficeRichText [list tag $tag {*}$config]
		clipboard append -type TkOfficeRichText \n
		set tags_written yes
	}
	if {$tags_written} {
		# separate tags from text content
		clipboard append -type TkOfficeRichText \n
	}

	# 2. Text content
	foreach tag [$w tag names $index1] {
		if {$tag eq "sel"} continue
		clipboard append -type TkOfficeRichText [list tagon $tag]
		clipboard append -type TkOfficeRichText \n
	}
	$w dump -command ::richtext::_clipboard_dump \
		-text -tag $index1 $index2  ;# Later also: -image -window
	_clipboard_flush_text
}

proc _clipboard_dump {type value index} {
	variable clipboard_temp_text

	# Depending on the editing history, "$w dump" may break a line
	# into multiple parts. Here we take the extra effort to combine
	# those into one "text" per line (in the absence of tags).

	switch $type {
		text {
			append clipboard_temp_text $value
			if {[string index $clipboard_temp_text end] eq "\n"} {
				_clipboard_flush_text
			}
		}
		tagon {
			if {$value ne "sel"} {
				_clipboard_flush_text
				clipboard append -type TkOfficeRichText [list tagon $value]
				clipboard append -type TkOfficeRichText \n
			}
		}
		tagoff {
			if {$value ne "sel"} {
				_clipboard_flush_text
				clipboard append -type TkOfficeRichText [list tagoff $value]
				clipboard append -type TkOfficeRichText \n
			}
		}
	}
}

proc _clipboard_flush_text {} {
	variable clipboard_temp_text

	if {$clipboard_temp_text ne {}} {
		clipboard append -type TkOfficeRichText [list text $clipboard_temp_text]
		clipboard append -type TkOfficeRichText \n
		set clipboard_temp_text {}
	}
}

}
