# Copyright 2021-2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

proc create_editor {f} {
	# The argument $f should be a frame, ttk::frame or toplevel.

	foreach child {document xscroll yscroll tags properties properties_scroll} {
		set $child [fix_toplevel_child $f.$child]
	}

	grid [text $document -wrap word] \
		-row 0 -column 0 -sticky nesw
	grid [ttk::scrollbar $yscroll -orient vertical] \
		-row 0 -column 1 -sticky ns
	grid [ttk::scrollbar $xscroll -orient horizontal] \
		-row 1 -column 0 -sticky we
	$document configure -xscrollcommand [list $xscroll set] \
		-yscrollcommand [list $yscroll set]
	$xscroll configure -command [list $document xview]
	$yscroll configure -command [list $document yview]

	richtext::reset_widget $document

	grid [tag_list_create $tags -for $document] \
		-row 0 -column 2 -rowspan 2 -sticky nesw

	richtext::properties_frame_create $properties -for $document
	grid [make_scrolling_frame $properties_scroll $properties] \
		-row 0 -column 3 -rowspan 2 -sticky nesw
	scrolling_frame_width_mode $properties_scroll increase-only

	grid rowconfigure $f 0 -weight 1
	grid columnconfigure $f 0 -weight 1

	highlight_setup $document
	richtext::set_event_bindings $document
	tag_list_link_to_panel $tags $properties
	properties_frame_link_to_tag_list $properties $tags
}

proc hide_tags_and_properties {f} {
	set tags [fix_toplevel_child $f.tags]
	set properties_scroll [fix_toplevel_child $f.properties_scroll]

	grid forget $tags
	grid forget $properties_scroll
	tag_list_disable_refresh $tags
}

proc show_tags_and_properties {f} {
	set tags [fix_toplevel_child $f.tags]
	set properties_scroll [fix_toplevel_child $f.properties_scroll]

	grid $tags -row 0 -column 2 -rowspan 2 -sticky nesw
	grid $properties_scroll -row 0 -column 3 -rowspan 2 -sticky nesw
	tag_list_enable_refresh $tags
}

}
