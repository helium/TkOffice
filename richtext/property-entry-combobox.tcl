# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

proc combobox_entry_create {frame property args} {
	variable combobox_internal_values

	if {[llength $args]&1 != 0} {
		error "number of extra arguments must be even, have [llength $args]"
	}
	set label {UNDEFINED LABEL}
	set values {px cm mm in pt}
	set displayvalues {}
	set width 12
	set pady 2
	set gridmode normal
	foreach {key value} $args {
		if {$key in {-label -values -displayvalues -width -pady -gridmode}} {
			set [string range $key 1 end] $value
		} else {
			error "unknown property \"$key\": must be -label, -values, -displayvalues, -width, -pady, or -gridmode"
		}
	}

	ttk::label $frame.${property}_l -text $label
	ttk::combobox $frame.$property -state readonly -width $width
	ttk::button $frame.${property}_clear -text [mc cancel_button] -style Toolbutton

	# Different column-span configuration if this entry line is in the Colors pane
	if {$gridmode eq {colorpane}} {
		set line [list $frame.${property}_l - $frame.$property $frame.${property}_clear]
	} else {
		set line [list $frame.${property}_l $frame.$property - $frame.${property}_clear]
	}
	grid {*}$line -sticky nsw -pady $pady
	grid configure $frame.$property -padx {0 3} -sticky nesw

	if {$displayvalues eq {}} {
		$frame.$property configure -values $values
	} else {
		set combobox_internal_values($frame.$property) $values
		$frame.$property configure -values $displayvalues
		bind $frame.$property <Destroy> \
			[list unset richtext::combobox_internal_values($frame.$property)]
	}

	bind $frame.$property <<ComboboxSelected>> \
		[list richtext::combobox_entry_changed $frame $property]
	bind $frame.$property <ButtonPress-1> {focus_intercept_click %W}
	ttk::bindMouseWheel $frame.$property {focus_intercept_mousewheel %W}
	$frame.${property}_clear configure -command \
		[list richtext::combobox_entry_clear $frame $property]
}

proc combobox_entry_update {frame property options} {
	variable combobox_internal_values

	if {[dict exists $options -$property]} {
		set value [dict get $options -$property]
		if {[info exists combobox_internal_values($frame.$property)]} {
			set idx [lsearch -exact $combobox_internal_values($frame.$property) $value]
			set value [lindex [$frame.$property cget -values] $idx]
		}
		$frame.$property set $value
		$frame.${property}_clear state !disabled
	} else {
		$frame.$property set {}
		$frame.${property}_clear state disabled
	}
}

proc combobox_entry_changed {frame property} {
	variable combobox_internal_values

	if {[info exists combobox_internal_values($frame.$property)]} {
		set internal_values $combobox_internal_values($frame.$property)
		set new_value [lindex $internal_values [$frame.$property current]]
	} else {
		set new_value [$frame.$property get]
	}
	if {$new_value ne {}} {
		tag_list_property_set $frame -$property $new_value
		$frame.${property}_clear state !disabled
	} else {
		tag_list_property_set $frame -$property {} -unset
		$frame.${property}_clear state disabled
	}
}

proc combobox_entry_clear {frame property} {
	tag_list_property_set $frame -$property {} -unset
	$frame.$property set {}
	$frame.${property}_clear state disabled
}

}
