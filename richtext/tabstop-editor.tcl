# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

variable buttonpress_for_menu no

icons::load  tab-center,13  tab-numeric,13  tab-left,13  tab-right,13

proc tabstop_editor_create {w} {
	set wframe [ttk_text $w]
	$w configure -font TkDefaultFont -width 20 -padx 0 -pady 0 \
		-spacing1 2 -spacing3 2 -wrap none -insertwidth 1
	bindtag_replace $w Text TabstopEditor
	doubleclick_workaround::add_to_widget $w
	event generate $w <Configure> -when tail

	ttk_menu $w.typemenu
	fix_simple_popup_menu $w.typemenu
	$w.typemenu add command -image tab-left,13 -label [mc tab_type_left] -compound left \
		-command [list richtext::tabstop_editor_select_type $w left]
	$w.typemenu add command -image tab-right,13 -label [mc tab_type_right] -compound left \
		-command [list richtext::tabstop_editor_select_type $w right]
	$w.typemenu add command -image tab-center,13 -label [mc tab_type_center] -compound left \
		-command [list richtext::tabstop_editor_select_type $w center]
	$w.typemenu add command -image tab-numeric,13 -label [mc tab_type_numeric] -compound left \
		-command [list richtext::tabstop_editor_select_type $w numeric]
	$w tag bind typemenu <Enter> {%W configure -cursor arrow}
	$w tag bind typemenu <Leave> {%W configure -cursor xterm}
	$w tag bind typemenu <ButtonPress-1> {richtext::tabstop_editor_typemenu_open %W}

	ttk_menu $w.unitmenu
	fix_simple_popup_menu $w.unitmenu
	$w tag bind unitmenu <Enter> {%W configure -cursor {}}
	$w tag bind unitmenu <Leave> {%W configure -cursor xterm}
	$w tag bind unitmenu <ButtonPress-1> {richtext::tabstop_editor_unitmenu_open %W}

	$w tag configure highlight -background #ffa
	$w tag configure error -background #faa
	$w tag raise sel

	# Returns the containing frame, which is useful for geometry management.
	# To access the actual text widget, use the "w" argument supplied to this proc.
	return $wframe
}

bind TabstopEditor <Configure> {richtext::tabstop_editor_configure %W}
bind TabstopEditor <Motion> {richtext::tabstop_editor_arrow_redraw %W}
# The "event generate" part in this binding works around
# sporadic missing execution of the <ButtonPress-1> tag binding.
# This seems to relate to this <Enter> binding inserting text
# with the tag in question.
bind TabstopEditor <Enter> {richtext::tabstop_editor_arrow_redraw %W
	event generate %W <Motion> -x %x -y %y -rootx %X -rooty %Y -when head}
bind TabstopEditor <Leave> {richtext::tabstop_editor_leave %W %x %y}
bind TabstopEditor <Destroy> {richtext::tabstop_editor_destroy %W}

bind TabstopEditor <ButtonPress-1> {richtext::tabstop_editor_click %W %x %y}
bind TabstopEditor <<DoubleClick>> {richtext::tabstop_editor_doubleclick %W %x %y}
bind TabstopEditor <<FocusOutPrepare>> {richtext::tabstop_editor_focus_out_prepare %W}
# There is already an event binding from ttk_text to which the present binding must be added.
bind TabstopEditor <FocusOut> {richtext::tabstop_editor_focus_out %W}
bind TabstopEditor <<FocusOutDelayed>> {richtext::tabstop_editor_focus_out_delayed %W}
bind TabstopEditor <Return> {richtext::tabstop_editor_enter_key %W}
bind TabstopEditor <KP_Enter> {richtext::tabstop_editor_enter_key %W}
bind TabstopEditor <Escape> {richtext::tabstop_editor_escape %W}
bind TabstopEditor <BackSpace> {richtext::tabstop_editor_backspace %W}
bind TabstopEditor <Delete> {richtext::tabstop_editor_delete %W}
bind TabstopEditor <Left> {richtext::tabstop_editor_key_left %W}
bind TabstopEditor <Right> {richtext::tabstop_editor_key_right %W}
bind TabstopEditor <Home> {richtext::tabstop_editor_key_home %W}
bind TabstopEditor <End> {richtext::tabstop_editor_key_end %W}
bind TabstopEditor <Up> {richtext::tabstop_editor_key_up %W}
bind TabstopEditor <Down> {richtext::tabstop_editor_key_down %W}
bind TabstopEditor <KeyPress> {richtext::tabstop_editor_key %W %A}
# Ignore all Alt, Meta, and Control keypresses unless explicitly bound.
# Otherwise, if a widget binding for one of these is defined, the
# <KeyPress> class binding will also fire and insert the character,
# which is wrong.
bind TabstopEditor <Alt-KeyPress> {return}
bind TabstopEditor <Meta-KeyPress> {return}
bind TabstopEditor <Control-KeyPress> {return}
bind TabstopEditor <Mod4-KeyPress> {return}
if {[tk windowingsystem] eq "aqua"} {
	bind TabstopEditor <Command-KeyPress> {return}
}

bind TabstopEditor <B1-Motion> {richtext::tabstop_editor_mouse_drag %W %x %y}
bind TabstopEditor <ButtonRelease-1> {richtext::tabstop_editor_arrow_redraw %W}
bind TabstopEditor <Shift-ButtonPress-1> {richtext::tabstop_editor_shift_click %W %x %y}
bind TabstopEditor <Shift-Left> {richtext::tabstop_editor_shift_key_left %W}
bind TabstopEditor <Shift-Right> {richtext::tabstop_editor_shift_key_right %W}
bind TabstopEditor <Shift-Home> {richtext::tabstop_editor_shift_key_home %W}
bind TabstopEditor <Shift-End> {richtext::tabstop_editor_shift_key_end %W}

bind TabstopEditor <<TripleClick>> {richtext::tabstop_editor_tripleclick %W %x %y}
bind TabstopEditor <Shift-Up> {richtext::tabstop_editor_shift_key_up %W}
bind TabstopEditor <Shift-Down> {richtext::tabstop_editor_shift_key_down %W}
bind TabstopEditor <Control-ButtonPress-1> {richtext::tabstop_editor_control_click %W %x %y}
bind TabstopEditor <Control-Up> {richtext::tabstop_editor_control_key_up %W}
bind TabstopEditor <Control-Down> {richtext::tabstop_editor_control_key_down %W}
bind TabstopEditor <Control-space> {richtext::tabstop_editor_control_space %W}

bind TabstopEditor <<Copy>> {richtext::tabstop_editor_copy %W}
bind TabstopEditor <<Cut>> {richtext::tabstop_editor_cut %W}
bind TabstopEditor <<Paste>> {richtext::tabstop_editor_paste %W}

ttk::bindMouseWheel TabstopEditor {richtext::tabstop_editor_mousewheel %W %x %y}

proc tabstop_editor_configure {w} {
	set width [winfo width $w]
	if {$width <= 1} return
	set right_end [expr {
		$width - 2*([$w cget -padx] + [$w cget -highlightthickness] + [$w cget -border])
	}]
	set font [$w cget -font]
	set decimal_tab [expr {20 + [font measure $font 00000]}]
	$w configure -tabs [list 5 $decimal_tab numeric $right_end right]
}

proc tabstop_editor_destroy {w} {
	variable tabstop_editor_arrow_line
	variable focusout_for_menu
	variable saved_line_index
	variable saved_line_type
	variable saved_line_number
	variable saved_line_unit
	variable up_down_prev_pos
	variable up_down_origin
	variable last_validation_attempt

	unset -nocomplain tabstop_editor_arrow_line($w)
	unset -nocomplain focusout_for_menu($w)
	unset -nocomplain saved_line_index($w) saved_line_type($w) \
		saved_line_number($w) saved_line_unit($w)
	unset -nocomplain up_down_prev_pos($w) up_down_origin($w)
	unset -nocomplain last_validation_attempt($w)
}

# Loads the tabstop editor from a given "-tabs" string
proc tabstop_editor_set {w tabs} {
	if {$tabs eq {}} {
		tabstop_editor_make_unset $w
		return
	}
	tabstop_editor_prepare_normal $w

	set len [llength $tabs]
	for {set i 0} {$i < $len} {incr i} {
		set distance [lindex $tabs $i]
		set type [lindex $tabs $i+1]  ;# out-of-bounds indexing returns empty string
		if {$type ni {left right center numeric}} {
			set type left
		} else {
			incr i
		}
		lassign [screen_distance_parse $distance] distance_value distance_unit
		tabstop_editor_insert_tabstop $w end $type $distance_value $distance_unit
		$w insert end "\n"
	}
	tabstop_editor_update_height $w
}

proc tabstop_editor_make_unset {w} {
	$w delete 1.0 end
	tabstop_editor_arrow_remove $w
	$w insert end "\t[mc tabs_unset]"
	switch [ttk_currentTheme] {
		clam {set bg #dcdad5}
		classic - alt - default {set bg #d9d9d9}
	}
	$w configure -bg $bg
	tabstop_editor_update_height $w
}

proc tabstop_editor_prepare_normal {w} {
	$w delete 1.0 end
	tabstop_editor_arrow_remove $w
	$w configure -bg white
}

proc tabstop_editor_insert_tabstop {w mark type distance unit} {
	set distance [screen_distance_normalize_integer $distance $unit]
	$w insert $mark \t typemenu
	# "-name image" leads to auto-incrementing identifiers (image, image#1, ...)
	# that do not contain the comma of the icon name. Without "-name image",
	# identifiers containing a comma would be generated, which do not work as
	# text indices in Tk 8.5.13 (e.g. tclkit-gui-8513.exe).
	set image [$w image create $mark -image tab-$type,13 -name image]
	$w tag add typemenu $image
	$w insert $mark "\t$distance $unit"
}

proc tabstop_editor_update_height {w} {
	set lines [$w count -displaylines 1.0 end]
	if {$lines <= 10} {
		$w configure -height $lines
		ttk_text_scrollbar $w off
	} else {
		$w configure -height 10
		ttk_text_scrollbar $w on
	}
}

# Produces a "-tabs" string from the tabstops editor's content
proc tabstop_editor_get {w} {
	if {[$w cget -background] ne {white}} {
		return {}
	}

	set sort_table [tabstop_editor_get_sorting_table $w include_tab_being_created]
	# Sort based on the "pixels" entry (format: type number unit index pixels).
	set sort_table [lsort -real -index 4 $sort_table]

	set output [list]
	foreach row $sort_table {
		lassign $row type number unit  ;# index and pixels not needed
		set tk_pixels [screen_distance_compose $number $unit]
		lappend output $tk_pixels
		if {$type ne {left}} {
			lappend output $type
		}
	}
	if {$output eq {}} {
		set output { }
	}
	return $output
}

proc tabstop_editor_get_sorting_table {w {include_tab_being_created ""}} {
	variable saved_line_index
	variable saved_line_type
	variable saved_line_number
	variable saved_line_unit

	if {[$w cget -background] ne {white}} {
		return [list]
	}
	set sort_table [list]
	if {$include_tab_being_created ne ""} {
		# Used in tabstop_editor_get
		set end_idx end
	} else {
		# Used for tabstop_editor_insert_sorted (must exclude a tab being created)
		set end_idx {end-1c linestart}
	}
	foreach {key value index} [$w dump -image 1.0 $end_idx] {
		if {$key ne {image}} continue
		if {[info exists saved_line_index($w)]
		&& [$w compare $saved_line_index($w) == "$index linestart"]} {
			# Tab is being edited, use saved value
			set type $saved_line_type($w)
			set number $saved_line_number($w)
			set unit $saved_line_unit($w)
			set index $saved_line_index($w)
		} else {
			switch [$w image cget $index -image] {
				tab-left,13 {set type left}
				tab-right,13 {set type right}
				tab-center,13 {set type center}
				tab-numeric,13 {set type numeric}
			}
			set tabstop [tabstop_editor_get_text_line $w $index]
			lassign [tabstop_editor_split_number_unit $tabstop] number unit invalid
			if {$invalid || $number <= 0.0} {
				if {[$w compare $index > {end-1c linestart}]} {
					# Last entry (newly created tab), ignore if invalid
					break
				}
				error "inconsistency detected while getting tabstops from $w"
			}
			set index [$w index "$index linestart"]
		}
		set pixels [screen_distance_to_pixels $number $unit]
		lappend sort_table [list $type $number $unit $index $pixels]
	}
	return $sort_table
}

proc tabstop_editor_get_text_line {w index} {
	if {[$w tag ranges unitmenu] ne {}
	&& [$w compare unitmenu.first >= "$index linestart"]
	&& [$w compare unitmenu.first <= "$index lineend"]} {
		set last unitmenu.first
	} else {
		set last "$index lineend"
	}
	return [string trim [$w get "$index linestart" $last]]
}

proc tabstop_editor_split_number_unit {text {default_unit {}}} {
	lassign [concat {*}[split $text]] number unit
	if {![string is double $number]} {
		# Try to recover from missing space between number and unit (Example: "1.1cm")
		set unit [string range $text end-1 end]
		set number [string range $text 0 end-2]
	}
	# Remove trailing post-comma zeros
	if {[string match {*.*} $number]} {
		while {[string match {*0} $number] && ![string match {*.0} $number]} {
			set number [string range $number 0 end-1]
		}
	}
	# Remove leading zeros
	while {[string match {0?*} $number] && ![string match {0.*} $number]} {
		set number [string range $number 1 end]
	}
	if {$unit eq {}} {
		set unit $default_unit
	}
	# Validate
	set invalid [catch {screen_distance_compose $number $unit}]
	return [list $number $unit $invalid]
}

# Left-side menu: tab type (left/right/centered/numeric)
proc tabstop_editor_typemenu_open {w} {
	variable buttonpress_for_menu
	variable focusout_for_menu

	if {[tabstop_editor_focus_intercept $w]} return

	set focusout_for_menu($w) true

	if {[$w get current] eq "\t"} {
		$w mark set typeimage {current + 1 char}
	} else {
		$w mark set typeimage current
	}
	lassign [$w bbox typeimage] x y - h
	set rootx [winfo rootx $w]
	set rooty [winfo rooty $w]
	set buttonpress_for_menu yes
	tk_popup $w.typemenu [expr {$rootx+$x}] [expr {$rooty+$y+$h+[$w cget -spacing3]}]

	set image [$w image cget typeimage -image]
	set lastidx [$w.typemenu index end]
	for {set i 0} {$i <= $lastidx} {incr i} {
		if {[$w.typemenu entrycget $i -image] eq $image} {
			$w.typemenu activate $i
			break
		}
	}
}

proc tabstop_editor_select_type {w type} {
	$w image configure typeimage -image tab-$type,13
	event generate $w <<TabsEdited>> -when head

	tabstop_editor_arrow_redraw $w
}

# Right-side menu: measurement unit / unit conversion / tab removal
proc tabstop_editor_unitmenu_open {w} {
	variable buttonpress_for_menu
	variable saved_line_index
	variable focusout_for_menu

	if {[tabstop_editor_focus_intercept $w]} return

	set focusout_for_menu($w) true

	set current [$w index current]  ;# may be corrupted by tabstop_editor_reformat_line

	# Validate tab entry if currently edited
	if {"being_edited" in [$w tag names current]} {
		set prev_error_range [$w tag ranges error]
		set ret [tabstop_editor_reformat_line $w $current]
		tabstop_editor_arrow_redraw $w
		if {!$ret} {
			if {$prev_error_range eq {}} {
				return  ;# first try on invalid tab entry
			}
			if {[info exists saved_line_index($w)]} {
				tabstop_editor_restore_line $w
				$w tag remove error 1.0 end
				tabstop_editor_arrow_redraw $w
			} else {
				return  ;# a tab is being created, for which no restore information exists
			}
		}
	}

	$w.unitmenu delete 0 end
	$w.unitmenu add command -label [mc tab_remove] \
		-command [list richtext::tabstop_editor_delete_at $w $current]

	# Unit change entries
	$w.unitmenu add separator
	set line [tabstop_editor_get_text_line $w $current]
	lassign [tabstop_editor_split_number_unit $line] number unit_selected
	foreach unit {px cm mm in pt} {
		$w.unitmenu add command -label "$number $unit" \
			-command [list richtext::tabstop_editor_change_at $w $current $number $unit]
		if {$unit eq $unit_selected} {
			set active_menu_line [$w.unitmenu index end]
		}
	}

	# Unit conversion entries
	$w.unitmenu add separator
	set converted [screen_distance_unit_convert $number $unit_selected]
	foreach unit {px cm mm in pt} {
		if {[dict exists $converted $unit]} {
			set number_converted [dict get $converted $unit]
			$w.unitmenu add command -label "$number_converted $unit" \
				-command [list richtext::tabstop_editor_change_at $w $current $number_converted $unit]
		}
	}

	# Post the menu
	set menu_pos $current
	set range [$w tag nextrange unitmenu "$current linestart" "$current lineend"]
	if {$range ne {}} {
		set menu_pos [lindex $range 0]
	}
	lassign [$w bbox $menu_pos] x y - h
	set rootx [winfo rootx $w]
	set rooty [winfo rooty $w]
	set buttonpress_for_menu yes
	tk_popup $w.unitmenu [expr {$rootx+$x}] [expr {$rooty+$y+$h+[$w cget -spacing3]}]
	if {[info exists active_menu_line]} {
		$w.unitmenu activate $active_menu_line
	}
}

proc tabstop_editor_delete_at {w index} {
	variable saved_line_index
	variable saved_line_type
	variable saved_line_number
	variable saved_line_unit

	if {[info exists saved_line_index($w)]} {
		$w mark set tmp $saved_line_index($w)
	}
	set creating_tab [$w compare "$index lineend" == end-1c]
	set editing_tab [expr {"being_edited" in [$w tag names $index]}]
	if {!$creating_tab} {
		$w delete "$index linestart" "$index linestart + 1 line"
	} else {
		$w delete "$index linestart" "$index lineend"
	}
	if {[info exists saved_line_index($w)]} {
		set saved_line_index($w) [$w index tmp]
		$w mark unset tmp
	}
	tabstop_editor_update_height $w

	if {$editing_tab} {
		$w tag remove being_edited 1.0 end
		$w tag remove highlight 1.0 end
		$w tag remove error 1.0 end
		unset -nocomplain saved_line_index($w) saved_line_type($w) \
			saved_line_number($w) saved_line_unit($w)
	}

	event generate $w <<TabsEdited>> -when head
	tabstop_editor_arrow_redraw $w
}

proc tabstop_editor_change_at {w index number unit} {
	variable saved_line_index
	variable saved_line_number
	variable saved_line_unit

	set type [tabstop_editor_get_type $w $index]
	tabstop_editor_replace $w $index $type $number $unit
	$w tag remove error 1.0 end

	if {[info exists saved_line_index($w)]} {
		if {[$w compare $saved_line_index($w) == "$index linestart"]} {
			# Change saved values, so they are considered by [tabstop_editor_get]
			set saved_line_number($w) $number
			set saved_line_unit($w) $unit
		}
	}

	event generate $w <<TabsEdited>> -when head
	tabstop_editor_arrow_redraw $w
}

# Dynamic menu arrow at the right side of the line touched by the mouse pointer
proc tabstop_editor_arrow_redraw {w {-force {}}} {
	variable tabstop_editor_arrow_line

	if {[winfo containing {*}[winfo pointerxy $w]] ne $w} {
		# pointer not inside tab editor (usually from menu binding),
		# "current" mark would be invalid
		tabstop_editor_arrow_remove $w
		return
	}
	if {[tabstop_editor_has_multiline_selection $w]} {
		tabstop_editor_arrow_remove $w
		return
	}
	if {${-force} eq {-force}} {
		lassign [winfo pointerxy $w] x y
		$w mark set current @[expr {$x-[winfo rootx $w]}],[expr {$y-[winfo rooty $w]}]
		unset -nocomplain tabstop_editor_arrow_line($w)
	}

	set line [$w index {current linestart}]
	if {[info exists tabstop_editor_arrow_line($w)]} {
		if {$tabstop_editor_arrow_line($w) eq $line} return
	}
	tabstop_editor_arrow_remove $w

	if {[$w compare $line == "$line lineend"]} return  ;# last empty line does not get an arrow
	if {[$w cget -background] ne {white}} return  ;# editor is in "tabs unset" mode

	set tags [$w tag names $line]
	lremoveitem_nocomplain tags typemenu
	lappend tags arrowline
	set oldinsert [$w index insert]
	catch {$w mark gravity sel_anchor left}
	$w insert "$line lineend" \t $tags " \u25be " [concat $tags unitmenu]
	$w mark set insert $oldinsert
	set tabstop_editor_arrow_line($w) $line
}

proc tabstop_editor_arrow_remove {w} {
	variable tabstop_editor_arrow_line
	unset -nocomplain tabstop_editor_arrow_line($w)

	set ranges [$w tag ranges arrowline]
	if {$ranges ne {}} {
		$w delete {*}$ranges
	}
}

proc tabstop_editor_arrow_unset {w index} {
	variable tabstop_editor_arrow_line
	if {![info exists tabstop_editor_arrow_line($w)]} return
	if {[$w compare "$index linestart" == $tabstop_editor_arrow_line($w)]} {
		# Arrow was in the line that the caller deleted
		unset tabstop_editor_arrow_line($w)
		# Arrow will be redrawn on next <Motion> event
	}
}

proc tabstop_editor_leave {w x y} {
	if {[xy_outside_window $w $x $y]} {
		tabstop_editor_arrow_remove $w
	}
}

# Prerequisites for editing
proc tabstop_editor_replace {w index type distance unit} {
	set old_insert [$w index insert]
	$w mark set tmp $index
	set tags [$w tag names tmp]
	$w delete {tmp linestart} {tmp lineend}
	tabstop_editor_arrow_unset $w tmp

	tabstop_editor_insert_tabstop $w tmp $type $distance $unit

	if {"being_edited" in $tags} {
		$w tag add being_edited {tmp linestart} {tmp linestart + 1 line}
	}
	if {"highlight" in $tags} {
		$w tag add highlight {tmp linestart} {tmp linestart + 1 line}
	}

	$w mark unset tmp
	$w mark set insert $old_insert
}

proc tabstop_editor_save_line {w index} {
	variable saved_line_index
	variable saved_line_type
	variable saved_line_number
	variable saved_line_unit

	set text [tabstop_editor_get_text_line $w $index]
	lassign [tabstop_editor_split_number_unit $text] number unit

	set saved_line_index($w) [$w index "$index linestart"]
	set saved_line_type($w) [tabstop_editor_get_type $w $index]
	set saved_line_number($w) $number
	set saved_line_unit($w) $unit
}

proc tabstop_editor_get_saved_unit {w} {
	variable saved_line_unit

	if {[info exists saved_line_unit($w)]} {
		return $saved_line_unit($w)
	} else {
		return [tabstop_editor_get_default_unit $w]
	}
}

proc tabstop_editor_get_default_unit {w} {
	set occurrences [dict create px 0 pt 0 in 0 cm 0 mm 0]
	foreach {key value index} [$w dump -image 1.0 end-1c] {
		if {$key ne {image}} continue
		set tabstop [tabstop_editor_get_text_line $w $index]
		lassign [tabstop_editor_split_number_unit $tabstop] number unit invalid
		if {$invalid} continue
		dict incr occurrences $unit
	}
	set max_occurrences [tcl::mathfunc::max {*}[dict values $occurrences]]
	# Default unit in case no tabs have been defined
	if {$max_occurrences == 0} {return cm}
	# Select unit from existing tabs that occurs most frequently
	dict for {key value} $occurrences {
		if {$value eq $max_occurrences} {
			return $key
		}
	}
}

proc tabstop_editor_restore_line {w} {
	variable saved_line_index
	variable saved_line_type
	variable saved_line_number
	variable saved_line_unit

	if {![info exists saved_line_index($w)]} {return false}

	set type $saved_line_type($w)
	set distance $saved_line_number($w)
	set unit $saved_line_unit($w)
	tabstop_editor_replace $w $saved_line_index($w) $type $distance $unit

	return true
}

proc tabstop_editor_reformat_line {w index} {
	set index [$w index $index]
	set text [tabstop_editor_get_text_line $w $index]
	set default_unit [tabstop_editor_get_saved_unit $w]
	lassign [tabstop_editor_split_number_unit $text $default_unit] number unit invalid
	if {!$invalid} {
		set type [tabstop_editor_get_type $w $index]
		tabstop_editor_replace $w $index $type $number $unit
		return true
	} else {
		$w tag add error "$index linestart" "$index linestart + 1 line"
		return false
	}
}

proc tabstop_editor_get_type {w index} {
	foreach {key value idx} [$w dump -image "$index linestart" "$index lineend"] {
		set image [$w image cget $idx -image]
		return [string map {tab- {} ,13 {}} $image]
	}
}

proc tabstop_editor_begin {w index} {
	variable saved_line_index
	variable saved_line_type
	variable saved_line_number
	variable saved_line_unit

	if {[$w tag ranges being_edited] ne {}} {
		error "tabstop_editor_begin called while already editing a line"
	}
	if {[$w cget -background] ne {white}} {
		# Editor was in "tabs are unset" mode
		tabstop_editor_prepare_normal $w
		tabstop_editor_update_height $w
		event generate $w <<TabsEdited>> -when head
	}
	if {[$w compare "$index lineend" != end-1c]} {
		# Start editing an existing tab entry
		tabstop_editor_save_line $w $index
	} else {
		# Create a new tab with default settings (Left 0.0 cm)
		$w delete "$index linestart" "$index lineend"
		tabstop_editor_arrow_unset $w $index
		set saved_index [$w index $index]
		set default_unit [tabstop_editor_get_default_unit $w]  ;# cm if no tabs exist
		$w mark set tmp $index
		tabstop_editor_insert_tabstop $w tmp left 0.0 $default_unit
		$w mark unset tmp
		set index $saved_index

		set start [tabstop_editor_restrict_cursor $w "$index linestart"]
		set end [tabstop_editor_restrict_cursor $w "$index lineend"]
		$w tag remove sel 1.0 end
		$w tag add sel $start $end
		$w mark set sel_anchor $start
		$w mark set insert $end

		unset -nocomplain saved_line_index($w) saved_line_type($w) \
			saved_line_number($w) saved_line_unit($w)
	}
	$w tag add being_edited "$index linestart" "$index linestart + 1 line"
	$w tag add highlight "$index linestart" "$index linestart + 1 line"
}

proc tabstop_editor_finish {w {on_error fail_on_error}} {
# Possible values for on_error:
# - fail_on_error
# - restore_on_error
# - restore_on_old_error
# Return value: true if line editing was successful, false otherwise
	variable saved_line_index
	variable saved_line_type
	variable saved_line_number
	variable saved_line_unit
	variable last_validation_attempt

	if {[$w tag ranges being_edited] eq {}} {
		return true
	}
	set tabpos [tabstop_editor_get_text_line $w insert]
	if {$on_error eq {restore_on_old_error}} {
		if {[info exists last_validation_attempt($w)] && $tabpos eq $last_validation_attempt($w)} {
			set on_error restore_on_error
		} else {
			set on_error fail_on_error
		}
	}
	set default_unit [tabstop_editor_get_saved_unit $w]
	lassign [tabstop_editor_split_number_unit $tabpos $default_unit] number unit invalid
	if {$tabpos eq {}} {
		if {[info exists saved_line_index($w)]} {
			# Remove tab being edited
			$w delete {insert linestart} {insert linestart + 1 line}
		} else {
			# Remove tab being created (keep final newline)
			$w delete {insert linestart} {insert lineend}
		}
		tabstop_editor_arrow_unset $w insert
		$w mark set insert [tabstop_editor_restrict_cursor $w insert]
		tabstop_editor_update_height $w
	} elseif {$invalid} {
		if {$on_error eq {restore_on_error}} {
			set ret [tabstop_editor_restore_line $w]
			if {!$ret} {
				# Remove invalid tab being created
				$w delete {insert linestart} {insert lineend}
				tabstop_editor_arrow_unset $w insert
			}
		} else {
			$w tag add error being_edited.first being_edited.last
			set last_validation_attempt($w) $tabpos
			return false
		}
	} else {
		set creating_tab [expr {![info exists saved_line_index($w)]}]
		# Make spacing between number and unit consistent
		tabstop_editor_reformat_line $w insert
		if {$creating_tab} {
			if {$number == 0.0} {
				# Still contains the default value, delete!
				$w delete {insert linestart} {insert lineend}
				tabstop_editor_arrow_unset $w insert
			} else {
				set type [tabstop_editor_get_type $w insert]
				$w delete {insert linestart} {insert lineend}
				$w tag remove highlight 1.0 end
				tabstop_editor_insert_sorted $w $type $number $unit
				set keep_highlight 1
				tabstop_editor_arrow_redraw $w -force
			}
			tabstop_editor_update_height $w
		}
	}
	unset -nocomplain last_validation_attempt($w)
	$w tag remove error 1.0 end
	$w tag remove being_edited 1.0 end
	if {![info exists keep_highlight]} {
		$w tag remove highlight 1.0 end
	}
	unset -nocomplain saved_line_index($w) saved_line_type($w) \
		saved_line_number($w) saved_line_unit($w)
	event generate $w <<TabsEdited>> -when head
	return true
}

proc tabstop_editor_insert_sorted {w type number unit} {
	set sort_table [tabstop_editor_get_sorting_table $w]
	# Sort based on the "pixels" entry (format: type number unit index pixels).
	if {[lsort -real -index 4 $sort_table] eq $sort_table} {
		# Tabs are sorted, insert at a suitable location to keep order intact
		set new_px [screen_distance_to_pixels $number $unit]
		set len [llength $sort_table]
		for {set idx 0} {$idx < $len} {incr idx} {
			set entry_px [lindex $sort_table $idx 4]
			if {$new_px < $entry_px} break
			if {$new_px == $entry_px} {
				# Tab to be inserted already exists (same px value), delete!
				set index [lindex $sort_table $idx 3]
				$w delete "$index linestart" "$index linestart + 1 line"
				break
			}
		}
		# $idx equals $len if no "break" has occurred!
	} else {
		# Pre-existing tabs are not sorted, insert at end
		set idx [llength $sort_table]
	}
	set dest_index [expr {$idx+1}].0
	$w insert $dest_index \n
	$w mark set tmp $dest_index
	tabstop_editor_insert_tabstop $w tmp $type $number $unit
	$w mark unset tmp
	$w tag add highlight $dest_index "$dest_index + 1 line"
}

# Editing behavior
proc tabstop_editor_focus_intercept {w {-noupdate {}}} {
	return [catch {focus_intercept_click $w ${-noupdate}}]
}

proc tabstop_editor_click {w x y} {
	variable buttonpress_for_menu

	# Cannot use [$w tag names current] due to interaction with tabstop_editor_unitmenu_open
	# which may have modified the text content!
	set tag_names [$w tag names @$x,$y]
	# Clicks on type or unit menu are already handled by tag bindings,
	# but they have no way to prevent processing this widget binding.
	if {"typemenu" in $tag_names || "unitmenu" in $tag_names} return

	if {[tabstop_editor_focus_intercept $w]} return

	set buttonpress_for_menu no
	focus $w
	$w tag remove sel 1.0 end

	if {"being_edited" ni $tag_names} {
		# Try to finish editing the currently edited entry.
		# If that fails (bad tab distance), stay on that entry if it was not already marked red.
		if {![tabstop_editor_finish $w restore_on_old_error]} return
		tabstop_editor_begin $w @$x,$y
	}
	tabstop_editor_move_cursor_xy $w $x $y
	$w mark set sel_anchor insert
}

proc tabstop_editor_doubleclick {w x y} {
	if {[tabstop_editor_focus_intercept $w -noupdate]} return

	focus $w
	$w tag remove sel 1.0 end
	set start [tabstop_editor_restrict_cursor $w "@$x,$y linestart"]
	set end [tabstop_editor_restrict_cursor $w "@$x,$y lineend - 1 char"]
	$w tag add sel $start $end
	$w mark set sel_anchor $start
	$w mark set insert $end
}

proc tabstop_editor_escape {w} {
	set not_editing [expr {[$w tag ranges being_edited] eq {}}]
	if {$not_editing && [$w tag ranges sel] eq {}} {
		event generate $w <<Escape>>
		return
	}

	$w tag remove sel 1.0 end
	$w mark set sel_anchor insert

	if {$not_editing} return
	set ret [tabstop_editor_restore_line $w]
	if {!$ret} {
		# Tab was being created
		$w delete {insert linestart} {insert lineend}
		tabstop_editor_arrow_unset $w insert
	}
	$w tag remove highlight 1.0 end
	$w tag remove being_edited 1.0 end
	$w tag remove error 1.0 end
	# tabstop_editor_begin $w insert
}

proc tabstop_editor_enter_key {w} {
	variable saved_line_index

	if {[$w tag ranges being_edited] eq {}} return

	if {![info exists saved_line_index($w)]} {
		# Pressing Enter while creating a new tab
		tabstop_editor_key_down $w
	} else {
		if {![tabstop_editor_finish $w fail_on_error]} return
		tabstop_editor_begin $w insert
	}
}

# Called by <ButtonPress> event handlers of other widgets
# which allow interrupting a focus change if the tab editor
# contains invalid content
proc tabstop_editor_focus_out_prepare {w} {
	tabstop_editor_finish $w restore_on_old_error
}

proc tabstop_editor_has_invalid_entry {w} {
	expr {[$w tag ranges error] ne {}}
}

proc tabstop_editor_focus_out {w} {
	variable focusout_for_menu
	if {[info exists focusout_for_menu($w)]} {
		unset focusout_for_menu($w)
		return
	}
	event generate $w <<FocusOutDelayed>> -when tail
}

proc tabstop_editor_focus_out_delayed {w} {
	if {[focus] eq $w} {
		# window was re-focused by other events in the queue
		return
	}
	$w tag remove sel 1.0 end
	if {![tabstop_editor_finish $w restore_on_old_error]} {
		focus $w  ;# Invalid input, take focus back
	}
}

proc tabstop_editor_move_cursor_xy {w x y} {
	set cursor_pos [tk::TextClosestGap $w $x $y]
	if {[$w compare $cursor_pos == "@$x,$y linestart + 1 line"]} {
		set cursor_pos @$x,$y
	}
	$w mark set insert [tabstop_editor_restrict_cursor $w $cursor_pos]
}

proc tabstop_editor_restrict_cursor {w index} {
	if {[$w compare $index < "$index linestart + 3 chars"]} {
		set index "$index linestart + 3 chars"
	}
	if {"arrowline" in [$w tag names $index]} {
		set index arrowline.first
	}
	return $index
}

proc tabstop_editor_key {w character} {
	if {$character eq {}} return
	if {[$w tag ranges being_edited] eq {}} return

	if {[tk::TextCursorInSelection $w]} {
		$w delete sel.first sel.last
	}
	$w insert insert $character
	$w mark set sel_anchor insert
	$w see insert
}

proc tabstop_editor_backspace {w} {
	if {[$w tag ranges being_edited] eq {}} return

	if {[tk::TextCursorInSelection $w]} {
		$w delete sel.first sel.last
	} elseif {[$w compare insert > {insert linestart + 3 chars}]} {
		$w delete {insert - 1 char}
	}
	$w mark set sel_anchor insert
	$w see insert
}

proc tabstop_editor_delete {w} {
	if {[$w tag ranges being_edited] eq {}} return

	if {[tk::TextCursorInSelection $w]} {
		$w delete sel.first sel.last
	} elseif {[$w compare insert < {insert lineend}]} {
		if {"arrowline" in [$w tag names insert]} return
		$w delete insert
	}
	$w mark set sel_anchor insert
	$w see insert
}

proc tabstop_editor_key_left {w} {
	$w tag remove sel 1.0 end
	$w mark set insert [tabstop_editor_restrict_cursor $w {insert - 1 char}]
	$w mark set sel_anchor insert
	$w see insert
}

proc tabstop_editor_key_right {w} {
	$w tag remove sel 1.0 end
	if {[$w compare insert < {insert lineend}]
	&& "arrowline" ni [$w tag names insert]} {
		$w mark set insert {insert + 1 char}
	}
	$w mark set sel_anchor insert
	$w see insert
}

proc tabstop_editor_key_home {w} {
	$w tag remove sel 1.0 end
	$w mark set insert {insert linestart + 3 chars}
	$w mark set sel_anchor insert
	$w see insert
}

proc tabstop_editor_key_end {w} {
	$w tag remove sel 1.0 end
	if {"arrowline" ni [$w tag names {insert lineend - 1 char}]} {
		$w mark set insert {insert lineend}
	} else {
		$w mark set insert arrowline.first
	}
	$w mark set sel_anchor insert
	$w see insert
}

proc tabstop_editor_key_up {w} {
	if {![tabstop_editor_finish $w restore_on_old_error]} return
	$w tag remove sel 1.0 end
	tabstop_editor_cursor_up_down $w -1
	tabstop_editor_begin $w insert
	$w mark set sel_anchor insert
	$w see insert
}

proc tabstop_editor_key_down {w} {
	if {![tabstop_editor_finish $w restore_on_old_error]} return
	$w tag remove sel 1.0 end
	tabstop_editor_cursor_up_down $w 1
	tabstop_editor_begin $w insert
	# After tabstop_editor_finish and tabstop_editor_begin, we only
	# have a selection if a new tab (0.0 cm) is in creation.
	if {[$w tag ranges sel] eq {}} {
		$w mark set sel_anchor insert
	}
	$w see insert
}

proc tabstop_editor_cursor_up_down {w dir} {
	variable up_down_prev_pos
	variable up_down_origin

	set insert [$w index insert]
	if {![info exists up_down_prev_pos($w)]} {
		set up_down_prev_pos($w) $insert
		set up_down_origin($w) $insert
	} elseif {$up_down_prev_pos($w) != $insert} {
		# Cursor has been moved by other means since last Up/Down key event
		set up_down_origin($w) $insert
	}
	set lines [$w count -displaylines $up_down_origin($w) $insert]
	set new_pos [$w index "$up_down_origin($w) + [expr {$lines + $dir}] displaylines"]
	set new_pos [tabstop_editor_restrict_cursor $w $new_pos]
	set up_down_prev_pos($w) $new_pos

	$w mark set insert $new_pos
	$w see insert
}

# Selection behavior (Shift+Action)
proc tabstop_editor_mouse_drag {w x y} {
	variable buttonpress_for_menu

	if {$buttonpress_for_menu} return
	if {[tabstop_editor_focus_intercept $w -noupdate]} return

	focus $w
	# Cannot use [$w tag names current] due to interaction with tabstop_editor_unitmenu_open
	# which may have modified the text content!
	set tag_names [$w tag names @$x,$y]

	if {[$w cget -background] ne {white}} return  ;# Editor is in "tabs are unset" mode

	if {"arrowline" in $tag_names} return

	if {"being_edited" in $tag_names} {
		# Cursor inside the currently edited entry
		tabstop_editor_move_cursor_xy $w $x $y
		tabstop_editor_update_selection $w
	} else {
		# Cursor outside the currently edited entry: multi-line selection
		if {![tabstop_editor_has_multiline_selection $w]} return
		tabstop_editor_move_cursor_xy $w $x $y
		tabstop_editor_update_contiguous_multiline_selection $w
	}
}

proc tabstop_editor_shift_click {w x y} {
	if {[tabstop_editor_focus_intercept $w]} return

	focus $w
	# Cannot use [$w tag names current] due to interaction with tabstop_editor_unitmenu_open
	# which may have modified the text content!
	set tag_names [$w tag names @$x,$y]
	# Clicks on type or unit menu are already handled by tag bindings,
	# but they have no way to prevent processing this widget binding.
	if {"typemenu" in $tag_names || "unitmenu" in $tag_names} return

	if {[$w cget -background] ne {white}} return  ;# Editor is in "tabs are unset" mode

	if {"being_edited" in $tag_names} {
		# Click inside the currently edited entry
		tabstop_editor_move_cursor_xy $w $x $y
		tabstop_editor_update_selection $w
	} else {
		# Click outside the currently edited entry: multi-line selection
		if {![tabstop_editor_finish $w restore_on_old_error]} return
		tabstop_editor_move_cursor_xy $w $x $y
		tabstop_editor_update_contiguous_multiline_selection $w
	}
}

proc tabstop_editor_shift_key_left {w} {
	if {[$w tag ranges being_edited] eq {}} return
	$w mark set insert [tabstop_editor_restrict_cursor $w {insert - 1 char}]
	tabstop_editor_update_selection $w
	$w see insert
}

proc tabstop_editor_shift_key_right {w} {
	if {[$w tag ranges being_edited] eq {}} return
	if {[$w compare insert < {insert lineend}]
	&& "arrowline" ni [$w tag names insert]} {
		$w mark set insert {insert + 1 char}
	}
	tabstop_editor_update_selection $w
	$w see insert
}

proc tabstop_editor_shift_key_home {w} {
	if {[$w tag ranges being_edited] eq {}} return
	$w mark set insert {insert linestart + 3 chars}
	tabstop_editor_update_selection $w
	$w see insert
}

proc tabstop_editor_shift_key_end {w} {
	if {[$w tag ranges being_edited] eq {}} return
	if {"arrowline" ni [$w tag names {insert lineend - 1 char}]} {
		$w mark set insert {insert lineend}
	} else {
		$w mark set insert arrowline.first
	}
	tabstop_editor_update_selection $w
	$w see insert
}

proc tabstop_editor_update_selection {w} {
	$w tag remove sel 1.0 end
	if {[$w compare sel_anchor < insert]} {
		$w tag add sel sel_anchor insert
	} else {
		$w tag add sel insert sel_anchor
	}
}

# Procedures for multi-line selection
proc tabstop_editor_tripleclick {w x y} {
	if {[tabstop_editor_focus_intercept $w -noupdate]} return

	tabstop_editor_finish $w restore_on_error
	$w mark set insert @$x,$y
	$w tag remove sel 1.0 end
	if {[$w compare insert == end-1c]} return
	$w tag add sel {insert linestart} {insert linestart + 1 line}
	tabstop_editor_arrow_remove $w
	$w see insert
}

proc tabstop_editor_shift_key_up {w} {
	if {![tabstop_editor_finish $w restore_on_old_error]} return
	tabstop_editor_cursor_up_down $w -1
	tabstop_editor_update_contiguous_multiline_selection $w
}

proc tabstop_editor_shift_key_down {w} {
	if {![tabstop_editor_finish $w restore_on_old_error]} return
	tabstop_editor_cursor_up_down $w 1
	tabstop_editor_update_contiguous_multiline_selection $w
}

proc tabstop_editor_control_click {w x y} {
	if {[tabstop_editor_focus_intercept $w]} return

	focus $w
	# Cannot use [$w tag names current] due to interaction with tabstop_editor_unitmenu_open
	# which may have modified the text content!
	set tag_names [$w tag names @$x,$y]
	# Clicks on type or unit menu are already handled by tag bindings,
	# but they have no way to prevent processing this widget binding.
	if {"typemenu" in $tag_names || "unitmenu" in $tag_names} return

	if {[$w cget -background] ne {white}} return  ;# Editor is in "tabs are unset" mode

	if {![tabstop_editor_finish $w restore_on_old_error]} return
	tabstop_editor_move_cursor_xy $w $x $y
	tabstop_editor_toggle_multiline_selection $w
}

proc tabstop_editor_control_key_up {w} {
	if {![tabstop_editor_finish $w restore_on_old_error]} return
	tabstop_editor_cursor_up_down $w -1
	$w mark set sel_anchor insert
	$w mark set multiline_last insert
	$w see insert
}

proc tabstop_editor_control_key_down {w} {
	if {![tabstop_editor_finish $w restore_on_old_error]} return
	tabstop_editor_cursor_up_down $w 1
	$w mark set sel_anchor insert
	$w mark set multiline_last insert
	$w see insert
}

proc tabstop_editor_control_space {w} {
	if {![tabstop_editor_finish $w restore_on_old_error]} return
	tabstop_editor_toggle_multiline_selection $w
}

proc tabstop_editor_update_contiguous_multiline_selection {w} {
	$w see insert
	if {[$w compare insert == end-1c]} return

	if {"multiline_last" in [$w mark names]} {
		if {[$w compare multiline_last < sel_anchor]} {
			$w tag remove sel {multiline_last linestart} {sel_anchor linestart + 1 line}
		} else {
			$w tag remove sel {sel_anchor linestart} {multiline_last linestart + 1 line}
		}
	} else {
		$w tag remove sel 1.0 end
	}
	if {[$w compare insert < sel_anchor]} {
		$w tag add sel {insert linestart} {sel_anchor linestart + 1 line}
	} else {
		$w tag add sel {sel_anchor linestart} {insert linestart + 1 line}
	}
	$w mark set multiline_last insert
	tabstop_editor_arrow_remove $w
	# Never select last empty line
	$w tag remove sel end-1c end
}

proc tabstop_editor_toggle_multiline_selection {w} {
	$w see insert
	if {[$w compare insert == end-1c]} return

	if {"sel" in [$w tag names {insert linestart}]} {
		$w tag remove sel {insert linestart} {insert linestart + 1 line}
	} else {
		$w tag add sel {insert linestart} {insert linestart + 1 line}
		$w mark set sel_anchor insert
	}
	$w mark set multiline_last insert
	tabstop_editor_arrow_remove $w
}

proc tabstop_editor_has_multiline_selection {w} {
	set sel_ranges [$w tag ranges sel]
	if {$sel_ranges eq {}} {
		return no
	}
	set idx [lindex $sel_ranges 0]
	if {[$w compare $idx == "$idx linestart"]} {
		return yes
	} else {
		return no
	}
}

# Clipboard
proc tabstop_editor_copy {w} {
	set sel_ranges [$w tag ranges sel]
	if {$sel_ranges eq {}} return

	if {[tabstop_editor_has_multiline_selection $w]} {
		clipboard clear
		foreach {first last} $sel_ranges {
			set current $first
			while {[$w compare $current < $last]} {
				set type [tabstop_editor_get_type $w $current]
				set tabstop [tabstop_editor_get_text_line $w $current]
				lassign [tabstop_editor_split_number_unit $tabstop] number unit invalid
				if {$invalid} {
					error "inconsistency detected while copying tabstops from $w"
				}
				clipboard append "$type $number $unit\n"
				set current [$w index "$current + 1 line"]
			}
		}
	} else {
		clipboard clear
		clipboard append [$w get sel.first sel.last]
	}
}

proc tabstop_editor_cut {w} {
	set sel_ranges [$w tag ranges sel]
	if {$sel_ranges eq {}} return
	set multiline [tabstop_editor_has_multiline_selection $w]

	tabstop_editor_copy $w
	$w delete {*}$sel_ranges

	if {$multiline} {
		event generate $w <<TabsEdited>> -when head
		tabstop_editor_update_height $w
	}
}

proc tabstop_editor_paste {w} {
	if {[catch {
		set clipboard [clipboard get]
	}]} return

	if {[string match {left *} $clipboard] || [string match {right *} $clipboard]
	|| [string match {center *} $clipboard] || [string match {numeric *} $clipboard]} {
		# Multiple whole tabs in clipboard
		foreach line [split $clipboard \n] {
			set line [string trim $line]
			if {$line eq {}} continue
			lassign [split $line] type number unit
			set invalid [catch {screen_distance_compose $number $unit}]
			if {$invalid} {
				return
			}
			lappend data $type $number $unit
		}
		unset clipboard
		foreach {type number unit} $data {
			tabstop_editor_insert_sorted $w $type $number $unit
		}
		event generate $w <<TabsEdited>> -when head
		tabstop_editor_update_height $w
		$w see insert
	} elseif {[$w tag ranges being_edited] ne {}} {
		$w insert insert [lindex [split $clipboard \n] 0]
	}
}

# Mouse wheel: increment/decrement similar to spinboxes
proc tabstop_editor_mousewheel {w x y steps} {
	if {[tabstop_editor_focus_intercept $w]} return

	# The number is incremented on the gesture that normally scrolls up
	set steps [expr {-$steps}]

	focus $w

	if {[tabstop_editor_has_multiline_selection_with_single_unit $w]} {
		tabstop_editor_mousewheel_multiline $w $x $y $steps
		return
	}

	if {"being_edited" ni [$w tag names current]} {
		if {![tabstop_editor_finish $w fail_on_error]} return
		set inside_being_edited 0
	} else {
		set inside_being_edited 1
	}

	set line [tabstop_editor_get_text_line $w current]
	lassign [tabstop_editor_split_number_unit $line] number unit invalid
	if {$invalid} return

	set decimal_tab [lindex [$w cget -tabs] 1]
	set number [tabstop_editor_step $number $unit $steps [expr {$x > $decimal_tab}]]

	tabstop_editor_change_at $w current $number $unit
	if {!$inside_being_edited} {
		$w tag remove sel 1.0 end
		tabstop_editor_begin $w @$x,$y
		tabstop_editor_move_cursor_xy $w $x $y
		$w mark set sel_anchor insert
	}
}

proc tabstop_editor_step {number unit steps fine} {
	if {$fine} {
		# Fine steps (mouse is to the right of the decimal point)
		switch $unit {
			cm {set step_size 0.1}
			in {set step_size 0.125}
			mm - pt - px {set step_size 1}
		}
	} else {
		# Coarse steps (mouse is to the left of the decimal point)
		switch $unit {
			cm - in {set step_size 1.0}
			mm - px {set step_size 10}
			pt {set step_size 12}
		}
	}
	set new_number [screen_distance_cleanup [expr {$number + $steps * $step_size}] $unit]
	if {$new_number > 0.0} {
		return $new_number
	} else {
		return $number
	}
}

proc tabstop_editor_has_multiline_selection_with_single_unit {w} {
	if {![tabstop_editor_has_multiline_selection $w]} {
		return no
	}
	foreach {first last} [$w tag ranges sel] {
		set current $first
		while {[$w compare $current < $last]} {
			set tabstop [tabstop_editor_get_text_line $w $current]
			lassign [tabstop_editor_split_number_unit $tabstop] number unit invalid
			if {$invalid} {
				error "inconsistency detected while getting tab units in $w"
			}
			if {[info exists prev_unit]} {
				if {$unit ne $prev_unit} {
					return no
				}
			}
			set prev_unit $unit
			set current [$w index "$current + 1 line"]
		}
	}
	return yes
}

proc tabstop_editor_mousewheel_multiline {w x y steps} {
	set decimal_tab [lindex [$w cget -tabs] 1]
	set fine [expr {$x > $decimal_tab}]

	foreach {first last} [$w tag ranges sel] {
		set current $first
		while {[$w compare $current < $last]} {
			set line [tabstop_editor_get_text_line $w $current]
			lassign [tabstop_editor_split_number_unit $line] number unit invalid
			if {$invalid} return

			set number [tabstop_editor_step $number $unit $steps $fine]
			set type [tabstop_editor_get_type $w $current]
			tabstop_editor_replace $w $current $type $number $unit
			$w tag add sel $current "$current + 1 line"

			set current [$w index "$current + 1 line"]
		}
	}
	event generate $w <<TabsEdited>> -when head
}

}
