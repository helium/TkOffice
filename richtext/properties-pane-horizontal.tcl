# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

proc properties_pane_horizontal {f mode} {
	ttk::labelframe $f -text [mc property_pane_horizontal_title] -style Panel.TLabelframe
	grid columnconfigure $f 2 -weight 1
	grid rowconfigure $f 1 -weight 1

	# Properties for tags and whole text widget:
	grid [ttk::label $f.tabs_l -text [mc property_tabs]] \
	     [tabstop_editor_create $f.tabs] \
	     - \
	     [ttk::button $f.tabs_clear -text [mc cancel_button] -style Toolbutton] \
	     -sticky nsw -pady 2
	grid configure $f.tabs_frame -rowspan 2 -sticky nesw -padx {0 3}
	$f.tabs configure -width 15
	tabstop_editor_set $f.tabs {}

	combobox_entry_create $f tabstyle -label [mc property_tabstyle] \
		-values {tabular wordprocessor} \
		-displayvalues [list [mc property_tabstyle_tabular] [mc property_tabstyle_wordprocessor]]

	switch $mode widget {
		# Properties for whole text widget only:
		size_entry_create $f padx -label [mc property_padx]

	} tag {
		# Properties for tags only:
		combobox_entry_create $f justify -label [mc property_justify] -pady {10 2} \
			-values {left center right} -displayvalues [list \
				[mc property_justify_left] [mc property_justify_center] \
				[mc property_justify_right] \
			]
		size_entry_create $f lmargin1 -label [mc property_lmargin1]
		size_entry_create $f lmargin2 -label [mc property_lmargin2]
		size_entry_create $f rmargin -label [mc property_rmargin]

		combobox_entry_create $f wrap -label [mc property_wrap] \
			-values {word char none} -displayvalues [list \
				[mc property_wrap_word] [mc property_wrap_char] [mc property_wrap_none] \
			]

	} default {
		error "invalid mode \"$mode\": must be tag or widget"
	}

	bind $f.tabs <<TabsEdited>> [list richtext::properties_pane_horizontal_set_tabs $f]
	$f.tabs_clear configure -command [list richtext::properties_pane_horizontal_clear_tabs $f]

	return $f
}

proc properties_pane_horizontal_update {f mode options} {
	# -tabs property
	if {[dict exists $options -tabs]} {
		set tabs [dict get $options -tabs]
		$f.tabs_clear state !disabled
	} else {
		set tabs {}
		$f.tabs_clear state disabled
	}
	tabstop_editor_set $f.tabs $tabs

	combobox_entry_update $f tabstyle $options

	switch $mode widget {
		size_entry_update $f padx $options
	} tag {
		combobox_entry_update $f justify $options
		size_entry_update $f lmargin1 $options
		size_entry_update $f lmargin2 $options
		size_entry_update $f rmargin $options
		combobox_entry_update $f wrap $options
	}
}

proc properties_pane_horizontal_update_single {f option value {-unset {}}} {
	if {${-unset} ne {-unset}} {
		set opt_list [list $option $value]
	} else {
		set opt_list [list]
	}
	switch $option {
		"-tabs" {
			if {${-unset} ne {-unset}} {
				tabstop_editor_set $f.tabs $value
				$f.tabs_clear state !disabled
			} else {
				tabstop_editor_set $f.tabs {}
				$f.tabs_clear state disabled
			}
		}
		"-tabstyle" { combobox_entry_update $f tabstyle $opt_list }
		"-padx"     { size_entry_update $f padx $opt_list }
		"-justify"  { combobox_entry_update $f justify $opt_list }
		"-lmargin1" { size_entry_update $f lmargin1 $opt_list }
		"-lmargin2" { size_entry_update $f lmargin2 $opt_list }
		"-rmargin"  { size_entry_update $f rmargin $opt_list }
		"-wrap"     { combobox_entry_update $f wrap $opt_list }
	}
}

proc properties_pane_horizontal_set_tabs {f} {
	set tabs [tabstop_editor_get $f.tabs]
	if {$tabs ne {}} {
		tag_list_property_set $f -tabs $tabs
		$f.tabs_clear state !disabled
	} else {
		tag_list_property_set $f -tabs {} -unset
		$f.tabs_clear state disabled
	}
}

proc properties_pane_horizontal_clear_tabs {f} {
	tabstop_editor_set $f.tabs {}
	tag_list_property_set $f -tabs {} -unset
	$f.tabs_clear state disabled
}

}
