# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

proc properties_pane_vertical {f mode} {
	ttk::labelframe $f -text [mc property_pane_vertical_title] -style Panel.TLabelframe
	grid columnconfigure $f 2 -weight 1

	# Properties for tags and whole text widget:
	size_entry_create $f spacing1 -label [mc property_spacing1]
	size_entry_create $f spacing2 -label [mc property_spacing2]
	size_entry_create $f spacing3 -label [mc property_spacing3]

	switch $mode widget {
		# Properties for whole text widget only:
		size_entry_create $f pady -label [mc property_pady] -pady {10 2}

	} tag {

	} default {
		error "invalid mode \"$mode\": must be tag or widget"
	}

	return $f
}

proc properties_pane_vertical_update {f mode options} {
	size_entry_update $f spacing1 $options
	size_entry_update $f spacing2 $options
	size_entry_update $f spacing3 $options

	switch $mode widget {
		size_entry_update $f pady $options
	}
}

proc properties_pane_vertical_update_single {f option value {-unset {}}} {
	if {${-unset} ne {-unset}} {
		set opt_list [list $option $value]
	} else {
		set opt_list [list]
	}
	switch $option {
		"-spacing1" { size_entry_update $f spacing1 $opt_list }
		"-spacing2" { size_entry_update $f spacing2 $opt_list }
		"-spacing3" { size_entry_update $f spacing3 $opt_list }
		"-pady"     { size_entry_update $f pady $opt_list }
	}
}

}
