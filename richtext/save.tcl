# Copyright 2020-2023 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: (GPL-3.0-or-later OR MIT OR TCL)

namespace eval richtext {

init_namespace

variable config_of_widget
variable save_temp_text


proc save_document_to_channel {w chan} {
	# TkOffice documents are encoded as UTF-8 with Unix (LF) line endings
	# on all platforms.
	fconfigure $chan -encoding utf-8 -translation lf

	puts $chan "# TkOffice text document"
	puts $chan ""
	save_widget_to_channel $w $chan
}

proc save_widget_to_channel {w chan {indent {}}} {
	variable config_of_widget
	variable allowed_config
	variable save_temp_text {}

	# 0. Newline if indented
	if {$indent ne {}} {
		puts $chan ""
	}

	# 1. Config options of whole widget
	set config [config_of_widget $w]
	if {[dict size $config] > 0} {
		puts -nonewline $chan $indent
		puts $chan [list config {*}$config]
		puts $chan ""  ;# 2 newlines to separate config from tags
	}

	# 2. Tag attributes
	set tags_written no
	foreach tag [$w tag names] {
		if {$tag eq "sel"} continue
		set config [config_of_tag $w $tag]
		puts -nonewline $chan $indent
		puts $chan [list tag $tag {*}$config]
		set tags_written yes
	}
	if {$tags_written} {
		puts $chan ""  ;# separate tags from text content
	}

	# 3. Text content
	$w dump -command [list ::richtext::_save_dump $w $chan $indent] \
		-text -tag 1.0 end-1c  ;# Later also: -image -window
	_save_flush_text $w $chan $indent
}

proc _save_dump {w chan indent type value index} {
	variable save_temp_text

	# Depending on the editing history, "$w dump" may break a line
	# into multiple parts. Here we take the extra effort to combine
	# those into one "text" per line (in the absence of tags).

	switch $type {
		text {
			append save_temp_text $value
			if {[string index $save_temp_text end] eq "\n"} {
				set save_temp_text [string range $save_temp_text 0 end-1]
				_save_flush_text $w $chan $indent
				puts -nonewline $chan $indent
				puts $chan [list newline]
			}
		}
		tagon {
			if {$value ne "sel"} {
				_save_flush_text $w $chan $indent
				puts -nonewline $chan $indent
				puts $chan [list tagon $value]
			}
		}
		tagoff {
			if {$value ne "sel"} {
				_save_flush_text $w $chan $indent
				puts -nonewline $chan $indent
				puts $chan [list tagoff $value]
			}
		}
	}
}

proc _save_flush_text {w chan indent} {
	variable save_temp_text

	if {$save_temp_text ne {}} {
		puts -nonewline $chan $indent
		puts $chan [list text $save_temp_text]
		set save_temp_text {}
	}
}

}
