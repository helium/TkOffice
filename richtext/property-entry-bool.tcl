# Copyright 2022 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

proc bool_entry_create {frame property args} {
	if {[llength $args]&1 != 0} {
		error "number of extra arguments must be even, have [llength $args]"
	}
	set label {UNDEFINED LABEL}
	set pady 2
	foreach {key value} $args {
		if {$key in {-label -pady}} {
			set [string range $key 1 end] $value
		} else {
			error "unknown property \"$key\": must be -label or -pady"
		}
	}

	grid \
		[ttk::label $frame.${property}_l -text $label] \
		[ttk::frame $frame.${property}_f] \
		- \
		[ttk::button $frame.${property}_clear -text [mc cancel_button] -style Toolbutton] \
		-sticky nsw -pady $pady
	grid configure $frame.${property}_f -padx {0 3}
	$frame.${property}_clear configure -command [list richtext::bool_entry_clear $frame $property]

	grid \
		[ttk::radiobutton $frame.${property}_on -text [mc property_on]] \
		[ttk::radiobutton $frame.${property}_off -text [mc property_off]] \
		-in $frame.${property}_f -padx {0 2}
	set cmd [list richtext::bool_entry_changed $frame $property]
	$frame.${property}_on configure -style Toolbutton -variable $frame.$property -value 1 \
		-command $cmd
	$frame.${property}_off configure -style Toolbutton -variable $frame.$property -value 0 \
		-command $cmd

	raise $frame.${property}_clear  ;# fix Tab focus (tk_focusNext) order
}

proc bool_entry_update {frame property options} {
	if {[dict exists $options -$property]} {
		set bool [dict get $options -$property]
		set ::$frame.$property [expr {$bool ? 1 : 0}]
		$frame.${property}_clear state !disabled
	} else {
		set ::$frame.$property {}  ;# both radiobuttons not pressed
		$frame.${property}_clear state disabled
	}
}

proc bool_entry_changed {frame property} {
	set new_value [get ::$frame.$property]
	tag_list_property_set $frame -$property $new_value
	$frame.${property}_clear state !disabled
}

proc bool_entry_clear {frame property} {
	tag_list_property_set $frame -$property {} -unset
	set ::$frame.$property {}
	$frame.${property}_clear state disabled
}

}
