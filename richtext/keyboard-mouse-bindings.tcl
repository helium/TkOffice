# Copyright 2022, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval richtext {

init_namespace

bind RichText <1> {
	tk::TextButton1 %W %x %y
	%W tag remove sel 0.0 end
}
bind RichText <B1-Motion> {
	set tk::Priv(x) %x
	set tk::Priv(y) %y
	tk::TextSelectTo %W %x %y
}
bind RichText <Double-1> {
	set tk::Priv(selectMode) word
	tk::TextSelectTo %W %x %y
	catch {%W mark set insert sel.first}
}
bind RichText <Triple-1> {
	set tk::Priv(selectMode) line
	tk::TextSelectTo %W %x %y
	catch {%W mark set insert sel.first}
}
bind RichText <Shift-1> {
	tk::TextResetAnchor %W @%x,%y
	set tk::Priv(selectMode) char
	tk::TextSelectTo %W %x %y
}
bind RichText <Double-Shift-1>	{
	set tk::Priv(selectMode) word
	tk::TextSelectTo %W %x %y 1
}
bind RichText <Triple-Shift-1>	{
	set tk::Priv(selectMode) line
	tk::TextSelectTo %W %x %y
}
bind RichText <B1-Leave> {
	set tk::Priv(x) %x
	set tk::Priv(y) %y
	tk::TextAutoScan %W
}
bind RichText <B1-Enter> {
	tk::CancelRepeat
}
bind RichText <ButtonRelease-1> {
	tk::CancelRepeat
}
# An operation that moves the insert mark without making it
# one end of the selection
bind RichText <Control-1> {
	%W mark set insert @%x,%y
}
# stop an accidental double click triggering <Double-Button-1>
bind RichText <Double-Control-1> { # nothing }
# stop an accidental movement triggering <B1-Motion>
bind RichText <Control-B1-Motion> { # nothing }
bind RichText <Left> {
	tk::TextSetCursor %W insert-1displayindices
}
bind RichText <Right> {
	tk::TextSetCursor %W insert+1displayindices
}
bind RichText <Up> {
	tk::TextSetCursor %W [tk::TextUpDownLine %W -1]
}
bind RichText <Down> {
	tk::TextSetCursor %W [tk::TextUpDownLine %W 1]
}
bind RichText <Shift-Left> {
	tk::TextKeySelect %W [%W index {insert - 1displayindices}]
}
bind RichText <Shift-Right> {
	tk::TextKeySelect %W [%W index {insert + 1displayindices}]
}
bind RichText <Shift-Up> {
	tk::TextKeySelect %W [tk::TextUpDownLine %W -1]
}
bind RichText <Shift-Down> {
	tk::TextKeySelect %W [tk::TextUpDownLine %W 1]
}
bind RichText <Control-Left> {
	tk::TextSetCursor %W [tk::TextPrevPos %W insert tcl_startOfPreviousWord]
}
bind RichText <Control-Right> {
	tk::TextSetCursor %W [tk::TextNextWord %W insert]
}
bind RichText <Control-Up> {
	tk::TextSetCursor %W [tk::TextPrevPara %W insert]
}
bind RichText <Control-Down> {
	tk::TextSetCursor %W [tk::TextNextPara %W insert]
}
bind RichText <Shift-Control-Left> {
	tk::TextKeySelect %W [tk::TextPrevPos %W insert tcl_startOfPreviousWord]
}
bind RichText <Shift-Control-Right> {
	tk::TextKeySelect %W [tk::TextNextWord %W insert]
}
bind RichText <Shift-Control-Up> {
	tk::TextKeySelect %W [tk::TextPrevPara %W insert]
}
bind RichText <Shift-Control-Down> {
	tk::TextKeySelect %W [tk::TextNextPara %W insert]
}
bind RichText <Prior> {
	tk::TextSetCursor %W [tk::TextScrollPages %W -1]
}
bind RichText <Shift-Prior> {
	tk::TextKeySelect %W [tk::TextScrollPages %W -1]
}
bind RichText <Next> {
	tk::TextSetCursor %W [tk::TextScrollPages %W 1]
}
bind RichText <Shift-Next> {
	tk::TextKeySelect %W [tk::TextScrollPages %W 1]
}
bind RichText <Control-Prior> {
	%W xview scroll -1 page
}
bind RichText <Control-Next> {
	%W xview scroll 1 page
}

bind RichText <Home> {
	tk::TextSetCursor %W {insert display linestart}
}
bind RichText <Shift-Home> {
	tk::TextKeySelect %W {insert display linestart}
}
bind RichText <End> {
	tk::TextSetCursor %W {insert display lineend}
}
bind RichText <Shift-End> {
	tk::TextKeySelect %W {insert display lineend}
}
bind RichText <Control-Home> {
	tk::TextSetCursor %W 1.0
}
bind RichText <Control-Shift-Home> {
	tk::TextKeySelect %W 1.0
}
bind RichText <Control-End> {
	tk::TextSetCursor %W {end - 1 indices}
}
bind RichText <Control-Shift-End> {
	tk::TextKeySelect %W {end - 1 indices}
}
bind RichText <Control-Tab> {
	focus [tk_focusNext %W]
}
bind RichText <Control-Shift-Tab> {
	focus [tk_focusPrev %W]
}
bind RichText <Control-a> {
	%W tag add sel 1.0 end
}
bind RichText <Control-A> {
	%W tag remove sel 1.0 end
}

# Actions that change the text contents

bind RichTextEditable <Tab> {
	if {[%W cget -state] eq "normal"} {
		richtext::TextInsert %W \t
		focus %W
		break
	}
}
bind RichTextEditable <Shift-Tab> {
	# Needed only to keep <Tab> binding from triggering;  doesn't
	# have to actually do anything.
	break
}
bind RichTextEditable <Return> {
    richtext::TextInsert %W \n
}
bind RichTextEditable <Delete> {
	richtext::DeleteKey %W
}
bind RichTextEditable <BackSpace> {
	richtext::BackSpace %W
}
bind RichTextEditable <KeyPress> {
	richtext::TextInsert %W %A
}

# Ignore all Alt, Meta, and Control keypresses unless explicitly bound.
# Otherwise, if a widget binding for one of these is defined, the
# <KeyPress> class binding will also fire and insert the character,
# which is wrong.  Ditto for <Escape>.

bind RichTextEditable <Alt-KeyPress> {# nothing }
bind RichTextEditable <Meta-KeyPress> {# nothing}
bind RichTextEditable <Control-KeyPress> {# nothing}
bind RichTextEditable <Escape> {# nothing}
bind RichTextEditable <KP_Enter> {# nothing}
if {[tk windowingsystem] eq "aqua"} {
	bind RichTextEditable <Command-KeyPress> {# nothing}
}

# Derived from ::tk::TextInsert
proc TextInsert {w s} {
	if {$s eq "" || [$w cget -state] eq "disabled"} {
		return
	}
	if {[::tk::TextCursorInSelection $w]} {
		undo::undoable_edit_step $w Replace sel.first sel.last $s
	} else {
		undo::undoable_edit_step $w Insert insert $s
		undo::join_enable $w
	}
	$w see insert
}

proc DeleteKey {w} {
	if {[tk::TextCursorInSelection $w]} {
		if {[$w compare sel.first != end-1c]} {
			undo::undoable_edit_step $w Delete sel.first sel.last
		} else {
			# avoid no-change Delete action on special last linebreak
			$w tag remove sel 1.0 end
		}
	} elseif {[$w compare end != insert+1c]} {
		undo::undoable_edit_step $w Delete insert
		undo::join_enable $w
	}
	$w see insert
}

proc BackSpace {w} {
	if {[tk::TextCursorInSelection $w]} {
		if {[$w compare sel.first != end-1c]} {
			undo::undoable_edit_step $w Delete sel.first sel.last
		} else {
			# avoid no-change Delete action on special last linebreak
			$w tag remove sel 1.0 end
		}
	} elseif {[$w compare insert != 1.0]} {
		undo::undoable_edit_step $w Delete insert-1c
		undo::join_enable $w
	}
	$w see insert
}

bind RichTextEditable <<Undo>> {
	undo::undo %W
}
bind RichTextEditable <<Redo>> {
	undo::redo %W
}
bind RichTextEditable <<Cut>> {
	richtext::cut_selection %W
	break
}
bind RichTextEditable <<Copy>> {
	richtext::copy_selection %W
	break
}
bind RichTextEditable <<Paste>> {
	richtext::clipboard_paste %W
	break
}

proc set_event_bindings {textwidget} {
	if {"Text" in [bindtags $textwidget]} {
		bindtag_replace $textwidget Text RichText
	}
	if {"RichText" ni [bindtags $textwidget]} {
		bindtag_add $textwidget RichText
	}
	if {"RichTextEditable" ni [bindtags $textwidget]} {
		bindtag_add $textwidget RichTextEditable
	}
}

}
