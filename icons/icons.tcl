# Copyright 2020-2021, 2024 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

namespace eval icons {

variable files
if {[package vsatisfies $tk_version 8.6] || ![catch {package present tkpng}]} {
	set files(application-exit,16) [file join Tango-renamed 16x16 actions application-exit.png]
	set files(document-properties,16) [file join Tango 16x16 actions document-properties.png]
	set files(document-new,16) [file join Tango 16x16 actions document-new.png]
	set files(document-new-drawing,48) [file join TkOffice 48x48 document-new-drawing.png]
	set files(document-new-richtext,48) [file join TkOffice 48x48 document-new-richtext.png]
	set files(document-open,16) [file join Tango 16x16 actions document-open.png]
	set files(document-open,48) [file join Tango 48x48 actions document-open.png]
	set files(document-revert,16) [file join gnome 16x16 actions document-revert.png]
	set files(document-save,16) [file join Tango 16x16 actions document-save.png]
	set files(document-save-as,16) [file join Tango 16x16 actions document-save-as.png]
	set files(down,16) [file join Tango 16x16 actions down.png]
	set files(edit-redo,16) [file join Tango 16x16 actions edit-redo.png]
	set files(edit-undo,16) [file join Tango 16x16 actions edit-undo.png]
	set files(help,16) [file join Tango 16x16 actions help.png]
	set files(list-add,16) [file join Tango 16x16 actions list-add.png]
	set files(list-remove,16) [file join Tango 16x16 actions list-remove.png]
	set files(tab-center,13) [file join TkOffice 13x13 tab-center.png]
	set files(tab-left,13) [file join TkOffice 13x13 tab-left.png]
	set files(tab-numeric,13) [file join TkOffice 13x13 tab-numeric.png]
	set files(tab-right,13) [file join TkOffice 13x13 tab-right.png]
	set files(up,16) [file join Tango 16x16 actions up.png]
	set files(view-refresh,16) [file join Tango 16x16 actions view-refresh.png]
	set files(window-close,16) [file join gnome 16x16 actions window-close.png]
} else {
	set files(application-exit,16) [file join Tango-renamed 16x16 actions application-exit.gif]
	set files(document-properties,16) [file join Tango 16x16 actions document-properties.gif]
	set files(document-new,16) [file join Tango 16x16 actions document-new.gif]
	set files(document-new-drawing,48) [file join TkOffice 48x48 document-new-drawing.gif]
	set files(document-new-richtext,48) [file join TkOffice 48x48 document-new-richtext.gif]
	set files(document-open,16) [file join Tango 16x16 actions document-open.gif]
	set files(document-open,48) [file join Tango 48x48 actions document-open.gif]
	set files(document-revert,16) [file join gnome 16x16 actions document-revert.gif]
	set files(document-save,16) [file join Tango 16x16 actions document-save.gif]
	set files(document-save-as,16) [file join Tango 16x16 actions document-save-as.gif]
	set files(down,16) [file join Tango 16x16 actions down.gif]
	set files(edit-redo,16) [file join Tango 16x16 actions edit-redo.gif]
	set files(edit-undo,16) [file join Tango 16x16 actions edit-undo.gif]
	set files(help,16) [file join Tango 16x16 actions help.gif]
	set files(list-add,16) [file join Tango 16x16 actions list-add.gif]
	set files(list-remove,16) [file join Tango 16x16 actions list-remove.gif]
	set files(tab-center,13) [file join TkOffice 13x13 tab-center.gif]
	set files(tab-left,13) [file join TkOffice 13x13 tab-left.gif]
	set files(tab-numeric,13) [file join TkOffice 13x13 tab-numeric.gif]
	set files(tab-right,13) [file join TkOffice 13x13 tab-right.gif]
	set files(up,16) [file join Tango 16x16 actions up.gif]
	set files(view-refresh,16) [file join Tango 16x16 actions view-refresh.gif]
	set files(window-close,16) [file join gnome 16x16 actions window-close.gif]
}
set files(tcl-powered-animated) [file join TclTk tcl-powered-animated.gif]

proc load {args} {
	variable files
	set images [image names]
	foreach name $args {
		if {$name in $images} continue
		# The variable $::dir is used inside the tclIndex. It points
		# to the directory that contains the tclIndex.tcl file.
		image create photo $name -file [file join $::dir icons $files($name)]
	}
}

}
