# Copyright 2012, 2020-2021 Tibor Stolz <tibor.stolz@revamp-it.ch>
# SPDX-License-Identifier: GPL-3.0-or-later

proc lremove {listvar start end} {
	upvar $listvar Listvar
	set Listvar [lreplace $Listvar $start $end]
}

proc lremoveitem {listvar item} {
	upvar $listvar Listvar
	set pos [lsearch -exact $Listvar $item]
	if {$pos==-1} {error "item $item not found"}
	set Listvar [lreplace $Listvar $pos $pos]
}

proc lremoveitem_nocomplain {listvar item} {
	upvar $listvar Listvar
	set pos [lsearch -exact $Listvar $item]
	if {$pos==-1} return
	set Listvar [lreplace $Listvar $pos $pos]
}

proc list_contains_any_of {list keys} {
	set contains false
	foreach key $keys {
		set contains [expr {$contains || ($key in $list)}]
	}
	return $contains
}
